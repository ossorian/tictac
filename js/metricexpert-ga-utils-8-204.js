/*
 *  Google Analytics helpers
 *  Copyright (c) 2014 metricexpert.ru
 *  UTF-8 version
 *  Version: 2.04  Universal Analytics version
 */

/*
* Helper functions
*/ 
var meUtils = new function() {

    // converts russian into translit
    this.toTranslit = function(text) {
        return text.replace(
            /([а-яё])/gi,
            function (all, ch, space, words, i) {
                var code = ch.charCodeAt(0),
                    index = code == 1025 || code == 1105 ? 0 : code > 1071 ? code - 1071 : code - 1039,
                    t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh',
                        'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
                        'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh',
                        'shch', '', 'y', '', 'e', 'yu', 'ya'];
                return t[index];
            });
    };
    
    // returns a value of query string argument
    this.getQueryParam = function (name, uri) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(uri);
        if (results === null){
            return "";
        }
        else{
            return decodeURIComponent( (results[1]+'').replace(/\+/g, '%20') );
        }
    };
    
    // finds search argument in the uri, 
    // e.g. for the uri: abc.com/?q=term it returns 'q'
    this.getSearchArgName = function(uri){
        var matches = uri.match(/(?:([#]|[?]|[&]))(?:(q|text|p))=[^&]*/i);
        if(matches === null){
            return '';
        }else{
            return matches[2];
        }
    };

    this.getTopLevelDomain = function(domain){
        if(!domain){
            return null;
        }
        var parts = domain.split('.');
        if(parts.length>1){
            return parts[ parts.length-2 ]+'.'+parts[ parts.length-1 ];
        }
        return null;
    };

    this.setCookie = function(c_name, value, exdays) {
        var exdate=new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=encodeURIComponent(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie=c_name + "=" + c_value + "; path=/" + ";domain=."+this.getTopLevelDomain(window.location.host);
    };

    this.getCookie = function(c_name)
    {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) {
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1) {
            c_value = null;
        }else{
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = c_value.length;
            }
            c_value = decodeURIComponent(c_value.substring(c_start,c_end));
        }
        return c_value;
    };
}();

/*
* Saves the first keyword and referrer for the new visitors
* Fills in the UA custom dimensions:
*    Landing, Referer, Keyword, Campaign
* Usage:
*   meFirstTouch.track();
*/
var meFirstTouch = new function(){

    // check if this is a first time visitor according to GA
    this.firstTimeVisitor = function(){
        return meUtils.getCookie('__ftch')!='1';
    };

    this.normalizeUrl = function(url){
        if(!url){
            return '';
        }
        return decodeURIComponent(url.replace('http://','').replace('www.','').replace(/\+/g, '%20'));
    };

    this.getCampaign = function(referer, landing, keyword){
        var campaign = meUtils.getQueryParam('utm_campaign', landing);
        if (!campaign){
            if(referer.match(/aclk\?/i))
                campaign = '(google adwords)';
            else if(referer.match(/\/clck\/jsredir\?/i))
                campaign = '(yandex direct)';
            else if(keyword && referer.match(/yandex\./i))
                campaign = '(yandex organic)';
            else if(keyword && referer.match(/google\./i))
                campaign = '(google organic)';
        }

        campaign = campaign ? campaign : '(none)';
        return campaign;
    };

    this.getVisitorSource = function(){
        var searchArg = meUtils.getSearchArgName(document.referrer);
        var keyword_from_url = null;
        var keyword = '(none)';
        if(searchArg){
            keyword_from_url = meUtils.getQueryParam(searchArg, document.referrer);
            keyword = keyword_from_url;
        }else if(document.referrer.indexOf('google.')!==-1){
            keyword = "google not provided: " + document.title.split('|')[0].trim();
        }

        var campaign = this.getCampaign(document.referrer, window.location.href, keyword_from_url);

        var referer = this.normalizeUrl(document.referrer);
        referer = referer ? referer : '(none)';

        var landing = this.normalizeUrl(window.location.pathname);
        landing = landing ? landing : '(none)';

        var result={
            'Landing': landing,
            'Referer': referer,
            'Keyword': keyword,
            'Campaign': campaign
        };
        return result;
    };



    this.setSrcToGa = function(src){
        if(!src){
            return;
        }
        ga('set', 'dimension1', src.Referer);
        ga('set', 'dimension2', src.Keyword);
        ga('set', 'dimension3', src.Campaign);
        ga('set', 'dimension4', src.Landing);
    };

    // Tracks first touch keyword and referrer for the new visitors
    // sets 4 custom dimensions
    // must be called before ga('send')
    this.track = function(){
        try {
            if( this.firstTimeVisitor() ){
                var src = this.getVisitorSource();
                meUtils.setCookie('__ftch', '1', 365);
                this.setSrcToGa(src);
            }
        } catch (err) {}
    };
}();

