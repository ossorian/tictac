// интервал между сменой картинок
var slideDelay = 5000;
// скорость смены
var slideSpeed = 600;
var mayPlay = true;

var currentSlide = 0;
	
var IE='\v'=='v';
if (IE) {
	var timerMulti;
	function changeSlideIe (slidesArray, slidesCount) {
		slidesArray.eq(currentSlide).css( {'display' : 'none'} );
		if ( currentSlide+1 == slidesCount ) {
			currentSlide = 0;
		}
		else {
			currentSlide++;
		}
		sliderSwitch.removeClass('active');
		sliderSwitch.eq(currentSlide).addClass('active');
		slidesArray.eq(currentSlide).css({'display' : 'block'});
		slidesArray.css({ 'z-index' : 991 });
		slidesArray.eq(currentSlide).css({ 'z-index' : 992 });
	}
}


$(document).ready(function(){
	
	var slidesArray = $('.picslide');
	var slidesCount = slidesArray.length;
	slidesArray.css({ 'opacity' : 0 });
	slidesArray.eq(0).css({ 'opacity' : 1, 'z-index' : 992 });
	slidesArray.eq(0).delay(slideDelay);
	
	$('.picslider').append('<div class="slider_switch"></div>');
	for (var i = 0; i < slidesCount; i++) {
	    $('.slider_switch').append('<a href="javascript:void(0)"></a>');
	}
	var sliderSwitch = $('.slider_switch a');
	sliderSwitch.eq(0).addClass('active');
	
	var isClicked = false;
	$('.slider_switch a').click( function () {
		var arrPar = $(this);
		if ( !isClicked ) {
			if (IE) {
				slidesArray.eq(currentSlide).css( {'display' : 'none'} );
					currentSlide = sliderSwitch.index(arrPar);
					sliderSwitch.removeClass('active');
					sliderSwitch.eq(currentSlide).addClass('active');
					slideCurName.html( slidesArray.eq(currentSlide).find('.slider_name').html() );
					slideCurZero = (currentSlide+1)+'';
					doZero ();
					slideCurBlock.text( slideCurZero );
				slidesArray.eq(currentSlide).css({'display' : 'block'});
			}
			else {
				slidesArray.eq(currentSlide).stop(true, true);
				mayPlay = false;
				isClicked = true;
				slidesArray.eq(currentSlide).css( {'opacity' : 1 });
				slidesArray.eq(currentSlide).animate( {'opacity' : 0}, slideSpeed, function () {
					currentSlide = sliderSwitch.index(arrPar);
					sliderSwitch.removeClass('active');
					sliderSwitch.eq(currentSlide).addClass('active');
					slidesArray.eq(currentSlide).animate({'opacity' : 1}, slideSpeed, function () {
						isClicked = false;
						slidesArray.css({ 'z-index' : 991 });
						slidesArray.eq(currentSlide).css({ 'z-index' : 992 });
						slidesArray.eq(currentSlide).animate({ 'left' : 0 }, slideDelay, function () {
							mayPlay = true;
							changeSlide ();
						});
					});
				});
			}
		}
	});
	
	function changeSlide () {
		if ( !mayPlay ) {
			return false;
		}
		$(slidesArray.eq(currentSlide)).animate( {'opacity' : 0}, slideSpeed, function () {
			if ( !mayPlay ) {
				return false;
			}
			if ( currentSlide+1 == slidesCount ) {
				currentSlide = 0;
			}
			else {
				currentSlide++;
			}
			sliderSwitch.removeClass('active');
			sliderSwitch.eq(currentSlide).addClass('active');
			slidesArray.css({ 'z-index' : 991 });
			slidesArray.eq(currentSlide).css({ 'z-index' : 992 });
			slidesArray.eq(currentSlide).animate({'opacity' : 1}, slideSpeed).delay(slideDelay);
			if ( !mayPlay ) {
				return false;
			}
			changeSlide ();
		});
	}
	
	if($.browser.msie) {
		slidesArray.css({ 'display' : 'none', 'opacity' : 1 });
		slidesArray.eq(0).css({ 'display' : 'block' });
		timerMulti = window.setInterval("changeSlideIe($('.slide'), $('.slide').length);", slideDelay);
	}
	else {
		changeSlide ();
	}
	
});