$(document).ready( function () {

    $(document).on("click", "li", function () {
        console.log("qewqewq");
    })
	// Корзина
	// появление/скрытие
	var cartSwitcher = $('.cart_switcher');
	var cartInfoBlock = $('.cart_info');
	cartSwitcher.on('click', function () {
		if ( cartSwitcher.hasClass('opened') ) {
			cartInfoBlock.fadeOut(300);
			cartSwitcher.removeClass('opened');
		}
		else {
			cartInfoBlock.fadeIn(300);
			cartSwitcher.addClass('opened');
		}
	});
	// удаление элемента
	$('.cart_info_delete a').live('click', function () {
		$(this).parent().parent().remove();
	});


	// Поиск
	$('.search_block input[type="reset"]').on('click', function () {
		var searchTextInput = $('.search_block input[type="text"]');
		searchTextInput.val( searchTextInput.attr('placeholder') );
	});


	// Презентация
/* начало выпадающее меню */
	$('.cat_short_info').css({ 'opacity' : 0 });
	$('.presentation.switchable .categories a').on('click', function () {
		if ( !$('.presentation').hasClass('selected') ) {
			$('.picslider').css({ 'opacity' : 0 });
			$('.presentation').addClass('selected');
		}
		var thePar = $(this).parent();
		var wasActive = $('.categories li.active');
		var catInfoShowing = $('.cat_short_info').eq( $('.categories li').index(thePar) );
		if ( wasActive.length ) {
			wasActive.removeClass('active');
			$('.cat_short_info.shown').animate({ 'opacity' : 0 }, 300, function () {
				catInfoShowing.animate({ 'opacity' : 1 }, 300);
				$('.cat_short_info.shown').removeClass('shown');
				catInfoShowing.addClass('shown');
			});
		}
		else {
			catInfoShowing.animate({ 'opacity' : 1 }, 300);
			catInfoShowing.addClass('shown');
		}
		thePar.addClass('active');
	});
	$('.presentation .categories li').on('mouseenter', function () {
		if ( !$(this).hasClass('current') ) {
			var catShort = $(this).find('.cat_short_info');
			var thePar = $(this).parent();
			catShort.css({'z-index' : 1001});
			$('.presentation').addClass('selected');
			$(this).addClass('active');
			//catShort.animate({ 'opacity' : 1 }, 300);
			catShort.css({ 'opacity' : 1 });
		}
	});
	$('.presentation .categories li').on('mouseleave', function () {
		if ( !$(this).hasClass('current') ) {
			var catShort = $(this).find('.cat_short_info');
			var thePar = $(this).parent();
			catShort.css({'z-index' : 1000});
			$('.presentation').removeClass('selected');
			$(this).removeClass('active');
			//catShort.animate({ 'opacity' : 0 }, 300);
			catShort.css({ 'opacity' : 0 });
		}
	});
/* конец */

	$('.filterable a').on('click', function () {
		$(this).toggleClass('active');
	});

	$('.category_description .category_switcher a, .brand_info_hide a').on('click', function () {
		if ( $('.category_description').hasClass('opened') ) {
			$('.brand_info').slideUp(500, function () {
				$('.category_description').removeClass('opened');
				var brandShowLink = $('.category_switcher a');
				brandShowLink.html('<span>'+brandShowLink.find('span').html()+'</span> &darr;');
			});
		}
	});
	$('.category_description .category_switcher a').on('click', function () {
		if ( !$('.category_description').hasClass('opened') ) {
			$('.category_description').addClass('opened');
			var brandShowLink = $('.category_switcher a');
			brandShowLink.html('<span>'+brandShowLink.find('span').html()+'</span> &uarr;');
			$('.brand_info').slideDown(500);
		}
	});

	
	$('.text-open a').on('click', function () {
			
			var brandShowLink = $('.text-open a');
			
			if($(this).hasClass('opened')){
				brandShowLink.html('<span>'+brandShowLink.find('span').html()+'</span> &darr;');
			}
			else{
				brandShowLink.html('<span>'+brandShowLink.find('span').html()+'</span> &uarr;');
			}
			$(this).toggleClass('opened');
			$(this).parent('.text-open').next('.text-open-info').toggle(500);
	});

	

	// Прокрутка часов
	if ( $('.clock_scroller').length ) {
		var scrollItemWidth = 180;
		var scrollItemMargin = 40;
		$('.clock_scroller_container').css({ 'width' : $('.clock_scroller_item').length * (scrollItemWidth+scrollItemMargin) });
		$('.clock_scroller_overlay').jScrollPane({ 'horizontalDragMaxWidth' : 18, 'horizontalDragMinWidth' : 18 });
	}

/*
	// Бренды
	if ( $('.brands_block').length ) {
		var brandMaxHeight = 77;
		$('.brands_block a img').each( function () {
			var brandImg = $(this);
			$(this).on('load', function () {
				var brandImgHeight = brandImg.height()/2;
				brandImg.parent().css({ 'height' : brandImgHeight, 'margin-top' : (brandMaxHeight-brandImgHeight)/2 });
				brandImg.on('mouseenter', function () {
					brandImg.css({ 'margin-top' : -1*brandImgHeight });
				});
				brandImg.on('mouseleave', function () {
					brandImg.css({ 'margin-top' : 0 });
				});
			});
		});
	}
*/

	// Каталог
	$('.coming_feed_link, .catalog_item_close a').on('click', function () {
		$(this).closest('.catalog_item').toggleClass('active');
	});


	// Плэйсхолдеры
	if ( $('input[placeholder]').length ) {
		$('input[placeholder]').each( function () {
			var theInput = $(this);
			var theInputTip = theInput.attr('placeholder');
			theInput.data('placeholder', theInputTip);
			theInput.attr('placeholder', '');
			theInput.val(theInputTip);
			theInput.addClass('placeholdered');
			theInput.on('focus', function() {
				if ( theInput.val() == theInputTip ) {
					theInput.val('');
					theInput.removeClass('placeholdered');
				}
			});
			theInput.on('blur', function() {
				if ( theInput.val() == '' ) {
					theInput.val(theInputTip);
					theInput.addClass('placeholdered');
				}
			});
		});
	}
	if ( $('textarea[placeholder]').length ) {
		$('textarea[placeholder]').each( function () {
			var theInput = $(this);
			var theInputTip = theInput.attr('placeholder');
			theInput.data('placeholder', theInputTip);
			theInput.attr('placeholder', '');
			theInput.val(theInputTip);
			theInput.addClass('placeholdered');
			theInput.on('focus', function() {
				if ( theInput.val() == theInputTip ) {
					theInput.val('');
					theInput.removeClass('placeholdered');
				}
			});
			theInput.on('blur', function() {
				if ( theInput.val() == '' ) {
					theInput.val(theInputTip);
					theInput.addClass('placeholdered');
				}
			});
		});
	}

	// Форма заказа
	$("#ORDER_PROP_20").live("change", function(){
		if(this.value == 'other') {$('.another').show();} else {$('.another').hide();}
	});

	// Валидация форм
	$('.validable').attr( 'disabled', 'disabled' );
	$('.required').on('keyup', function () {
		var isHasEmpty = false;
		$(this).closest('form').find('.required').each( function () {
			if ( $(this).val() == '' || $(this).val() == $(this).data('placeholder') ) {
				isHasEmpty = true;
				return false;
			}
		});
		if ( isHasEmpty ) {
			$(this).closest('form').find('.validable').attr( 'disabled', 'disabled' );
		}
		else {
			$(this).closest('form').find('.validable').removeAttr("disabled");
		}
	});


	// Переключатель картинок
	if ( $('.product_previews').length ) {
		var bigPic = $('.product_pic_big img');
		var previewsTrack = $('.product_previews_track');
		var previewsBlockWidth = $('.product_previews_container').width();
		var firstInTrack = $('.product_previews_track a').eq(0);
		var lastInTrack;
		var previewsCount = $('.product_previews_track a').length;
		var previewsTrackWidth = 0;

		var bigPicLeft = 0;
		var bigPicTop = 0;
		var bigPicWidth = 0;
		var bigPicHeight = 0;
		var bigPicRight = 0;
		var bigPicBottom = 0;
		var zoomPic;
		var zoomPicLeft = 0;
		var zoomPicTop = 0;
		var zoomPicWidth = 0;
		var zoomPicHeight = 0;
		var zoomerWidth = 0;
		var zoomer;
		var zoomK = 0;
		var zoomIndex = 0;
		function setZoomPic (zi) {
			bigPicLeft = bigPic.offset().left;
			bigPicTop = bigPic.offset().top;
			bigPicWidth = bigPic.width();
			bigPicHeight = bigPic.height();
			bigPicRight = bigPicLeft+bigPicWidth;
			bigPicBottom = bigPicTop+bigPicHeight;
			zoomPic = $('.product_zoom img').eq(zi);
			$('.product_zoom img').css({ 'opacity' : 0 });
			zoomPic.css({ 'opacity' : 1 });
			zoomPicWidth = zoomPic.width();
			zoomPicHeight = zoomPic.height();
			zoomerWidth = 466*bigPicWidth/zoomPicWidth;
			zoomK = zoomPicWidth/bigPicWidth;
			$('.zoomer').remove();
			$('body').append('<div style="width:'+zoomerWidth+'px; height:'+zoomerWidth+'px;" class="zoomer"></div>');
			zoomer = $('.zoomer');
		}
		setZoomPic (zoomIndex);
		$('body').on('mousemove', function (e) {
			var mouseLeft = e.pageX;
			var mouseTop = e.pageY;
			if ( mouseLeft > bigPicLeft && mouseLeft < bigPicRight && mouseTop > bigPicTop && mouseTop < bigPicBottom )
			{
				$('.product_right').css({'opacity' : 0});
				$('.product_zoom').css({'display' : 'block' });
				zoomer.animate({ 'opacity' : 1 }, 500);
				zoomer.css({ 'left' : mouseLeft-(zoomerWidth/2), 'top' : mouseTop-(zoomerWidth/2) });
				var zoomLeft = -1*(mouseLeft-bigPicLeft-zoomerWidth/2)*zoomK;
				var zoomTop = -1*(mouseTop-bigPicTop-zoomerWidth/2)*zoomK;
				zoomPic.css({ 'left' : zoomLeft, 'top' : zoomTop });
			}
			else {
				$('.product_right').css({'opacity' : 1});
				$('.product_zoom').css({'display' : 'none' });
				zoomer.css({ 'opacity' : 0, 'left' : -999, 'top': -999 });
			}
		});

		$('.product_previews_track a').each( function () {
			var thePreview = $(this);
			thePreview.data('href', thePreview.attr('href'));
			thePreview.attr('href', 'javascript:void(0)');
			var thePreviewWidth = thePreview.find('img').width();
			thePreview.data('width', thePreviewWidth);
			thePreview.data('left', previewsTrackWidth);
			previewsTrackWidth = previewsTrackWidth + thePreviewWidth;
			thePreview.data('right', previewsTrackWidth);
			if ( thePreview.data('right') < previewsBlockWidth && thePreview.data('right') >= previewsBlockWidth-50 ) {
				lastInTrack = thePreview;
			}
			thePreview.on('click', function () {
				$('.product_previews_track a').removeClass('active');
				thePreview.addClass('active');
				bigPic.animate({'opacity' : 0}, 300, function () {
					bigPic.attr('src', thePreview.data('href'));
					bigPic.attr('width', '');
					bigPic.attr('height', '');
					bigPic.on('load', function() {
						bigPic.animate({'opacity' : 1}, 300);
						zoomIndex = $('.product_previews_track a').index(thePreview);
						setZoomPic (zoomIndex);
					});
				});
			});
		});
		previewsTrack.css({ 'width' : previewsTrackWidth });
		if ( previewsTrackWidth > previewsBlockWidth ) {
			$('.previews_arrow_right').on('click', function () {
				if ( $('.product_previews_track a').index(lastInTrack) != previewsCount-1 ) {
					previewsTrack.animate({'left' : -1*firstInTrack.data('right')}, 300);
					firstInTrack = firstInTrack.next();
					lastInTrack = lastInTrack.next();
				}
			});
			$('.previews_arrow_left').on('click', function () {
				if ( $('.product_previews_track a').index(firstInTrack) != 0 ) {
					firstInTrack = firstInTrack.prev();
					previewsTrack.animate({'left' : -1*firstInTrack.data('left')}, 300);
					lastInTrack = lastInTrack.prev();
				}
			});
		}
	}


	// Разворот описания
	$('.desc_switcher a').on('click', function () {
		$('.desc_switcher').toggleClass('up');
		$('.hidden_desc').slideToggle(400);
	});


	// Всплывающие окна
	$('.popup_starter a').on('click', function () {
		$(this).parent().parent().find('.popup').fadeToggle(300);
	});
	$('.closelink a').on('click', function () {
		$(this).parent().parent().fadeToggle(300);
	});


	// FAQ
	$('.faqsec > a').on('click', function () {
		var faqLink = $(this);
		if ( faqLink.hasClass('active') ) {
			faqLink.next('.faq_answer').slideUp(300);
			faqLink.removeClass('active');
		}
		else {
			faqLink.next('.faq_answer').slideDown(300);
			faqLink.addClass('active');
		}
	});
	$('.faq_close').on('click', function () {
		var faqAnswer = $(this).parent();
		faqAnswer.slideUp(300);
		faqAnswer.prev('a').removeClass('active');
	});


	// Новости
	if ( $('.news_block').length ) {
		var newsBlindTop = $('.blinding_top');
		var newsBlindBottom = $('.blinding_bottom');
		newsBlindTop.css({'opacity' : 0});
		var cursorPosStart;
		var cursorPosNow;
		var cursorPosDif;
		var beforeTop;
		var mayDrag = false;
		var newsDrag = $('.news_drag');
		var newsDragHeight = newsDrag.height();
		var dragContainerHeight = $('.news_container').height();
		newsDrag.on('mousedown', function (e) {
			cursorPosStart = e.pageY;
			beforeTop = Math.floor(newsDrag.css('top').slice(0, -2)*1);
			mayDrag = true;
		});
		$('body').on('mouseup', function (e) {
			mayDrag = false;
		});
		newsDrag.on('mousemove', function (e) {
			if ( mayDrag ) {
				cursorPosNow = e.pageY;
				cursorPosDif = cursorPosNow-cursorPosStart;
				if ( cursorPosDif < 300 && beforeTop+cursorPosDif < 0 && beforeTop+cursorPosDif+newsDragHeight-10 > dragContainerHeight ) {
					newsDrag.css({ 'top' : beforeTop+cursorPosDif });
				}
				if ( beforeTop+cursorPosDif > -10 ) {
					newsBlindTop.css({'opacity' : 0});
				}
				else if ( beforeTop+cursorPosDif < -10 ) {
					newsBlindTop.css({'opacity' : 1});
				}
				if ( beforeTop+cursorPosDif+newsDragHeight-20 < dragContainerHeight ) {
					newsBlindBottom.css({'opacity' : 0});
				}
				else {
					newsBlindBottom.css({'opacity' : 1});
				}
			}
		});
	}



	// игра
	var timerCicle;
	var cicleStep = 0;
	function cicleIcon (cicleObject) {
		cicleStep++;
		if ( cicleStep < 9 ) {
			cicleObject.addClass('c'+cicleStep);
		}
		else {
			cicleStep = 0;
			cicleObject.attr('class', 'newgame');
			window.clearTimeout(timerCicle);
		}
	}
	$('.newgame a').on('click', function () {
		var ngLink = $(this).parent();
		timerCicle = window.setInterval(function () { cicleIcon (ngLink); }, 30);
		playAgain();
	});



	// Аяксовое добавление в корзину
	$(".add2cart a, .productAdd2Cart").live("click", function(e){
		var productId = $(this).attr('data-product-id');
		var productImageSrc = $(this).attr('data-product-image');
		$.ajax({
			type: "POST",
			url: "/ajax/cart.php",
			data: "addProductId=" + productId,
			success: function(msg){
                $('#del_wishlist_' + productId).hide(); // скрытие для wishlist
				$('#smallcart').html(msg);
				// Корзина появление/скрытие
				var cartSwitcher = $('.cart_switcher');
				var cartInfoBlock = $('.cart_info');
				cartSwitcher.on('click', function () {
					if ( cartSwitcher.hasClass('opened') ) {
						cartInfoBlock.fadeOut(300);
						cartSwitcher.removeClass('opened');
					}
					else {
						cartInfoBlock.fadeIn(300);
						cartSwitcher.addClass('opened');
					}
				});
				// Уведомление о добавлении элемента
				$("#modalAddBasketImageSrc").attr("src", productImageSrc);
				$('#modalAddBasket').arcticmodal();
			}
		});
		return false;
    });
});
