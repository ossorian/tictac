$(document).ready(function() {
/* Помощники на jQuery */
	$('ul li:first-child').addClass('first');
	$('ul li:last-child').addClass('last');
	(function($) {
		$.fn.autoClear = function () {
			// сохраняем во внутреннюю переменную текущее значение
			$(this).each(function() {
				$(this).data("autoclear", $(this).attr("value"));
			});
			$(this)
				.bind('focus', function() {   // обработка фокуса
					if ($(this).attr("value") == $(this).data("autoclear")) {
						$(this).attr("value", "").addClass('autoclear-normalcolor');
					}
				})
				.bind('blur', function() {    // обработка потери фокуса
					if ($(this).attr("value") == "") {
						$(this).attr("value", $(this).data("autoclear")).removeClass('autoclear-normalcolor');
					}
				});
			return $(this);
		}
	})(jQuery);
	$('input[type=text],input[type=password],textarea').autoClear();


	$('.filter_button a').click(function() {
		$(this).toggleClass('show').parent().prev().slideToggle();
		( $(this).is('.show') ) ? $(this).text('Свернуть фильтр') : $(this).text('Подобрать часы по характеристикам');
		if ($('input[type=slider]').is(':visible'))$('input[type=slider]').slider({ from: 1000, to: 100000, step: 100, smooth: true, round: 0, dimension: "&nbsp;i", skin: "blue" });
		//$('.mechanism_block').width(make_width($('.mechanism_block'),2));
		//$('.country_block').width(make_width($('.country_block'),2));
		//$('.style_block').width(make_width($('.style_block'),4));
		//$('.color_block').width(make_width($('.color_block'),2));
		$('.hron_block').width(make_width($('.hron_block'),2));
		return false;
	});
	function make_width(elem,count) {
		var width_elem = 10 * (count - 1);
		elem.find('label').slice(0,count).each(function() {
			width_elem += $(this).outerWidth();
		});
		elem.find('label').eq(count-1).css('margin-right',0);
		return width_elem;
	};
	$.fn.toggleCheck=function(){$(this).prop('checked', !($(this).is(':checked')));}
	$('.filter_block label').click(function() {
		$(this).toggleClass('checked');
		$(this).find('input').toggleCheck();
		return false;
	});
});