$(function (e) {

    //search
    $('.search__icon').click(function () {
        $('.header .logo, .certificate, .h-b-right__content, .h-b-left__content, .search').addClass('search_hidden');
        $('.search_visible').removeClass('hidden');
        $('.header-head__bottom').css('height', '64px');
        $('.auth, .h-basket').css('display', 'none');
    });
    $('.search_visible__close').click(function () {
        $('.header .logo, .certificate, .h-b-right__content, .h-b-left__content, .search').removeClass('search_hidden');
        $('.search_visible').addClass('hidden');
        $('.header-head__bottom').css('height', 'auto');
    });


    //TOGGLE MENU
    $(document).on('click', '.menu__btn', function () {
        $(this).toggleClass('menu__btn_active');
        $('.toggle_menu').slideToggle();
        $('.h-basket').hide();
        $('.auth').hide();
        $('.header__head_mob .icon-icon-cart').removeClass('icon-icon-small-close');
        $('.header__head_mob .icon-icon-cart').parent().css('padding-top', '0px');
    });

    //BREND COLLUMNS
    $(document).on('click', '.view_four_items', function () {
        $('.brend__col').removeClass('col-md-4');
        $('.brend__col').addClass('col-md-3');
        $('.reminder, .o-reminder').addClass('rem__padd');
    });
    $(document).on('click', '.view_three_items', function () {
        $('.brend__col').removeClass('col-md-3');
        $('.brend__col').addClass('col-md-4');
        $('.reminder, .o-reminder').removeClass('rem__padd');
    });

    // AUTHORIZATION
    $(document).on('click', '.auth__close', function () {
        $('.auth').hide();
        $('.forgot-pass__content').addClass('hidden');
        $('.auth').css({
            'height': 'auto'
        });
    });
    $(document).on('click', '.acc__icon', function () {
        $('.checkout .auth').hide();
        $('.header .auth').toggle();
        $('.h-basket').hide();
    });
    $(document).on('click', '.login__btn', function () {
        $('.checkout .auth').toggle();
        $('.header .auth').hide();
        $('.h-basket').hide();
    });
    $(document).on('click', '.header .forgot__pass', function () {
        $('.header .forgot-pass__content').removeClass('hidden');
        $('.header .auth').css({
            'height': '265px'
        });
    });
    $(document).on('click', '.checkout .forgot__pass', function () {
        $('.checkout .forgot-pass__content').removeClass('hidden');
        $('.checkout .auth').css({
            'height': '200px'
        });
    });
    $(document).on('click', '.checkout .log-in', function () {
        $('.checkout .forgot-pass__content').addClass('hidden');
        $('.checkout .auth').css({
            'height': 'auto'
        });
    });
    $(document).on('click', '.auth__tab', function () {
        $('.forgot-pass__content').addClass('hidden');
        $('.auth').css({
            'height': 'auto'
        });
    });
    $(".auth__tab-item").not(":first").hide();
    $(document).on('click', '.auth .auth__tab', function () {
        $(".auth .auth__tab").removeClass("auth_active").eq($(this).index()).addClass("auth_active");
        $(".auth__tab-item").hide().eq($(this).index()).fadeIn();
    }).eq(0).addClass("auth_active");

    // HEADER BASKET
    $(document).on('click', '.header__head .header__basket', function () {
        $('.h-basket').toggle();
        $('.auth').hide();
    });
    $(document).on('click', '.header__head_mob .header__basket', function () {
        $('.h-basket').toggle();
        $('.auth').hide();
        $('.toggle_menu').hide();
        $('.menu__btn').removeClass('menu__btn_active');
				$('.header__head_mob .icon-icon-cart').toggleClass('icon-icon-small-close');
    });
		$(document).on('click', '#smallcart .icon-icon-small-close', function() {
				$('.h-basket').hide();
		});
    $(document).on('click', '.nav__link', function () {
        $('.h-basket').hide();
        $('.auth').hide();
    });

    /*    //WISHLIST ICON
     $('.favorites__icon .icon-icon-wishlist').click(function() {
     $(this).toggleClass('icon-icon-wishlist-select');
     $(this).toggleClass('icon-icon-wishlist');
     });*/

    //PRODUCT COLOR
    $('.colors span').click(function () {
        $('.colors span').removeClass('product_color_active');
        $(this).addClass('product_color_active');
    });
    //PRODUCT SIZE
    $('.sizes span').click(function () {
        $('.sizes span').removeClass('product_size_active');
        $(this).addClass('product_size_active');
    });
    //PRODUCT TABS
    $(".product__body").not(":first").hide();
    $(".product__content .product__tab").click(function () {
        $(".product__content .product__tab").removeClass("product__tab_active").eq($(this).index()).addClass("product__tab_active");
        $(".product__body").hide().eq($(this).index()).fadeIn();
    }).eq(0).addClass("product__tab_active");


    // SCROLL TO
    $('a[href^="#to"]').on("click", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    //GAME
    $('.game__close').click(function () {
        $('.game__wrap').hide();
    });
    $('.game__text').click(function () {
        $('.game__wrap').show();
    })

    //PROFILE TABS
    $(".profule-content__item").not(":first").hide();
    $(".profile__body .profile__tab").click(function () {
        $(".profile__body .profile__tab").removeClass("profile__tab_active").eq($(this).index()).addClass("profile__tab_active");
        $(".profule-content__item").hide().eq($(this).index()).fadeIn();
    }).eq(0).addClass("profile__tab_active");

    //ORDER LINK
    $('.order__number').click(function () {
        $(this).parents('.order-table__body').find('.order_toggle').slideToggle();
    });

    //ACCOURDEON
    $(".accordeon dd").prev().click(function () {
        $(this).parents(".accordeon").find("dd").not(this).slideUp().prev().removeClass("acc_active");
        $(this).next().not(":visible").slideDown().prev().addClass("acc_active");
    });


    // SLIDERS (Слайдеры)
    // HEADER SLIDER
    $('.header__slider').slick({
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 4000,
    });
    $('.brends__slider').slick({
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 2,
    });
    // PRODUCT SLIDER
    $('.one-product__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.one-product__slider_navigation',
        dots: false,
        infinite: true,
        focusOnSelect: true,
        arrows: true,
        swipe: false,
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: false,
                    dots: true,
                    focusOnSelect: false,
                    swipe: true,
                }
            }
        ]
    });
    $('.one-product__slider_navigation').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        infinite: true,
        asNavFor: '.one-product__slider',
        focusOnSelect: true,
        centerMode: false,
        vertical: true,
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    arrows: false,
                }
            }
        ]
    });
    // FOR MAN FOOT SLIDER
    $('.bc-foot__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        infinite: true,
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
    });
    // SALE SLIDER
    $('.sale__slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        infinite: true,
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
    // FOR MAN HEAD SLIDER
    $('.tab__slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
    $('.tabs__slider').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        fade: true,
        adaptiveHeight: true,
        draggable: false,
        swipe: false,
        touchMove: false,
        customPaging: function (slider, i) {
            var thumb = $(slider.$slides[i]).data('thumb');
            return '<span>' + thumb + '</span>';
        },
    });

    $(".ch__more").click(function () {
        $('.ch__about_hidden').slideToggle();
        $(this).toggleClass('ch__more_active');
        $(this).text(function (i, text) {
            return text === "Показать подробнее" ? "Свернуть" : "Показать подробнее";
        })
    });

    // POPUP'S (Всплывающее окно)
    $('.inst__link').magnificPopup({
        mainClass: 'mfp-fade',
        closeMarkup: '<button class="mfp-close mfp-close__btn icon-icon-small-close"></button>'
    });



    /*    $('.quick-view').magnificPopup({
     mainClass: 'mfp-fade',
     closeMarkup: '<button class="mfp-close mfp-close__btn icon-icon-small-close"></button>'
     });*/
    $('.quick-view').click(function () {
        $('.quick-view__popup').css({
            'opacity': '1',
            'visibility': 'visible',
            'height': 'auto'
        });
    });

    // VIEW FILTER
    $('.view > div').click(function () {
        $('.view > div').removeClass('view_three_items_active');
        $(this).addClass('view_three_items_active');
    });

    //REMINDER FORM PRODUCT
    $('.no-product__order').click(function () {
        $('.o-reminder').hide();
        $(this).parents('.no-item').find('.o-reminder').show();
    });
    $('.o-reminder__close').click(function () {
        $(this).parent().hide();
    });
    $('.no-product__reminder').click(function () {
        $('.reminder').hide();
        $(this).parents('.no-item').find('.reminder').show();
    });
    $('.reminder__close').click(function () {
        $(this).parent().hide();
    });


    $('.preorder_btn').click(function() {
        $('.o-reminder').hide();
        $(this).parents('.item').find('.o-reminder').show();
    });



    //INSTAGRAM CLOSE ICON
    $('.mfp-close').addClass('icon-icon-small-close');



    //FILTER ITEMS
    $('label.filter__item').click(function () {
        if ($(this).hasClass('filter__item_active')) {
            $(this).removeClass('filter__item_active');
            $(this).find('input[type=checkbox]').removeAttr('checked');
        } else {
            $(this).addClass('filter__item_active');
            $(this).find('input[type=checkbox]').prop('checked', true);
        }
        var form = $('#filter_form').serialize();
        var sectionId = $('#section_id').attr('data-id');

        if ($('div.filter_brands_mobile').css('display') !== 'none') {
            sectionId = 0;
        }

        $.ajax({
            type: "GET",
            url: "/ajax/ajax_filter.php",
            data: form + '&section_id=' + sectionId,
            success: function (resp) {
                $('#filter_result').html(resp);
            }
        });

        return false;
    });

    // Селект]

    $('body select').each(function () {
        var $this = $(this), numberOfOptions = $(this).children('option').length;

        $this.addClass('sel-hidden');
        $this.wrap('<div class="sel"></div>');
        $this.after('<div class="sel-styled"></div>');

        var $styledsel = $this.next('div.sel-styled');
        // $styledsel.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'sel-options'
        }).insertAfter($styledsel);

        for (var i = 0; i < numberOfOptions; i++) {
            if ($this.children('option').eq(i).attr('selected') === 'selected'){
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val(),
                    class: 'li_active',
                    onclick: $this.attr('onchange'),
                    value: $this.children('option').eq(i).val()
                }).appendTo($list);
                $styledsel.text($this.children('option').eq(i).text())
            } else {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val(),
                    onclick: $this.attr('onchange'),
                    value: $this.children('option').eq(i).val()
                }).appendTo($list);
            }
        }

        var $listItems = $list.children('li');

        $styledsel.click(function (e) {
            e.stopPropagation();
            $('div.sel-styled.active').not(this).each(function () {
                $(this).removeClass('active').next('ul.sel-options').hide();
            });
            $(this).toggleClass('active').next('ul.sel-options').toggle();
        });

        $listItems.click(function (e) {
            e.stopPropagation();
            $styledsel.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            //console.log($this.val());
        });

        $(document).click(function () {
            $styledsel.removeClass('active');
            $list.hide();
        });

        //set sort value
        var param = getParameterByName('sort_by');
        if (param != null) {
            $('.sel-options li').each(function () {
                if ($(this).attr('rel') == param) {
                    $styledsel.text($(this).text());
                    $(this).parent().find('li').removeClass('li_active');
                    $(this).addClass('li_active');
                }
            });
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    //SELECT CHECK
    $('.sel-options li').click(function () {
        if ($(this).closest('div.brend__panel').length > 0) {
            var sort = $(this).attr('rel');
            var param = getParameterByName('sort_by');
            var cur_url = document.location.href;

            if (param != null) {
                var url = document.location.href.replace(param, sort);
            } else {
                if (document.location.href.indexOf('?') > -1) {
                    var url = cur_url.toString() + "&sort_by=" + sort;
                } else {
                    var url = cur_url.toString() + "?sort_by=" + sort;
                }
            }
            document.location = url;
        }

        $(this).parent().find('li').removeClass('li_active');
        $(this).addClass('li_active');
    });

    //СЧЕТЧИК
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        var id = $input.attr('data-id');
        //updateQuantity(id, count);
        $input.val(count);

        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        var id = $input.attr('data-id');
        var count = parseInt($input.val()) + 1;
        //updateQuantity(id, count);
        $input.val(parseInt($input.val()) + 1);

        $input.change();
        return false;
    });

    $(document).on('change', 'input.basket_quantity_input', function () {
        var quantity = $(this).val();
        var id = parseInt($(this).attr('data-id'));

        if (quantity <= 0 || isNaN(quantity) || typeof quantity == 'undefined' || quantity.length <= 0 ){
            quantity = 1;
            $(this).val(1);
        }

        $('#quantity__input_'+id).val(quantity);

        updateQuantity(id, quantity);
    });

    //DISCOUNT INPUT
    $('.discount__input').click(function () {
        $('.discount__btn').show();
    });

    // BREND TEXT TOGGLE
    $('.brend-buy__head').click(function () {
        $('.brend-buy__text_hidden').slideToggle();
        $('.brend-buy__head span').toggleClass('brend-arrow_active');
    });

    //FILTER TOGGLE
    $('.filter').click(function () {
        $('.filter_toggle').slideToggle();
        $(this).toggleClass('filter_active');
    });
		
		var resized = false;
		$(window).resize(function () {								
				var wid = $(window).width();								
				if (wid > 991) {
					if (!resized) {
						$('.tovar-img')
								.wrap('<span style="display:inline-block"></span>')
								.css('display', 'block')
								.parent()
								.zoom({
										magnify: 1
								});
						resized = true;
					}
				} else {
					$('.tovar-img').trigger('zoom.destroy');
					resized = false;
				}							
		});
    if($(window).width() > 991) {
        $('.tovar-img')
            .wrap('<span style="display:inline-block"></span>')
            .css('display', 'block')
            .parent()
            .zoom({
                magnify: 1
            });
    } else {
        $('.tovar-img').trigger('zoom.destroy');
    }

    /*// PRICE SLIDER
     var keypressSlider = document.getElementById('keypress');
     var input0 = document.getElementById('input-with-keypress-0');
     var input1 = document.getElementById('input-with-keypress-1');
     var inputs = [input0, input1];

     /!*noUiSlider.create(keypressSlider, {
     start: [3000, 10000],
     connect: true,
     tooltips: false,
     range: {
     'min': 0,
     'max': 20000
     }
     });*!/

     function setSliderHandle(i, value) {
     var r = [null,null];
     r[i] = value;
     keypressSlider.noUiSlider.set(r);
     }

     // Listen to keydown events on the input field.
     inputs.forEach(function(input, handle) {

     input.addEventListener('change', function(){
     setSliderHandle(handle, this.value);
     });

     input.addEventListener('keydown', function( e ) {

     var values = keypressSlider.noUiSlider.get();
     var value = Number(values[handle]);

     // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
     var steps = keypressSlider.noUiSlider.steps();

     // [down, up]
     var step = steps[handle];

     var position;

     // 13 is enter,
     // 38 is key up,
     // 40 is key down.
     switch ( e.which ) {

     case 13:
     setSliderHandle(handle, this.value);
     break;

     case 38:

     // Get step to go increase slider value (up)
     position = step[1];

     // false = no step is set
     if ( position === false ) {
     position = 1;
     }

     // null = edge of slider
     if ( position !== null ) {
     setSliderHandle(handle, value + position);
     }

     break;

     case 40:

     position = step[0];

     if ( position === false ) {
     position = 1;
     }

     if ( position !== null ) {
     setSliderHandle(handle, value - position);
     }

     break;
     }
     });
     });

     keypressSlider.noUiSlider.on('update', function( values, handle ) {
     inputs[handle].value = Math.round(values[handle]);
     });*/

});

$(function() {
	if (document.getElementById('watches_by_color')) {
		// Отступы в разделе "Для нее" и "Для него"
		var list = document.getElementById('watches_by_color').querySelector('.slick-dots');
		var height = list.clientHeight;
		list.parentNode.style.paddingTop = height + 45 + 'px';
	}
	
	// События для добавления прокрутки в корзину
	$('.header__basket').click(shrinkCart);	
	$('.cart__icon').click(shrinkCart);	
	$(window).resize(shrinkCart);	
	$(window).on('orientationchange', shrinkCart);
	$(window).scroll(function() {
		if ($(window).scrollTop() >= 200) {
			$('.header__basket').trigger('stick');
		} else {
			$('.header__basket').trigger('unstick');
		}
	});		
});
// Убрать полосы прокрутки, когда корзина прилипает
var stick, height;	
var mob = !($(window).width() > 991) ;
var stickCart = new CustomEvent('stick'), unStickCart = new CustomEvent('unstick');
$('.header__basket').on('stick', function() {		
	if (!stick && shrinked) {
		if (mob) {
			height = $('#smallcart_mob .scrollbar-outer').css('max-height');			
			$('#smallcart_mob .scrollbar-outer').css('max-height', '+=3');
		} else {
			height = $('#smallcart .scrollbar-outer').css('max-height');			
			$('#smallcart .scrollbar-outer').css('max-height', '+=118');	
		}
		stick = true;		
	}
});
$('.header__basket').on('unstick', function() {
	if (mob) {		
			$('#smallcart_mob .scrollbar-outer').css('max-height', height);
		} else {
			$('#smallcart .scrollbar-outer').css('max-height', height);
		}
	stick = false, height = null;
});

// Добавить скролл в таблицу, если не влезает на экран
var shrinked = false;
function shrinkCart() {	
	var mob = !($(window).width() > 991) ;
	var container = (mob) ? document.getElementById('smallcart_mob') : document.getElementById('smallcart');	
	var screenOffset = (mob) ? 57	: 98;	
	var stickMobMenuHeight = (mob) ? 74 : 0;
	var pmbtb = (mob) ?  188 : 138; // высота всех элементов в корзине кроме товара
	var cartItems = container.querySelectorAll('.h-basket__item');		
	var cartHeight = (106)*cartItems.length + screenOffset + pmbtb; // 106 = margin-top + height + padding + border товара в корзине
	
	var usefulScreenHeight = (stick) ? $(window).height() - stickMobMenuHeight : $(window).height() - screenOffset - 20;

	if (usefulScreenHeight < cartHeight) {
		$('.scrollbar-outer').scrollbar();	
		var scrollArea = cartItems[0].closest('.scroll-wrapper');					
		scrollArea.style.maxHeight = usefulScreenHeight - pmbtb + 'px';			
		shrinked = true;		
	}			
}

// Сделать меню в мобильной версии со скроллом
$('.header__head_mob .menu__btn').click(shrinkMobMenu);
function shrinkMobMenu() {
	var screenHeight = $(window).height();
	var menuHeight = $('.header__head_mob').outerHeight();
	$('.header__head_mob .toggle_menu').height(screenHeight - menuHeight);
	console.log(screenHeight, menuHeight);
}

var animation;
$(document).on('click', '.quick-view__popup .add_to_cart_btn', function() {		
	var text = $(this).html();
	var btn = this;	
	
	$(this).html('&#10004 Добавлено');
	
	if (!animation) {
		animation = setTimeout(function() {
			$(btn).html(text);
			animation = null;
		}, 1000);		
	}
});

// LOADER (Загрузчик)
$(window).load(function () {
    $(".loader_inner").fadeOut();
    $(".loader").delay(400).fadeOut("slow");
});



