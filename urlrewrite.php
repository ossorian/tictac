<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/catalog/([/-_.a-zA-Z0-9]+)/([0-9a-zA-Z_-]+).*/([\\w-]+)/#",
		"RULE" => "SECTION_CODE=\$2&CODE=\$3&qwe=qwe",
		"ID" => "",
		"PATH" => "/catalog/detail.php",
	),
    array(
        "CONDITION" => "#^/catalog/straps/([\\w-]+)/#",
        "RULE" => "CODE=\$1&qwe=qwe",
        "ID" => "",
        "PATH" => "/catalog/detail.php",
    ),
	array(
		"CONDITION" => "#^/catalog/([/-_.a-zA-Z0-9]+)/([0-9a-zA-Z_-]+).*/#",
		"RULE" => "SECTION_CODE=\$2&qwe=qwe",
		"ID" => "catalog.section",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/site_bu/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/site_bu/personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/([\\w-]+)#",
		"RULE" => "SECTION_CODE=\$1&qwe=qwe",
		"ID" => "",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/site_bu/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/site_bu/news/index.php",
	),
	array(
		"CONDITION" => "#^/articles/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/articles/index.php",
	),
);

?>