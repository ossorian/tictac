<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("TicTacToy.ru");
?>

    <!-- BY COLOR (По цвету) -->
    <div class="header__body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header__banner">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_him/includes/include_main_image.php",
                            Array(),
                            Array("MODE" => "text", "NAME" => "промо текст")
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="by-color sections">
        <div class="container" id="watches_by_color">

            <div class="row">
                <div class="col-xs-12">
                    <h2>Мужские часы по цвету</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="tabs__slider slider__for">
                        <?
                        $cache = new CPHPCache();
                        $cache_time = 3600;
                        $cache_id = 'special_offers123543_for_her';
                        if($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, false))
                        {
                            $arPropColor = $cache->GetVars();
                        }
                        elseif($cache->StartDataCache())
                        {
                            $arPropColor = array();
                            $res = CIBlockProperty::GetPropertyEnum(51);
                            while ($propColor = $res->GetNext()) {
                                if ((int)$propColor['ID'] !== 113 && (int)$propColor['ID'] !== 114 && (int)$propColor['ID'] !== 120
                                    && (int)$propColor['ID'] !== 108 && (int)$propColor['ID'] !== 110 && (int)$propColor['ID'] !== 111 && (int)$propColor['ID'] !== 112
                                ) {
                                    $arPropColor[] = $propColor;
                                }
                            }

                            $cache->EndDataCache($arPropColor);
                        }


                        $index = 0;
                        foreach ($arPropColor as $propColor) {
                                $index++;
                                ?>
                                <div class="tab__item<?= $index == 1 ? ' active-item' : '' ?>"
                                     data-thumb="<?= $propColor['VALUE'] ?>">
                                    <div class="tab__slider">
                                        <?
                                        $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND_MODEL", "CATALOG_PRICE_1");
                                        $arFilter = array(
                                            "IBLOCK_ID" => 5,
                                            "ACTIVE" => "Y",
                                            ">CATALOG_PRICE_1" => 0,
                                            "PROPERTY_FILTER_COLOR" => $propColor['ID'],
                                            "PROPERTY_SEX" => 6,
                                            "IBLOCK_SECTION_ID" => array(172),
                                            "SECTION_ACTIVE" => "Y"
                                        );
                                        $position = 0 ;
                                        $resElem = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 16), $arSelect);
                                        while ($arElem = $resElem->GetNext()) { ?>
                                            <?$position++?>
                                            <? $picture = CFile::GetPath($arElem['PREVIEW_PICTURE']); ?>

                                                <div class="slide">
                                                    <div class="item">

                                                        <div class="favorites">
                                                            <span class="favorites__icon" data-id="<?= $arElem['ID'] ?>">
                                                                <i class="icon-icon-wishlist"></i>
                                                            </span>
                                                            <p class="favorites__text">Избранное</p>
                                                        </div>
                                                        <a href="#qv__popup" class="quick-view" data-id="<?= $arElem['ID'] ?>">
                                                            <span class="quick-view__icon">
                                                                <i class="icon-icon-quick-view"></i>
                                                            </span>
                                                            <span class="quick-view__text">Быстрый просмотр</span>
                                                        </a>
                                                        <a href="<?= $arElem['DETAIL_PAGE_URL'] ?>"
                                                           data-seo="Для него - Мужские часы по цвету"
                                                           onclick="
                                                               dataLayer.push({
                                                               'event': 'productClick',
                                                               'ecommerce': {
                                                               'click': {
                                                               'actionField': {'list': 'Для него - Мужские часы по цвету'},     // Список с которого был клик.
                                                               'products': [{
                                                               'name': '<?= $arElem['NAME']?>',       	// Наименование товара.
                                                               'id': '<?= $arElem['ID']?>',  				// Идентификатор товара в базе данных.
                                                               'price': '<?= $arElem['CATALOG_PRICE_1']?>',   				// Цена одной единицы купленного товара
                                                               'brand': '<?= $arElem['PROPERTY_BRAND_MODEL_VALUE']?>',  			 //Бренд товара
                                                               'category': 'часы', 			 //Категория товара
                                                               'variant': '<?= $propColor['VALUE'] ?>',  				//Вариант товара, например цвет
                                                               'position': <?= $position?>  				//Позиция товара в списке
                                                               }]
                                                               }
                                                               }
                                                               });"
                                                        >
                                                            <img src="<?= $picture ?>" alt="">
                                                            <p class="item__name"><?= $arElem['PROPERTY_BRAND_MODEL_VALUE'] ?></p>
                                                            <p class="item__name"><?= $arElem['NAME'] ?></p>
                                                        </a>
                                                    </div>
                                                </div>

                                        <? } ?>
                                    </div>
                                </div>
                                <?
                        } ?>
                    </div>
                </div>
            </div>
            <? $APPLICATION->IncludeFile(
                SITE_DIR . "for_him/includes/include_filter_block.php",
                Array(),
                Array("MODE" => "text", "NAME" => "фильтр")
            ); ?>
            <div class="by-color__items_big">
                <div class="row">
                    <div class="col-sm-6">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_him/includes/include_1_block.php",
                            Array(),
                            Array("MODE" => "text", "NAME" => "блок")
                        ); ?>
                    </div>
                    <div class="col-sm-6">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_him/includes/include_2_block.php",
                            Array(),
                            Array("MODE" => "text", "NAME" => "блок")
                        ); ?>
                    </div>
                    <div class="col-sm-6">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_him/includes/include_3_block.php",
                            Array(),
                            Array("MODE" => "text", "NAME" => "блок")
                        ); ?>
                    </div>

                    <div class="col-sm-6">
                        <div class="bc-foot__slider">
                            <?
                            require_once($_SERVER['DOCUMENT_ROOT'] . '/for_him/includes/include_filter_block.php');
                            $minimum_price = defined('MINIMUM_PRICE') ? MINIMUM_PRICE : 0;
                            $maximum_price = defined('MAXIMUM_PRICE') ? MAXIMUM_PRICE : 0;

                            $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND_MODEL", "CATALOG_PRICE_1");
                            $arFilter = array(
                                "IBLOCK_ID" => 5,
                                "ACTIVE" => "Y",
                                "><CATALOG_PRICE_1" => array($minimum_price, $maximum_price),
                                "PROPERTY_SEX" => 6,
                                "!SECTION_ID" => array(146, 165, 152, 171),
                                "SECTION_ACTIVE" => "Y"
                            );
                            $position = 0 ;
                            $resElem = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 10), $arSelect);
                            while ($arElem = $resElem->GetNext()) {
                                $position++;
                                $picture = CFile::GetPath($arElem['PREVIEW_PICTURE']);
                                ?>
                                <a href="<?= $arElem['DETAIL_PAGE_URL'] ?>"
                                   data-seo="Для него"
                                   onclick="
                                       dataLayer.push({
                                       'event': 'productClick',
                                       'ecommerce': {
                                       'click': {
                                       'actionField': {'list': 'Для него'},     // Список с которого был клик.
                                       'products': [{
                                       'name': '<?= $arElem['NAME']?>',       	// Наименование товара.
                                       'id': '<?= $arElem['ID']?>',  				// Идентификатор товара в базе данных.
                                       'price': '<?= $arElem['CATALOG_PRICE_1']?>',   				// Цена одной единицы купленного товара
                                       'brand': '<?= $arElem['PROPERTY_BRAND_MODEL_VALUE']?>',  			 //Бренд товара
                                       'category': 'часы', 			 //Категория товара
                                       'variant': '<?= $arElem['PROPERTY_FILTER_COLOR_VALUE'] ?>',  				//Вариант товара, например цвет
                                       'position': <?= $position?>  				//Позиция товара в списке
                                       }]
                                       }
                                       }
                                       });"
                                >
                                    <div class="slide">
                                        <div class="bc-foot__slide">
                                            <img src="<?= $picture ?>" alt="">
                                            <p class="item__name"><?= $arElem['PROPERTY_BRAND_MODEL_VALUE'] ?></p>
                                            <p class="item__name"><?= $arElem['NAME'] ?></p>
                                        </div>
                                    </div>
                                </a>
                                <?
                            }
                            ?>
                        </div>
                        <div class="bc-foot__text">
                            <? $APPLICATION->IncludeFile(
                                SITE_DIR . "for_him/includes/include_filter_text_block.php",
                                Array(),
                                Array("MODE" => "text", "NAME" => "заголовок")
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- BY COLOR (По цвету) -->


    <!-- SALE (РАСПРОДАЖА) -->
    <section class="sections sale">
        <? $APPLICATION->IncludeFile(
            SITE_DIR . "for_him/includes/include_promo_text.php",
            Array(),
            Array("MODE" => "text", "NAME" => "промо текст")
        ); ?>
        <div class="container">
            <div class="sale__banner">
                <div class="row">
                    <div class="col-xs-12">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_him/includes/include_sunglasses_block.php",
                            Array(),
                            Array("MODE" => "html", "NAME" => "блок")
                        ); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="sale__slider">
                        <?
                        $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND_MODEL");
                        $arFilter = array(
                            "IBLOCK_ID" => 5,
                            "ACTIVE" => "Y",
                            "PROPERTY_SEX" => 6,
                            "SECTION_ID" => 165,
                            "SECTION_ACTIVE" => "Y"
                        );
                        $resElem = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 16), $arSelect);
                        while ($arElem = $resElem->GetNext()) {
                            $picture = CFile::GetPath($arElem['PREVIEW_PICTURE']);
                            ?>
                            <a href="<?= $arElem['DETAIL_PAGE_URL'] ?>">
                                <div class="slide">
                                    <div class="dropdown__item">
                                        <div class="dd-item-img__wrap">
                                            <img src="<?= $picture ?>" class="dd-item__img"
                                                 alt="<?= $arElem['NAME'] ?>">
                                        </div>
                                        <p class="dd-item__name"><?= $arElem['NAME'] ?></p>
                                    </div>
                                </div>
                            </a>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="sale__banner">
                <div class="row">
                    <div class="col-xs-12">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_him/includes/include_accessories_block.php",
                            Array(),
                            Array("MODE" => "html", "NAME" => "блок")
                        ); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="sale__slider">
                        <?
                        $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND_MODEL");
                        $arFilter = array(
                            "IBLOCK_ID" => 5,
                            "ACTIVE" => "Y",
                            "PROPERTY_SEX" => 6,
                            "SECTION_ID" => 171,
                            "SECTION_ACTIVE" => "Y"
                        );
                        $resElem = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 16), $arSelect);
                        while ($arElem = $resElem->GetNext()) {
                            $picture = CFile::GetPath($arElem['PREVIEW_PICTURE']);
                            ?>
                            <a href="<?= $arElem['DETAIL_PAGE_URL'] ?>">
                                <div class="slide">
                                    <div class="dropdown__item">
                                        <div class="dd-item-img__wrap">
                                            <img src="<?= $picture ?>" class="dd-item__img"
                                                 alt="<?= $arElem['NAME'] ?>">
                                        </div>
                                        <p class="dd-item__name"><?= $arElem['NAME'] ?></p>
                                    </div>
                                </div>
                            </a>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SALE (РАСПРОДАЖА) -->
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>