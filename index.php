<?
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Купить дизайнерские наручные часы (мужские и женские) недорого в российском интернет-магазине TicTacToy.ru. Только самые интересные стильные решения дизайна часов в ассортименте. ✔ Качество ✔Цена ✔Доставка по всей РФ.");
$APPLICATION->SetPageProperty("title", "Купить дизайнерские наручные часы (мужские и женские) недорого в российском интернет-магазине TicTacToy.ru");
$APPLICATION->SetTitle("TicTacToy.ru - магазин дизайнерских часов");
?>



	<!-- ADVANTAGES (Преимущества) -->
	<section class="advantages">
		<div class="container">
			<div class="adv__items">
				<div class="row">
					<div class="col-xs-4">
						<div class="adv-item__wrap">
							<div class="adv__item">
								<span class="adv-item__icon">
									<i class="icon-icon-original"></i>
								</span>
								<p class="adv-item__text">Оригинальная продукция</p>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="adv-item__wrap">
							<div class="adv__item">
								<span class="adv-item__icon">
									<i class="icon-icon-instock"></i>
								</span>
								<p class="adv-item__text">Все товары в наличии</p>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="adv-item__wrap">
							<div class="adv__item">
								<span class="adv-item__icon">
									<i class="icon-icon-shipping"></i>
								</span>
								<p class="adv-item__text">Доставка по всей России</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ADVANTAGES (Преимущества) -->


	<!-- INSTAGRAM (Инстаграм) -->
	<section class="instagram">
		<div class="container">
			<div class="inst__items">
				<div class="row mob_row">
					<div class="col-md-3 mob_pad">
						<div class="instagram__item">
							<p>Купите часы, представленные в нашем Инстаграме</p>
							<a href="/instagram/" class="btn btn_black inst__btn"><i class="icon-icon-inst"></i>Все товары</a>
						</div>
					</div>

					</div>
				
					<script src="//foursixty.com/media/scripts/fs.embed.v2.5.js" data-feed-id="tictactoy-ltd" data-open-links-in-same-page="false" data-theme="lookbook_v2_5" data-page-size='7'></script><style>.fs-has-links::after {  padding:  11px 16.5px; border: 2px solid rgb(255, 255, 255); color:rgb(255, 255, 255); content: "Купить образ";  }.fs-has-links:hover:after { background-color: rgb(255, 255, 255); color: rgba(0,0,0,0.8)}.fs-wrapper div.fs-text-container .fs-entry-title, div.fs-detail-title{font-family:calibri;font-style:italic;font-weight:bold;}div.fs-text-container .fs-entry-date, div.fs-detail-container .fs-post-info, div.fs-wrapper div.fs-has-links::after, .fs-text-product, .fs-overlink-text{font-family:;font-style:normal;font-weight:bold;}.fs-wrapper div.fs-text-container * {color:rgb(255, 255, 255)}.fs-wrapper div.fs-text-container {background-color:rgba(0,0,0,0.8); margin: 0px}div.fs-entry-date{display:none}div.fs-entry-title{display:none}.fs-wrapper div.fs-timeline-entry{ margin: 8px }</style>
				</div>
			</div>
		</div>
	</section>
	<!-- INSTAGRAM (Инстаграм) -->


	<!-- POPULAR (Популярные товары) -->
	<section class="popular sections">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2>Популярные товары</h2>
				</div>
			</div>
			<?
			global $indexFilter;
			$indexFilter = array(">PROPERTY_INDEX" => 0, "SECTION_ID" => 172, "INCLUDE_SUBSECTIONS" => "Y");
			?>
			<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "popular_goods_new", Array(
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"IBLOCK_TYPE" => "TicTacToy",	// Тип инфо-блока
				"IBLOCK_ID" => "5",	// Инфо-блок
				"SECTION_ID" => "",	// ID раздела
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_USER_FIELDS" => "",	// Свойства раздела
				"ELEMENT_SORT_FIELD" => "PROPERTY_INDEX",	// По какому полю сортируем элементы
				"ELEMENT_SORT_ORDER" => "ASC",	// Порядок сортировки элементов
				"FILTER_NAME" => "indexFilter",	// Имя массива со значениями фильтра для фильтрации элементов
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
				"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
				"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
				"BASKET_URL" => "/basket/",	// URL, ведущий на страницу с корзиной покупателя
				"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
				"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
				"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
				"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
				"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
				"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
				"PAGE_ELEMENT_COUNT" => "8",	// Количество элементов на странице
				"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
				"PROPERTY_CODE" => array(	// Свойства
					0 => "NEW",
					1 => "IMAGE",
				),
				"PRICE_CODE" => array(	// Тип цены
					0 => "BASE",
				),
				"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
				"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
				"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
				"PRODUCT_PROPERTIES" => "",	// Характеристики товара
				"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"CACHE_FILTER" => "Y",	// Кэшировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
				"PAGER_TITLE" => "Товары",	// Название категорий
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => "",	// Название шаблона
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			),
				false
			);?>
			<?
			global $indexFilter;
			$indexFilter = array(">PROPERTY_INDEX" => 0, "SECTION_ID" => 171, "INCLUDE_SUBSECTIONS" => "Y");
			?>
			<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "popular_goods_new", Array(
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"IBLOCK_TYPE" => "TicTacToy",	// Тип инфо-блока
				"IBLOCK_ID" => "5",	// Инфо-блок
				"SECTION_ID" => "",	// ID раздела
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_USER_FIELDS" => "",	// Свойства раздела
				"ELEMENT_SORT_FIELD" => "PROPERTY_INDEX",	// По какому полю сортируем элементы
				"ELEMENT_SORT_ORDER" => "ASC",	// Порядок сортировки элементов
				"FILTER_NAME" => "indexFilter",	// Имя массива со значениями фильтра для фильтрации элементов
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
				"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
				"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
				"BASKET_URL" => "/basket/",	// URL, ведущий на страницу с корзиной покупателя
				"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
				"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
				"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
				"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
				"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
				"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
				"PAGE_ELEMENT_COUNT" => "8",	// Количество элементов на странице
				"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
				"PROPERTY_CODE" => array(	// Свойства
					0 => "NEW",
					1 => "IMAGE",
				),
				"PRICE_CODE" => array(	// Тип цены
					0 => "BASE",
				),
				"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
				"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
				"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
				"PRODUCT_PROPERTIES" => "",	// Характеристики товара
				"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"CACHE_FILTER" => "Y",	// Кэшировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
				"PAGER_TITLE" => "Товары",	// Название категорий
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => "",	// Название шаблона
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			),
				false
			);?>
			<?
			global $indexFilter;
			$indexFilter = array(">PROPERTY_INDEX" => 0, "SECTION_ID" => 165, "INCLUDE_SUBSECTIONS" => "Y");
			?>
			<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "popular_goods_new", Array(
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"IBLOCK_TYPE" => "TicTacToy",	// Тип инфо-блока
				"IBLOCK_ID" => "5",	// Инфо-блок
				"SECTION_ID" => "",	// ID раздела
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_USER_FIELDS" => "",	// Свойства раздела
				"ELEMENT_SORT_FIELD" => "PROPERTY_INDEX",	// По какому полю сортируем элементы
				"ELEMENT_SORT_ORDER" => "ASC",	// Порядок сортировки элементов
				"FILTER_NAME" => "indexFilter",	// Имя массива со значениями фильтра для фильтрации элементов
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
				"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
				"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
				"BASKET_URL" => "/basket/",	// URL, ведущий на страницу с корзиной покупателя
				"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
				"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
				"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
				"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
				"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
				"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
				"PAGE_ELEMENT_COUNT" => "8",	// Количество элементов на странице
				"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
				"PROPERTY_CODE" => array(	// Свойства
					0 => "NEW",
					1 => "IMAGE",
				),
				"PRICE_CODE" => array(	// Тип цены
					0 => "BASE",
				),
				"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
				"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
				"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
				"PRODUCT_PROPERTIES" => "",	// Характеристики товара
				"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"CACHE_FILTER" => "Y",	// Кэшировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
				"PAGER_TITLE" => "Товары",	// Название категорий
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => "",	// Название шаблона
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			),
				false
			);?>
			<div class="popular__nav">
				<div class="row mob_row">
					<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.section.list",
						"index_menu",
						array(
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_GROUPS" => "N",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
							"COMPONENT_TEMPLATE" => "index_menu",
							"COUNT_ELEMENTS" => "N",
							"IBLOCK_ID" => "5",
							"IBLOCK_TYPE" => "TicTacToy",
							"SECTION_CODE" => "",
							"SECTION_FIELDS" => array(
								0 => "",
								1 => "",
							),
							"SECTION_ID" => $_REQUEST["SECTION_ID"],
							"SECTION_URL" => "",
							"SECTION_USER_FIELDS" => array(
								0 => "",
								1 => "UF_LOGO",
								2 => "UF_RETARGETING",
								3 => "UF_BRAND_BOLD",
								4 => "UF_POPULAR_BRAND",
								5 => "UF_POPULAR_LOGO",
								6 => "UF_MAIN_PICTURE",
								7 => "",
							),
							"SHOW_PARENT_NAME" => "Y",
							"TOP_DEPTH" => "2",
							"VIEW_MODE" => "LINE"
						),
						false
					);?>
				</div>
			</div>
		</div>
	</section>
	<!-- POPULAR (Популярные товары) -->

	<!-- BRENDS (Популярные бренды) -->
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"tree_popular_brands",
	array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "popular_brands",
		"COUNT_ELEMENTS" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "TicTacToy",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "DESCRIPTION",
			6 => "PICTURE",
			7 => "DETAIL_PICTURE",
			8 => "IBLOCK_TYPE_ID",
			9 => "IBLOCK_ID",
			10 => "IBLOCK_CODE",
			11 => "IBLOCK_EXTERNAL_ID",
			12 => "DATE_CREATE",
			13 => "CREATED_BY",
			14 => "TIMESTAMP_X",
			15 => "MODIFIED_BY",
			16 => "",
		),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_LOGO",
			1 => "UF_RETARGETING",
			2 => "UF_BRAND_BOLD",
			3 => "UF_POPULAR_BRAND",
			4 => "UF_POPULAR_LOGO",
			5 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "4",
		"VIEW_MODE" => "LINE"
	),

	false
);?>
	<!-- POPULAR (Популярные товары) -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
