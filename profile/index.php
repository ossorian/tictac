<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("TicTacToy.ru");
?>
<?
global $USER;
if (!$USER->IsAuthorized()){
    LocalRedirect('/');
}
?>


<section class="profile sections">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Личный кабинет</h2>
            </div>
        </div>
        <div class="profile__body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="profile__tabs">
                        <span class="profile__tab">Мои заказы</span>
                        <span class="profile__tab">Настройки</span>
                    </div>
                    <div class="profile__content">
                        <div class="profule-content__item profile__content_active">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:sale.personal.order",
                                "personal_order",
                                array(
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "3600",
                                    "CACHE_TYPE" => "A",
                                    "CUSTOM_SELECT_PROPS" => array(),
                                    "HISTORIC_STATUSES" => array(
                                        0 => "F",
                                    ),
                                    "NAV_TEMPLATE" => "",
                                    "ORDERS_PER_PAGE" => "20",
                                    "PATH_TO_BASKET" => "/personal/cart",
                                    "PATH_TO_PAYMENT" => "/personal/order/payment/",
                                    "SAVE_IN_SESSION" => "Y",
                                    "SEF_MODE" => "N",
                                    "SET_TITLE" => "Y",
                                    "STATUS_COLOR_F" => "gray",
                                    "STATUS_COLOR_N" => "green",
                                    "STATUS_COLOR_PSEUDO_CANCELLED" => "red",
                                    "AJAX_MODE" => "Y",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "STATUS_COLOR_SD" => "gray"
                                ),
                                false
                            ); ?>

                        </div>
                        <div class="profule-content__item">
                            <? $APPLICATION->IncludeComponent("bitrix:main.profile", "profile", Array(
                                "AJAX_MODE" => "Y",    // Включить режим AJAX
                                "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                                "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                                "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                                "CHECK_RIGHTS" => "N",    // Проверять права доступа
                                "SEND_INFO" => "N",    // Генерировать почтовое событие
                                "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
                                "USER_PROPERTY" => array(    // Показывать доп. свойства
                                    0 => "UF_ORG_INN",
                                    1 => "UF_ORG_OGRN",
                                ),
                                "USER_PROPERTY_NAME" => "",    // Название закладки с доп. свойствами
                            ),
                                false
                            ); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
