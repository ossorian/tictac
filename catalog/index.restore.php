<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Каталог часов");


// Сортировка по умолчанию
if (empty($_GET["sort"])) {$_GET["sort"] = "sort"; $_REQUEST["sort"] = "sort";}
if (empty($_GET["order"])) {$_GET["order"] = "asc"; $_REQUEST["order"] = "asc";}

// Если это промо раздел то сортировка по своему алгоритму
if($_REQUEST["SECTION_CODE"] == "promo" and $_GET["sort"] != "CATALOG_PRICE_1") {
	$_GET["sort"] = "PROPERTY_SORT";
	$_GET["order"] = "ASC";
	$_REQUEST["sort"] = "PROPERTY_SORT";
	$_REQUEST["order"] = "ASC";
}

// Переадресация разделов
if ($_REQUEST["SECTION_CODE"] == false and empty($_GET["q"]) and !isset($_GET["arrFilter_cf"]["1"]["LEFT"])) {
	$arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "GLOBAL_ACTIVE" => "Y");
	$res = CIBlockSection::GetList(Array("sort" => "asc"), $arFilter);
	if($arFields = $res->GetNext()) {
		header("Location: ".$arFields["SECTION_PAGE_URL"]);
		die();
	}
}

// тут был фильтр

// Для фильтра показываем только товары в наличии
if(isset($_GET["arrFilter_cf"]["1"]["LEFT"]))
	$arrFilter[">CATALOG_QUANTITY"] = 0;

// Если указано м+ж то убираем фильтрацию по этим полям
if(count($arrFilter["PROPERTY"]["SEX"]) >= 2)
	unset($arrFilter["PROPERTY"]["SEX"]);

// Если это поиск по названию то показываем соответствующие товары
if(!empty($_GET["q"])) {
	$arrFilter = array();
	$arrFilter["%NAME"] = $_GET["q"];
}

?>


<?$sectId = $APPLICATION->IncludeComponent("bitrix:catalog.section", "catalog", array(
	"IBLOCK_TYPE" => "TicTacToy",
	"IBLOCK_ID" => "5",
	"SECTION_ID" => "",
	"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
	"SECTION_USER_FIELDS" => array(
		0 => "UF_RETARGETING",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => $_REQUEST["sort"],
	"ELEMENT_SORT_ORDER" => $_REQUEST["order"],
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER2" => "desc",
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
	"HIDE_NOT_AVAILABLE" => "N",
	"PAGE_ELEMENT_COUNT" => "999",
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "NEW",
		1 => "SEX",
		2 => "WAIT",
		3 => "",
	),
	"OFFERS_LIMIT" => "5",
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/cart/",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "N",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "3600",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "Y",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "Y",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => array(
		0 => "BASE",
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

<div style='height: auto;'>
 <!-- seo -->

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
	"PATH" => "/catalog/incs/".$sectId.".php"
	),
	false
);?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>