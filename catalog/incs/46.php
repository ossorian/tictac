<div class="text-open">
 <a href="javascript:void(0)"><span>Подробнее</span> ↓</a>
</div>
<div class="text-open-info">
<p>Дизайнерские наручные часы WEWOOD разрабатываются в итальянской Флоренции, где в 2010 году была создана компания WEWOOD. Флоренция являются символом творчества и искусства, эпохи Возрождения и культурной столицей Италии, что целиком соответствует философии бренда.
<p>Прогрессивный бренд WEWOOD предоставляет возможность по-новому взглянуть на дизайн, роскошь и натуральность. Бренд предлагает каждому новое открытие окружающего природного мира, его красоты, гениальности и вдохновляющего натурального дизайна. Помимо этого, если WEWOOD часы купить, это позволит лично принять участие в его защите и воссоздании.

<p>Инновационные и на 100% деревянные часы WEWOOD производятся из дорогостоящего вторичного сырья, а также из пород дорогостоящей и редкой древесины – красного, черного дерева и прочей. Помимо этого, данные хронометры полностью безопасны, создаются из неалергенных пород дерева, живых природных материалов. Вместе с тем, дизайнерские часы наручные WEWOOD оборудованы нетоксичным часовым механизмом Myota от знаменитого японского производителя.

<p>В то же время, деревянные часы WEWOOD отличаются крайне ответственным подходом к их изготовлению. Компания заботится об охране лесов и обязуется высаживать в почву одно дерево за каждые приобретенные дизайнерские часы ВИВУД. Именно поэтому лозунг компании звучит так «One Watch – One Tree – One Planet» - одни часы, одно дерево, одна планета.

<p>Дизайнерские часы WEWOOD выполняются в различных цветовых сочетаниях, с использованием разных древесных пород, включая клен, сосну и многие другие. Все это позволяет цвету часов изменять цветовую гамму от черного до светлого кленового.

<p>Конечно, ключевой особенностью каждого хронометра компании становится производство из всевозможных древесных пород, с применением только натуральной древесины первичной переработки, а также отсутствием токсинов и аллергенов. Бренд WEWOOD дарит своим единомышленникам возможность совместного участия в программе спасения природы нашей планеты. После каждых купленных часов компания высаживает в землю дерево, а к часам предоставляется сертификат, который подтверждает, что благодаря владельцу планета получила еще одно дерево. Эту возможность оценят по существу все те, кто предпочитает товары из натуральных материалов и заботится о природе, желая ей здоровья и долгого существования. 
</div>