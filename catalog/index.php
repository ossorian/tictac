<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Каталог часов");
global $USER;
// Сортировка по умолчанию
if (empty($_GET["sort"])) {
    $_GET["sort"] = "sort";
    $_REQUEST["sort"] = "sort";
}
if (empty($_GET["order"])) {
    $_GET["order"] = "asc";
    $_REQUEST["order"] = "asc";
}

// Если это промо раздел то сортировка по своему алгоритму
if ($_REQUEST["SECTION_CODE"] == "promo" and $_GET["sort"] != "CATALOG_PRICE_1") {
    $_GET["sort"] = "PROPERTY_SORT";
    $_GET["order"] = "ASC";
    $_REQUEST["sort"] = "PROPERTY_SORT";
    $_REQUEST["order"] = "ASC";
}

// Переадресация разделов
if ($_REQUEST["SECTION_CODE"] == false and empty($_GET["q"]) and !isset($_GET["arrFilter_cf"]["1"]["LEFT"]) && empty($_GET['set_filter'])) {
//	$arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "GLOBAL_ACTIVE" => "Y");
//	$res = CIBlockSection::GetList(Array("sort" => "asc"), $arFilter);
//	if($arFields = $res->GetNext()) {

    //header("Location: /catalog/watches/");
    die();

//	}
}

// тут был фильтр

?>

<?
$filterCurrentTemplate = '';
$curPage = $APPLICATION->GetCurPage();
//watch
if (is_numeric(stripos($curPage, '/watches/')) || $_GET['PARENT_SECTION'] == 172) {
    $filterCurrentTemplate = 1;
}

//straps
if (is_numeric(stripos($curPage, '/straps/')) || $_GET['PARENT_SECTION'] == 146) {
    $filterCurrentTemplate = 2;
}

//sunglasses
if (is_numeric(stripos($curPage, '/sunglasses/')) || $_GET['PARENT_SECTION'] == 165) {
    $filterCurrentTemplate = 3;
}

//accessories
if (is_numeric(stripos($curPage, '/accessories/')) || $_GET['PARENT_SECTION'] == 171) {
    $filterCurrentTemplate = 4;
}

?>


<?

if (!empty($_GET["q"])) {
    $searchFilter = array(
        array(
            "LOGIC" => "OR",
            "%NAME" => $_GET["q"],
            "%PROPERTY_BRAND_MODEL_VALUE" => $_GET["q"]
        ),
        'IBLOCK_ID' => 5,
        'ACTIVE' => 'Y'
    );
    $searchRes = CIBlockElement::GetList(array(), $searchFilter, array(), false, array('ID'));
    $filterCurrentTemplate = 1;
}
?>

<? if (!empty($_GET['q']) && empty($searchRes)) { ?>
    <section class="brend sections">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="search-no-result">Ничего не найдено, попробуйте изменить параметры поиска</h4>
                </div>
            </div>
        </div>
    </section>
<? } else { ?>
    <?
    $viewActiveThree = 'view_three_items_active';
    $viewActiveFour = '';
    $arCur = array_filter(explode("/", $curPage), function($value) { return $value !== ''; });
    $sectCode = end($arCur);
    $rsSect = CIBlockSection::GetList(array(), array("IBLOCK_ID" => 5, "CODE" => $sectCode), false, array('ID', 'DEPTH_LEVEL'));
    if ($arSect = $rsSect->GetNext()) {
        if ($arSect['DEPTH_LEVEL'] == 1) {
            $viewActiveThree = '';
            $viewActiveFour = 'view_three_items_active';
        } else {
            $viewActiveThree = 'view_three_items_active';
            $viewActiveFour = '';
        }
    }

    ?>

    <?
//	var_dump($_SERVER);
	$path = parse_url($_SERVER["REQUEST_URI"]);
	$code = basename($path["path"]);
	
    $data = array();
    $arFilter = Array('IBLOCK_ID' => 5, 'GLOBAL_ACTIVE' => 'Y', 'CODE' => $code);
    $db_list = CIBlockSection::GetList(Array(), $arFilter, false, ["nTopCount" => 1]);
    while ($ar_result = $db_list->GetNext()) {
        $data = $ar_result;
    }
    if ($data['PICTURE']) $picture = CFile::GetPath($data['PICTURE']);

    ?>

	
    <section class="brend sections">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 id="section_id" data-id="<?=$data["ID"]?>"><?=$data["NAME"]?></h1>
                    <p class="brend__desc"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="brend__panel clearfix">
                        <div class="filter btn btn_trans_grey">
							<span class="filter__icon">
								<i class="icon-icon-filter"></i>
							</span>
                            <p class="filter__text">Фильтр</p>
                        </div>
                        <div class="view">
                            <p class="view__text">Вид</p>
                            <div class="view_three_items <?= $viewActiveThree?>">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="view_four_items <?= $viewActiveFour?>">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                        <div class="sort-by">
                            <p class="sort-by__text hidden-xs">Сортировать</p>
                            <div class="select__wrap hidden-xs">
                                <select>
                                    <option value="high_price" data-sort="high_price">по возрастанию цены</option>
                                    <option value="low_price" data-sort="low_price">по убыванию цены</option>
                                    <option value="new" data-sort="new">по новизне</option>
                                    <option value="popular" data-sort="popular">по популярности</option>
                                </select>
                            </div>
                            <div class="select__wrap hidden-lg hidden-md hidden-sm">
                                <select>
                                    <option value="hide">Сортировать</option>
                                    <option value="high_price" data-sort="high_price">По возрастанию цены</option>
                                    <option value="low_price" data-sort="low_price">По убыванию цены</option>
                                    <option value="new" data-sort="new">По новизне</option>
                                    <option value="popular" data-sort="popular">По популярности</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?


        $APPLICATION->IncludeComponent(
            "bitrix:catalog.filter",
            "new_filter",
            array(
                "IBLOCK_TYPE" => "TicTacToy",
                "IBLOCK_ID" => "5",
                "FILTER_NAME" => "arrFilter",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "SEX",
                    1 => "PROP_MATERIAL",
                    2 => "FILTER_MECHANISM",
                    3 => "FILTER_COLOR",
                    4 => "FILTER_COUNTRY",
                    5 => "FILTER_STYLE",
                    6 => "MATERIAL_STRAP",
                    7 => "FILTER_HRON",
                    8 => "WATCH_GLASS",
                    9 => "TYPE_STRAP",
                    10 => "PROP_LINSES_COLOR",
                    11 => "PROP_COLOR_OF_FRAME",
                    12 => "WIDTH_OF_STRAP_ATTACHMENT",
                    13 => "STRAP_WIDTH",
                    14 => "LINSES_COLOR",
                    15 => "HRON",
                    16 => "TYPE_OF_ACCESSORY",
                    17 => "SIZE_OF_RING",
                    18 => "MATERIAL_OF_ACCESSORY",
                    19 => "ACCESSORY_INSERTION",
                    20 => "STRAP_COLOR",
                    21 => "FRAME_MATERIAL",
                    22 => "ACCESSORY_COLOR",
                    23 => "",
                ),
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000",
                "CACHE_GROUPS" => "Y",
                "LIST_HEIGHT" => "5",
                "TEXT_WIDTH" => "20",
                "NUMBER_WIDTH" => "5",
                "SAVE_IN_SESSION" => "N",
                "COMPONENT_TEMPLATE" => "new_filter",
                "PAGER_PARAMS_NAME" => "arrPager",
                "FILTER_CURRENT_TEMPLATE" => $filterCurrentTemplate
            ),
            false
        );
        ?>

        <?


        // Для фильтра показываем только товары в наличии
        if (isset($_GET["arrFilter_cf"]["1"]["LEFT"]))
            $arrFilter[">CATALOG_QUANTITY"] = 0;

        // Если указано м+ж то убираем фильтрацию по этим полям
        if (count($arrFilter["PROPERTY"]["SEX"]) >= 2)
            unset($arrFilter["PROPERTY"]["SEX"]);

        // Если это поиск по названию то показываем соответствующие товары
        if (!empty($_GET["q"])) {
//	$arrFilter = array();
//	$arrFilter["%NAME"] = $_GET["q"];
//	$arrFilter["%PROPERTY_BRAND_MODEL_VALUE"] = $_GET["q"];

            $arrFilter = array(
                array(
                    "LOGIC" => "OR",
                    "%NAME" => $_GET["q"],
                    "%PROPERTY_BRAND_MODEL_VALUE" => $_GET["q"]
                )
            );
        }

        if (!empty($_REQUEST['arrFilter_pf']['FILTER_BRAND'])) {
            $arrFilter[] = array('SECTION_ID' => $_REQUEST['arrFilter_pf']['FILTER_BRAND']);
        } else if (!empty($_GET['PARENT_SECTION'])) {
            $arrFilter['SECTION_ID'] = $_REQUEST['PARENT_SECTION'];
            $arrFilter['INCLUDE_SUBSECTIONS'] = 'Y';
        }

        if (!empty($_REQUEST['arrFilter_pf']['PROP_MATERIAL'])) {
            $arrFilter['?PROPERTY_PROP_MATERIAL'] = array_values($_REQUEST['arrFilter_pf']['PROP_MATERIAL']);
        }

        if (!empty($_REQUEST['arrFilter_pf']['MATERIAL_OF_ACCESSORY'])) {
            $arrFilter['PROPERTY_MATERIAL_OF_ACCESSORY'] = array_values($_REQUEST['arrFilter_pf']['MATERIAL_OF_ACCESSORY']);
        }
        if (!empty($_REQUEST['arrFilter_pf']['SIZE_OF_RING'])) {
            $arrFilter['PROPERTY_SIZE_OF_RING'] = array_values($_REQUEST['arrFilter_pf']['SIZE_OF_RING']);
        }
        if (!empty($_REQUEST['arrFilter_pf']['FRAME_MATERIAL'])) {
            $arrFilter['?PROPERTY_FRAME_MATERIAL'] = array_values($_REQUEST['arrFilter_pf']['FRAME_MATERIAL']);
        }
        if (!empty($_REQUEST['arrFilter_pf']['WATCH_GLASS'])) {
            $arrFilter['PROPERTY_WATCH_GLASS'] = array_values($_REQUEST['arrFilter_pf']['WATCH_GLASS']);
        }
        if (!empty($_REQUEST['arrFilter_pf']['PROP_COLOR_OF_FRAME'])) {
            $arrFilter['PROPERTY_PROP_COLOR_OF_FRAME'] = array_values($_REQUEST['arrFilter_pf']['PROP_COLOR_OF_FRAME']);
        }
        if (!empty($_REQUEST['arrFilter_pf']['STRAP_WIDTH'])) {
            $arrFilter['PROPERTY_STRAP_WIDTH'] = array_values($_REQUEST['arrFilter_pf']['STRAP_WIDTH']);
        }
        if (!empty($_REQUEST['arrFilter_pf']['ACCESSORY_INSERTION_VALUE'])) {
            $arrFilter['PROPERTY_ACCESSORY_INSERTION_VALUE'] = $_REQUEST['arrFilter_pf']['ACCESSORY_INSERTION_VALUE'];
        }
        if (!empty($_REQUEST['arrFilter_pf']['TYPE_STRAP'])) {
            $arrFilter['PROPERTY_TYPE_STRAP'] = array_values($_REQUEST['arrFilter_pf']['TYPE_STRAP']);
        }

        $GLOBALS['arrFilter'] = $arrFilter;


        $sort_by = 'sort';
        $promo_input = '';
        if (is_numeric(stripos($APPLICATION->GetCurPage(), '/promo/'))) {
            $sort_by = 'property_SORT';
            $promo_input = 'PROMO';
        }
        $sort_order = 'asc';

        if (!empty($_GET['sort_by'])) {
            if ($_GET['sort_by'] == 'high_price') {
                $sort_by = 'catalog_PRICE_1';
                $sort_order = 'asc';
            }
            if ($_GET['sort_by'] == 'low_price') {
                $sort_by = 'catalog_PRICE_1';
                $sort_order = 'desc';
            }
            if ($_GET['sort_by'] == 'new') {
                $sort_by = 'PROPERTY_NEW';
                $sort_order = 'desc';
            }
            if ($_GET['sort_by'] == 'popular') {
                $sort_by = 'show_counter';
                $sort_order = 'desc';
            }
        }

       /* $uf_arresult = CIBlockSection::GetList(
                Array("SORT"=>"ASC"),
                Array("IBLOCK_ID" => 5, "CODE"=>$_REQUEST["SECTION_CODE"]),
                false,
                array("ID","IBLOCK_ID","NAME","DEPTH_LEVEL"),
                false
        );

        if($sectCurPage = $uf_arresult->GetNext()){
            if($sectCurPage["DEPTH_LEVEL"] != "1" ){

            }
        }*/

        ?>
        <? $sectId = $APPLICATION->IncludeComponent("bitrix:catalog.section", "catalog_brand", array(
            'PROMO_INPUT'=> $promo_input,
            "IBLOCK_TYPE" => "TicTacToy",
            "IBLOCK_ID" => "5",
            "SECTION_ID" => "",
            "SECTION_CODE" => $_REQUEST["SECTION_CODE"],
            "SECTION_USER_FIELDS" => array(
                0 => "UF_RETARGETING",
                1 => "",
            ),
            "ELEMENT_SORT_FIELD" => $sort_by,
            "ELEMENT_SORT_ORDER" => $sort_order,
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER2" => "desc",
            "FILTER_NAME" => "arrFilter",
            "INCLUDE_SUBSECTIONS" => "Y",
            "SHOW_ALL_WO_SECTION" => "Y",
            "HIDE_NOT_AVAILABLE" => "N",
            "PAGE_ELEMENT_COUNT" => "999",
            "LINE_ELEMENT_COUNT" => "3",
            "PROPERTY_CODE" => array(
                0 => "NEW",
                1 => "SEX",
                2 => "WAIT",
                3 => "",
            ),
            "OFFERS_LIMIT" => "5",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "BASKET_URL" => "/cart/",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "Y",
            "CACHE_TIME" => "3600",
            "CACHE_GROUPS" => "Y",
            "META_KEYWORDS" => "-",
            "META_DESCRIPTION" => "-",
            "BROWSER_TITLE" => "-",
            "ADD_SECTIONS_CHAIN" => "Y",
            "DISPLAY_COMPARE" => "N",
            "SET_TITLE" => "Y",
            "SET_STATUS_404" => "Y",
            "CACHE_FILTER" => "Y",
            "PRICE_CODE" => array(
                0 => "BASE",
            ),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_PROPERTIES" => "",
            "USE_PRODUCT_QUANTITY" => "N",
            "CONVERT_CURRENCY" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Товары",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "USE_MAIN_ELEMENT_SECTION"=>"Y"
        ),
            false
        ); ?>
    </section>
    <!-- ABOUT BREND (О Бренде) -->
    <?
	if (empty($data)) {
		$data = array();
		$arFilter = Array('IBLOCK_ID' => 5, 'GLOBAL_ACTIVE' => 'Y', 'ID' => $sectId);
		$db_list = CIBlockSection::GetList(Array(), $arFilter, false);
		while ($ar_result = $db_list->GetNext()) {
			$data = $ar_result;
		}
		$picture = CFile::GetPath($data['PICTURE']);
	}
    ?>
    <section class="about-brend">
        <div class="container">
            <div class="about-brend__content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="about-brend__logo">
                            <a href="javascript:void(0)">
                                <img src="<?= $picture ?>" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <?= $data['DESCRIPTION'] ?>
                    </div>
                </div>
            </div>
            <div class="about-brend__buy">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <p class="brend-buy__text brend-buy__head clearfix">Почему купить дизайнерские
                            часы <?= $data['NAME'] ?> будет отличным решением? <span><i class="icon-icon-big-arrow"></i></span>
                        </p>
                        <div class="brend-buy__text_hidden">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                ".default",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/catalog/incs/" . $sectId . ".php"
                                ),
                                false
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ABOUT BREND (О Бренде) -->
<? } ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>