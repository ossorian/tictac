<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Предзаказ");
?>

<?
$sort_by = 'sort';
$sort_order = 'asc';

if (!empty($_GET['sort_by'])){
	if ($_GET['sort_by'] == 'high_price'){
		$sort_by = 'catalog_PRICE_1';
		$sort_order = 'asc';
	}
	if ($_GET['sort_by'] == 'low_price'){
		$sort_by = 'catalog_PRICE_1';
		$sort_order = 'desc';
	}
	if ($_GET['sort_by'] == 'new'){
		$sort_by = 'PROPERTY_NEW';
		$sort_order = 'desc';
	}
	if ($_GET['sort_by'] == 'popular'){
		$sort_by = 'show_counter';
		$sort_order = 'desc';
	}
}?>
<?$GLOBALS['arrFilterNew'] = array("ACTIVE"=>"Y", "SECTION_ACTIVE" => "Y", "PROPERTY_ON_PREORDER_VALUE" => "Y" /*"!PROPERTY_WAIT"=>false*/)?>
<?$sectId = $APPLICATION->IncludeComponent("bitrix:catalog.section", "catalog_products", array(
	"IBLOCK_TYPE" => "TicTacToy",
	"IBLOCK_ID" => "5",
	"SECTION_ID" => "",
	"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
	"SECTION_USER_FIELDS" => array(
		0 => "UF_RETARGETING",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => $sort_by,
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER" => $sort_order,
	"ELEMENT_SORT_ORDER2" => "desc",
	"FILTER_NAME" => "arrFilterNew",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
	"HIDE_NOT_AVAILABLE" => "N",
	"PAGE_ELEMENT_COUNT" => "999",
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "NEW",
		1 => "SEX",
		2 => "WAIT",
		3 => "",
	),
	"OFFERS_LIMIT" => "5",
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/cart/",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "N",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "Y",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "Y",
	"CACHE_FILTER" => "Y",
	"PRICE_CODE" => array(
		0 => "BASE",
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "Y",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>