<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Каталог часов");
?><? $element = $APPLICATION->IncludeComponent(
    "bitrix:catalog.element",
    "element_detail",
    Array(
        "ACTION_VARIABLE" => "action",
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "BACKGROUND_IMAGE" => "-",
        "BASKET_URL" => "/cart/",
        "BROWSER_TITLE" => "-",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000",
        "CACHE_TYPE" => "A",
        "CHECK_SECTION_ID_VARIABLE" => "N",
        "COMPONENT_TEMPLATE" => "catalog",
        "CONVERT_CURRENCY" => "N",
        "DETAIL_URL" => "",
        "DISPLAY_COMPARE" => "N",
        "ELEMENT_CODE" => $_REQUEST["CODE"],
        "ELEMENT_ID" => "",
        "HIDE_NOT_AVAILABLE" => "N",
        "IBLOCK_ID" => "5",
        "IBLOCK_TYPE" => "TicTacToy",
        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
        "LINK_IBLOCK_ID" => "",
        "LINK_IBLOCK_TYPE" => "",
        "LINK_PROPERTY_SID" => "",
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "OFFERS_LIMIT" => "0",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRICE_CODE" => array("BASE"),
        "PRICE_VAT_INCLUDE" => "Y",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPERTIES" => array(),
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PROPERTY_CODE" => array("NEW", "SEX", "WAIT", "RECOMMENDED", "PROP_SIZE", "PROP_WRIST", "PROP_WEIGHT", "PROP_COUNTRY", "PROP_MATERIAL", "PROP_MECHANISM", "PROP_GLASS", "PROP_WATER", "PROP_WARRANTY", "RETARGETING", "GALLERY", "FRAME_MATERIAL", "LENSES", "UF_ABSORB", "WIDTH_BETWEEN_EARINGS", "WIDTH_OF_STRAP_ATTACHMENT"),
        "SECTION_CODE" => $_REQUEST["SECTION_CODE"],
        "SECTION_ID" => "",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "SECTION_URL" => "",
        "SEF_MODE" => "N",
        "SET_BROWSER_TITLE" => "Y",
        "SET_CANONICAL_URL" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "Y",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SHOW_DEACTIVATED" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "USE_ELEMENT_COUNTER" => "Y",
        "USE_MAIN_ELEMENT_SECTION" => "N",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "N"
    )
); ?>


<? if (!empty($element) && !empty($_SESSION["FOR_GOOGLE_USER_FROM"])) {
    $res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 5, "ID" => $element, "CATALOG_PRICE_1" > 0), false, false, array("*"));
    if ($obElement = $res->GetNextElement()) {

        $arFields = $obElement->GetFields();
        $arProps = $obElement->GetProperties();

        $arPrice = CCatalogProduct::GetOptimalPrice($arFields['ID']);
//print_r($arPrice);

        $cat = '';

        $nav = CIBlockSection::GetNavChain(false, $arFields['IBLOCK_SECTION_ID']);
        if ($arFirst = $nav->GetNext()) {
            if ($arFirst['ID'] == 172) {
                $cat = 'часы';
            } else if ($arFirst['ID'] == 146) {
                $cat = "ремешки";
            } else if ($arFirst['ID'] == 171) {
                $cat = "украшения";
            } else if ($arFirst['ID'] == 165) {
                $cat = "очки";
            }
        }

        ?>
        <script>
            dataLayer.push({
                'event': 'productDetail',
                'ecommerce': {
                    'detail': {
                        'actionField': {'list': '<?= $_SESSION["FOR_GOOGLE_USER_FROM"]?>'},    // Список в котором находится товар.
                        'products': [{
                            'name': '<?= strip_tags($arFields['NAME'])?>',
                            'id': '<?= $arFields['ID']?>',
                            'price': '<?= $arPrice['RESULT_PRICE']['DISCOUNT_PRICE']?>',
                            'brand': '<?= $arProps['BRAND_MODEL']['VALUE_ENUM']?>',
                            'category': '<?= $cat?>',
                            'variant': '<?= implode(',', $arProps['FILTER_COLOR']['VALUE_ENUM'])?>'
                        }]
                    }
                }
            });
        </script>
    <?
    $_SESSION["FOR_GOOGLE_USER_FROM"] = "";
    }
} ?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
