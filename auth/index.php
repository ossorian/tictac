<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Вход");
?>
<? global $USER; ?>
<? if (!$USER->IsAuthorized()) { ?>
    <? if ($_GET['change_password']) { ?>
        <? $APPLICATION->SetTitle("Востановление пароля"); ?>
        <div class="container b_about_company">
            <div class="row">
                <div class="col-xs-12">
                    <h3><?= $APPLICATION->GetTitle() ?></h3>
                </div>
            </div>
            <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "change_pass", Array(
                "FORGOT_PASSWORD_URL" => "",    // Страница забытого пароля
                "PROFILE_URL" => "",    // Страница профиля
                "REGISTER_URL" => "",    // Страница регистрации
                "SHOW_ERRORS" => "Y",    // Показывать ошибки
            ),
                false
            ); ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:system.auth.changepasswd",
                ".default",
                Array()
            ); ?>
        </div>
    <? } else { ?>
        <main class="content mobile__auth">
            <!-- PASTE THIS-->
            <br><br><br>
            <section class="auth__login">

                <div class="container">
                    <h2 class="auth__title">Войти</h2>
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:system.auth.form",
                        "main_auth_mob",
                        Array(
                            "COMPONENT_TEMPLATE" => "main_auth_mob",
                            "FORGOT_PASSWORD_URL" => "",
                            "PROFILE_URL" => "",
                            "REGISTER_URL" => "",
                            "SHOW_ERRORS" => "Y"
                        )
                    );
                    ?>
                </div>

            </section>

            <section class="forgot-pass__content_mob hidden">
                <br><br><br>
                <div class="container">
                    <h2 class="auth__title">Восстановление пароля</h2>
                <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "change_pass", Array(
                    "FORGOT_PASSWORD_URL" => "",    // Страница забытого пароля
                    "PROFILE_URL" => "",    // Страница профиля
                    "REGISTER_URL" => "",    // Страница регистрации
                    "SHOW_ERRORS" => "Y",    // Показывать ошибки
                ),
                    false
                ); ?>
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:system.auth.forgotpasswd",
                    "forgot_mob",
                    Array(
                        "AJAX_MODE" => "Y",
                        "AJAX_OPTION_SHADOW" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N"
                    )
                ); ?>
                    </div>
            </section>

            <section class="auth__register">
                <div class="container">
                    <h2 class="auth__title">Регистрация</h2>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.register",
                        "main_register_mob",
                        array(
                            "AUTH" => "Y",
                            "REQUIRED_FIELDS" => array(
                                0 => "EMAIL",
                            ),
                            "SET_TITLE" => "N",
                            "SHOW_FIELDS" => array(
                                0 => "EMAIL",
                            ),
                            "SUCCESS_PAGE" => "",
                            "USER_PROPERTY" => array(
                            ),
                            "USER_PROPERTY_NAME" => "",
                            "USE_BACKURL" => "Y",
//                            "AJAX_MODE" => "Y",
//                            "AJAX_OPTION_SHADOW" => "N",
//                            "AJAX_OPTION_JUMP" => "N",
//                            "AJAX_OPTION_STYLE" => "Y",
//                            "AJAX_OPTION_HISTORY" => "N",
                            "COMPONENT_TEMPLATE" => "main_register_mob"
                        ),
                        false
                    ); ?>
                </div>
            </section><!-- PASTE THIS-->
        </main>

    <? } ?>
<? } else {
    LocalRedirect("/");
} ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>