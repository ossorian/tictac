<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");
?>
    <section class="fav-tovars sections">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Избранные товары</h2>
                </div>
            </div>
            <div class="fav-tovars__body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="fav-tovars__content">
                            <div class="fav-tovars-content__item">

                                <? $GLOBALS['arrFilterFavorites'] = array("ID" => $_SESSION["CATALOG_COMPARE_LIST"][5]["ITEMS"]); ?>
                                <?$APPLICATION->IncludeComponent("bitrix:catalog.section", "favorites", Array(
                                    "AJAX_MODE" => "N",	// Включить режим AJAX
                                    "IBLOCK_TYPE" => "TicTacToy",	// Тип инфо-блока
                                    "IBLOCK_ID" => "5",	// Инфо-блок
                                    "SECTION_ID" => "",	// ID раздела
                                    "SECTION_CODE" => "",	// Код раздела
                                    "SECTION_USER_FIELDS" => "",	// Свойства раздела
                                    "ELEMENT_SORT_FIELD" => "PROPERTY_INDEX",	// По какому полю сортируем элементы
                                    "ELEMENT_SORT_ORDER" => "ASC",	// Порядок сортировки элементов
                                    "FILTER_NAME" => "arrFilterFavorites",	// Имя массива со значениями фильтра для фильтрации элементов
                                    "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                                    "SHOW_ALL_WO_SECTION" => empty($_SESSION["CATALOG_COMPARE_LIST"][5]["ITEMS"]) ? "N" : "Y",	// Показывать все элементы, если не указан раздел
                                    "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
                                    "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
                                    "BASKET_URL" => "/basket/",	// URL, ведущий на страницу с корзиной покупателя
                                    "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
                                    "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
                                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
                                    "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
                                    "SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
                                    "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
                                    "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
                                    "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
                                    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                                    "DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
                                    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                                    "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                                    "PAGE_ELEMENT_COUNT" => "99",	// Количество элементов на странице
                                    "LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
                                    "PROPERTY_CODE" => array(	// Свойства
                                        0 => "NEW",
                                        1 => "IMAGE",
                                    ),
                                    "PRICE_CODE" => array(	// Тип цены
                                        0 => "BASE",
                                    ),
                                    "USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
                                    "SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
                                    "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
                                    "PRODUCT_PROPERTIES" => "",	// Характеристики товара
                                    "USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
                                    "CACHE_TYPE" => "A",	// Тип кеширования
                                    "CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                    "CACHE_FILTER" => "N",	// Кэшировать при установленном фильтре
                                    "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                                    "PAGER_TITLE" => "Товары",	// Название категорий
                                    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                                    "PAGER_TEMPLATE" => "",	// Название шаблона
                                    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",	// Время кеширования страниц для обратной навигации
                                    "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                                    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                                    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                ),
                                    false
                                );?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>