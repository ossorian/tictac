<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("TicTacToy.ru");
?><?$APPLICATION->IncludeComponent("bitrix:sale.order.ajax", "main_order", Array(
	    "PAY_FROM_ACCOUNT" => "N",	// Позволять оплачивать с внутреннего счета
		"COUNT_DELIVERY_TAX" => "N",	// Рассчитывать налог для доставки
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",	// Позволять оплачивать с внутреннего счета только в полном объеме
		"ALLOW_AUTO_REGISTER" => "Y",	// Оформлять заказ с автоматической регистрацией пользователя
		"SEND_NEW_USER_NOTIFY" => "N",	// Отправлять пользователю письмо, что он зарегистрирован на сайте
		"DELIVERY_NO_AJAX" => "Y",	// Рассчитывать стоимость доставки сразу
		"PROP_1" => "",	// Не показывать свойства для типа плательщика "Физическое лицо" (s1)
		"PROP_2" => "",	// Не показывать свойства для типа плательщика "Юридическое лицо" (s1)
		"PATH_TO_BASKET" => "/cart/",	// Страница корзины
		"PATH_TO_PERSONAL" => "",	// Страница персонального раздела
		"PATH_TO_PAYMENT" => "/order/payment/",	// Страница подключения платежной системы
		"PATH_TO_AUTH" => "",	// Страница авторизации
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
	),
	false
);?><?
// Для метрики
if(!empty($_GET["ORDER_ID"])) {
	// Смотрим весь заказ
	if ($arOrder = CSaleOrder::GetByID($_GET["ORDER_ID"])) {

		    $arBasketItems = array();

		    $dbBasketItems = CSaleBasket::GetList(
				array(),
				array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"LID" => SITE_ID,
					"ORDER_ID" => $arOrder["ID"]
				),
				false,
				false,
				array("*")
			);

			$arItems = array();
		    while ($arr = $dbBasketItems->Fetch()) {
				$arItems[] = $arr;
			}

			$yandexMetrikaOrder = true;
?>
<script type="text/javascript">
var yaParams = {
  order_id: "<?=$arOrder["ID"]?>",
  order_price: <?=$arOrder["PRICE"]?>,
  currency: "<?=$arOrder["CURRENCY"]?>",
  exchange_rate: 1,
  goods:
     [
		<?foreach($arItems as $k => $item):?>
        {
          id: "<?=$item["PRODUCT_ID"]?>",
          name: "<?=str_replace("\"", "\\\"", $item["NAME"])?>",
          price: <?=$item["PRICE"]?>,
          quantity: <?=intval($item["QUANTITY"])?>
        }<?if(is_array($arItems[$k+1])):?>,<?endif;?>

        <?endforeach;?>
      ]
};
</script>
<?
	}
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
