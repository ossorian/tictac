<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

if (!empty($_POST['id'])) {
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.element",
        "quick_show",
        Array(
            "ACTION_VARIABLE" => "action",
            "ADD_DETAIL_TO_SLIDER" => "N",
            "ADD_ELEMENT_CHAIN" => "N",
            "ADD_PICT_PROP" => "-",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "ADD_SECTIONS_CHAIN" => "Y",
            "ADD_TO_BASKET_ACTION" => array("BUY"),
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/personal/basket.php",
            "BROWSER_TITLE" => "-",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "N",
            "CHECK_SECTION_ID_VARIABLE" => "N",
            "COMPONENT_TEMPLATE" => ".default",
            "CONVERT_CURRENCY" => "N",
            "DETAIL_PICTURE_MODE" => "IMG",
            "DETAIL_URL" => "",
            "DISPLAY_COMPARE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PREVIEW_TEXT_MODE" => "E",
            "ELEMENT_CODE" => "",
            "ELEMENT_ID" => (int)$_POST['id'],
            "HIDE_NOT_AVAILABLE" => "N",
            "IBLOCK_ID" => "5",
            "IBLOCK_TYPE" => "TicTacToy",
            "LABEL_PROP" => "-",
            "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
            "LINK_IBLOCK_ID" => "",
            "LINK_IBLOCK_TYPE" => "",
            "LINK_PROPERTY_SID" => "",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_COMPARE" => "Сравнить",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_LIMIT" => "0",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array("BASE"),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRICE_VAT_SHOW_VALUE" => "N",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPERTIES" => array("BRAND_MODEL", "NEW", "SEX", "RECOMMENDED", "FILTER_MECHANISM", "FILTER_COLOR", "FILTER_COUNTRY", "FILTER_STYLE", "SHOW_IN_MENU", "FILTER_HRON", "PRODUCT_DAY"),
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "",
            "PRODUCT_SUBSCRIPTION" => "N",
            "PROPERTY_CODE" => array("", "BRAND_MODEL", "SORT", "NEW", "SEX", "WAIT", "RECOMMENDED", "INDEX", "BRAND", "PROP_SIZE", "PROP_WRIST", "PROP_WEIGHT", "PROP_COUNTRY", "PROP_MATERIAL", "PROP_MECHANISM", "PROP_GLASS", "PROP_WATER", "PROP_WARRANTY", "FILTER_MECHANISM", "FILTER_COLOR", "FILTER_COUNTRY", "FILTER_STYLE", "RETARGETING", "SEO_TEXT", "UF_ABSORB", "LENSES", "FRAME_MATERIAL", "SHOW_IN_MENU", "FILTER_HRON", "PRODUCT_DAY", "WIDTH_OF_STRAP_ATTACHMENT", "WIDTH_BETWEEN_EARINGS", ""),
            "SECTION_CODE" => "",
            "SECTION_ID" => $_REQUEST["SECTION_ID"],
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "SECTION_URL" => "",
            "SEF_MODE" => "N",
            "SET_BROWSER_TITLE" => "Y",
            "SET_CANONICAL_URL" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "Y",
            "SET_META_KEYWORDS" => "Y",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "Y",
            "SHOW_404" => "N",
            "SHOW_CLOSE_POPUP" => "N",
            "SHOW_DEACTIVATED" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "TEMPLATE_THEME" => "blue",
            "USE_COMMENTS" => "N",
            "USE_ELEMENT_COUNTER" => "Y",
            "USE_MAIN_ELEMENT_SECTION" => "N",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "USE_VOTE_RATING" => "N"
        )
    );
}
?>
    <script>

    </script>
<?

// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>