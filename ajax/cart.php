<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

// Удаление из корзины
if(!empty($_POST["deleteProductId"]))
	CSaleBasket::Delete($_POST["deleteProductId"]);

// Добавление в корзину
if(!empty($_POST["addProductId"])) {

    $PROPS = array();
    if (!empty($_POST['color'])){
        $PROPS['COLOR'] = array(
            "NAME" => 'Цвет',
            "VALUE" => $_POST['color'],
            "CODE" => 'COLOR'
        );
    }

    if (!empty($_POST['size'])){
        $PROPS['SIZE'] = array(
            "NAME" => 'Размер',
            "VALUE" => $_POST['size'],
            "CODE" => 'SIZE'
        );
    }

	Add2BasketByProductID($_POST["addProductId"], 1, array(), $PROPS);
	// Удаляем товар из wishlist
	$dbBasketItems = CSaleBasket::GetList(array("NAME" => "ASC", "ID" => "ASC"), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL", "PRODUCT_ID" => $_POST["addProductId"], "DELAY" => "Y"));
	while ($arItems = $dbBasketItems->Fetch()) {
		CSaleBasket::Delete($arItems["ID"]);
	}
}

$is_tictactoy = stripos($_SERVER['SERVER_NAME'],"tictactoy");
$is_tictac  = (SITE_ID == 's1' || is_numeric($is_tictactoy));

if ($is_tictac) {
    $APPLICATION->IncludeComponent("bitrix:sale.basket.basket", $_POST['template'], array(
        "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
        "COLUMNS_LIST" => array(
            0 => "NAME",
            1 => "PRICE",
            2 => "TYPE",
            3 => "QUANTITY",
            4 => "DELETE",
            5 => "DISCOUNT",
        ),
        "PATH_TO_ORDER" => "/cart/",
        "HIDE_COUPON" => "N",
        "QUANTITY_FLOAT" => "N",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "SET_TITLE" => "N"
    ),
        false
    );

}
// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>