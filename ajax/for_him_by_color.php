<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
?>
    <div class="row">
        <div class="col-xs-12">
            <h2>Мужские часы по цвету</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="tabs__slider">
                <?
                $index = 0;
                $res = CIBlockProperty::GetPropertyEnum(51);
                while ($propColor = $res->GetNext()) {
                    if ((int)$propColor['ID'] !== 113 && (int)$propColor['ID'] !== 114 && (int)$propColor['ID'] !== 120) {
                        $index++;
                        ?>
                        <div class="tab__item<?= $index == 1 ? ' active-item' : '' ?>"
                             data-thumb="<?= $propColor['VALUE'] ?>">
                            <div class="tab__slider">
                                <?
                                $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND_MODEL");
                                $arFilter = array(
                                    "IBLOCK_ID" => 5,
                                    "ACTIVE"=>"Y",
                                    "PROPERTY_FILTER_COLOR" => $propColor['ID'],
                                    "PROPERTY_SEX" => 6,
                                    "!SECTION_ID"=>array(146, 165, 152),
                                    "SECTION_ACTIVE"=>"Y"
                                );
                                $resElem = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 16), $arSelect);
                                while ($arElem = $resElem->GetNext()) { ?>
                                    <? $picture = CFile::GetPath($arElem['PREVIEW_PICTURE']); ?>
                                    <div class="slide">
                                        <div class="item">
                                            <img src="<?= $picture?>" alt="">
                                            <p class="item__name"><?= $arElem['PROPERTY_BRAND_MODEL_VALUE']?></p>
                                            <p class="item__name"><?= $arElem['NAME'] ?></p>
                                        </div>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                        <?
                    }
                } ?>
            </div>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>