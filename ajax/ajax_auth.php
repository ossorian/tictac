<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    $APPLICATION->IncludeComponent(
        "bitrix:system.auth.form",
        "change_pass",
        Array(
            "COMPONENT_TEMPLATE" => "main_auth",
            "FORGOT_PASSWORD_URL" => "",
            "PROFILE_URL" => "",
            "REGISTER_URL" => "",
            "SHOW_ERRORS" => "Y"
        )
    );
}