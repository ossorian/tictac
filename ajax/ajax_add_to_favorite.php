<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    if (!$_REQUEST['id']) return;

    if ($_REQUEST['action'] == 'ADD_TO_COMPARE_LIST') {
        $_SESSION["CATALOG_COMPARE_LIST"][5]["ITEMS"][$_REQUEST['id']] = $_REQUEST['id'];

    }
    if ($_REQUEST['action'] == 'DELETE_FROM_COMPARE_LIST') {
        unset($_SESSION["CATALOG_COMPARE_LIST"][5]["ITEMS"][$_REQUEST['id']]);

    }

}