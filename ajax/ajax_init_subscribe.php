<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    if (!empty($_REQUEST['DISABLE_SUBSCRIBE']) && $_REQUEST['DISABLE_SUBSCRIBE'] === "Y") {
        $_SESSION['DISABLE_SUBSCRIBE'] = 'Y';
        exit();
    }

    if (empty($_SESSION['DISABLE_SUBSCRIBE'])) {

        CModule::IncludeModule("sale");
        CModule::IncludeModule("iblock");

        $APPLICATION->IncludeComponent("phpdev:subscribe", "catalog_subscribe", Array(
            "IBLOCK_ID" => "18",    // Инфо-блок
            "CACHE_TYPE" => "A",    // Тип кеширования
            "CACHE_TIME" => "3600",    // Время кеширования (сек.)
            "CACHE_GROUPS" => "Y",    // Учитывать права доступа
        ),
            false
        );
    }
}