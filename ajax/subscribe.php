<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("subscribe");

if (!check_email($_POST["EMAIL"]) and $_POST["mode"] == "add") {
	echo "Wrong e-mail";
	exit();
}

if ($_POST["mode"] == "add") {
	$arFields = Array(
		"USER_ID"   => ($USER->IsAuthorized() ? $USER->GetID() : false),
		"FORMAT"    => "text",
		"EMAIL"     => $_POST["EMAIL"],
		"ACTIVE"    => "Y",
		"RUB_ID"    => array(2),
		"CONFIRMED" => 'Y'
	);
	$subscr = new CSubscription;
	$ID = $subscr->Add($arFields);
	if ($ID > 0) {
		CSubscription::Authorize($ID);
		echo "You have been successfully subscribed to the newsletter.";
	}
	else {
		echo "Error adding subscription: " . str_replace("<br>", "", $subscr->LAST_ERROR);
	}
}
elseif ($_POST["mode"] == "delete") {
	$aSubscr = CSubscription::GetUserSubscription();
	if ($aSubscr["ID"] > 0) {
		CSubscription::Delete($aSubscr["ID"]);
		echo "Subscription deleted.";
	}
	else {
		echo "Error deleting subscription.";
	}
}

// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>