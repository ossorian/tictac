<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// Подключение инфоблоков
CModule::IncludeModule("iblock");

// Проверяем правильность введенных данных
if (empty($_POST["name"])) {
	echo LANGUAGE_ID == "ru" ? "Необходимо указать имя" : "Wrong name";
	die();
}
if (empty($_POST["email"])) {
	echo LANGUAGE_ID == "ru" ? "Неверно указан e-mail" : "Wrong e-mail";
	die();
}
if (empty($_POST["text"])) {
	echo LANGUAGE_ID == "ru" ? "Необходимо указать текст вопроса" : "Need write text of question";
	die();
}

$site_format = CSite::GetDateFormat("FULL");
$php_format = $DB->DateFormatToPHP($site_format);
$time = date($php_format, time());

// Отправляем сообщение администратору
$arEventFields = array(
	"FIO"     => $_POST["fio"],
    "EMAIL"   => $_POST["email"],
    "USER"    => $USER->GetID(),

    "ITEM"    => $_POST["id"],
	"NAME"    => $_POST["name"],
    "LINK"    => $_POST["link"],
    "TEXT"    => $_POST["text"],

	"TIME"    => $time,
);
CEvent::Send("TICTACTOY_QUESTION", "s1", $arEventFields);


// Создаем элемент инфоблока
$el = new CIBlockElement;
$PROP = array();
$PROP["USER"]  = $USER->GetID();
$PROP["FIO"]   = $_POST["fio"];
$PROP["EMAIL"] = $_POST["email"];
$PROP["ITEM"]  = $_POST["id"];
$arLoadProductArray = Array(
	"IBLOCK_SECTION_ID" => false,
	"IBLOCK_ID"      => 9,
	"PROPERTY_VALUES"=> $PROP,
	"NAME"           => $_POST["name"],
	"ACTIVE"         => "Y",
	"ACTIVE_FROM"    => $time,
	"PREVIEW_TEXT"   => $_POST["text"]
);
$el->Add($arLoadProductArray);

echo "OK";

// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>