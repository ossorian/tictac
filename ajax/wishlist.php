<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

$product_id = intval($_GET["product_id"]);

// Добавление в wishlist
if ($_GET["action"] == 'add' and $product_id > 0 and $item = CIBlockElement::GetByID($product_id)->GetNext()) {
	$arFields = array(
		"PRODUCT_ID"             => $item["ID"],
		"QUANTITY"               => 1,
		"LID"                    => LANG,
		"DELAY"                  => "Y",
		"NAME"                   => $item["NAME"],
		"MODULE"                 => "catalog",
		"PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider",
		"DETAIL_PAGE_URL"        => $item["DETAIL_PAGE_URL"]
	);
	if (CSaleBasket::Add($arFields))
		echo 'OK';
	else
		echo "Error 1";
}
elseif ($_GET["action"] == 'delete' and $product_id > 0) {
	$dbBasketItems = CSaleBasket::GetList(array("ID" => "ASC"), array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL", "PRODUCT_ID" => $product_id));
	if ($arItems = $dbBasketItems->Fetch()) {
		CSaleBasket::Delete($arItems["ID"]);
		echo 'OK';
	}
	else {
		echo "Error 2";
	}
}
else {
	echo 'Error 0';
}





// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>