<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// Подключение инфоблоков
CModule::IncludeModule("iblock");

// Проверяем правильность введенных данных
if (empty($_POST["name"])) {
	echo LANGUAGE_ID == "ru" ? "Необходимо указать имя" : "Wrong name";
	die();
}
if (empty($_POST["email"]) or !check_email($_POST["email"])) {
	echo LANGUAGE_ID == "ru" ? "Неверно указан e-mail" : "Wrong e-mail";
	die();
}

$time = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time());

// Отправляем сообщение администратору
$arEventFields = array(
	"FIO"     => $_POST["fio"],
    "EMAIL"   => $_POST["email"],
    "USER"    => $USER->GetID(),

    "ITEM"    => $_POST["id"],
	"NAME"    => $_POST["name"],
    "LINK"    => $_POST["link"],

	"TIME"    => $time,
);
CEvent::Send("TICTACTOY_WAIT", "s1", $arEventFields);


// Создаем элемент инфоблока
$el = new CIBlockElement;
$PROP = array();
$PROP["USER"]  = $USER->GetID();
$PROP["FIO"]   = $_POST["fio"];
$PROP["EMAIL"] = $_POST["email"];
$PROP["ITEM"]  = $_POST["id"];
$arLoadProductArray = Array(
	"IBLOCK_SECTION_ID" => false,
	"IBLOCK_ID"      => 8,
	"PROPERTY_VALUES"=> $PROP,
	"NAME"           => $_POST["name"],
	"ACTIVE"         => "Y",
	"ACTIVE_FROM"    => $time
);
$el->Add($arLoadProductArray);

echo LANGUAGE_ID == "ru" ? "Спасибо за заявку. Наш менеджер свяжется с Вами как только поступят часы." : "We will get in touch with you as soon the watch will appear in stock again.";

// Подписываем пользователя на товар
if(intval($USER->GetID()) == 0) {
	$USER->SimpleRegister($_POST["email"]);
	$user = new CUser;
	$user->Update($USER->GetID(), Array("NAME" => $_POST["fio"]));
}
Add2BasketByProductID($_POST["id"], 0, array('SUBSCRIBE' => 'Y', 'CAN_BUY' => 'N'), array());

// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>