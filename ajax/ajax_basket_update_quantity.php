<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    if (!$_REQUEST['id']) return; 
	
	CModule::IncludeModule("sale");
	$arFields = array(
		"QUANTITY" => $_POST["quantity"]
	);
	CSaleBasket::Update($_POST["id"], $arFields);
}