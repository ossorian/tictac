<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    if (!$_REQUEST['id']) return;
	
	CModule::IncludeModule("sale");
	CModule::IncludeModule("catalog");
	$cBasket = new CSaleBasket;

/* $cBasket->Delete($_POST['basketID']);
die; */
	
	if (empty($_POST['basketID'])) {
		if ($quantity = intval($_POST["quantity"]) && $quantity > 0) {
			$basketID = Add2BasketByProductID($_REQUEST['id'], $_POST["quantity"]);
		}
		if ($basketID) echo json_encode(['basketID' => $basketID]);
		else echo json_encode(['error' => 'error']);
	}
	else if (!empty($_POST["quantity"])) {
		$basketItem = $cBasket->GetByID($_POST['basketID']);
		$newQuantity = intval($basketItem["QUANTITY"]) + intval($_POST["quantity"]);
		
		if ($newQuantity <=0) $cBasket->Delete($_POST['basketID']);
		else {
			$arFields = array(
				"QUANTITY" => $newQuantity
			);
			$cBasket->Update($_REQUEST['basketID'], $arFields);
		}
		
		if ($newQuantity <= 0) $result = ['basketID' => 0, 'newQuantity' => $newQuantity];
		else $result = ['basketID' => $_POST['basketID'], 'newQuantity' => $newQuantity];
		echo json_encode($result);
	}
}