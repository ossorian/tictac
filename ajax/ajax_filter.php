<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");


$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array(
    "IBLOCK_ID"=>IntVal(5),
    "ACTIVE_DATE"=>"Y",
    "ACTIVE"=>"Y",
    "SECTION_ACTIVE"=>"Y",
    ">CATALOG_QUANTITY" => 0
);

if (!empty($_REQUEST['section_id'])){
    $arFilter['SECTION_ID'] = array(/*$_REQUEST['section_id']*/$_REQUEST['PARENT_SECTION']);
    $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
}

if (!empty($_REQUEST['arrFilter_pf']['FILTER_BRAND'])){
    if (!empty($_REQUEST['arrFilter_pf']['FILTER_BRAND'])) {
        $arFilter['SECTION_ID'] = $_REQUEST['arrFilter_pf']['FILTER_BRAND'];
    } else {
        $arFilter['SECTION_ID'] = $_REQUEST['section_id'];
    }
} else if (!empty($_REQUEST['PARENT_SECTION']) && empty($_REQUEST['section_id'])) {
    $arFilter['SECTION_ID'][] = $_REQUEST['PARENT_SECTION'];
    $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
}



$price_min = !empty($_REQUEST['arrFilter_cf'][1]['LEFT']) ? $_REQUEST['arrFilter_cf'][1]['LEFT'] : 0;
$price_max = !empty($_REQUEST['arrFilter_cf'][1]['RIGHT']) ? $_REQUEST['arrFilter_cf'][1]['RIGHT'] : 0;

foreach ($_REQUEST['arrFilter_pf'] as $filterName => $filterValue){
    if ($filterName === 'PROP_MATERIAL') {
        $arFilter['?PROPERTY_PROP_MATERIAL'] = $filterValue;
    } else if ($filterName === 'FRAME_MATERIAL') {
        $arFilter['?PROPERTY_FRAME_MATERIAL'] = $filterValue;
    } else {
        $arFilter['PROPERTY_' . $filterName] = $filterValue;
    }
}


//print_r($arFilter);

$data = array();
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
while($ob = $res->GetNext()){
    $price = '';
    $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $ob['ID'], 'CATALOG_GROUP_ID' => 1));
    if ($arPrice = $rsPrices->Fetch())
    {
        $price = $arPrice['PRICE'];
    }
    if ((float)$price_min <= (float)$price && (float)$price <= (float)$price_max){
        $data[] = $ob;
    }
}

$count = count($data);

if (!empty($count)){
    echo 'Найдено ' . $count . ' ' . morph($count, 'товар', 'товара', 'товаров') .
        '<a href="javascript:void(0);" class="btn" id="set_filter_btn">Показать</a><a href="javascript:void(0);" class="reset__btn" id="reset_filter_btn">Сбросить</a>';
} else {
    echo 'Ничего не найдено <a href="javascript:void(0);" class="reset__btn" id="reset_filter_btn">Сбросить</a><a href="javascript:void(0);" class="btn">&nbsp;</a>';
}


// подключение служебной части эпилога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>