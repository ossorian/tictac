<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    if (!empty($_REQUEST['DISABLE_PRODUCT_OF_THE_DAY']) && $_REQUEST['DISABLE_PRODUCT_OF_THE_DAY'] === "Y") {
        $_SESSION['DISABLE_PRODUCT_OF_THE_DAY'] = 'Y';
        exit();
    }

    if (empty($_SESSION['DISABLE_PRODUCT_OF_THE_DAY'])) {
        if (empty($_REQUEST['SECTION_ID'])) {
            exit();
        }

        $SECTION_ID = $_REQUEST['SECTION_ID'];
        $itemCount = $_REQUEST['ITEMS_COUNT'];

        CModule::IncludeModule("sale");
        CModule::IncludeModule("iblock");

        $arSection = array();
        $res = CIBlockSection::GetByID($SECTION_ID);
        if ($ar_res = $res->GetNext()) {
            if (!empty($ar_res['IBLOCK_SECTION_ID'])) {
                $SECTION_ID = $ar_res['IBLOCK_SECTION_ID'];

            }
            $arSection = $ar_res;
        }

        $arProductOfTheDay = array();
        $arFilter = array('IBLOCK_ID' => 5, "PROPERTY_PRODUCT_DAY_VALUE" => "Y", 'ACTIVE' => 'Y', 'SECTION_ACTIVE' => 'Y', 'SECTION_ID' => $SECTION_ID, "INCLUDE_SUBSECTIONS" => "Y",);
        $res = CIBlockElement::GetList(array('TIMESTAMP_X' => 'DESC'), $arFilter, false, array('nPageSize' => 1), array('*'));
        if ($obProductOfTheDay = $res->GetNextElement()) {
            $arFields = $obProductOfTheDay->GetFields();
            $arProps = $obProductOfTheDay->GetProperties();
            $arPrice['PRICES'] = CPrice::GetBasePrice($arFields['ID']);

            $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                $arPrice['PRICES']["ID"],
                $USER->GetUserGroupArray(),
                "N",
                SITE_ID
            );
            $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                $arPrice['PRICES']["PRICE"],
                $arPrice['PRICES']["CURRENCY"],
                $arDiscounts
            );
            $arPrice['PRICES']["DISCOUNT_PRICE"] = $discountPrice;

            $arProductOfTheDay = array_merge($arFields, $arProps, $arPrice);
            $arProductOfTheDay['PRODUCT_OF_THE_DAY'] = 'Y';

            $picture = resizeImage($arProductOfTheDay['PREVIEW_PICTURE'], 270, 322, 1) ?>
            <?
            $elPrice = 0;
            if ($arProductOfTheDay["PRICES"]["DISCOUNT_PRICE"] < $arProductOfTheDay["PRICES"]["PRICE"]) {
                $elPrice =$arProductOfTheDay["PRICES"]["DISCOUNT_PRICE"];
                $price = '<p class="item__price">
                                                    <span class="color_red">' . CurrencyFormat($arProductOfTheDay["PRICES"]["DISCOUNT_PRICE"], $arProductOfTheDay['PRICES']['CURRENCY']) . '<span class="rub">₽</span></span> 
                                                    <span class="item-price__before">' . CurrencyFormat($arProductOfTheDay["PRICES"]["PRICE"], $arProductOfTheDay['PRICES']['CURRENCY']) . ' <span class="rub">₽</span></span>
                                                </p>';
                $sale = '<span class="label label_sale">Sale</span>';
            } else {
                $elPrice = $arProductOfTheDay["PRICES"]["PRICE"];
                $price = '<p class="item__price">' . CurrencyFormat($arProductOfTheDay["PRICES"]["PRICE"], $arProductOfTheDay['PRICES']['CURRENCY']) . ' <span class="rub">₽</span></p>';
            }

            ?>


            <div class="col-md-<?= $arSection['DEPTH_LEVEL'] == 1 ? 3 : 4 ?> col-sm-6 brend__col">
                <input type="hidden" id="ITEM_ID" value="<?= $arProductOfTheDay['ID'] ?>">
                <div class="item item-day">
								<span class="item__close close-item-of-the-day">
								<i class="icon-icon-small-close"></i>
							</span>
                    <? if ($arProductOfTheDay["DISPLAY_PROPERTIES"]["NEW"]["DISPLAY_VALUE"] == "да") { ?>
                        <span class="label label_new">New</span>
                    <? } ?>

                    <div class="favorites">
									<span class="favorites__icon" data-id="<?= $arProductOfTheDay['ID'] ?>">
										<i class="icon-icon-wishlist"></i>
									</span>
                        <p class="favorites__text">Избранное</p>
                    </div>
                    <a href="#qv__popup" class="quick-view" data-id="<?= $arProductOfTheDay['ID'] ?>">
								<span class="quick-view__icon">
									<i class="icon-icon-quick-view"></i>
								</span>
                        <span class="quick-view__text">Быстрый просмотр</span>
                    </a>
                    <a href="<?= $arProductOfTheDay['DETAIL_PAGE_URL'] ?>"
                       onclick="
                           dataLayer.push({
                           'event': 'productClick',
                           'ecommerce': {
                           'click': {
                           'actionField': {'list': '<?= $APPLICATION->GetCurPage()?>'},     // Список с которого был клик.
                           'products': [{
                           'name': '<?= strip_tags($arProductOfTheDay['NAME'])?>',       	// Наименование товара.
                           'id': '<?= $arProductOfTheDay['ID']?>',  				// Идентификатор товара в базе данных.
                           'price': '<?= $elPrice?>',   				// Цена одной единицы купленного товара
                           'brand': '<?= $arProps['BRAND_MODEL']['VALUE_ENUM']?>',  			 //Бренд товара
                           'category': 'часы', 			 //Категория товара
                           'variant': '<?= implode(',', $arProps['FILTER_COLOR']['VALUE_ENUM'])?>',				//Вариант товара, например цвет
                           'position': '1'   				//Позиция товара в списке
                           }]
                           }
                           }
                           });"
                    >
                        <img src="<?= $picture["SRC"] ?>"
                             alt="<?= $arProductOfTheDay["NAME"] ?>" title="<?= $arProductOfTheDay["NAME"] ?>"
                             class="item-preview-img"/>

                        <p class="item__name"><?= $arProductOfTheDay['NAME'] ?> <span class="label-day">Товар дня</span>
                        </p>
                    </a>
                    <?= $price ?>
                    <!--								<a href="#" class="preorder link_grey">Предзаказ</a>-->
                </div>
            </div>

            <?
        }

    }
}
