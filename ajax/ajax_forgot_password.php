<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    $APPLICATION->IncludeComponent("bitrix:system.auth.form", "change_pass", Array(
        "FORGOT_PASSWORD_URL" => "",    // Страница забытого пароля
        "PROFILE_URL" => "",    // Страница профиля
        "REGISTER_URL" => "",    // Страница регистрации
        "SHOW_ERRORS" => "Y",    // Показывать ошибки
    ),
        false
    ); ?>
    <?
    $APPLICATION->IncludeComponent(
        "bitrix:system.auth.forgotpasswd",
        "ajax_response",
        Array(
            "AJAX_MODE" => "N",
            "AJAX_OPTION_SHADOW" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N"
        )
    );
}
