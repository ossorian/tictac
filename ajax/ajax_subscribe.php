<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if (strlen($_POST["EMAIL"]) > 1 && !check_email($_POST["EMAIL"])) die();

    CModule::IncludeModule("iblock");
        $el = new CIBlockElement;

        $arFilter = array('IBLOCK_ID' => 18, 'PROPERTY_EMAIL' => $_POST['EMAIL']);
        $res = $el->GetList(array(), $arFilter);
        if ($ob = $res->GetNextElement()){
            echo 'Вы уже подписаны на рассылку!';
            die();
        }


        $arInsert = Array(
            "IBLOCK_SECTION" => false,
            "IBLOCK_CODE" => 'phpdev_subscriber',
            "IBLOCK_ID"      => 18,
            "PROPERTY_VALUES"=> array('EMAIL' => $_POST['EMAIL']),
            "NAME"           => $_POST['NAME'],
            "ACTIVE"         => "Y",            // активен
        );

        $res = $el->Add($arInsert);

    if ($res){
		\Emails::sendSubsribe($_POST['EMAIL']);
        echo 'Вы успешно подписались на рассылку';
        $_SESSION['DISABLE_SUBSCRIBE'] = 'Y';
    }


}

