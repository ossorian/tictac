<?php require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
//CCatalogExport::PreGenerateExport(9);

header("Content-Disposition: attachment; filename=yandex.xml");
print "<?xml version='1.0' encoding='UTF-8'?>";
?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?=date("Y-m-d H:i")?>">
<shop>
<name>Интернет-магазин tictactoy.ru</name>
<company>Интернет-магазин tictactoy.ru</company>
<url>http://tictactoy.ru/</url>
<platform>1C-Bitrix</platform>
<currencies><currency id="RUB" rate="1" plus="0"/></currencies>
<categories>
<?php
$arSec = Array(27,28,31,32,33,34,35,36,37,38,39,45,46,47,48,49,50,51,52,53,55,56,57,58,59,60,61,62,63,64,66,67,68,70,72,73,74,75,76,77,125,126,127,129,130,136);
$arFilter = Array('IBLOCK_ID' => 5, 'GLOBAL_ACTIVE'=>'Y','ID'=>$arSec);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {?>
	<category id="<?=$ar_result["ID"]?>" parentId="<?=$ar_result["IBLOCK_SECTION_ID"]?>"><?=$ar_result["NAME"]?></category>
	<?
  }
  ?>
</categories>
<offers>
<?php
$arSelect = Array("ID","CODE","NAME","MIN_PRICE","CATALOG_GROUP_1","IBLOCK_ID","IBLOCK_SECTION_ID","DETAIL_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL","PROPERTY_BRAND_MODEL");
$arFilter = Array("IBLOCK_ID" => 5, "ACTIVE"=>"Y",'SECTION_ID'=>$arSec,">CATALOG_QUANTITY" => 0);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
  $arFields = $ob->GetFields();
 $d = $arFields["PREVIEW_TEXT"];
$descr = str_replace('&mdash;','-',$d);
$descr0 = str_replace('&ndash;','-',$descr);
$descr2 = str_replace('&laquo;',' ',$descr0);
$descr3 = str_replace('&raquo;',' ',$descr2);
$descr4 = str_replace('&bull',' ',$descr3);
$descr5 = str_replace('&amp;',' ',$descr4);
$descr6 = str_replace('&deg;',' ',$descr5);
$descr7 = str_replace('&nbsp;',' ',$descr6);
$descr8 = str_replace('&auml;',' ',$descr7);
$descr9 = str_replace('&ouml;',' ',$descr8);
$descr10 = str_replace('&lt;',' ',$descr9);
$descr11 = str_replace('&gt;',' ',$descr10);
$descr11 = str_replace('&;',' and ',$descr10);
$title1 = str_replace("&amp;"," ",strip_tags(htmlspecialchars_decode($arFields["NAME"])));
$title2  = str_replace("&"," and ",$title1);
$kol = strlen($arFields["PREVIEW_TEXT"]);
 ?>
 <?if( $kol > 45 ){?>
<offer id="<?=$arFields["ID"]?>"  available="true">
	<url>http://<?=$_SERVER["SERVER_NAME"]?><?=$arFields["DETAIL_PAGE_URL"]?></url>
	<price><?=$arFields["CATALOG_PRICE_1"];?></price>
	<currencyId>RUB</currencyId>
	<categoryId><?=$arFields["IBLOCK_SECTION_ID"]?></categoryId>
	<picture>http://<?=$_SERVER["SERVER_NAME"]?><?=CFile::GetPath($arFields["DETAIL_PICTURE"])?></picture>
	<name><?=$arFields["PROPERTY_BRAND_MODEL_VALUE"]?> <?=str_replace("&","&amp;",strip_tags(htmlspecialchars_decode($arFields["NAME"])))?></name>
	<description><?=strip_tags(<?=strip_tags($descr11);?>);?></description>
	<manufacturer_warranty>true</manufacturer_warranty>
	<local_delivery_cost>300</local_delivery_cost>
</offer>
<?
 }
 }
?></offers>


</shop></yml_catalog>
