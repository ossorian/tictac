<?
function customAutoload($className) {
    if (!CModule::RequireAutoloadClass($className)) {

		$mainPath = $_SERVER["DOCUMENT_ROOT"]."/local/phplib/";
		$className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        $path = $mainPath . ucfirst($className) . '.php';

        if (file_exists($path)) {
            require_once $path;
            return true;
        }
        return false;
    }
}
spl_autoload_register('customAutoload', true);

//if($_SERVER['REMOTE_ADDR'] == '5.228.252.38'){


$is_https       = (isset($_SERVER['HTTP_X_HTTPS']) && $_SERVER['HTTP_X_HTTPS'] == 1);
//$is_tictac  = (SITE_ID == 's1');
$is_tictactoy = stripos($_SERVER['SERVER_NAME'],"tictactoy");
$is_tictac  = (SITE_ID == 's1' || is_numeric($is_tictactoy));
$has_www        = preg_match('/^www\./', $_SERVER['HTTP_HOST']);
$is_cart    = preg_match('/cart\/|order\/|sale\.ajax\.locations\/templates|ajax\/cart_full\.php/', $_SERVER['REQUEST_URI']);

if ($is_tictac) {
                if ($is_https) {
                                if (!$has_www) {
                                                header("HTTP/1.1 301 Moved Permanently");
                                                header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
                                                exit();
                                }
		else {
                                        header("HTTP/1.1 301 Moved Permanently");
                                        header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
                                        exit();
                                }
                }
} else {
                if ($is_https) {
                                if ($has_www) {
                                                header("HTTP/1.1 301 Moved Permanently");
                                                header("Location: https://" . str_replace('www.', '', $_SERVER['HTTP_HOST']) . $_SERVER['REQUEST_URI']);
                                                exit();
                                }
                } else {
                                if ($has_www) {
                                                header("HTTP/1.1 301 Moved Permanently");
                                                header("Location: https://" . str_replace('www.', '', $_SERVER['HTTP_HOST']) . $_SERVER['REQUEST_URI']);
                                                exit();
                                }
                }
}



#catalog redirect
$arUri = explode('/', $_SERVER['REDIRECT_URL']);

$catArr = array(
	"straps",
	"sunglasses",
	"accessories",
	"new",
	"preorder",
	"sale",
	"watches",
);

if ($is_tictac) {
	if (!empty($arUri) && (strtolower($arUri[1]) === 'catalog')) {

		if (!empty($arUri[2])) {
			if (!in_array(strtolower($arUri[2]), $catArr)) {
				if (!empty($arUri[3])) {
					header("HTTP/1.1 301 Moved Permanently");
					header('Location: /catalog/watches/' . $arUri[2] . '/' . $arUri[3] . '/');
					exit();
				} else {
					header("HTTP/1.1 301 Moved Permanently");
					header('Location: /catalog/watches/' . $arUri[2] . '/');
					exit();
				}
			} else if (strtolower($arUri[2]) === 'sale' && !empty($arUri[3])){
				header("HTTP/1.1 301 Moved Permanently");
				header('Location: /catalog/watches/' . $arUri[2] . '/' . $arUri[3] . '/');
				exit();
			}
		} else {
			if ($_REQUEST["SECTION_CODE"] == false and empty($_GET["q"]) and !isset($_GET["arrFilter_cf"]["1"]["LEFT"]) and empty($_GET['set_filter'])) {
				header("HTTP/1.1 301 Moved Permanently");
				header('Location: /catalog/watches/');
				exit();
			}
		}
	}
}
#end catalog redirect


// if(!$is_https && !$has_www && $is_tictac && $is_cart){
// 	header("HTTP/1.1 301 Moved Permanently");
// 	header("Location: https://www." . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
// 	exit();
// } elseif(!$is_https && $is_tictac && $is_cart){
// 	header("HTTP/1.1 301 Moved Permanently");
// 	header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
// 	exit();
// }
// elseif(!$has_www && $is_tictac && !$is_cart){
// 	header("HTTP/1.1 301 Moved Permanently");
// 	header("Location: http://www." . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
// 	exit();
// }
// elseif($has_www && $is_https && $is_tictac && !$is_cart){
//     header("HTTP/1.1 301 Moved Permanently");
// 	header("Location: http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
// 	exit();
// }

// }

require('oldFunctions.php');

function ss($o)
{
	$bt =  debug_backtrace();
	$bt = $bt[0];
	$dRoot = $_SERVER["DOCUMENT_ROOT"];
	$dRoot = str_replace("/","\\",$dRoot);
	$bt["file"] = str_replace($dRoot,"",$bt["file"]);
	$dRoot = str_replace("\\","/",$dRoot);
	$bt["file"] = str_replace($dRoot,"",$bt["file"]);
	?>
	<div style='font-size:9pt; color:#000; background:#fff; border:1px dashed #000;'>
	<div style='padding:3px 5px; background:#99CCFF; font-weight:bold;'>File: <?=$bt["file"]?> [<?=$bt["line"]?>]</div>
	<pre style='padding:10px;'><?print_r($o)?></pre>
	</div>
	<?
	}

require_once("include/debug.php");
require_once("include/waranty.php");

// Подключаем api смс
require_once("Smsbliss.php");
define("SMS_LOGIN", "Tictactoy");
define("SMS_PASSWORD", "robin777");

/*
if($_SERVER['REMOTE_ADDR'] == '94.41.247.56') {
		$gate = new Smsbliss_JsonGate(SMS_LOGIN, SMS_PASSWORD);
		$messages = array(
			array(
				"clientId" => 1,
				"phone"=> "+79631313397",
				"text"=> "_1",
				"sender"=> "TicTacToyRu"
			),
			array(
				"clientId" => 2,
				"phone"=> "89631313397",
				"text"=> "_2",
				"sender"=> "TicTacToyRu"
			),
			array(
				"clientId" => 3,
				"phone"=> "+7-963-131-33-97",
				"text"=> "_3",
				"sender"=> "TicTacToyRu"
			),
			array(
				"clientId" => 4,
				"phone"=> "8 963 131 33 97",
				"text"=> "_4",
				"sender"=> "TicTacToyRu"
			),
			array(
				"clientId" => 5,
				"phone"=> "+7 963 131-33-97",
				"text"=> "_5",
				"sender"=> "TicTacToyRu"
			),
		);
		var_dump($gate->send($messages, 'tictactoyTest2Queue'));
		die();
}
*/


//
// Отправка номера трека при смене статуса заказа
//
//AddEventHandler("sale", "OnSaleStatusOrder", Array("Tictactoy", "ChangeStatus"));
//AddEventHandler("sale", "OnBeforeOrderUpdate", Array("Tictactoy", "Update"));
AddEventHandler("sale", "OnSaleDeliveryOrder", Array("Tictactoy", "Delivery"));

// Для отправки смс что не дозвонились
AddEventHandler("sale", "OnSaleStatusOrder", Array("Tictactoy", "SmsChangeStatus"));
AddEventHandler("sale", "OnBeforeOrderUpdate", Array("Tictactoy", "SmsUpdate"));

// Обработчик прописывающий количество товара в зависимости от свойства
AddEventHandler("catalog", "OnBeforeProductUpdate", Array("Tictactoy", "OnBeforeProductUpdate"));

//устанавливаем только 1 товар дня
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("Tictactoy", "OnAfterIBlockElementUpdateHandler"));

class Tictactoy {

	static $noHandlerProduct = false;


	//станавливаем только 1 товар дня для каждого из разделов каталога
	function OnAfterIBlockElementUpdateHandler(&$arFields)
	{
		CModule::IncludeModule("iblock");
		$resProductDay = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, 'ID'=>$arFields["ID"]), false, false, array('ID', 'IBLOCK_SECTION_ID', 'PROPERTY_PRODUCT_DAY'));
		if ($propProductDay = $resProductDay->GetNext()) {
			if (!empty($propProductDay['PROPERTY_PRODUCT_DAY_VALUE'])) {

				$section_ID = 0;
				$rsSec = CIBlockSection::GetList(array(), array('ID' => $propProductDay['IBLOCK_SECTION_ID']), false, array());
				while ($arSec = $rsSec->GetNext()) {
					$section_ID = $arSec['IBLOCK_SECTION_ID'];
				}

				$res = CIBlockElement::GetList(Array(), array('IBLOCK_ID' => 5, '!PROPERTY_PRODUCT_DAY_VALUE' => false, '!ID' => $arFields["ID"], 'SECTION_ID' => $section_ID), false, false, array('ID', 'NAME', 'PROPERTY_PRODUCT_DAY'));
				while ($ob = $res->GetNext()) {
					CIBlockElement::SetPropertyValuesEx($ob['ID'], 5, array("PRODUCT_DAY" => false));
				}
			}
		}
	}

	//
	// Методы для работы с кол-вом товара
	//

	// Проставляем правильное количество при редактировании
	function OnBeforeProductUpdate($ID, &$arFields) {
//		if(self::$noHandlerProduct) return true; // не вызываем при массовой обработке
//		CModule::IncludeModule("iblock");
//		$res = CIBlockElement::GetProperty(5, $ID, array("sort" => "asc"), array("CODE" => "WAIT"));
//		if ($prop = $res->Fetch()) {
//			$arFields["QUANTITY"] = trim($prop["VALUE"]) != "" ? 0 : 999;
//		}
//
//		return true;
	}

	// Сбрасывает количество всех товаров в нужное
	function ResetProductQuantityAll() {
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("catalog");
		$i = 0;
		self::$noHandlerProduct = true;
		$res = CIBlockElement::GetList(Array("ID"=>"ASC"), array("IBLOCK_ID"=>5), false, false, array("ID", "IBLOCK_ID", "PROPERTY_WAIT"));
		while($el = $res->Fetch()) {
			CCatalogProduct::Update($el['ID'], array('QUANTITY' => (trim($el["PROPERTY_WAIT_VALUE"]) != "") ? 0 : 999));
			$i++;
		}
		self::$noHandlerProduct = false;
		return $i;
	}



	//
	// Методы для работы с интернет магазином
	//

	function Delivery ($id, $val) {

		// Если доставка разрешена
		if ($val == "Y" and $arOrder = CSaleOrder::GetByID($id)) {

				// Свойства
				$db_props = CSaleOrderPropsValue::GetOrderProps($id);
				while ($arProps = $db_props->Fetch()) {
					if($arProps["ORDER_PROPS_ID"] == 1) {
						$name = $arProps["VALUE"];
					}
					elseif($arProps["ORDER_PROPS_ID"] == 2) {
						$email = $arProps["VALUE"];
					}
					elseif($arProps["ORDER_PROPS_ID"] == 23) {
						$track = $arProps["VALUE"]; // старый вариант
					}
					elseif($arProps["ORDER_PROPS_ID"] == 3) {
						$phone = $arProps["VALUE"];
					}
				}
				$track = $arOrder["DELIVERY_DOC_NUM"];

				// Состав заказа
				$order_list = "";
				$db_basket = CSaleBasket::GetList(Array(),Array("ORDER_ID"=>$id));
				while ($item = $db_basket->Fetch()) {
					$order_list .= $item["NAME"] . " - " . intval($item["QUANTITY"]) . "шт.\r\n";
				}

				// Почтовое событие
				if($track != false) {
					$arEventFields = array(
						"ORDER_ID" => $id,
					    "DATE_INSERT_FORMAT" => $arOrder["DATE_INSERT_FORMAT"],
					    "FIO" => $name,
					    "TRACK_NUMBER" => $track,
					    "EMAIL" => $email,
					    "ORDER_LIST" => $order_list
					);
					CEvent::Send("TICTACTOY_TRACK", "s1", $arEventFields);
					if(strlen($phone) >= 9)
						self::SendSms($id, $phone, $track);
				}

		}
	}



	// Отправка смс что не дозвонились при статусе T
	function SmsChangeStatus($id, $status) {

		// Статус должен быть указан как отправленно
		if ($status == "T") {
			// Получаем всю информацию по заказу
			if ($arOrder = CSaleOrder::GetByID($id)) {
				// Свойства
				$db_props = CSaleOrderPropsValue::GetOrderProps($id);
				while ($arProps = $db_props->Fetch()) {
					if($arProps["ORDER_PROPS_ID"] == 3) {
						$gate = new Smsbliss_JsonGate(SMS_LOGIN, SMS_PASSWORD);
						$messages = array(
							array(
								"clientId" => $id,
								"phone"=> $arProps["VALUE"],
								"text"=> "К сожалению, нам не удалось дозвониться до Вас для подтверждения заказа. Пожалуйста перезвоните нам по бесплатному номеру 8-800-234-43-31, чтобы мы могли исполнить Ваш заказ.",
								"sender"=> "TicTacToy"
							)
						);
						$gate->send($messages);
					}
				}
			}
		}
    }



	// Отправка смс что не дозвонились при статусе T
	function SmsUpdate($id, $order) {

		// Получаем всю информацию по заказу
		if ($arOrder = CSaleOrder::GetByID($id)) {
			if($order["STATUS_ID"] == "T" and $order["STATUS_ID"] != $arOrder["STATUS_ID"]) {
				$gate = new Smsbliss_JsonGate(SMS_LOGIN, SMS_PASSWORD);
				$messages = array(
					array(
						"clientId" => $id,
						"phone"=> $_POST["ORDER_PROP_3"],
						"text"=> "К сожалению, нам не удалось дозвониться до Вас для подтверждения заказа. Пожалуйста перезвоните нам по бесплатному номеру 8-800-234-43-31, чтобы мы могли исполнить Ваш заказ.",
						"sender"=> "TicTacToy"
					)
				);
				$gate->send($messages);
			}
		}
	}



	// отключен, старый
	function Update($id, $order) {

		// Получаем всю информацию по заказу
		if ($arOrder = CSaleOrder::GetByID($id)) {

			if($order["STATUS_ID"] == "F" and $order["STATUS_ID"] != $arOrder["STATUS_ID"]) {

				// Свойства
				$name = $_POST["ORDER_PROP_1"];
				$email = $_POST["ORDER_PROP_2"];
				$track = $_POST["ORDER_PROP_23"];
				$phone = $_POST["ORDER_PROP_3"];

				// Состав заказа
				$order_list = "";
				foreach ($_POST["PRODUCT"] as $item) {
					$order_list .= $item["NAME"] . " - " . intval($item["QUANTITY"]) . "шт.\r\n";
				}

				// Почтовое событие
				if($track != false) {
					$arEventFields = array(
						"ORDER_ID" => $id,
					    "DATE_INSERT_FORMAT" => date("d.m.Y"),
					    "FIO" => $name,
					    "TRACK_NUMBER" => $track,
					    "EMAIL" => $email,
					    "ORDER_LIST" => $order_list
					);
					CEvent::Send("TICTACTOY_TRACK", "s1", $arEventFields);
					self::SendSms($id, $phone, $track);
				}
			}
		}
	}



	// отключен, старый
	function ChangeStatus($id, $status) {

		// Статус должен быть указан как отправленно
		if ($status == "F") {
			// Получаем всю информацию по заказу
			if ($arOrder = CSaleOrder::GetByID($id)) {

				// Свойства
				$db_props = CSaleOrderPropsValue::GetOrderProps($id);
				while ($arProps = $db_props->Fetch()) {
					if($arProps["ORDER_PROPS_ID"] == 1) {
						$name = $arProps["VALUE"];
					}
					elseif($arProps["ORDER_PROPS_ID"] == 2) {
						$email = $arProps["VALUE"];
					}
					elseif($arProps["ORDER_PROPS_ID"] == 23) {
						$track = $arProps["VALUE"];
					}
					elseif($arProps["ORDER_PROPS_ID"] == 3) {
						$phone = $arProps["VALUE"];
					}
				}

				// Состав заказа
				$order_list = "";
				$db_basket = CSaleBasket::GetList(Array(),Array("ORDER_ID"=>$id));
				while ($item = $db_basket->Fetch()) {
					$order_list .= $item["NAME"] . " - " . intval($item["QUANTITY"]) . "шт.\r\n";
				}

				// Почтовое событие
				if($track != false) {
					$arEventFields = array(
						"ORDER_ID" => $id,
					    "DATE_INSERT_FORMAT" => $arOrder["DATE_INSERT_FORMAT"],
					    "FIO" => $name,
					    "TRACK_NUMBER" => $track,
					    "EMAIL" => $email,
					    "ORDER_LIST" => $order_list
					);
					CEvent::Send("TICTACTOY_TRACK", "s1", $arEventFields);
					self::SendSms($id, $phone, $track);
				}
			}
		}
    }



    // Отправляем смс с номером трека
    function SendSms ($order, $phone, $track) {

		// Не даем отправлять более 1 смс
		static $run = 0;
		$run++;
		if($run > 1) return true;

		if($order == false or $phone == false or $track == false) return false;
		$gate = new Smsbliss_JsonGate(SMS_LOGIN, SMS_PASSWORD);
		$messages = array(
			array(
				"clientId" => $order,
				"phone"=> $phone,
				"text"=> "Vash zakaz N".$order." byl otpravlen. Nomer posylki: ".$track.". Otsledit prohojdenie otpravleniya mojno na saite www.pochta.ru",
				"sender"=> "TicTacToy"
			)
		);
		$gate->send($messages);
		return true;
	}
}



// Правильное окончание слов для социалок
function social_end_word($num) {
	if (strlen($num)>1 && substr($num, strlen($num)-2, 1)=="1") {
		return "участников";
	}
	else {
		$c = IntVal(substr($num, strlen($num)-1, 1));
		if ($c==0 || ($c>=5 && $c<=9))
			return "участников";
		elseif ($c==1)
			return "участник";
		else
			return "участника";
	}
}






/*Version 0.3 2011-04-25*/
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "BXIBlockAfterSave");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "BXIBlockAfterSave");
AddEventHandler("catalog", "OnPriceAdd", "BXIBlockAfterSave");
AddEventHandler("catalog", "OnPriceUpdate", "BXIBlockAfterSave");
function BXIBlockAfterSave($arg1, $arg2 = false)
{
$ELEMENT_ID = false;
	$IBLOCK_ID = false;
	$OFFERS_IBLOCK_ID = false;
	$OFFERS_PROPERTY_ID = false;

	//Check for catalog event
	if(is_array($arg2) && $arg2["PRODUCT_ID"] > 0)
	{
		//Get iblock element
		$rsPriceElement = CIBlockElement::GetList(
			array(),
			array(
				"ID" => $arg2["PRODUCT_ID"],
			),
			false,
			false,
			array("ID", "IBLOCK_ID")
		);
		if($arPriceElement = $rsPriceElement->Fetch())
		{
			$arCatalog = CCatalog::GetByID($arPriceElement["IBLOCK_ID"]);
			if(is_array($arCatalog))
			{
				//Check if it is offers iblock
				if($arCatalog["OFFERS"] == "Y")
				{
					//Find product element
					$rsElement = CIBlockElement::GetProperty(
						$arPriceElement["IBLOCK_ID"],
						$arPriceElement["ID"],
						"sort",
						"asc",
						array("ID" => $arCatalog["SKU_PROPERTY_ID"])
					);
					$arElement = $rsElement->Fetch();
					if($arElement && $arElement["VALUE"] > 0)
					{
						$ELEMENT_ID = $arElement["VALUE"];
						$IBLOCK_ID = $arCatalog["PRODUCT_IBLOCK_ID"];
						$OFFERS_IBLOCK_ID = $arCatalog["IBLOCK_ID"];
						$OFFERS_PROPERTY_ID = $arCatalog["SKU_PROPERTY_ID"];
					}
				}
				//or iblock wich has offers
				elseif($arCatalog["OFFERS_IBLOCK_ID"] > 0)
				{
					$ELEMENT_ID = $arPriceElement["ID"];
					$IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
					$OFFERS_IBLOCK_ID = $arCatalog["OFFERS_IBLOCK_ID"];
					$OFFERS_PROPERTY_ID = $arCatalog["OFFERS_PROPERTY_ID"];
				}
				//or it's regular catalog
				else
				{
					$ELEMENT_ID = $arPriceElement["ID"];
					$IBLOCK_ID = $arPriceElement["IBLOCK_ID"];
					$OFFERS_IBLOCK_ID = false;
					$OFFERS_PROPERTY_ID = false;
				}
			}
		}
	}
	//Check for iblock event
	elseif(is_array($arg1) && $arg1["ID"] > 0 && $arg1["IBLOCK_ID"] > 0)
	{
		//Check if iblock has offers
		$arOffers = CIBlockPriceTools::GetOffersIBlock($arg1["IBLOCK_ID"]);
		if(is_array($arOffers))
		{
			$ELEMENT_ID = $arg1["ID"];
			$IBLOCK_ID = $arg1["IBLOCK_ID"];
			$OFFERS_IBLOCK_ID = $arOffers["OFFERS_IBLOCK_ID"];
			$OFFERS_PROPERTY_ID = $arOffers["OFFERS_PROPERTY_ID"];
		}
	}

	if($ELEMENT_ID)
	{
		static $arPropCache = array();
		if(!array_key_exists($IBLOCK_ID, $arPropCache))
		{
			//Check for MINIMAL_PRICE property
			$rsProperty = CIBlockProperty::GetByID("MINIMUM_PRICE", $IBLOCK_ID);
			$arProperty = $rsProperty->Fetch();
			if($arProperty)
				$arPropCache[$IBLOCK_ID] = $arProperty["ID"];
			else
				$arPropCache[$IBLOCK_ID] = false;
		}

		if($arPropCache[$IBLOCK_ID])
		{
			//Compose elements filter
			$arProductID = array($ELEMENT_ID);
			if($OFFERS_IBLOCK_ID)
			{
				$rsOffers = CIBlockElement::GetList(
					array(),
					array(
						"IBLOCK_ID" => $OFFERS_IBLOCK_ID,
						"PROPERTY_".$OFFERS_PROPERTY_ID => $ELEMENT_ID,
					),
					false,
					false,
					array("ID")
				);
				while($arOffer = $rsOffers->Fetch())
					$arProductID[] = $arOffer["ID"];
			}

			$minPrice = false;
			$maxPrice = false;
			//Get prices
			$rsPrices = CPrice::GetList(
				array(),
				array(
					"BASE" => "Y",
					"PRODUCT_ID" => $arProductID,
				)
			);
			while($arPrice = $rsPrices->Fetch())
			{
				$PRICE = $arPrice["PRICE"];

				if($minPrice === false || $minPrice > $PRICE)
					$minPrice = $PRICE;

				if($maxPrice === false || $maxPrice < $PRICE)
					$maxPrice = $PRICE;
			}

			//Save found minimal price into property
			if($minPrice !== false)
			{
				CIBlockElement::SetPropertyValuesEx(
					$ELEMENT_ID,
					$IBLOCK_ID,
					array(
						"MINIMUM_PRICE" => $minPrice,
						"MAXIMUM_PRICE" => $maxPrice,
					)
				);
			}
		}
	}
}


function resizeImage($id, $width, $height, $type = 3)
{
	$arReturn = array();
	if (is_array($id)) {
		$arReturn["ALT"] = $id["ALT"];
		$arReturn["TITLE"] = $id["TITLE"];
	}
	$type = $type >= 1 && $type <= 3 ? $type : 3;
	$arTypeResize = array(
		1 => BX_RESIZE_IMAGE_EXACT,
		2 => BX_RESIZE_IMAGE_PROPORTIONAL,
		3 => BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
	);

	return array_merge($arReturn, array_change_key_case(CFile::ResizeImageGet(
		$id,
		array("width" => $width, "height" => $height),
		$arTypeResize[$type],
		true,
		false,
		false,
		100
	),
		CASE_UPPER));
}

AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);

function _Check404Error()
{
	if (defined('ERROR_404') && ERROR_404 == 'Y' || CHTTP::GetLastStatus() == "404 Not Found") {
		GLOBAL $APPLICATION;
		$APPLICATION->RestartBuffer();
		require $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
		require $_SERVER['DOCUMENT_ROOT'] . '/404.php';
		require $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
	}
}

function morph($n, $f1, $f2, $f5)
{
	$n = abs(intval($n)) % 100;
	if ($n > 10 && $n < 20) return $f5;
	$n = $n % 10;
	if ($n > 1 && $n < 5) return $f2;
	if ($n == 1) return $f1;
	return $f5;
}
function mainProductNumberWordEndings($num) 
{
	if (strlen($num)>1 && substr($num, strlen($num)-2, 1)=="1") {
		return GetMessage("SMALL_BASKET_MODEL");
	}
	else {
		$c = IntVal(substr($num, strlen($num)-1, 1));
		if ($c==0 || ($c>=5 && $c<=9))
			return GetMessage("SMALL_BASKET_TOP21");
		elseif ($c==1)
			return GetMessage("SMALL_BASKET_TOP22");
		else
			return GetMessage("SMALL_BASKET_TOP23");
	}
}

function curl_get($host, $referer = null) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_REFERER, $referer);
    curl_setopt($ch, CURLOPT_USERAGENT, "Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.9.168 Version/11.51");
    curl_setopt($ch, CURLOPT_URL, $host);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $html = curl_exec($ch);
    echo curl_error($ch);
    curl_close($ch);
    return $html;
}

AddEventHandler("sale", "OnOrderNewSendEmail", ["PhpDevOrgEvents", "OnOrderNewSendEmail"]);
AddEventHandler("sale", "OnSaleCalculateOrder", ["PhpDevOrgEvents", "OnSaleCalculateOrder"]);

class PhpDevOrgEvents
{
    static public function OnOrderNewSendEmail($orderID, &$eventName, &$arFields)
    {
        CModule::IncludeModule("sale");
        $userPhone = '';
        $userAddress = '';
        $db_props = CSaleOrderPropsValue::GetOrderProps($orderID);
        while ($arProps = $db_props->Fetch())
        {
            if ($arProps['CODE'] == 'PHONE') {
                $userPhone = $arProps['VALUE'];
            }
            if ($arProps['CODE'] == 'LOCATION') {
                $arVal = CSaleLocation::GetByID($arProps["VALUE"], LANGUAGE_ID);
                $userAddress = htmlspecialchars($arVal["COUNTRY_NAME"].", ".$arVal["CITY_NAME"]);
            }
            if ($arProps['CODE'] == 'ADDRESS') {
                $userAddress .= ', ' . $arProps["VALUE"];
            }
        }

        $arFields['USER_PHONE'] = $userPhone;
        $arFields['USER_DELIVERY_ADDRESS'] = $userAddress;
		
		\Order::makeNewOrderLetterFields($arFields);
    }

    static public function OnSaleCalculateOrder(&$arOrder){
        $pay_system = 1;//платежная на которую не распотроняется изменение доставки
        $delivery_ids_10k = array(1, 6);
        $delivery_ids_7k = array(34, 35);
        if ($arOrder['PAY_SYSTEM_ID'] != $pay_system) {
            if (in_array($arOrder['DELIVERY_ID'], $delivery_ids_10k) && $arOrder['ORDER_PRICE'] > 10000) {
                $arOrder['PRICE'] = $arOrder['PRICE'] - $arOrder['DELIVERY_PRICE'];
                $arOrder['DELIVERY_PRICE'] = 0;
                $arOrder['PRICE_DELIVERY'] = 0;
            }
            if (in_array($arOrder['DELIVERY_ID'], $delivery_ids_7k) && $arOrder['ORDER_PRICE'] > 7000) {
                $arOrder['PRICE'] = $arOrder['PRICE'] - $arOrder['DELIVERY_PRICE'];
                $arOrder['DELIVERY_PRICE'] = 0;
                $arOrder['PRICE_DELIVERY'] = 0;
            }
        }
    }
}
?>