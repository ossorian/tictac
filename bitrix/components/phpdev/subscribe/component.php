<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule("iblock")) die('CIBlockElement не подключен!!');

$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());

if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 3600;
if ($arParams["CACHE_TYPE"] == "N" || ($arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "N"))
    $arParams["CACHE_TIME"] = 0;

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["PARAMS_HASH"]) && $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]) {
    $APPLICATION->RestartBuffer();



	if (empty($_POST["recaptcha_response"])) {
		echo json_encode(array('ERROR' => 'Пройдите проверку от ботов ниже.'));
		die();
	}
	$response = $_POST["recaptcha_response"];
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	  $data = [
		'secret' => '6LdFqPwUAAAAACEll_oGR0HkMkfBMhBxZeer2W2L',
		'response' => $_POST["recaptcha_response"]
	  ];
	$options = [
		'http' => [
		  'method' => 'POST',
		  'content' => http_build_query($data)
		]
	  ];
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$result=json_decode($verify);
	if (!$result->success) {
		echo json_encode(array('ERROR' => 'Проверка от ботов пройдена ошибочно!'));
		die();
	}

	if (empty($_POST['EMAIL'])) {
		echo json_encode(array('ERROR' => 'Укажите ваш электронный адрес для рассылки.'));
		die();
	}

    if (check_bitrix_sessid()) {
        if (strlen($_POST["EMAIL"]) > 1 && !check_email($_POST["EMAIL"])) die();
        if (!$_POST['NAME']) die();

        $el = new CIBlockElement;

        $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'PROPERTY_EMAIL' => $_POST['EMAIL']);
        $res = $el->GetList(array(), $arFilter);
        if ($ob = $res->GetNextElement()){
            echo json_encode(array('ERROR' => 'Вы уже подписаны на рассылку!'));
            die();
        }


        $arInsert = Array(
            "IBLOCK_SECTION" => false,
            "IBLOCK_CODE" => 'phpdev_subscriber',
            "IBLOCK_ID"      => $arParams['IBLOCK_ID'],
            "PROPERTY_VALUES"=> array('EMAIL' => $_POST['EMAIL']),
            "NAME"           => $_POST['NAME'],
            "ACTIVE"         => "Y",            // активен
        );

        $res = $el->Add($arInsert);
		\Emails::sendSubsribe($_POST['EMAIL']);
        echo json_encode(array('SUCCESS' => 'Y'));
    }

    die();
}

$this->IncludeComponentTemplate();
?>
