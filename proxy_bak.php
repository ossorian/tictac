<?php require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");
header("Content-Disposition: attachment; filename=yandex.xml");
print "<?xml version='1.0' encoding='UTF-8'?>";
?>

<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?=date("Y-m-d H:i")?>">
<shop>
<name>Интернет-магазин tictactoy.ru</name>
<company>Интернет-магазин tictactoy.ru</company>
<url>http://tictactoy.ru/</url>
<platform>1C-Bitrix</platform>
<currencies><currency id="RUB" rate="1" plus="0"/></currencies>
<categories>
<?php

$arFilter = Array('IBLOCK_ID' => 5, 'GLOBAL_ACTIVE'=>'Y');
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {
    ?>
	<category id="<?=$ar_result["ID"]?>" parentId="<?=$ar_result["IBLOCK_SECTION_ID"]?>"><?=$ar_result["NAME"]?></category>	
	<?
  }
  ?>  
</categories>
<offers> 
<?php
$arSelect = Array("ID","CODE","NAME","MIN_PRICE","CATALOG_GROUP_1","IBLOCK_ID","IBLOCK_SECTION_ID","DETAIL_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID" => 5, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
  $arFields = $ob->GetFields();  
 ?>
<offer id="<?=$arFields["ID"]?>"  available="true">
	<url>http://<?=$_SERVER["SERVER_NAME"]?><?=$arFields["DETAIL_PAGE_URL"]?></url>
	<price><?=$arFields["CATALOG_PRICE_1"];?></price>
	<currencyId>RUB</currencyId>
	<categoryId><?=$arFields["IBLOCK_SECTION_ID"]?></categoryId>
	<picture>http://<?=$_SERVER["SERVER_NAME"]?><?=CFile::GetPath($arFields["DETAIL_PICTURE"])?></picture>
	<name><?=str_replace("&","&amp;",strip_tags(htmlspecialchars_decode($arFields["NAME"])))?></name>
	<description><?=strip_tags($arFields["PREVIEW_TEXT"]);?></description>
	<sales_notes>Оплата наличными, пластиковой картой, Б/Н</sales_notes>
	<manufacturer_warranty>true</manufacturer_warranty>
	<local_delivery_cost>500</local_delivery_cost>
</offer>
<? 
 }
?></offers>


</shop></yml_catalog>