<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("TicTacToy.ru");
?>

    <!-- BY COLOR (По цвету) -->
    <div class="header__body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header__banner">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_her/includes/include_main_image.php",
                            Array(),
                            Array("MODE" => "text", "NAME" => "промо текст")
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="by-color sections">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <h2>Украшения</h2>
                </div>
            </div>

                        <?
                        global $indexFilter;
                        $indexFilter = array("SECTION_ID" => 171, "INCLUDE_SUBSECTIONS" => "Y");
                        ?>
                        <?$APPLICATION->IncludeComponent("bitrix:catalog.section", "for_her_slider", Array(
                            "AJAX_MODE" => "N",	// Включить режим AJAX
                            "IBLOCK_TYPE" => "TicTacToy",	// Тип инфо-блока
                            "IBLOCK_ID" => "5",	// Инфо-блок
                            "SECTION_ID" => "",	// ID раздела
                            "SECTION_CODE" => "",	// Код раздела
                            "SECTION_USER_FIELDS" => "",	// Свойства раздела
                            "ELEMENT_SORT_FIELD" => "rand",	// По какому полю сортируем элементы
                            "ELEMENT_SORT_ORDER" => "ASC",	// Порядок сортировки элементов
                            "FILTER_NAME" => "indexFilter",	// Имя массива со значениями фильтра для фильтрации элементов
                            "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                            "SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
                            "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
                            "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
                            "BASKET_URL" => "/basket/",	// URL, ведущий на страницу с корзиной покупателя
                            "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
                            "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
                            "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
                            "SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
                            "META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
                            "META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
                            "BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
                            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                            "DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
                            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                            "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                            "PAGE_ELEMENT_COUNT" => "8",	// Количество элементов на странице
                            "LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
                            "PROPERTY_CODE" => array(	// Свойства
                                0 => "NEW",
                                1 => "IMAGE",
                            ),
                            "PRICE_CODE" => array(	// Тип цены
                                0 => "BASE",
                            ),
                            "USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
                            "SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
                            "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
                            "PRODUCT_PROPERTIES" => "",	// Характеристики товара
                            "USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
                            "CACHE_TYPE" => "A",	// Тип кеширования
                            "CACHE_TIME" => "3600",	// Время кеширования (сек.)
                            "CACHE_FILTER" => "Y",	// Кэшировать при установленном фильтре
                            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                            "PAGER_TITLE" => "Товары",	// Название категорий
                            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                            "PAGER_TEMPLATE" => "",	// Название шаблона
                            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",	// Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        ),
                            false
                        );?>

            <? $APPLICATION->IncludeFile(
                SITE_DIR . "for_her/includes/include_filter_block.php",
                Array(),
                Array("MODE" => "text", "NAME" => "фильтр")
            ); ?>
            <div class="by-color__items_big">
                <div class="row">
                    <div class="col-sm-6">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_her/includes/include_1_block.php",
                            Array(),
                            Array("MODE" => "text", "NAME" => "блок")
                        ); ?>
                    </div>
                    <div class="col-sm-6">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_her/includes/include_2_block.php",
                            Array(),
                            Array("MODE" => "text", "NAME" => "блок")
                        ); ?>
                    </div>
                    <div class="col-sm-6">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_her/includes/include_3_block.php",
                            Array(),
                            Array("MODE" => "text", "NAME" => "блок")
                        ); ?>
                    </div>

                    <div class="col-sm-6">
                        <div class="bc-foot__slider">
                            <?
                            require_once($_SERVER['DOCUMENT_ROOT'] . '/for_her/includes/include_filter_block.php');
                            $minimum_price = defined('MINIMUM_PRICE_WOMEN') ? MINIMUM_PRICE_WOMEN : 0;
                            $maximum_price = defined('MAXIMUM_PRICE_WOMEN') ? MAXIMUM_PRICE_WOMEN : 0;

                            $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND_MODEL", "PROPERTY_FILTER_COLOR", "CATALOG_PRICE_1");
                            $arFilter = array(
                                "IBLOCK_ID" => 5,
                                "ACTIVE" => "Y",
                                "><CATALOG_PRICE_1" => array($minimum_price, $maximum_price),
                                "PROPERTY_SEX" => 7,
                                "IBLOCK_SECTION_ID" => array(146, 165, 152, 171),
                                "SECTION_ACTIVE" => "Y"
                            );
                            $position = 0 ;
                            $resElem = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 10), $arSelect);
                            while ($arElem = $resElem->GetNext()) {
                                $position++;
                                $picture = CFile::GetPath($arElem['PREVIEW_PICTURE']);
                                ?>
                                <a href="<?= $arElem['DETAIL_PAGE_URL'] ?>"
                                   data-seo="Для нее"
                                   onclick="
                                       dataLayer.push({
                                       'event': 'productClick',
                                       'ecommerce': {
                                       'click': {
                                       'actionField': {'list': 'Для нее'},     // Список с которого был клик.
                                       'products': [{
                                       'name': '<?= $arElem['NAME']?>',       	// Наименование товара.
                                       'id': '<?= $arElem['ID']?>',  				// Идентификатор товара в базе данных.
                                       'price': '<?= $arElem['CATALOG_PRICE_1']?>',   				// Цена одной единицы купленного товара
                                       'brand': '<?= $arElem['PROPERTY_BRAND_MODEL_VALUE']?>',  			 //Бренд товара
                                       'category': 'часы', 			 //Категория товара
                                       'variant': '<?= $arElem['PROPERTY_FILTER_COLOR_VALUE'] ?>',  				//Вариант товара, например цвет
                                       'position': <?= $position?>  				//Позиция товара в списке
                                       }]
                                       }
                                       }
                                       });"
                                >
                                    <div class="slide">
                                        <div class="bc-foot__slide">
                                            <img src="<?= $picture ?>" alt="">
                                            <p class="item__name"><?= $arElem['PROPERTY_BRAND_MODEL_VALUE'] ?></p>
                                            <p class="item__name"><?= $arElem['NAME'] ?></p>
                                        </div>
                                    </div>
                                </a>
                                <?
                            }
                            ?>
                        </div>
                        <div class="bc-foot__text">
                            <? $APPLICATION->IncludeFile(
                                SITE_DIR . "for_her/includes/include_filter_text_block.php",
                                Array(),
                                Array("MODE" => "text", "NAME" => "заголовок")
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- BY COLOR (По цвету) -->


    <!-- SALE (РАСПРОДАЖА) -->
    <section class="sections sale">
        <? $APPLICATION->IncludeFile(
            SITE_DIR . "for_her/includes/include_promo_text.php",
            Array(),
            Array("MODE" => "text", "NAME" => "промо текст")
        ); ?>
        <div class="container">
            <div class="sale__banner">
                <div class="row">
                    <div class="col-xs-12">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_her/includes/include_sunglasses_block.php",
                            Array(),
                            Array("MODE" => "html", "NAME" => "блок")
                        ); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="sale__slider">
                        <?
                        $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND_MODEL", "CATALOG_PRICE_1", "PROPERTY_FILTER_COLOR");
                        $arFilter = array(
                            "IBLOCK_ID" => 5,
                            "ACTIVE" => "Y",
                            ">CATALOG_PRICE_1" => 0,
                            "PROPERTY_SEX" => 7,
                            "SECTION_ID" => 165,
                            "SECTION_ACTIVE" => "Y",
//                            "INCLUDE_SUBSECTIONS" => "Y"
                        );
                        $resElem = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 16), $arSelect);
                        while ($arElem = $resElem->GetNext()) {
                            $picture = CFile::GetPath($arElem['PREVIEW_PICTURE']);
                            ?>
                            <a href="<?= $arElem['DETAIL_PAGE_URL'] ?>"
                               data-seo="Для нее - очки"
                               onclick="
                                   dataLayer.push({
                                   'event': 'productClick',
                                   'ecommerce': {
                                   'click': {
                                   'actionField': {'list': 'Для нее - очки'},     // Список с которого был клик.
                                   'products': [{
                                   'name': '<?= $arElem['NAME']?>',       	// Наименование товара.
                                   'id': '<?= $arElem['ID']?>',  				// Идентификатор товара в базе данных.
                                   'price': '<?= $arElem['CATALOG_PRICE_1']?>',   				// Цена одной единицы купленного товара
                                   'brand': '<?= $arElem['PROPERTY_BRAND_MODEL_VALUE']?>',  			 //Бренд товара
                                   'category': 'очки', 			 //Категория товара
                                   'variant': '<?= $arElem['PROPERTY_FILTER_COLOR_VALUE'] ?>',  				//Вариант товара, например цвет
                                   'position': <?= $position?>  				//Позиция товара в списке
                                   }]
                                   }
                                   }
                                   });"
                            >
                                <div class="slide">
                                    <div class="dropdown__item">
                                        <div class="dd-item-img__wrap">
                                            <img src="<?= $picture ?>" class="dd-item__img"
                                                 alt="<?= $arElem['NAME'] ?>">
                                        </div>
                                        <p class="dd-item__name"><?= $arElem['NAME'] ?></p>
                                    </div>
                                </div>
                            </a>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="sale__banner">
                <div class="row">
                    <div class="col-xs-12">
                        <? $APPLICATION->IncludeFile(
                            SITE_DIR . "for_her/includes/include_accessories_block.php",
                            Array(),
                            Array("MODE" => "html", "NAME" => "блок")
                        ); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="sale__slider">
                        <?
                        $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_BRAND_MODEL", "CATALOG_PRICE_1", "PROPERTY_FILTER_COLOR");
                        $arFilter = array(
                            "IBLOCK_ID" => 5,
                            "ACTIVE" => "Y",
                            ">CATALOG_PRICE_1" => 0,
                            "PROPERTY_SEX" => 7,
                            "SECTION_ID" => 171,
                            "SECTION_ACTIVE" => "Y"
                        );
                        $resElem = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 16), $arSelect);
                        while ($arElem = $resElem->GetNext()) {
                            $picture = CFile::GetPath($arElem['PREVIEW_PICTURE']);
                            ?>
                            <a href="<?= $arElem['DETAIL_PAGE_URL'] ?>"
                               data-seo="Для нее - украшения"
                               onclick="
                                   dataLayer.push({
                                   'event': 'productClick',
                                   'ecommerce': {
                                   'click': {
                                   'actionField': {'list': 'Для нее - украшения'},     // Список с которого был клик.
                                   'products': [{
                                   'name': '<?= $arElem['NAME']?>',       	// Наименование товара.
                                   'id': '<?= $arElem['ID']?>',  				// Идентификатор товара в базе данных.
                                   'price': '<?= $arElem['CATALOG_PRICE_1']?>',   				// Цена одной единицы купленного товара
                                   'brand': '<?= $arElem['PROPERTY_BRAND_MODEL_VALUE']?>',  			 //Бренд товара
                                   'category': 'украшения', 			 //Категория товара
                                   'variant': '<?= $arElem['PROPERTY_FILTER_COLOR_VALUE'] ?>',  				//Вариант товара, например цвет
                                   'position': <?= $position?>  				//Позиция товара в списке
                                   }]
                                   }
                                   }
                                   });"
                            >
                                <div class="slide">
                                    <div class="dropdown__item">
                                        <div class="dd-item-img__wrap">
                                            <img src="<?= $picture ?>" class="dd-item__img"
                                                 alt="<?= $arElem['NAME'] ?>">
                                        </div>
                                        <p class="dd-item__name"><?= $arElem['NAME'] ?></p>
                                    </div>
                                </div>
                            </a>
                            <?
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SALE (РАСПРОДАЖА) -->
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>