<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

define('URL_EN', 'http://itsmywrist.com');
define('IBLOCK_ID', 16);
$PRICE_CODE = array(
    0 => "BASE",
);
$NO_SECTION = array(
    82, 83
);
ignore_user_abort(true);
set_time_limit(0);

function writeXls($colum, $data) {
    global $xls;
    $row = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O');
    foreach ($data as $key => $value) {
        $xls->setCellValue($row[$key] . $colum, html_entity_decode($value));
    }
}

//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
//error_reporting(E_ALL&&~E_NOTICE);
require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/include/Classes.PHPExcel/PHPExcel.php';
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");

$objPHPExcel->setActiveSheetIndex(0);

$xls = $objPHPExcel->getActiveSheet();
$colum = 1;
$myArr = array('id', 'title', 'description', 'google product category', 'product type', 'link', 'additional image link', 'condition', 'price', 'availability', 'image link', 'brand', 'gender', 'color', 'identifier');
$xls->getStyle("A{$colum}:O{$colum}")->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        )
);
writeXls($colum++, $myArr);
$xls->getColumnDimension('A')->setWidth(15);
$xls->getColumnDimension('B')->setWidth(40);
$xls->getColumnDimension('C')->setWidth(60);
$xls->getColumnDimension('D')->setWidth(35);
$xls->getColumnDimension('E')->setWidth(15);
$xls->getColumnDimension('F')->setWidth(60);
$xls->getColumnDimension('G')->setWidth(70);
$xls->getColumnDimension('H')->setWidth(15);
$xls->getColumnDimension('I')->setWidth(15);
$xls->getColumnDimension('J')->setWidth(15);
$xls->getColumnDimension('K')->setWidth(70);
$xls->getColumnDimension('L')->setWidth(15);
$xls->getColumnDimension('M')->setWidth(15);
$xls->getColumnDimension('N')->setWidth(15);
$xls->getColumnDimension('O')->setWidth(15);

if (!CModule::IncludeModule('iblock') or ! CModule::IncludeModule('catalog')) {
    exit;
}
$arResultPrices = CIBlockPriceTools::GetCatalogPrices(IBLOCK_ID, $PRICE_CODE);
$arConvertParams = array();


//preMemory();
$arSelect = array('ID', 'IBLOCK_ID', 'NAME', 'CATALOG_QUANTITY', 'CATALOG_GROUP_1', 'DETAIL_PAGE_URL', "DETAIL_PICTURE", "PREVIEW_PICTURE", 'PROPERT_SEX', 'PREVIEW_TEXT', 'DETAIL_TEXT');
$arFilter = Array(
    'TYPE' => 'tictactoy_en',
    'IBLOCK_ID' => IBLOCK_ID,
    'ACTIVE' => 'Y',
);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while ($obElement = $res->GetNextElement()) {
    $arItem = $obElement->GetFields();
//    if($arItem['ID']!=11253) {
//        continue;
//    }
    $PROPERTIES = $obElement->GetProperties();

    $url = URL_EN . $arItem['DETAIL_PAGE_URL'];
//    if ($content = file_get_contents($url)) {
//        preg_match('~<title>(.+)</title>~iUus', $content, $tmp);
//        if (!empty($tmp[1])) {
//            $arItem['TITLE'] = $tmp[1];
//        }
//        preg_match('~<META NAME="description" CONTENT="(.+)">~iUus', $content, $tmp);
//        if (!empty($tmp[1])) {
//            $arItem['META_DESCRIPTION'] = $tmp[1];
//        }
//    }
//    $arItem['CAT_PRICES'] = $arResultPrices;

    $dbPrice = CPrice::GetList(
                    array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"), array("PRODUCT_ID" => $arItem['ID']), false, false, array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
    );
    while ($arPrice = $dbPrice->Fetch()) {
        $arItem['PRICE'] = CIBlockPriceTools::GetItemPrices(IBLOCK_ID, $arResultPrices, $arItem, true, $arConvertParams, 0, 's2');
    }

    $db_old_groups = CIBlockElement::GetElementGroups($arItem['ID'], true);
    $arItem['SECTION_NAME'] = '';
    while ($ar_group = $db_old_groups->Fetch()) {
        if (!in_array($ar_group["ID"], $NO_SECTION)) {
            $arItem['SECTION_NAME'] = $ar_group['NAME'];
        }
    }

    $DISPLAY_PROPERTIES = array();
    foreach (array('GALLERY') as $pid) {
        $prop = &$PROPERTIES[$pid];
        if ((is_array($prop["VALUE"]) && count($prop["VALUE"]) > 0) ||
                (!is_array($prop["VALUE"]) && strlen($prop["VALUE"]) > 0)) {
            $DISPLAY_PROPERTIES[$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop);
        }
    }
    $arItem["IMAGE"] = $arItem["ADDATION"] = '';
    if (!empty($DISPLAY_PROPERTIES['GALLERY']['FILE_VALUE'])) {
        if (isset($DISPLAY_PROPERTIES['GALLERY']['FILE_VALUE']['ID'])) {
            $arItem["IMAGE"] = URL_EN . $DISPLAY_PROPERTIES['GALLERY']['FILE_VALUE']['SRC'];
        } else {
            foreach ($DISPLAY_PROPERTIES['GALLERY']['FILE_VALUE'] as $value) {
                if (empty($arItem["IMAGE"])) {
                    $arItem["IMAGE"] = URL_EN . $value['SRC'];
                } else {
                    $arItem["ADDATION"] = URL_EN . $value['SRC'];
                    break;
                }
            }
        }
    }

    $arItem['DESCRIPTION'] = strip_tags($arItem['PREVIEW_TEXT'] . $arItem['DETAIL_TEXT'], '<script>');
    $arItem['DESCRIPTION'] = trim(preg_replace('~(<script[^>]*>.*</script>)~imUsS', '', $arItem['DESCRIPTION']));
    $arItem['DESCRIPTION'] = preg_replace('~(\s+)~imUsS', " ", $arItem['DESCRIPTION']);
    $arItem['DESCRIPTION'] = TruncateText(trim($arItem['DESCRIPTION']), 500);
    $content = preg_replace('~( )~', '', $arItem['DESCRIPTION']);
    $content = str_replace(array(' ', "\t", "\n", "\r", '&nbsp;'), '', $content);
    $isRus = preg_match('~([а-яЁ])~iUS', $content);
    if (empty($arItem['DESCRIPTION']) or empty($content) or $isRus) {
        continue;
    }

    if (empty($PROPERTIES['SEX']['VALUE']) or count($PROPERTIES['SEX']['VALUE']) > 1) {
        $gender = 'unisex';
    } else {
        $gender = each($PROPERTIES['SEX']['VALUE']);
        $gender = $gender['value'] === 'Men' ? 'male' : 'female';
    }
    $data = array(
        $arItem['ID'],
        $arItem['NAME'],
        $arItem['DESCRIPTION'],
        'Apparel & Accessories > Jewelry > Watch Accessories',
        'Jewelry > Watch Accessories',
        $url,
        $arItem['ADDATION'],
        empty($PROPERTIES['NEW']['VALUE']) ? '' : 'NEW',
        str_replace(array('&euro;', ''), '', $arItem['PRICE']['BASE']['PRINT_DISCOUNT_VALUE']) . ' €',
        $arItem['CATALOG_QUANTITY'] > 1 ? 'in stock' : '',
        $arItem['IMAGE'],
        $arItem['SECTION_NAME'],
        $gender,
        '',
        'false'
    );
//
//    pre($isRus, $arItem['DESCRIPTION'], $content);
//    pre($data, $arItem);
//    exit;
//    $xls->getStyle("A{$colum}:O{$colum}")->getAlignment()->setWrapText(true);
    $xls->getStyle("A{$colum}:O{$colum}")->applyFromArray(
            array(
                'alignment' => array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                    'wrap' => true
                ),
            )
    );
    writeXls($colum++, $data);
}

//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//header('Content-Disposition: attachment;filename="01simple.xlsx"');
//header('Cache-Control: max-age=0');
//// If you're serving to IE 9, then the following may be needed
//header('Cache-Control: max-age=1');
//
//// If you're serving to IE over SSL, then the following may be needed
//header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
//header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
//header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
//header('Pragma: public'); // HTTP/1.0
//
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(__DIR__ . '/exel.xlsx');
//$objWriter->save('php://output');
preMemory();
exit;
?>