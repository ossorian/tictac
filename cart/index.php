<?
if (!isset($_GET['ORDER_ID']) && empty($_GET['ORDER_ID'])) {
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("TicTacToy.ru");
?>
<? // if (!isset($_GET['ORDER_ID']) && !empty($_GET['ORDER_ID'])) { ?>
    <section class="checkout sections">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Оформить покупку</h2>
                </div>
            </div>

            <? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "main_basket", array(
                "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                "COLUMNS_LIST" => array(
                    0 => "NAME",
                    1 => "PRICE",
                    2 => "TYPE",
                    3 => "QUANTITY",
                    4 => "DELETE",
                    5 => "DISCOUNT",
                ),
                "PATH_TO_ORDER" => "/order/",
                "HIDE_COUPON" => "N",
                "QUANTITY_FLOAT" => "N",
                "PRICE_VAT_SHOW_VALUE" => "N",
                "SET_TITLE" => "N"
            ),
                false
            ); ?>

            <? if (!$USER->IsAuthorized()) { ?>
                <div class="row hidden-xs">
                    <div class="col-xs-12">
                        <p class="i-registered">Я зарегистрирован на сайте</p>
                        <a class="btn btn_black login__btn">Войти на сайт</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="auth">
                            <div class="auth__content">
								<span class="auth__close">
									<i class="icon-icon-small-close"></i>
								</span>
                                <div class="ch-auth__tab-item">
                                    <?
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:system.auth.form",
                                        "auth_in_order",
                                        Array(
                                            "COMPONENT_TEMPLATE" => "main_auth",
                                            "FORGOT_PASSWORD_URL" => "",
                                            "PROFILE_URL" => "",
                                            "REGISTER_URL" => "",
                                            "SHOW_ERRORS" => "Y"
                                        )
                                    );
                                    ?>
                                </div>
                                <div class="forgot-pass__content hidden">
                                    <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "change_pass", Array(
                                        "FORGOT_PASSWORD_URL" => "",    // Страница забытого пароля
                                        "PROFILE_URL" => "",    // Страница профиля
                                        "REGISTER_URL" => "",    // Страница регистрации
                                        "SHOW_ERRORS" => "Y",    // Показывать ошибки
                                    ),
                                        false
                                    ); ?>
                                    <?
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:system.auth.forgotpasswd",
                                        "forgot_pass_order",
                                        Array(
                                            "AJAX_MODE" => "Y",
                                            "AJAX_OPTION_SHADOW" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N"
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>

            <? $APPLICATION->IncludeComponent("bitrix:sale.order.ajax", "basket_order", Array(
                "PAY_FROM_ACCOUNT" => "N",    // Позволять оплачивать с внутреннего счета
                "COUNT_DELIVERY_TAX" => "N",    // Рассчитывать налог для доставки
                "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",    // Позволять оплачивать с внутреннего счета только в полном объеме
                "ALLOW_AUTO_REGISTER" => "Y",    // Оформлять заказ с автоматической регистрацией пользователя
                "SEND_NEW_USER_NOTIFY" => "N",    // Отправлять пользователю письмо, что он зарегистрирован на сайте
                "DELIVERY_NO_AJAX" => "Y",    // Рассчитывать стоимость доставки сразу
                "PROP_1" => "",    // Не показывать свойства для типа плательщика "Физическое лицо" (s1)
                "PROP_2" => "",    // Не показывать свойства для типа плательщика "Юридическое лицо" (s1)
                "PATH_TO_BASKET" => "/",    // Страница корзины
                "PATH_TO_PERSONAL" => "",    // Страница персонального раздела
                "PATH_TO_PAYMENT" => "/order/payment/",    // Страница подключения платежной системы
                "PATH_TO_AUTH" => "",    // Страница авторизации
                "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
            ),
                false
            ); ?>
        </div>
    </section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<? } else {

    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
    <meta charset="utf-8">

    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle() ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?= SITE_TEMPLATE_PATH ?>/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?= SITE_TEMPLATE_PATH ?>/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?= SITE_TEMPLATE_PATH ?>/img/favicon/apple-touch-icon-114x114.png">

    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/libs/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/libs/Magnific-Popup/magnific-popup.css">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/fonts/TTT__icons/css/ttt.css">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/libs/slick/slick.css">

    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/css/fonts.css">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/css/main.css">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/css/media.css">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TJMVXRZ');</script>
    <!-- End Google Tag Manager -->

    <!-- Yandex.Metrika counter -->
    <script src="https://mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
    <script type="text/javascript" >
        try {
            var yaCounter186353 = new Ya.Metrika({
                id:186353,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true,
                ecommerce:"dataLayer"
            });
        } catch(e) { }
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/186353" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

<? $APPLICATION->IncludeComponent("bitrix:sale.order.ajax", "basket_order", Array(
    "PAY_FROM_ACCOUNT" => "N",    // Позволять оплачивать с внутреннего счета
    "COUNT_DELIVERY_TAX" => "N",    // Рассчитывать налог для доставки
    "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
    "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",    // Позволять оплачивать с внутреннего счета только в полном объеме
    "ALLOW_AUTO_REGISTER" => "Y",    // Оформлять заказ с автоматической регистрацией пользователя
    "SEND_NEW_USER_NOTIFY" => "N",    // Отправлять пользователю письмо, что он зарегистрирован на сайте
    "DELIVERY_NO_AJAX" => "Y",    // Рассчитывать стоимость доставки сразу
    "PROP_1" => "",    // Не показывать свойства для типа плательщика "Физическое лицо" (s1)
    "PROP_2" => "",    // Не показывать свойства для типа плательщика "Юридическое лицо" (s1)
    "PATH_TO_BASKET" => "/",    // Страница корзины
    "PATH_TO_PERSONAL" => "",    // Страница персонального раздела
    "PATH_TO_PAYMENT" => "/order/payment/",    // Страница подключения платежной системы
    "PATH_TO_AUTH" => "",    // Страница авторизации
    "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
),
    false
); ?>
    <script src="<?= SITE_TEMPLATE_PATH?>/libs/jquery/jquery-1.11.2.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH?>/libs/Magnific-Popup/jquery.magnific-popup.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH?>/libs/slick/slick.js"></script>

    <script src="<?= SITE_TEMPLATE_PATH?>/js/common.js?rev=123wewert3r"></script>


<? } ?>