<?
//header("Content-Type: text/plain; charset=utf8");

$obCache = new CPageCache;
$life_time = 60 * 60;
$cache_id = "vk_social_include_5";

if ($obCache->StartDataCache($life_time, $cache_id, "/")) {

    $data = file_get_contents("http://vk.com/tictactoy_ru");
    $data = str_replace("<span class=\"num_delim\"> </span>", "", $data);
    //preg_match("|([\d]+) " . iconv("UTF-8", "windows-1251", "человек") . "|U", $data, $find);
    $data = str_replace("<em>", "", $data);
    $data = str_replace("</em>", "", $data);
    preg_match("|Участники ([\d]+)<|U", $data, $find);
    $count = $find[1]; // количество участников


    // Если англоязычная версия открылась
    if ($count < 1) {
        preg_match("|Members ([\d\,]+)|", $data, $find);
        $count = str_replace(",", "", $find[1]); // количество участников
    }

    // Если англоязычная версия открылась 2
    if ($count < 1) {
        preg_match("|<em class=\"pm_counter\">([\d\,]+)</a>|", $data, $find);
        $count = str_replace(",", "", $find[1]); // количество участников
    }


    $people = array();
    preg_match_all("#<div class=\"fl_l people_cell\">([\s\S]+)<small></small>#U", $data, $find);

    foreach ($find[1] as $current) {

        // Аватар
        preg_match("|src=\"([\w\.\-\/\:]+)\"|", $current, $ava);
        if (substr($ava[1], 0, 8) == "/images/") $ava[1] = "http://vk.com" . $ava[1];

        // Имя
        preg_match("|event\)\">([\s\S]+)<br />|U", $current, $name);

        // Ссылка
        preg_match("|href=\"([\s\S]+)\" onclick=\"|U", $current, $link);

        $people[] = array(
            "link" => "http://vk.com" . $link[1],
            "name" => iconv("windows-1251", "UTF-8", trim($name[1])),
            "ava" => $ava[1]
        );
    }

    ?>

    <a href="http://vk.com/tictactoy_ru" class="soc__sub soc__sub-vk">
									<span class="soc-sub__icon">
										<i class="icon-icon-vk"></i>
									</span>
        <span><?=number_format($count, 0, ',', ' ')?></span>
    </a>
<?
}
$obCache->EndDataCache();
?>