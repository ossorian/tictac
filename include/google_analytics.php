<!-- GA -->
<script type="text/javascript" src="/js/metricexpert-ga-utils-8-205.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-39386232-1', 'tictactoy.ru');
    ga('require', 'displayfeatures');

    meFirstTouch.track();

    ga('send', 'pageview');
</script>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=HY7kWDpUQ2QhxRfZW4ZywLnSX2fgd*baK4OF5ICPUqWYc6z2vmzdOUBBlpYO/UoTUm7dK51bMvkC/*9wjtUB49Q0JAZ4nkvCld9jCWFtLY5k*Bt6/*u8prES*6exH0yE6mRBTI0ak2fjaTzrp681Vwn/k*h*ODTWfM57D5n*H94-&pixel_id=1000034312';</script>
<!— Facebook Pixel Code —>
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1704759379844791');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1704759379844791&e.."
/></noscript>
<!— DO NOT MODIFY —>
<!— End Facebook Pixel Code —>

<!--
<script src="http://proactionone.ru/scripts/proactionone.init.js"></script>
<script src="http://proactionone.ru/scripts/proactionone.js"></script>
-->

<!-- BEGIN GIFTERY CODE -->
<script type="text/javascript"> var gifteryWidgetParams = { color: '#f16536' }; (function(){ var s = document.createElement('script');s.type = 'text/javascript';s.async = true; s.src = '//widget.giftery.ru/js/119730/11974/'; var ss = document.getElementsByTagName('script')[0];ss.parentNode.insertBefore(s, ss); })(); </script>
<!-- END GIFTERY CODE -->

<!-- BEGIN JIVOSITE CODE -->
<script type='text/javascript'>
(function(){ var widget_id = 'rg1GI6HfhF';var d=document;var w=window;function l(){ var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<!-- END JIVOSITE CODE -->

<script async src="https://cdn.onthe.io/io.js/4kgMdY3Uv679"></script>
