<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
$APPLICATION->SetTitle("404 Страница не найдена");
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
define("IS_404", "Y");
?> <!-- 404 -->
<div class="wrap-404">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="wrap-404_left">
					<h3>Такой страницы нет</h3>
					<p class="wrap-404__desc">
						А&nbsp;если вы уверены, что не ошиблись, то <a href="mailto:<?= COption::GetOptionString('main','email_from');?>" class="link_border">напишите<br>
						<span class="hidden-lg hidden-md hidden-sm dfg">&nbsp;</span>нам</a> — мы разберемся.
					</p>
					<p>
						 Не нашли, что искали? Возможно, Вам поможет поиск по сайту!
					</p>
					<form action="/catalog/" class="wrap-404__search" method="GET">
 <input type="text" name="q" class="wrap-404__input search_visible__input"> <input type="submit" value="Найти" class="btn btn_black wrap-404__btn">
					</form>
 <a href="/" class="logo__link"> <i class="icon-logo"></i> <span class="link_border">На главную</span> </a>
					<p class="wrap-404__p">
						 Также, Вам может помочь карта сайта.
					</p>
					<br>
					<?$APPLICATION->IncludeComponent("bitrix:main.map", "404_map", Array(
						"CACHE_TIME" => "3600",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"COL_NUM" => "2",	// Количество колонок
						"LEVEL" => "3",	// Максимальный уровень вложенности (0 - без вложенности)
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_DESCRIPTION" => "N",	// Показывать описания
					),
						false
					);?>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="wrap-404__img">
 <img src="/local/templates/new_tictac/img/404/404__clock.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</div>
    <!-- 404 -->
<?//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>