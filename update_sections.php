<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
if($_REQUEST['sections_update'] == 'Y'){
    $arSelect = Array();//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>IntVal(5), "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    //get elements
    $all_elements_ids = array();
    while($arResult = $res->GetNext()){
        $all_elements_ids[$arResult['ID']] = $arResult['IBLOCK_SECTION_ID'];
    }
    //get sections
    $all_secions_ids = array();
    $uf_arresult = CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => 5 ), false, array());
    while($uf_value = $uf_arresult->GetNext()){
        $all_secions_ids[$uf_value['ID']] = $uf_value['ACTIVE'];
    }
    //get elements groups
    $elements_w_groups = array();
    foreach ($all_elements_ids as $elem_id => $section_id){
        $db_old_groups = CIBlockElement::GetElementGroups($elem_id, true);
        $ar_new_groups = Array();
        while($ar_group = $db_old_groups->GetNext()){
            if($ar_group['ACTIVE'] == 'Y'){
                $elements_w_groups[$elem_id][] = $ar_group['ID'];
            }
        }
    }
    //update elements
    $el = new CIBlockElement;
    foreach ($elements_w_groups as $id => $ar_group){
        if(count($ar_group) > 0 && $all_elements_ids[$id] != $ar_group[0]){
            $arLoadProductArray = Array(
                "IBLOCK_SECTION_ID" => $ar_group[0]
            );
            $res = $el->Update($id, $arLoadProductArray);
            echo "<pre>";
            var_dump($res);
            echo "</pre>";
        }
    }

}else{
    echo "<pre>";
    print_r('wrong request');
    echo "</pre>";
}

