<?php

class Products {

	public static function makeLetterOrderList($arItems)
	{
		$str = '';
		foreach ($arItems as $product) {
			$str .= <<<PRODUCT
			  <div class="order__item" style="border-bottom:1px solid #e4e4e4;border-top:1px solid #e4e4e4;padding:25px 0">
				 <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;font-size:18px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
					<tbody style="font-size:18px">
					   <tr style="font-size:18px;padding:0;text-align:left;vertical-align:top">
						  <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Roboto,Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%">
							 <table style="border-collapse:collapse;border-spacing:0;font-size:18px;padding:0;text-align:left;vertical-align:top;width:100%">
								<tbody>
									<tr>
										<th>
											<a target="_blank" href="{$product["URL"]}">
												<img src="{$product["PICTURE"]}" alt="{$product["NAME"]}">
											</a>
										</th>
									</tr>
								</tbody>
							</table>
						  </th>
						  <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Roboto,Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%">
							 <table style="border-collapse:collapse;border-spacing:0;font-size:18px;padding:0;text-align:left;vertical-align:top;width:100%">
								<tr style="font-size:18px;padding:0;text-align:left;vertical-align:top">
								   <th style="Margin:0;color:#0a0a0a;font-family:Roboto,Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
									  <div style="font-size:18px">{$product["BRAND"]}</div>
									  <p style="Margin:0;Margin-bottom:20px;color:#0a0a0a;font-family:Roboto,Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin:0;margin-bottom:20px;padding:0;text-align:left">{$product["NAME"]}</p>
									  <div style="font-size:18px">{$product["QUANTITY"]} шт.</div>
									  <p class="order__price" style="Margin:0;Margin-bottom:20px;color:#0a0a0a;font-family:Roboto,Helvetica,Arial,sans-serif;font-size:18px;font-weight:700;line-height:1.3;margin:0;margin-bottom:20px;padding:0;text-align:left">{$product["PRICE"]}₽</p>
								   </th>
								</tr>
							 </table>
						  </th>
					   </tr>
					</tbody>
				 </table>
			  </div>
PRODUCT;
		}
		return $str;
	}

}