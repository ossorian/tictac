<?php
class Emails {
	
	const SAVE_LOG = true;
	
	public static function sendSubsribe($email)
	{
		$arFields["EMAIL"] = $email;
		$arFields["CURRENT_YEAR"] = date('Y');
		self::saveLog('SUBSCRIBE_NEW', $email);
		return \CEvent::Send('SUBSCRIBE_NEW', 's1', $arFields, "N");
	}
	
	public static function sendReminder($email)
	{
		$arFields["EMAIL"] = $email;
		$arFields["CURRENT_YEAR"] = date('Y');
		self::saveLog('REMINDER', $email);
		return \CEvent::Send('REMINDER', 's1', $arFields, "N");
	}

	public static function sendBasketReminder($userID, $arFields, $alreadySent)
	{
		$userData = \CUser::GetByID($userID)->Fetch();
		if ($userData["EMAIL"] && $arFields["ORDER_LIST"]) {
			$arFields["EMAIL"] = $userData["EMAIL"];
			if (in_array($arFields["EMAIL"], $alreadySent)) return false;
			$arFields["CURRENT_YEAR"] = date('Y');
			self::saveLog('BASKET_REMINDER', $arFields["EMAIL"]);
			\CEvent::Send('BASKET_REMINDER', 's1', $arFields, "N");
			return true;
		}
		return false;
	}
	
	private static function saveLog($letterType, $email)
	{
		if (self::SAVE_LOG) {
			$str = date('Y-m-d H:i:s') . ' : ' . $letterType . ' на адрес ' . $email . PHP_EOL;
			file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/local/letterLog.txt', $str, FILE_APPEND);
		}
	}

} 