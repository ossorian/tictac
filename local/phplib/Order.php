<?php

class Order {

	public static function makeNewOrderLetterFields(&$arFields)
	{
		//Сбор данных о продуктах
		if (empty($arFields["ORDER_ID"])) return;
		$dbResult = \CSaleBasket::GetList([], ["ORDER_ID" => $arFields["ORDER_ID"]]);
		$products = [];
		
		while($row = $dbResult->Fetch()) {
			$products[$row["PRODUCT_ID"]] = [
				"PRODUCT_ID" => $row["PRODUCT_ID"],
				"PRICE" => number_format($row["PRICE"], 0, ',', ' '),
				"QUANTITY" => intval($row["QUANTITY"]),
				"NAME" => $row["NAME"]
			];
		}

		if ($products) {
			//Получаем дополнителные данные товаров
			$dbResult = \CiblockElement::GetList([], ["IBLOCK_ID" => 5, "ID" => array_keys($products)], false, false, ["ID", "PREVIEW_PICTURE" ,"PROPERTY_BRAND_MODEL", "NAME", "DETAIL_PAGE_URL"]);
			while ($row = $dbResult->GetNext()) {
				$arProductData[$row["ID"]] = $row;
			}
			
			//Подставляем данные товаров в результат
			foreach ($products as $productID => &$data) {
					$data["URL"] = $arProductData[$productID]["DETAIL_PAGE_URL"];
					$data["BRAND"] = $arProductData[$productID]["PROPERTY_BRAND_MODEL_VALUE"];
					$data["PICTURE"] = \CFile::GetPath($arProductData[$productID]["PREVIEW_PICTURE"]);
			}
		}
		
		//Сбор данных заказа
		$order = \CSaleOrder::GetByID($arFields["ORDER_ID"]);
		$arFields["PRICE_BEFORE_DELIVERY"] = number_format($order["PRICE"] - $arFields["DELIVERY_PRICE"], 0, ',', ' ');
		
		$delivery = \CSaleDelivery::GetByID($order["DELIVERY_ID"]);
		$arFields["DELIVERY_NAME"] = $delivery["NAME"];
		
		$payment = \CSalePaySystem::GetByID($order["PAY_SYSTEM_ID"]);
		$arFields["PAYSYSTEM_NAME"] = $payment["NAME"];
		
		$arFields["ORDER_LIST_NEW"] = \Products::makeLetterOrderList($products);
		$arFields["CURRENT_YEAR"] = date('Y');
	}

}