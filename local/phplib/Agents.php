<?php

class Agents {

	public static function clientReminder()
	{
		global $DB;
		$remindTime1 = time() - 30 * 24 * 60 * 60;
		$remindTime2 = time() - 90 * 24 * 60 * 60;
		$remindDate1 = date('Y-m-d', $remindTime1);
		$remindDate2 = date('Y-m-d', $remindTime2);
		
		$result = $DB->query("SELECT ID, EMAIL FROM `b_user` WHERE DATE(LAST_LOGIN) = '$remindDate1' OR DATE(LAST_LOGIN) = '$remindDate2'");
		while($user = $result->Fetch()) {
			\Emails::sendReminder($user["EMAIL"]);
		}
		return 'Agents::clientReminder();';
	}

	public static function basketReminder()
	{
		Cmodule::IncludeModule('sale');
		Cmodule::IncludeModule('iblock');
		global $DB;
		
		$time = time() - 36000;
		$date = date('d.m.Y H:i:s', $time);
		$date2 = date('Y-m-d H:i:s', $time);
		$dbResult = \CSaleBasket::GetList([], [">DATE_INSERT" => $date, "ORDER_ID" => false]);
		
		//Раскидываем данные корзин по пользователям
		while($row = $dbResult->Fetch()) {
			if ($row["USER_ID"]) {
				$arResult[$row["USER_ID"]][] = [
					"PRODUCT_ID" => $row["PRODUCT_ID"],
					"PRICE" => number_format($row["PRICE"], 0, ',', ' '),
					"QUANTITY" => intval($row["QUANTITY"]),
					"NAME" => $row["NAME"]
				];
				$arProducts[] = $row["PRODUCT_ID"];
			}
		}
		
		//Получаем дополнителные данные товаров
		$dbResult = \CiblockElement::GetList([], ["IBLOCK_ID" => 5, "ID" => $arProducts], false, false, ["ID", "PREVIEW_PICTURE" ,"PROPERTY_BRAND_MODEL", "NAME", "DETAIL_PAGE_URL"]);
		while ($row = $dbResult->GetNext()) {
			$arProductData[$row["ID"]] = $row;
		}
		
		//Подставляем данные товаров в результат
		foreach ($arResult as $userID => &$products) {
			foreach ($products as &$data) {
				$data["URL"] = $arProductData[$data["PRODUCT_ID"]]["DETAIL_PAGE_URL"];
				$data["BRAND"] = $arProductData[$data["PRODUCT_ID"]]["PROPERTY_BRAND_MODEL_VALUE"];
				$data["PICTURE"] = \CFile::GetPath($arProductData[$data["PRODUCT_ID"]]["PREVIEW_PICTURE"]);
			}
		}
		
		//Проверяем последние отпрвленные письма
		$alreadySent = [];
		$dbResult = $DB->query($query = "SELECT * FROM `b_event` WHERE `EVENT_NAME` = 'BASKET_REMINDER' AND `SUCCESS_EXEC` = 'Y' AND `DATE_INSERT` > '{$date2}'"); 
//		echo $query;
		while ($row = $dbResult->Fetch()) {
			$data = unserialize($row["C_FIELDS"]);
			if ($data["EMAIL"]) $alreadySent[] = $data["EMAIL"];
		}
		
//		var_dump($alreadySent);
		
		//Далее преобразовать полученные данные $arResult в список для отправки, настроить время 10 ч. после того как корзину оставили, отправлять письма.
		foreach ($arResult as $userID => $products) {
			$arFields["ORDER_LIST"] = \Products::makeLetterOrderList($products);
			\Emails::sendBasketReminder($userID, $arFields, array_unique($alreadySent));
		}
		return 'Agents::basketReminder();';
	}

}