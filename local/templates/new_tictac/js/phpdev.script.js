function cartDelete(t, e) {
    $.ajax({
        type: "POST", url: "/ajax/cart.php", data: {deleteProductId: t, template: e}, success: function (t) {
            $("#smallcart").html(t), $("#smallcart_mob").html(t), $("div .h-basket").show(), updateBasketLine(), shrinkCart(), $(window).scrollTop() >= 200 ? $("div.h-basket").addClass("basket-relative") : $("div.h-basket").removeClass("basket-relative")
        }
    })
}
function updateBasketLine() {
    $.ajax({
        type: "POST", url: "/ajax/ajax_update_basket_line.php", success: function (t) {
            $("body .basket__count").remove(), $("body div.header__basket").append(t)
        }
    })
}
function quickView(t) {
    if (visited = t, $("img[id^=preload_]").length > 0)return !1;
    var e = $(t.currentTarget).attr("data-id"), a = $(t.currentTarget);
    $.ajax({
        type: "POST", url: "/ajax/ajax_quick_show.php", data: {id: e}, beforeSend: function () {
            a.find("i.icon-icon-quick-view").hide(), a.find("span.quick-view__icon").append('<img id="preload_' + e + '" class="quick_view_img_preload" src="/local/templates/new_tictac/img/preloader.gif">'), a.attr("disabled", "disabled")
        }, success: function (t) {
            $("#qv_popup_content").html(t), $.magnificPopup.open({
                items: {src: "#qv__popup"},
                mainClass: "mfp-fade",
                closeMarkup: '<button class="mfp-close mfp-close__btn icon-icon-small-close"></button>',
                callbacks: {
                    open: function () {
                        $(".quick-view__popup").addClass("quick-view__popup_active")
                    }, close: function () {
                        $(".quick-view__popup").removeClass("quick-view__popup_active")
                    }
                }
            }), $(".one-product__slider").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                asNavFor: ".one-product__slider_navigation",
                dots: !1,
                infinite: !0,
                focusOnSelect: !0,
                arrows: !0,
                swipe: !1,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
                responsive: [{breakpoint: 991, settings: {arrows: !1, dots: !0, focusOnSelect: !1, swipe: !0}}]
            }), $(".one-product__slider_navigation").slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: !0,
                dots: !1,
                infinite: !0,
                asNavFor: ".one-product__slider",
                focusOnSelect: !0,
                centerMode: !1,
                vertical: !0,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
                responsive: [{breakpoint: 1200, settings: {slidesToShow: 4}}, {breakpoint: 500, settings: {arrows: !1}}]
            }), $(window).width() > 991 ? $(".tovar-img").wrap('<span style="display:inline-block"></span>').css("display", "block").parent().zoom({magnify: 1}) : $(".tovar-img").trigger("zoom.destroy")
        }, complete: function (t, i) {
            a.find("i.icon-icon-quick-view").show(), $("#preload_" + e).remove(), $("div.item-nav.slick-current").trigger("click"), $("body.tovar-img").wrap('<span style="display:inline-block"></span>').css("display", "block").parent().zoom({magnify: 1})
        }
    }), t.preventDefault()
}
function cicleIcon(t) {
    ++cicleStep < 9 ? t.addClass("c" + cicleStep) : (cicleStep = 0, t.attr("class", "newgame"), window.clearTimeout(timerCicle))
}
function updateSmallBasketContent(t) {
    $.when($.ajax({
        type: "POST", url: "/ajax/cart.php", data: "template=header_basket", success: function (t) {
            $("#smallcart").html(t), $("p.discount__price").html($(t).find("div.h-basket__total span.rub").parent("span").html()), $("div.order_toggle__info div.order_toggle-text__right:first p.ottr__price").html($(t).find("div.h-basket__total span.rub").parent("span").html());
            var e = 0, a = 0;
            $("div.order_toggle__text div.order_toggle-text__right").each(function () {
                $(this).find("p.ottr__price").hasClass("color_red") ? e = parseInt($(this).find("p.ottr__price").text().replace(" ", "")) : a += parseInt($(this).find("p.ottr__price").text().replace(" ", ""))
            });
			console.log('here' + $(this).find("p.ottr__price").text());
//			if (!a) a = 0;
            var i = a - e;
            $("div.order_toggle__info div.ottr__total div.ottr-total__right p.ottr__price").html(sumFormat(i) + '<span class="rub">₽</span>')
        }
    }), $.ajax({
        type: "POST", url: "/ajax/cart.php", data: "template=header_basket_mob", success: function (t) {
            $("#smallcart_mob").html(t)
        }
    })).done(function () {
        t && ($(window).scrollTop() >= 200 ? $("div.h-basket").addClass("basket-relative") : $("div.h-basket").removeClass("basket-relative"), $(".h-basket").show()), shrinkCart(), $(".header__head_mob .icon-icon-cart").hasClass("icon-icon-small-close") || $(".header__head_mob .icon-icon-cart").addClass("icon-icon-small-close")
    })
}
function sumFormat(t) {
    return t += "", (t = new Array(4 - t.length % 3).join("U") + t).replace(/([0-9U]{3})/g, "$1 ").replace(/U/g, "")
}
function recalculateBasketItems() {
    var t = 0, e = 0, a = 0, i = 0, s = 0, w =0;
    $("div.checkout__table div.checkout-table__body").each(function () {
		
		if ($(this).find("input.warantyCost").prop("checked")) w = parseInt($(this).find("input.warantyCost").data('price'));
		else w = 0;
		
        t++, i = parseFloat($(this).find("input.cart_item_price_for_one").val()), s = parseInt($(this).find("input.quantity__input").val()), e += a = (i + w) * s, $(this).find("span.cart_item_total_price").html(sumFormat(a) + '  <span class="rub">₽</span> ')
    })
}
function updateTotalSumInOrder() {
    alert(parseInt($("div.h-basket__total:first").find("span.rub").parent("span").text().replace(" ", ""))), $("p.discount__price").html($("div.h-basket__total:first").find("span.rub").parent("span").html())
}
function updateQuantity(t, e) {
    $.ajax({
        type: "POST",
        url: "/ajax/ajax_basket_update_quantity.php",
        dataType: "html",
        data: {id: t, quantity: e},
        success: function (result) {
            updateBasketLine(), updateSmallBasketContent(), recalculateBasketItems(), submitForm();
        }
    })
}

function updateQuantity2(t, e, basketID, input) {
    $.ajax({
        type: "POST",
        url: "/ajax/ajax_basket_update_quantity2.php",
        dataType: "json",
        data: {id: t, quantity: e, 'basketID': basketID},
        success: function (res) {
			if (res.basketID) {
				$(input).data('basket', res.basketID);
			}
			else {
				$(input).data('basket', '');
			}
            updateBasketLine(), updateSmallBasketContent(), recalculateBasketItems(), submitForm();
        }
    })
}

function stylizeScrolls() {
    var t = ".checkout-form__head ul.sel-options";
    $(t).addClass("scrollbar-outer"), $(".scrollbar-outer").scrollbar(), $(".checkout-form__head div.sel-options").removeClass("sel-options").css({
        "max-height": "400px",
        width: "100%"
    }), $(t + " > li").css("width", "92%")
}
function checkFavorites() {
    $.ajax({
        type: "POST", url: "/ajax/ajax_check_favorites.php", dataType: "html", success: function (t) {
            parseInt(t) > 0 ? $("div.header__wishlist span.wishlist__icon span.wish").length <= 0 && $("div.header__wishlist span.wishlist__icon").append('<span class="wish"></span>') : $("div.header__wishlist span.wishlist__icon span.wish").remove()
        }
    })
}
function sharePopup(t) {
    window.open(t, "myWindow", "status = 1, height = 500, width = 360, resizable = 0")
}
$(document).ready(function () {
    if ($("#is_product_of_the_day").length > 0 && $('#PROMO_INPUT_HIDDEN').length == 0) {
        var t = $("#SECTION_ID").val(), e = $("#TOTAL_ELEMENT_COUNT").val();
        $.ajax({
            type: "POST",
            url: "/ajax/ajax_product_of_the_day.php",
            data: {SECTION_ID: t, ITEMS_COUNT: e},
            success: function (t) {
                var e = $(t).find("#ITEM_ID").val();
                e && $("div.catalog_to_ajax  div.favorites span.favorites__icon[data-id=" + e + "]").closest("div.col-md-4.col-sm-6.brend__col").remove();
                var a = $(".catalog_to_ajax div.brend__col ");
                a.eq(Math.floor(Math.random() * a.length)).after(t);
                if (window.location.href.indexOf("/catalog/watches/promo/") > -1) {
                    $('div.view_four_items').trigger('click');
                };
            }
        })
    }
    if( $("div.catalog_to_ajax").length > 0 && $('#PROMO_INPUT_HIDDEN').length == 0){
        $.ajax({
            type: "POST",
            url: "/ajax/ajax_init_subscribe.php",
            success: function (t) {
                $(".catalog_to_ajax div.brend__col ").eq(1).after(t);
                if (window.location.href.indexOf("/catalog/watches/promo/") > -1) {
                    $('div.view_four_items').trigger('click');
                };
            }
        });
    }
    if (window.location.href.indexOf("/catalog/watches/promo/") > -1) {
        $('div.view_four_items').trigger('click');
    };
    $("#section_id_init").length > 0 && ($("#section_id").text($("#section_id_init").val()), $("#section_id").attr("data-id", $("#section_id_init").attr("data-id")), $("p.brend__desc").text($("#section_id_init").attr("data-count") + " моделей"))
}), $(document).on("click", "#catalog_add_subscribe", function (t) {
    var e = $(this).closest("form").serialize();
    $.ajax({
        type: "POST", url: "/ajax/ajax_subscribe.php", data: e, success: function (t) {
            $("#catalog_succes_subscribe").text(t)
        }
    }), t.preventDefault()
}), $(document).on("click", ".close-item-of-the-day", function () {
    $(this).parent().parent().css("display", "none"), $.ajax({
        type: "POST",
        url: "/ajax/ajax_product_of_the_day.php",
        data: {DISABLE_PRODUCT_OF_THE_DAY: "Y"},
        success: function (t) {
        }
    })
}), $(document).on("click", ".close-subscribe-container", function () {
    $(this).closest("div.brend__col").css("display", "none"), $.ajax({
        type: "POST",
        url: "/ajax/ajax_init_subscribe.php",
        data: {DISABLE_SUBSCRIBE: "Y"},
        success: function (t) {
        }
    })
}), $(document).on("click", ".product__video", function () {
    var t = $(this).attr("data-src");
    if ($("#video_popup_container").attr("src", t), visited) {
        var e = $.magnificPopup.instance.close;
        $.magnificPopup.instance.close = function () {
            quickView(visited), $.magnificPopup.instance.close = e, $.magnificPopup.proto.close.call(this)
        }
    }
    $.magnificPopup.open({
        items: {src: "#you_pop"},
        type: "inline",
        mainClass: "mfp-fade",
        closeMarkup: '<button class="mfp-close mfp-close__btn mfp-close__btn-2 icon-icon-small-close"></button>'
    }), event.preventDefault()
}), $(document).on("click", "a.quick-view", function (t) {
    quickView(t), t.stopPropagation()
});
var visited;
$(document).on("click", "span.favorites__icon", function () {
    var t = $(this).attr("data-id"), e = $(this);
    $(this).find("i").hasClass("icon-icon-wishlist-select") ? $.ajax({
        type: "POST",
        url: "/ajax/ajax_add_to_favorite.php",
        data: {id: t, action: "DELETE_FROM_COMPARE_LIST"},
        success: function (a) {
            e.find("i").removeClass("icon-icon-wishlist-select"), e.find("i").addClass("icon-icon-wishlist"), e.find("i").attr("id") != "wish_" + t && ($("#wish_" + t).removeClass("icon-icon-wishlist-select"), $("#wish_" + t).addClass("icon-icon-wishlist")), checkFavorites()
        }
    }) : $.ajax({
        type: "POST",
        url: "/ajax/ajax_add_to_favorite.php",
        data: {id: t, action: "ADD_TO_COMPARE_LIST"},
        success: function (a) {
            e.find("i").removeClass("icon-icon-wishlist"), e.find("i").addClass("icon-icon-wishlist-select"), e.find("i").attr("id") != "wish_" + t && ($("#wish_" + t).addClass("icon-icon-wishlist-select"), $("#wish_" + t).removeClass("icon-icon-wishlist")), checkFavorites()
        }
    })
}), $(document).on("click", "div.favorites__items span.item__close", function () {
    var t = $(this).attr("data-id"), e = $(this), a = 0;
    $.ajax({
        type: "POST",
        url: "/ajax/ajax_add_to_favorite.php",
        data: {id: t, action: "DELETE_FROM_COMPARE_LIST"},
        success: function (t) {
            e.parents("div.col-md-4.col-xs-6.mob_pad").remove(), $("div.col-md-4.col-xs-6.mob_pad").each(function () {
                a++
            }), 0 == a && ($("div.order__empty").removeClass("hidden"), $("div.favorites__items").addClass("hidden")), checkFavorites()
        }
    })
}), $(document).on("click", "div.header__wishlist", function () {
    window.location.href = "/favorites/"
}), $(document).on("click", "#set_filter_btn", function () {
    $(this).append('<input type="hidden" name="SECTION_ID" value="36">'), $("#filter_form").submit()
});
var timerCicle, cicleStep = 0;
$("p.game__restart").on("click", function () {
    $(this).parent();
    playAgain()
}), $(document).on("click", "a.add_to_cart_btn", function (t) {
    var e = $(this).attr("data-product-id"), a = ($(this).attr("data-product-image"), ""), i = "";
    $("div.colors").length > 0 && (a = $("div.colors span.product_color_active").attr("data-id")), $("div.product__size").length > 0 && (i = $("div.product__size span.product_size_active").text()), $.ajax({
        type: "POST",
        url: "/ajax/cart.php",
        data: {addProductId: e, color: a, size: i},
        success: function (t) {
            updateBasketLine(), updateSmallBasketContent("show")
        }
    }), t.preventDefault()
}), $(document).on("click", "input:radio[name=PAY_SYSTEM_ID]", function () {
    $("#paymentDescription").text($(this).attr("data-desr"))
}), $(document).on("change", "#smallcart", function () {
    alert($(this).html())
}), $(document).on("click", "a.delete.link_grey", function () {
    var t = $(this), e = $(this).attr("data-id");

    $.ajax({
        type: "POST",
        url: "/ajax/ajax_delete_basket_item.php",
        dataType: "html",
        data: {id: e},
        success: function (e) {
			
			var input = $('input[name="waranty' + e + '"]');
			if (input != 'undefined') {
				var basketID = $(input).data('basket');
				var productID = $(input).data('element');
			}
			if (input != 'undefined' && basketID != 'undefined') updateQuantity2(productID, '-1000', basketID, input);
		
            t.closest("div.checkout-table__body").remove();
            var a = 0;
            $("div.checkout__table div.checkout-table__body").each(function () {
                $(this).length > 0 && a++
            }), 0 == a && document.location.reload(), updateBasketLine(), updateSmallBasketContent(), submitForm()
        }
    })
}), $(document).on("click", "#pop_auth_btn", function (t) {
    var e = $(this).closest("form").serialize();
    $.ajax({
        type: "POST", url: "/ajax/ajax_auth.php", dataType: "html", data: e, success: function (t) {
            1 === parseInt(t) ? window.location.reload() : ($("#auth_errors").html(t), $("#auth_errors").show(), $("#auth_errors_in_order").html(t), $("#auth_errors_in_order").show(), $("#auth_errors_mob").html(t), $("#auth_errors_mob").show())
        }
    }), t.preventDefault()
}), $(document).on("click", "#forgot_pass_popup_btn", function (t) {
    $(this);
    var e = $(this).closest("form").serialize();
    $.ajax({
        type: "POST", url: "/ajax/ajax_forgot_password.php", dataType: "html", data: e, success: function (t) {
            parseInt(t);
            t.length < 150 ? ($("#forgot_pass_errors").attr("style", "margin: -25px 0 25px 0;"), $("#forgot_pass_errors").html(t), $("#forgot_pass_errors").show(), $("#forgot_pass_errors_mob").attr("style", "margin: -25px 0 25px 0;"), $("#forgot_pass_errors_mob").html(t), $("#forgot_pass_errors_mob").show(), $("#forgot_pass_errors_order").attr("style", "margin: -25px 0 25px 0;"), $("#forgot_pass_errors_order").html(t), $("#forgot_pass_errors_order").show()) : ($("#forgot_pass_errors").attr("style", "margin: -55px 0 25px 0; font-size: xx-small"), $("#forgot_pass_errors").html(t), $("#forgot_pass_errors").show(), $("#forgot_pass_errors_mob").attr("style", "margin: -55px 0 25px 0; font-size: xx-small"), $("#forgot_pass_errors_mob").html(t), $("#forgot_pass_errors_mob").show(), $("#forgot_pass_errors_order").attr("style", "margin: -55px 0 25px 0; font-size: xx-small"), $("#forgot_pass_errors_order").html(t), $("#forgot_pass_errors_order").show())
        }
    }), t.preventDefault()
}), $(document).on("click", "#vk_reg_btn", function (t) {
    $("#vk_auth_btn").trigger("click"), t.prevent_Default()
}), $(document).on("click", "#fb_reg_btn", function (t) {
    $("#fb_auth_btn").trigger("click"), t.prevent_Default()
}), $(".faqsec > a").on("click", function () {
    var t = $(this);
    t.hasClass("active") ? (t.next(".faq_answer").slideUp(300), t.removeClass("active")) : (t.next(".faq_answer").slideDown(300), t.addClass("active"))
}), $(".faq_close").on("click", function () {
    var t = $(this).parent();
    t.slideUp(300), t.prev("a").removeClass("active")
}), $(document).on("click", "div.sel-styled", function () {
    $("div.sel-styled.active").not(this).each(function () {
        $(this).removeClass("active").parent().find("ul.sel-options").hide()
    }), $(this).toggleClass("active").parent().find("ul.sel-options").toggle()
}), $(document).on("click", "ul.sel-options li", function () {
    var t = $(this).closest("div.sel").find("div.sel-styled.active"), e = $(this).closest("div.sel").find("select.sel-hidden"), a = $(this).closest("div.sel").find("ul.sel-options");
    t.text($(this).text()).removeClass("active"), e.val($(this).attr("rel")), a.hide(), $(this).parent().find("li").removeClass("li_active"), $(this).addClass("li_active")
}), $(document).ready(function () {
    $(".checkout-form__head div.sel-styled").on("click", function () {
        $(".checkout-form__head div.sel-styled.active").not(this).each(function () {
            $(this).removeClass("active").parent().find("ul.sel-options").hide()
        }), $(this).toggleClass("active").parent().find("ul.sel-options").toggle()
    });
    var t = document.querySelector(".checkout"), e = new MutationObserver(function (t) {
        stylizeScrolls()
    }), a = {childList: !0, subtree: !0};
    t && e.observe(t, a), stylizeScrolls()
}), $(document).on("mouseover", "a.watch_dynamic_link", function () {
    $(this).attr("data-id");
    $("li.brend_active").removeClass("brend_active"), $(this).closest("li").addClass("brend_active")
}), $(document).on("mouseover", "a.sunglasses_dynamic_link", function () {
    var t = $(this).attr("data-id");
    $("div.dynamic_content_sunglasses").hide(), $("div.dd-goggles__wrap div.dynamic_content_sunglasses[data-role=" + t + "]").show()
}), $(document).on("mouseover", "a.accessories_dynamic_link", function () {
    var t = $(this).attr("data-id");
    $("div.dynamic_content_accessories").hide(), $("div.dd-goggles__wrap div.dynamic_content_accessories[data-role=" + t + "]").show()
}), $(window).scroll(function () {
    $(window).scrollTop() >= 200 ? $("div.h-basket").addClass("basket-relative") : $("div.h-basket").removeClass("basket-relative")
}), $(document).on("click", ".mobile__auth .forgot__pass", function () {
    $(".mobile__auth .forgot-pass__content_mob").removeClass("hidden"), $(".mobile__auth .auth__login").hide()
}), $(document).on("click", ".mobile__auth .log-in", function () {
    $(".mobile__auth .forgot-pass__content_mob").addClass("hidden"), $(".mobile__auth .auth__login").show()
}), $(document).on("click", "a", function () {
    var t = $(this).attr("data-seo");
    t && $.ajax({
        type: "POST",
        url: "/ajax/ajax_user_from.php",
        data: {user_from: t},
        dataType: "html",
        success: function (t) {
        }
    })
});