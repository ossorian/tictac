function shrinkCart() {
	var t = !($(window).width() > 991),
		e = t ? document.getElementById("smallcart_mob") : document.getElementById("smallcart"),
		i = t ? 57 : 98,
		o = t ? 74 : 0,
		n = t ? 188 : 138,
		a = $(".h-basket__item").length > 0 ? e.querySelectorAll(".h-basket__item") : "",
		s = 106 * a.length + i + n,
		c = stick ? $(window).height() - o : $(window).height() - i - 20;
	c < s && ($(".scrollbar-outer").scrollbar(), a[0].closest(".scroll-wrapper").style.maxHeight = c - n + "px", shrinked = !0)
}

function shrinkMobMenu() {
	var t = $(window).height(),
		e = $(".header__head_mob").outerHeight();
	$(".header__head_mob .toggle_menu").height(t - e)
}

/* $(function(){
	$('div.warantyBlock').click(function(obj){
		var a = $(obj).find('input');
		console.log(a);
	});
});
 */
$(function (t) {
	function e(t, e) {
		e || (e = window.location.href), t = t.replace(/[\[\]]/g, "\\$&");
		var i = new RegExp("[?&]" + t + "(=([^&#]*)|&|#|$)").exec(e);
		return i ? i[2] ? decodeURIComponent(i[2].replace(/\+/g, " ")) : "" : null
	}
	$(".search__icon").click(function () {
		$(".header .logo, .certificate, .h-b-right__content, .h-b-left__content, .search").addClass("search_hidden"), $(".search_visible").removeClass("hidden"), $(".header-head__bottom").css("height", "64px"), $(".auth, .h-basket").css("display", "none")
	}), $(".search_visible__close").click(function () {
		$(".header .logo, .certificate, .h-b-right__content, .h-b-left__content, .search").removeClass("search_hidden"), $(".search_visible").addClass("hidden"), $(".header-head__bottom").css("height", "auto")
	}), $(document).on("click", ".menu__btn", function () {
		$(this).toggleClass("menu__btn_active"), $("html").toggleClass("noscroll"), $(".toggle_menu").slideToggle(), $(".h-basket").hide(), $(".auth").hide(), $(".header__head_mob .icon-icon-cart").removeClass("icon-icon-small-close"), $(".header__head_mob .icon-icon-cart").parent().css("padding-top", "0px")
	}), $(document).on("click", ".view_four_items", function () {
		$(".brend__col").removeClass("col-md-4"), $(".brend__col").addClass("col-md-3"), $(".reminder, .o-reminder").addClass("rem__padd")
	}), $(document).on("click", ".view_three_items", function () {
		$(".brend__col").removeClass("col-md-3"), $(".brend__col").addClass("col-md-4"), $(".reminder, .o-reminder").removeClass("rem__padd")
	}), $(document).on("click", ".auth__close", function () {
		$(".auth").hide(), $(".forgot-pass__content").addClass("hidden"), $(".auth").css({
			height: "auto"
		})
	}), $(document).on("click", ".acc__icon", function () {
		$(".checkout .auth").hide(), $(".header .auth").toggle(), $(".h-basket").hide()
	}), $(document).on("click", ".login__btn", function () {
		$(".checkout .auth").toggle(), $(".header .auth").hide(), $(".h-basket").hide()
	}), $(document).on("click", ".header .forgot__pass", function () {
		$(".header .forgot-pass__content").removeClass("hidden"), $(".header .auth").css({
			height: "265px"
		})
	}), $(document).on("click", ".checkout .forgot__pass", function () {
		$(".checkout .forgot-pass__content").removeClass("hidden"), $(".checkout .auth").css({
			height: "200px"
		})
	}), $(document).on("click", ".checkout .log-in", function () {
		$(".checkout .forgot-pass__content").addClass("hidden"), $(".checkout .auth").css({
			height: "auto"
		})
	}), $(document).on("click", ".auth__tab", function () {
		$(".forgot-pass__content").addClass("hidden"), $(".auth").css({
			height: "auto"
		})
	}), $(".auth__tab-item").not(":first").hide(), $(document).on("click", ".auth .auth__tab", function () {
		$(".auth .auth__tab").removeClass("auth_active").eq($(this).index()).addClass("auth_active"), $(".auth__tab-item").hide().eq($(this).index()).fadeIn()
	}).eq(0).addClass("auth_active"), $(document).on("click", ".header__head .header__basket", function () {
		$(".h-basket").toggle(), $(".auth").hide()
	}), $(document).on("click", ".header__head_mob .header__basket", function () {
		$(".h-basket").toggle(), $(".auth").hide(), $(".toggle_menu").hide(), $(".menu__btn").removeClass("menu__btn_active"), $(".header__head_mob .icon-icon-cart").toggleClass("icon-icon-small-close")
	}), $(document).on("click", "#smallcart .icon-icon-small-close", function () {
		$(".h-basket").hide()
	}), $(document).on("click", ".nav__link", function () {
		$(".h-basket").hide(), $(".auth").hide()
	}), $(".colors span").click(function () {
		$(".colors span").removeClass("product_color_active"), $(this).addClass("product_color_active")
	}), $(".sizes span").click(function () {
		$(".sizes span").removeClass("product_size_active"), $(this).addClass("product_size_active")
	}), $(".product__body").not(":first").hide(), $(".product__content .product__tab").click(function () {
		$(".product__content .product__tab").removeClass("product__tab_active").eq($(this).index()).addClass("product__tab_active"), $(".product__body").hide().eq($(this).index()).fadeIn()
	}).eq(0).addClass("product__tab_active"), $('a[href^="#to"]').on("click", function (t) {
		t.preventDefault();
		var e = $(this).attr("href"),
			i = $(e).offset().top;
		$("body,html").animate({
			scrollTop: i
		}, 1e3)
	}), $(".game__close").click(function () {
		$(".game__wrap").hide()
	}), $(".game__text").click(function () {
		$(".game__wrap").show()
	}), $(".profule-content__item").not(":first").hide(), $(".profile__body .profile__tab").click(function () {
		$(".profile__body .profile__tab").removeClass("profile__tab_active").eq($(this).index()).addClass("profile__tab_active"), $(".profule-content__item").hide().eq($(this).index()).fadeIn()
	}).eq(0).addClass("profile__tab_active"), $(".order__number").click(function () {
		$(this).parents(".order-table__body").find(".order_toggle").slideToggle()
	}), $(".accordeon dd").prev().click(function () {
		$(this).parents(".accordeon").find("dd").not(this).slideUp().prev().removeClass("acc_active"), $(this).next().not(":visible").slideDown().prev().addClass("acc_active")
	}), $(".header__slider").slick({
		dots: !0,
		arrows: !1,
		infinite: !0,
		slidesToShow: 1,
		autoplay: !0,
		autoplaySpeed: 4e3
	}), $(".brends__slider").slick({
		dots: !0,
		arrows: !1,
		infinite: !0,
		slidesToShow: 2
	}), $(".one-product__slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: ".one-product__slider_navigation",
		dots: !1,
		infinite: !0,
		focusOnSelect: !0,
		arrows: !0,
		swipe: !1,
		prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
		nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
		responsive: [{
			breakpoint: 991,
			settings: {
				arrows: !1,
				dots: !0,
				focusOnSelect: !1,
				swipe: !0
			}
		}]
	}), $(".one-product__slider_navigation").slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: !0,
		dots: !1,
		infinite: !0,
		asNavFor: ".one-product__slider",
		focusOnSelect: !0,
		centerMode: !1,
		vertical: !0,
		prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
		nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 4
			}
		}, {
			breakpoint: 500,
			settings: {
				arrows: !1
			}
		}]
	}), $(".bc-foot__slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: !1,
		infinite: !0,
		prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
		nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>'
	}), $(".sale__slider").slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: !1,
		infinite: !0,
		prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
		nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
		responsive: [{
			breakpoint: 991,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 767,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 480,
			settings: {
				slidesToShow: 1
			}
		}]
	}), $(".tab__slider").slick({
		infinite: !0,
		slidesToShow: 4,
		slidesToScroll: 1,
		prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon-left-arrow"></i></button>',
		nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon-right-arrow"></i></button>',
		responsive: [{
			breakpoint: 991,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 767,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 480,
			settings: {
				slidesToShow: 1
			}
		}]
	}), $(".tabs__slider").slick({
		infinite: !1,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: !1,
		dots: !0,
		fade: !0,
		adaptiveHeight: !0,
		draggable: !1,
		swipe: !1,
		touchMove: !1,
		customPaging: function (t, e) {
			return "<span>" + $(t.$slides[e]).data("thumb") + "</span>"
		}
	}), $(".ch__more").click(function () {
		$(".ch__about_hidden").slideToggle(), $(this).toggleClass("ch__more_active"), $(this).text(function (t, e) {
			return "Показать подробнее" === e ? "Свернуть" : "Показать подробнее"
		})
	}), $(".inst__link").magnificPopup({
		mainClass: "mfp-fade",
		closeMarkup: '<button class="mfp-close mfp-close__btn icon-icon-small-close"></button>'
	}), $(".quick-view").click(function () {
		$(".quick-view__popup").css({
			opacity: "1",
			visibility: "visible",
			height: "auto"
		})
	}), $(".view > div").click(function () {
		$(".view > div").removeClass("view_three_items_active"), $(this).addClass("view_three_items_active")
	}), $(".no-product__order").click(function () {
		$(".o-reminder").hide(), $(this).parents(".no-item").find(".o-reminder").show()
	}), $(".o-reminder__close").click(function () {
		$(this).parent().hide()
	}), $(".no-product__reminder").click(function () {
		$(".reminder").hide(), $(this).parents(".no-item").find(".reminder").show()
	}), $(".reminder__close").click(function () {
		$(this).parent().hide()
	}), $(".preorder_btn").click(function () {
		$(".o-reminder").hide(), $(this).parents(".item").find(".o-reminder").show()
	}), $(".mfp-close").addClass("icon-icon-small-close"), $("label.filter__item").click(function () {
		$(this).hasClass("filter__item_active") ? ($(this).removeClass("filter__item_active"), $(this).find("input[type=checkbox]").removeAttr("checked")) : ($(this).addClass("filter__item_active"), $(this).find("input[type=checkbox]").prop("checked", !0));
		var t = $("#filter_form").serialize(),
			e = $("#section_id").attr("data-id"),
			sect = '&section_id=';
		return "none" !== $("div.filter_brands_mobile").css("display") && (e = 0), $.ajax({
			type: "GET",
			url: "/ajax/ajax_filter.php",
			data: t + sect + e,
			success: function (t) {
				$("#filter_result").html(t)
			}
		}), !1
	}), $("body select").each(function () {
		var t = $(this),
			i = $(this).children("option").length;
		t.addClass("sel-hidden"), t.wrap('<div class="sel"></div>'), t.after('<div class="sel-styled"></div>');
		for (var o = t.next("div.sel-styled"), n = $("<ul />", {
				class: "sel-options"
			}).insertAfter(o), a = 0; a < i; a++) "selected" === t.children("option").eq(a).attr("selected") ? ($("<li />", {
			text: t.children("option").eq(a).text(),
			rel: t.children("option").eq(a).val(),
			class: "li_active",
			onclick: t.attr("onchange"),
			value: t.children("option").eq(a).val()
		}).appendTo(n), o.text(t.children("option").eq(a).text())) : $("<li />", {
			text: t.children("option").eq(a).text(),
			rel: t.children("option").eq(a).val(),
			onclick: t.attr("onchange"),
			value: t.children("option").eq(a).val()
		}).appendTo(n);
		var s = n.children("li");
		o.click(function (t) {
			t.stopPropagation(), $("div.sel-styled.active").not(this).each(function () {
				$(this).removeClass("active").next("ul.sel-options").hide()
			}), $(this).toggleClass("active").next("ul.sel-options").toggle()
		}), s.click(function (e) {
			e.stopPropagation(), o.text($(this).text()).removeClass("active"), t.val($(this).attr("rel")), n.hide()
		}), $(document).click(function () {
			o.removeClass("active"), n.hide()
		});
		var c = e("sort_by");
		null != c && $(".sel-options li").each(function () {
			$(this).attr("rel") == c && (o.text($(this).text()), $(this).parent().find("li").removeClass("li_active"), $(this).addClass("li_active"))
		})
	}), $(".sel-options li").click(function () {
		if ($(this).closest("div.brend__panel").length > 0) {
			var t = $(this).attr("rel"),
				i = e("sort_by"),
				o = document.location.href;
			if (null != i) n = document.location.href.replace(i, t);
			else if (document.location.href.indexOf("?") > -1) n = o.toString() + "&sort_by=" + t;
			else var n = o.toString() + "?sort_by=" + t;
			document.location = n
		}
		$(this).parent().find("li").removeClass("li_active"), $(this).addClass("li_active")
		if($(this).attr("value")=="other"){
        	$(".other_text").removeClass("hidden");
		}
		else{
            $(".other_text").addClass("hidden");
		}
	}), $(".minus").click(function () {
		var t = $(this).parent().find("input"),
			e = parseInt(t.val()) - 1;
		e = e < 1 ? 1 : e;
		t.attr("data-id");
		return t.val(e), t.change(), !1
	}), $(".plus").click(function () {
		var t = $(this).parent().find("input");
		t.attr("data-id"), parseInt(t.val());
		return t.val(parseInt(t.val()) + 1), t.change(), !1
	}), $(document).on("change", "input.basket_quantity_input", function () {
		var t = $(this).val(),
			e = parseInt($(this).attr("data-id"));
		(t <= 0 || isNaN(t) || void 0 === t || t.length <= 0) && (t = 1, $(this).val(1)), console.log("old : " + $("#quantity__input_" + e).val() + " new : " + t);
		var i = $(this),
			o = parseInt($("#quantity__input_" + e).val());
		o - t != 0 && (o > t ? dataLayer.push({
			event: "removeFromCart",
			ecommerce: {
				remove: {
					products: [{
						name: i.attr("data-name"),
						id: i.attr("data-googleid"),
						price: i.attr("data-price"),
						brand: i.attr("data-brand"),
						category: i.attr("data-category"),
						variant: i.attr("data-variant"),
						quantity: o - t
					}]
				}
			}
		}) : dataLayer.push({
			event: "addToCart",
			ecommerce: {
				currencyCode: "RUB",
				add: {
					products: [{
						name: i.attr("data-name"),
						id: i.attr("data-googleid"),
						price: i.attr("data-price"),
						brand: i.attr("data-brand"),
						category: i.attr("data-category"),
						variant: i.attr("data-variant"),
						quantity: t - o
					}]
				}
			}
		}));
		
		var prevQuant = $("#quantity__input_" + e).val();
		var diffQuant = t - prevQuant;
		var input = $('input[name="waranty' + e +'"]');
		var basketID = input.data('basket');
		if (basketID && input.prop("checked")) {
			updateQuantity2(e, diffQuant, basketID, input);
		}
		
		$("#quantity__input_" + e).val(t);
		updateQuantity(e, t);
		
	}), $(".discount__input").click(function () {
		$(".discount__btn").show()
	}), $(".brend-buy__head").click(function () {
		$(".brend-buy__text_hidden").slideToggle(), $(".brend-buy__head span").toggleClass("brend-arrow_active")
	}), $(".filter").click(function () {
		$(".filter_toggle").slideToggle(), $(this).toggleClass("filter_active")
	});
	var i = !1;
	$(window).resize(function () {
		$(window).width() > 991 ? i || $(".tovar-img").length > 0 && ($(".tovar-img").wrap('<span style="display:inline-block"></span>').css("display", "block").parent().zoom({
			magnify: 1
		}), i = !0) : ($(".tovar-img").trigger("zoom.destroy"), i = !1)
	}), $(window).width() > 991 ? $(".tovar-img").length > 0 && $(".tovar-img").wrap('<span style="display:inline-block"></span>').css("display", "block").parent().zoom({
		magnify: 1
	}) : $(".tovar-img").trigger("zoom.destroy")
}), $(".sel-options").length > 0 && $(".sel-options").scrollbar(), $(function () {
	if (document.getElementById("watches_by_color")) {
		var t = document.getElementById("watches_by_color").querySelector(".slick-dots"),
			e = t.clientHeight;
		t.parentNode.style.paddingTop = e + 45 + "px"
	}
	$(".header__basket").click(shrinkCart), $(".cart__icon").click(shrinkCart), $(window).resize(shrinkCart), $(window).on("orientationchange", shrinkCart), $(window).scroll(function () {
		$(window).scrollTop() >= 200 ? $(".header__basket").trigger("stick") : $(".header__basket").trigger("unstick")
	}), $(document).on("click", ".zoomImg", function () {
		return !1
	});
	
	if ( $('.brend').length ) {
		$(window).scroll(function() {			
			if ( $(window).scrollTop() > $('.brend__panel').parent().offset().top ) {
				$('.brend__panel, .filter_toggle').addClass('fixed');
				$('.filter_toggle').css({
					left: $('.brend__panel.fixed').offset().left,
					top: $('.brend__panel.fixed').outerHeight(true) + $('.brend__panel.fixed').position().top,
					width: $('.brend__panel').outerWidth(true)
				});
				$('.filter_toggle > .container').css('width', '100%');
			} else {
				$('.brend__panel, .filter_toggle').removeClass('fixed');
				$('.filter_toggle').css('width', '');
				$('.filter_toggle > .container').css('width', '');
			}
		});
		
		$(window).resize(function() {
			scrollable();
			preventClick();
		});
		scrollable();
		preventClick();
		function scrollable() {
			if ( $(window).height() < $('.filter_toggle').outerHeight() ) {
				$('.filter_toggle').addClass('scrollable');
			}
		}
		
		$(document).on('mouseenter', '.item', function() {
			var $img = $(this).find('img');
			src = $img.attr('src');
			var data = $img.attr('data-second');
			if (data) $img.attr('src', data);	
		});
		$(document).on('mouseleave', '.item', function() {
			$(this).find('img').attr('src', src);
		});
				
		function preventClick() {
			if ( $(window).width() < 1024 ) {
				$(document).one('click', '.item', function(event) {
					if ( $(this).find('img').attr('data-second') ) event.preventDefault();
				});	
			}
		}
	}
});
var stick, height, mob = !($(window).width() > 991),
	stickCart = new CustomEvent("stick"),
	unStickCart = new CustomEvent("unstick");
$(".header__basket").on("stick", function () {
	!stick && shrinked && (mob ? (height = $("#smallcart_mob .scrollbar-outer").css("max-height"), $("#smallcart_mob .scrollbar-outer").css("max-height", "+=3")) : (height = $("#smallcart .scrollbar-outer").css("max-height"), $("#smallcart .scrollbar-outer").css("max-height", "+=118")), stick = !0)
}), $(".header__basket").on("unstick", function () {
	mob ? $("#smallcart_mob .scrollbar-outer").css("max-height", height) : $("#smallcart .scrollbar-outer").css("max-height", height), stick = !1, height = null
});
var shrinked = !1;
$(".header__head_mob .menu__btn").click(shrinkMobMenu);
var animation;
$(document).on("click", ".quick-view__popup .add_to_cart_btn", function () {
	var t = $(this).html(),
		e = this;
	$(this).html("✔ Добавлено"), animation || (animation = setTimeout(function () {
		$(e).html(t), animation = null
	}, 1e3))
}), $(window).load(function () {
	$(".loader_inner").fadeOut(), $(".loader").delay(200).fadeOut("slow");
});
$(document).ready(function() {
    $(".sel-options li").each(function () {
        if ($(this).hasClass("li_active") && $(this).attr("value") == "other") {
       	 $(".other_text").removeClass("hidden");
        	//console.log($(this).attr("value"));
        }
    })
})

function toggleWaranty(input) {
	var checked = input.prop("checked");
	var productID = input.data('element');
	var basketID = input.data('basket');
	
	console.log("Галка :" + checked);
	console.log("productID :" + productID);
	console.log("basketID : " + basketID);
	
	var id = $(input).attr('name').replace('waranty', '');
	$('input.basket_quantity_input').each(function(i, el){
		if ($(el).data('id') == id) quantity = $(el).val();
	});
	
	if (checked) {
		updateQuantity2(productID, quantity, basketID, input);
		console.log('Увеличиваем на ' + quantity);
	} else {
		updateQuantity2(productID, -quantity, basketID, input);
		console.log('Уменьшаем на ' + quantity);
	}
}