<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
<? if (defined('ERROR_404') === false) { ?>
    <!-- FOOTER (Футер) -->
    <footer class="footer">
        <div class="footer__head">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-7">
                        <p class="footer__text">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/include_footer_sub_text.php"), false); ?>
                        </p>
                    </div>
                    <div class="col-lg-6 col-md-5">
                        <?
                        $APPLICATION->IncludeComponent("phpdev:subscribe", "footer_subscribe", Array(
                            "IBLOCK_ID" => "18",	// Инфо-блок
                            "CACHE_TYPE" => "A",	// Тип кеширования
                            "CACHE_TIME" => "3600",	// Время кеширования (сек.)
                            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                        ),
                            false
                        );?>
                    </div>
                </div>
				<div class="google-captcha-container" style="display:none">
					<?$APPLICATION->AddHeadScript("https://www.google.com/recaptcha/api.js")?>
					<div style="float: right;padding: 20px 0 0;" class="g-recaptcha" data-sitekey="6LdFqPwUAAAAAK5KInnPLAsYmiLwU43WziI87PzS"></div>
				</div>
            </div>
        </div>
        <div class="footer__body">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-7 clearfix">
                        <div class="footer__phone hidden-sm hidden-xs">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/footer_phone.php"), false); ?>
                        </div>
                        <div class="payment-methods hidden-sm hidden-xs">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/footer_payments.php"), false); ?>
                        </div>
                        <div class="footer__contacts clearfix">
                            <p class="footer-contacts__text">По вопросам и предложениям <br>обращайтесь на
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/email.php"), false); ?>
                            </p>
                            <div class="soc">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/social_vk_new.php"), false, array("HIDE_ICONS"=>"Y")); ?>
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/social_fb_new.php"), false, array("HIDE_ICONS"=>"Y")); ?>
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/social_inst_new.php"), false, array("HIDE_ICONS"=>"Y")); ?>
                            </div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/social.php"), false); ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-5 hidden-sm hidden-xs">
                        <div class="footer-list__wrap">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "footer_menu",
                                Array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "1",
                                    "MENU_CACHE_GET_VARS" => array(""),
                                    "MENU_CACHE_TIME" => "36000",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "ROOT_MENU_TYPE" => "top",
                                    "USE_EXT" => "N"
                                )
                            );?>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "footer_menu2",
                                array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "COMPONENT_TEMPLATE" => "footer_menu2",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "1",
                                    "MENU_CACHE_GET_VARS" => array(
                                    ),
                                    "MENU_CACHE_TIME" => "36000",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "ROOT_MENU_TYPE" => "bottom",
                                    "USE_EXT" => "N"
                                ),
                                false
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__foot">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/copyright.php"), false); ?>
                    </div>
                    <div class="col-md-3 hidden-sm hidden-xs">
                        <div class="game">
                            <p class="game__text">
								<span class="game__icon">
									<i class="icon-icon-game"></i>
								</span>
                                <span>Я хочу сыграть с вами в игру</span>
                            </p>
                            <div class="game__wrap">
								<span class="game__close">
									<i class="icon-icon-small-close"></i>
								</span>
                                <p class="game__restart">
									<span class="game-restart__icon">
										<i class="icon-icon-refresh"></i>
									</span>
                                    <span class="newgame">Новая игра</span>
                                </p>
                                <div class="tic-tac-toe clearfix">
                                    <div class="tic-tac-toe__col" id="A" onclick="yourChoice('A')"></div>
                                    <div class="tic-tac-toe__col" id="B" onclick="yourChoice('B')"></div>
                                    <div class="tic-tac-toe__col" id="C" onclick="yourChoice('C')"></div>
                                    <div class="tic-tac-toe__col" id="D" onclick="yourChoice('D')"></div>
                                    <div class="tic-tac-toe__col" id="E" onclick="yourChoice('E')"></div>
                                    <div class="tic-tac-toe__col" id="F" onclick="yourChoice('F')"></div>
                                    <div class="tic-tac-toe__col" id="G" onclick="yourChoice('G')"></div>
                                    <div class="tic-tac-toe__col" id="H" onclick="yourChoice('H')"></div>
                                    <div class="tic-tac-toe__col" id="I" onclick="yourChoice('I')"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 hidden-sm hidden-xs">
                        <div class="ff__soc">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/social_desktop.php"), false); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER (Футер) -->


    <div class="quick-view__popup" id="qv__popup">
        <section class="product" id="qv_popup_content">

        </section>
    </div>

    <div class="hidden">

    </div>
<? } ?>

<!--add to cart popup-->
<div class="hidden">
    <div class="add_to_cart__popup clearfix" id="add_to_cart_popup">
        <div class="add_to_cart-popup__content">
            <div class="add_to_cart-popup__item clearfix">
                <div class="add_to_cart-popup__img">
                    <img id="add_to_cart_img" src="<?= SITE_TEMPLATE_PATH?>/img/instagram/pict-1__popup_img.jpg" alt="">
                </div>
                <p>Товар добавлен в корзину</p>
            </div>
            <a class="btn btn_black product__btn" onclick="$.magnificPopup.close();">Продолжить покупки</a>
            <a href="/cart/" class="btn product__btn">Перейти в корзину</a>
        </div>
    </div>
</div>

<div class="hidden">
    <div class="youtube__popup" id="you_pop">
        <iframe src="https://www.youtube.com/embed/let3-zdajXI" frameborder="0" allowfullscreen id="video_popup_container"></iframe>
    </div>
</div>


<script src="<?= SITE_TEMPLATE_PATH ?>/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/libs/Magnific-Popup/jquery.magnific-popup.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/libs/slick/slick.js?rev=1567"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/libs/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/libs/TextInputEffects/js/classie.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/libs/sliderLine/wNumb.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/libs/sliderLine/nouislider.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/libs/zoom-master/jquery.zoom.min.js"></script>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/common.js?rev=<?= date("His")?>"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/phpdev.script.js?rev=<?= date("His")?>"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/game.js?rev=<?= date("His")?>"></script>

<!--share-->
<!--<script async src="https://static.addtoany.com/menu/page.js"></script>-->

<script>
    (function () {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function () {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(rtrim, '');
                };
            })();
        }

        [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
            // in case the input is already filled..
            if (inputEl.value.trim() !== '') {
                classie.add(inputEl.parentNode, 'input--filled');
            }

            // events:
            //inputEl.addEventListener('focus', onInputFocus);
            //inputEl.addEventListener('blur', onInputBlur);
        });

//        function onInputFocus(ev) {
//            classie.add(ev.target.parentNode, 'input--filled');
//        }
//
//        function onInputBlur(ev) {
//            if (ev.target.value.trim() === '') {
//                classie.remove(ev.target.parentNode, 'input--filled');
//            }
//        }

        if (window.jQuery) {

            jQuery(document).on('focus',"input.input__field", function(ev) {
                // something
                classie.add(ev.target.parentNode, 'input--filled');
            }).on('blur',"input.input__field", function(ev) {
                // something
                if (ev.target.value.trim() === '') {
                    classie.remove(ev.target.parentNode, 'input--filled');
                }
            });

        }

    })();
</script>

<!-- Yandex.Metrika counter --> 
<script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter186353 = new Ya.Metrika({ id:186353, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true, ut:"noindex", ecommerce:"dataLayer" }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> 
<!-- /Yandex.Metrika counter -->

<?/*?>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1704759379844791');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1704759379844791&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<?*/?>
</body>
</html>
