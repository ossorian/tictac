<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="container-fluid mt_2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><b>Подтверждение регистрации</b></h1>
            </div>
    <p><? echo $arResult["MESSAGE_TEXT"]?></p>

    <? //here you can place your own messages
    switch ($arResult["MESSAGE_CODE"]) {
        case "E01":
            //LocalRedirect("/");
            ?>
            <? //When user not found
            break;
        case "E02":
            echo '<div class="popup-text"><a href="#" class="sign-up-btn">'.GetMessage("AUROOM_LOGIN_BTN_TITLE").'</a></div>';
            ?>
            <? //User was successfully authorized after confirmation
            break;
        case "E03":
            //LocalRedirect("/");
            ?>
            <? //User already confirm his registration
            break;
        case "E04":
            ?><? //Missed confirmation code
            break;
        case "E05":
            ?><? //Confirmation code provided does not match stored one
            break;
        case "E06":
            echo '<div class="popup-text"><a href="#" class="sign-up-btn">'.GetMessage("AUROOM_LOGIN_BTN_TITLE").'</a></div>';
            ?>
            <? //Confirmation was successfull
            break;
        case "E07":
            ?><? //Some error occured during confirmation
            break;
    }
    ?>
    <? if ($arResult["SHOW_FORM"]): ?>

        <form method="post" action="<? echo $arResult["FORM_ACTION"] ?>" class="clearfix">

            <div class="row">
                <div class="col-xl-6 col-md-6 ">
                    <label><? echo GetMessage("CT_BSAC_LOGIN") ?>:</label>
                    <input type="text" name="<? echo $arParams["LOGIN"] ?>" maxlength="50"
                           value="<? echo(strlen($arResult["LOGIN"]) > 0 ? $arResult["LOGIN"] : $arResult["USER"]["LOGIN"]) ?>"
                           size="17" class="form-control"/>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-6 col-md-6 ">
                    <label><? echo GetMessage("CT_BSAC_CONFIRM_CODE") ?>:</label>
                    <input type="text" name="<? echo $arParams["CONFIRM_CODE"] ?>" maxlength="50"
                           value="<? echo $arResult["CONFIRM_CODE"] ?>" size="17" class="form-control"/>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6 col-md-6 ">
                    <input type="submit" class="btn btn-primary" value="<? echo GetMessage("CT_BSAC_CONFIRM") ?>"/>
                </div>
            </div>

                <input type="hidden" name="<? echo $arParams["USER_ID"] ?>" value="<? echo $arResult["USER_ID"] ?>"/>

        </form>

    <? elseif (!$USER->IsAuthorized()): ?>
        <?// LocalRedirect("/");?>
        <? //$APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "", array());?>
    <? endif ?>
    <? if ($USER->IsAuthorized()) {
        //LocalRedirect("/");
    } ?>
        </div>
    </div>
</div>

