<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?
ShowMessage($arParams["~AUTH_RESULT"]);
?>
<br>
<form method="post" action="<?= $arResult["AUTH_FORM"] ?>" name="bform" class="clearfix">
	<? if (strlen($arResult["BACKURL"]) > 0): ?>
		<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
	<? endif ?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="CHANGE_PWD">
	
	<div class="row">
		<div class="col-xl-6 col-md-6 ">

			<span class="input input--haruki name__input">
				<input class="input__field input__field--haruki" type="text" id="input-1" name="USER_LOGIN" maxlength="50" value="<?= $arResult["LAST_LOGIN"] ?>" />
				<label class="input__label input__label--haruki" for="input-1">
					<span class="input__label-content input__label-content--haruki"><?= GetMessage("AUTH_LOGIN") ?> *</span>
				</label>
			</span>

		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-md-6 ">

			<span class="input input--haruki name__input">
				<input class="input__field input__field--haruki" type="text" id="input-2" name="USER_CHECKWORD" maxlength="50" value="<?= $arResult["USER_CHECKWORD"] ?>" />
				<label class="input__label input__label--haruki" for="input-2">
					<span class="input__label-content input__label-content--haruki"><?= GetMessage("AUTH_CHECKWORD") ?> *</span>
				</label>
			</span>

		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-md-6 ">

			<span class="input input--haruki name__input">
				<input class="input__field input__field--haruki" type="password" id="input-3" name="USER_PASSWORD" maxlength="50" value="<?= $arResult["USER_PASSWORD"] ?>" autocomplete="off"/>
				<label class="input__label input__label--haruki" for="input-3">
					<span class="input__label-content input__label-content--haruki"><?= GetMessage("AUTH_NEW_PASSWORD_REQ") ?> *</span>
				</label>
			</span>


			<? if ($arResult["SECURE_AUTH"]): ?>
				<span class="bx-auth-secure" id="bx_auth_secure"
				      title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
				<script type="text/javascript">
					document.getElementById('bx_auth_secure').style.display = 'inline-block';
				</script>
			<? endif ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-md-6 ">

			<span class="input input--haruki name__input">
				<input class="input__field input__field--haruki" type="password" id="input-3" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>" autocomplete="off"/>
				<label class="input__label input__label--haruki" for="input-3">
					<span class="input__label-content input__label-content--haruki"><?= GetMessage("AUTH_NEW_PASSWORD_CONFIRM") ?> *</span>
				</label>
			</span>

		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 col-md-6 ">
			<p></p>
			<input class="btn btn_black ottr__btn" name="change_pwd" value="<?= GetMessage("AUTH_CHANGE") ?>"
			       type="submit">
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-md-6 ">
			<hr>
			<label><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></label><br>
			<label><span class="color_danger">*</span><?= GetMessage("AUTH_REQ") ?></label>
		</div>
	</div>
</form>


