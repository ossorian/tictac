<?
$MESS ['SMALL_BASKET_CART'] = "Моя корзина";
$MESS ['SMALL_BASKET_EXPAND'] = "Развернуть";
$MESS ['SMALL_BASKET_EMPTY'] = "Ваша корзина пуста";
$MESS ['SMALL_BASKET_CURRENCY'] = "руб.";
$MESS ['SMALL_BASKET_TOP1'] = "У вас";
$MESS ['SMALL_BASKET_TOP21'] = "товаров";
$MESS ['SMALL_BASKET_TOP22'] = "товар";
$MESS ['SMALL_BASKET_TOP23'] = "товара";
$MESS ['SMALL_BASKET_TOP3'] = "на сумму";
$MESS ['SMALL_BASKET_MODEL'] = "моделей";
$MESS ['SMALL_BASKET_DELETE'] = "удалить";
$MESS ['SMALL_BASKET_TOTAL'] = "Итого";
$MESS ['SMALL_BASKET_ORDER'] = "Оформить заказ";
?>