<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? $total = count($arResult["ITEMS"]["AnDelCanBuy"]); ?>
<? $arCart = array();?>
<div class="row">
    <div class="col-xs-12">
        <div class="h-basket">
            <div class="h-basket__content">
                <? if (!empty($total)) { ?>
                <p class="h-basket__title"><?= GetMessage("SMALL_BASKET_CART") ?></p>
								<div class='h-basket__item-wrapper'>
									<div class='h-basket__scroll scrollbar-outer'>
										<? foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arBasketItems) { ?>
												<? $picture = resizeImage($arBasketItems["IMAGE"], 50, 65, 1); ?>

												<div class="h-basket__item <?= $total - 1 == $key ? 'h-basket__item_last' : '' ?>">
														<span class="h-basket__delete" onclick="cartDelete('<?= $arBasketItems["ID"] ?>', 'header_basket_mob');
															dataLayer.push({
															'event': 'removeFromCart',
															'ecommerce': {
															'remove': {
															'products': [{
															'name': '<?= strip_tags($arBasketItems['NAME'])?>',
															'id': '<?= $arBasketItems['ID']?>',
															'price': '<?= $arBasketItems['PRICE']?>',   				// Цена одной единицы купленного товара
															'brand': '<?= $arBasketItems['BRAND']?>',
															'category': '<?=$arBasketItems['SEO_SECTION_NAME']?>',
															'variant': '<?=$arBasketItems['COLORS']?>',
															'quantity': '<?=$arBasketItems['QUANTITY']?>'
															}]
															}
															}
															});"><i
																		class="icon-icon-close"></i></span>
														<?if ($picture['SRC']):?>
															<img src="<?= $picture['SRC'] ?>" class="h-basket__img">
														<?endif?>
														<a href="<?= $arBasketItems["DETAIL_PAGE_URL"] ?>"
															 class="h-basket__name link"
														   onclick="
															   dataLayer.push({
															   'event': 'productClick',
															   'ecommerce': {
															   'click': {
															   'actionField': {'list': 'Корзина'},     // Список с которого был клик.
															   'products': [{
															   'name': '<?= strip_tags($arBasketItems['NAME'])?>',       	// Наименование товара.
															   'id': '<?= $arBasketItems['ID']?>',  				// Идентификатор товара в базе данных.
															   'price': '<?= $arBasketItems['PRICE']?>',   				// Цена одной единицы купленного товара
															   'brand': '<?= $arBasketItems['BRAND']?>',
															   'category': '<?=$arBasketItems['SEO_SECTION_NAME']?>',
															   'variant': '<?=$arBasketItems['COLORS']?>'
															   }]
															   }
															   }
															   });"
														><?= $arBasketItems["NAME"] ?></a>
														<p class="h-basket__price">
																		<span><?= $arBasketItems["PRICE_FORMATED"] ?>
																				<span class="rub">₽</span>
																		</span>
																<span>
																<? if (!empty($arBasketItems["DISCOUNT_PRICE_PERCENT"])) { ?>
																		<?= $arBasketItems["FULL_PRICE_FORMATED"] ?><span class="rub">₽</span>
																<? } ?>
																		</span>
														</p>
														<p class="h-basket__quantity">
															Количество: <?= $arBasketItems['QUANTITY']?>
														</p>
														<p class="h-basket__total">
															Итого: <?= $arBasketItems["PRICE_FORMATED_TOTAL"]?> <span class="rub">₽</span>
														</p>
												</div>

										<? } ?>
									</div>
								</div>
                <div class="h-basket__total clearfix">
                    <p>
                        <span><?= GetMessage("SMALL_BASKET_TOTAL") ?></span>
                        <span><?= $arResult["allSum_FORMATED"] ?> <span class="rub">₽</span></span>
                    </p>
                    <a href="/cart/" class="btn btn_black h-basket__btn"><?= GetMessage("SMALL_BASKET_ORDER") ?></a>
                </div>
                <? } else { ?>
                    <p class="h-basket__title"><?= GetMessage("SMALL_BASKET_EMPTY") ?></p>
                <? } ?>
            </div>
        </div>
    </div>
</div>


