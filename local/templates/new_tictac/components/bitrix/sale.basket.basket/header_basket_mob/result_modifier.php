<?
foreach($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arBasketItems) {
	// Определяем стоимость с учетом кол-ва
	$arResult["ITEMS"]["AnDelCanBuy"][$key]["PRICE_FORMATED_TOTAL"] = SaleFormatCurrency($arBasketItems["PRICE"]*$arBasketItems["QUANTITY"], $arBasketItems["CURRENCY"]);

	// Краткое описание товара
	$res = CIBlockElement::GetByID($arBasketItems["PRODUCT_ID"]);
	if($ar_res = $res->GetNext()) {
		$arResult["ITEMS"]["AnDelCanBuy"][$key]["PREVIEW_TEXT"] = $ar_res['~PREVIEW_TEXT'];
		$arResult["ITEMS"]["AnDelCanBuy"][$key]["IMAGE"] = $ar_res['PREVIEW_PICTURE'];
	}

    $res = CIBlockElement::GetList(array(),array('ID' => $arBasketItems['PRODUCT_ID']), false, false, array('*'));
    while ($arEl = $res->GetNextElement()) {
        $arFields = $arEl->GetFields();
        $arProps = $arEl->GetProperties();
        $arResult["ITEMS"]["AnDelCanBuy"][$key]['BRAND'] = $arProps['BRAND_MODEL']['VALUE_ENUM'];
        $arResult["ITEMS"]["AnDelCanBuy"][$key]['COLORS'] = implode(',', $arProps['FILTER_COLOR']['VALUE_ENUM']);

        $nav = CIBlockSection::GetNavChain(false, $arFields['IBLOCK_SECTION_ID']);
        if ($arFirst = $nav->GetNext()) {
            if ($arFirst['ID'] == 172) {
                $arResult["ITEMS"]["AnDelCanBuy"][$key]['SEO_SECTION_NAME'] = 'часы';
            } else if ($arFirst['ID'] == 146) {
                $arResult["ITEMS"]["AnDelCanBuy"][$key]['SEO_SECTION_NAME'] = "ремешки";
            } else if ($arFirst['ID'] == 171) {
                $arResult["ITEMS"]["AnDelCanBuy"][$key]['SEO_SECTION_NAME'] = "украшения";
            } else if ($arFirst['ID'] == 165) {
                $arResult["ITEMS"]["AnDelCanBuy"][$key]['SEO_SECTION_NAME'] = "очки";
            }
        }
    }

}
?>