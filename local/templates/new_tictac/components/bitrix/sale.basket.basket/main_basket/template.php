<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $ORDER_ARTICLE = array(); ?>

<? if (!empty($arResult["ITEMS"]["AnDelCanBuy"])) {?>

    <? $arCart = array();?>
<? foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arBasketItems) { ?>

    <?
    $arCart[] = array(
        'name' => strip_tags($arBasketItems['NAME']),
        'id' => $arBasketItems['PRODUCT_ID'],
        'price' => $arBasketItems['FULL_PRICE'],
        'brand'=> $arBasketItems['BRAND'],
        'category'=> $arBasketItems['SEO_SECTION_NAME'],
        'variant'=> $arBasketItems['COLORS'],
        'quantity' => $arBasketItems['QUANTITY']
    );
}?>

    <script>
        dataLayer.push({
            "event": "cart",
            "ecommerce": {
                "checkout": {
                    "actionField":{"step": 1},
                    "products": <?= json_encode($arCart)?>
                }
            }
        });
    </script>

<div class="checkout__body">
    <div class="row">
        <div class="col-xs-12">
            <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="basket_form" id="basket_form">
            <div class="checkout__table">
                <div class="checkout-table__head clearfix">
                    <div class="checkout-table__tr ctt-1">
                        <p>Товар</p>
                    </div>
                    <div class="checkout-table__tr ctt-2">
                        <p>Количество</p>
                    </div>
                    <div class="checkout-table__tr ctt-3">
                        <p>Цена</p>
                    </div>
                    <div class="checkout-table__tr ctt-3">
                        <p>Итого</p>
                    </div>
                </div>

                <? foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems) { ?>
                    <? $ORDER_ARTICLE[] = $arBasketItems['ID']; ?>
                    <? $picture = CFile::GetPath($arBasketItems['IMAGE'])?>
                    <div class="checkout-table__body clearfix">
                        <div class="checkout-table__tr ctt-1">
                            <div class="checkout__item clearfix">
                                <img src="<?= $picture?>" class="checkout-order__img" alt="<?= $arBasketItems['NAME']?>">
                                <a href="<?= $arBasketItems['DETAIL_PAGE_URL']?>" class="checkout__name link"
                                   onclick="
                                       dataLayer.push({
                                       'event': 'productClick',
                                       'ecommerce': {
                                       'click': {
                                       'actionField': {'list': 'Страница корзины'},     // Список с которого был клик.
                                       'products': [{
                                       'name': '<?= strip_tags($arBasketItems['NAME'])?>',       	// Наименование товара.
                                       'id': '<?= $arBasketItems['PRODUCT_ID']?>',  				// Идентификатор товара в базе данных.
                                       'price': '<?= $arBasketItems['PRICE']?>',   				// Цена одной единицы купленного товара
                                       'brand': '<?= $arBasketItems['BRAND']?>',
                                       'category': '<?=$arBasketItems['SEO_SECTION_NAME']?>',
                                       'variant': '<?=$arBasketItems['COLORS']?>'
                                       }]
                                       }
                                       }
                                       });"
                                ><?= $arBasketItems['NAME']?></a>
								<? if ($arBasketItems["WARANTY_PRICE"] && $arBasketItems["WARANTY_ELEMENT"]):?>
									<div class="warantyBlock" style="position: relative;bottom: 20px;cursor:default;">
										<input  
											onClick="toggleWaranty($(this))"
											class="warantyCost" 
											style="display:inline-block;" 
											type="checkbox" 
											name="waranty<?=$arBasketItems["ID"]?>" 
											data-price="<?=$arBasketItems["WARANTY_PRICE"]?>" 
											data-element="<?=$arBasketItems["WARANTY_ELEMENT"]?>" 
											data-basket="<?=$arBasketItems["WARANTY_BASKET"]?>" value="Y"
											<?if ($arBasketItems["WARANTY_BASKET"]):?>checked<?endif?>
											> <span style="font-size:90%">Добавить дополнительную гарантию на год <?=intval($arBasketItems["WARANTY_PRICE"])?><span class="rub">₽</span></span>
									</div>
								<? endif ?>

                                <? if (!empty($arBasketItems['PROPS'])) {?>
                                    <? foreach ($arBasketItems['PROPS'] as $prop) { ?>
                                        <? if ($prop['CODE'] === 'COLOR') { ?>
                                            <?
                                            $res = CIBlockElement::GetByID($prop['VALUE'])->GetNext();
                                            $color_picture = CFile::GetPath($res['PREVIEW_PICTURE']);
                                            ?>
                                            <p class="basket_prop"><?= $prop['NAME']?>: <span class="prop_color" style="background-image: url(<?=$color_picture?>)"></span></p>

                                        <? } else { ?>

                                            <p class="basket_prop"><?= $prop['NAME']?>: <?=$prop['VALUE']?></p>

                                        <? } ?>

                                    <? } ?>
                                <? } ?>


                                <p class="checkout__price hidden-lg hidden-md">
                                    <? if (!empty($arBasketItems['DISCOUNT_PRICE_PERCENT'])) { ?>
                                        <span class="color_red"><?= $arBasketItems['PRICE_FORMATED']?> <span class="rub">₽</span></span>
                                        <span class="ch-price__before"><?= $arBasketItems['FULL_PRICE_FORMATED']?> <span class="rub">₽</span></span>
                                    <? } else { ?>
                                        <span class="price__num"><?= $arBasketItems['FULL_PRICE_FORMATED']?>  <span class="rub">₽</span> </span>
                                    <? } ?>
                                </p>
                                <div class="number hidden-lg hidden-md clearfix">
                                    <span class="minus"
                                          data-name="<?= strip_tags($arBasketItems['NAME'])?>"
                                          data-id="<?= $arBasketItems['PRODUCT_ID']?>"
                                          data-price="<?= $arBasketItems['PRICE']?>"
                                          data-brand="<?= $arBasketItems['BRAND']?>"
                                          data-category="<?=$arBasketItems['SEO_SECTION_NAME']?>"
                                          data-variant="<?=$arBasketItems['COLORS']?>"
                                    ><i class="icon-icon-minus"></i></span>
                                    <input class="basket_quantity_input" type="text" value="1" data-id="<?=$arBasketItems['ID']?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                    <span class="plus"><i class="icon-icon-plus"></i></span>
                                </div>
                                <div class="checkout__favorites">
                                    <span class="favorites__icon" data-id="<?= $arBasketItems['ID'] ?>">
                                        <i class="icon-icon-wishlist<?= isset($_SESSION["CATALOG_COMPARE_LIST"][5]["ITEMS"][$arBasketItems['ID']]) ? '-select' : '' ?>"></i>
                                    </span>
                                </div>

                                <a class="delete link_grey" data-id="<?= $arBasketItems['ID']?>" onclick="dataLayer.push({
                                    'event': 'removeFromCart',
                                    'ecommerce': {
                                    'remove': {
                                    'products': [{
                                    'name': '<?= strip_tags($arBasketItems['NAME'])?>',
                                    'id': '<?= $arBasketItems['PRODUCT_ID']?>',
                                    'price': '<?= $arBasketItems['PRICE']?>',
                                    'brand': '<?= $arBasketItems['BRAND']?>',
                                    'category': '<?=$arBasketItems['SEO_SECTION_NAME']?>',
                                    'variant': '<?=$arBasketItems['COLORS']?>',
                                    'quantity': '<?= $arBasketItems['QUANTITY']?>'
                                    }]
                                    }
                                    }
                                    });">Удалить</a>
                            </div>
                        </div>
                        <div class="checkout-table__tr ctt-2 hidden-sm hidden-xs">
                            <div class="number clearfix">
                                <span class="minus"
                                      data-name="<?= strip_tags($arBasketItems['NAME'])?>"
                                      data-id="<?= $arBasketItems['PRODUCT_ID']?>"
                                      data-price="<?= $arBasketItems['PRICE']?>"
                                      data-brand="<?= $arBasketItems['BRAND']?>"
                                      data-category="<?=$arBasketItems['SEO_SECTION_NAME']?>"
                                      data-variant="<?=$arBasketItems['COLORS']?>"><i class="icon-icon-minus"></i></span>
                                <input
                                    class="basket_quantity_input"
                                    type="text" value="<?=$arBasketItems["QUANTITY"]?>"
                                    data-id="<?=$arBasketItems['ID']?>"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'

                                    data-name="<?= strip_tags($arBasketItems['NAME'])?>"
                                    data-googleid="<?= $arBasketItems['PRODUCT_ID']?>"
                                    data-price="<?= $arBasketItems['PRICE']?>"
                                    data-brand="<?= $arBasketItems['BRAND']?>"
                                    data-category="<?=$arBasketItems['SEO_SECTION_NAME']?>"
                                    data-variant="<?=$arBasketItems['COLORS']?>"

                                />
                                <span class="plus"><i class="icon-icon-plus"></i></span>
                            </div>
                        </div>
                        <div class="checkout-table__tr ctt-3 hidden-sm hidden-xs">
                            <p class="checkout__price">
                                <? if (!empty($arBasketItems['DISCOUNT_PRICE_PERCENT'])) { ?>
                                    <span class="color_red"><?= $arBasketItems['PRICE_FORMATED']?> <span class="rub">₽</span></span>
                                    <span class="ch-price__before"><?= $arBasketItems['FULL_PRICE_FORMATED']?> <span class="rub">₽</span></span>
                                <? } else { ?>
                                    <span class="price__num"><?= $arBasketItems['FULL_PRICE_FORMATED']?>  <span class="rub">₽</span> </span>
                                <? } ?>
                                <input type="hidden" class="cart_item_price_for_one" value="<?= $arBasketItems['PRICE']?>">
                                <input type="hidden" class="quantity__input" id="quantity__input_<?=$arBasketItems['ID']?>" value="<?= $arBasketItems['QUANTITY']?>">
                            </p>
                        </div>
                        <div class="checkout-table__tr ctt-3 hidden-sm hidden-xs">
                            <p class="checkout__price">
                                <span class="price__num cart_item_total_price"><?= $arBasketItems['PRICE_FORMATED_TOTAL']?>  <span class="rub">₽</span> </span>
                            </p>
                        </div>
                    </div>
                <?
                    $product[]=$arBasketItems['ID'];
                    $prices[]=$arBasketItems['PRICE'];
                } ?>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/remarketing.php","PAGE_TYPE"=>"cart","PRODUCT"=>$product,"PRICE"=>$prices), false);?>

                <div class="discount__wrap">
                    <div class="discount__total hidden-lg hidden-md hidden-sm">
                        <p class="discount__price"><?= $arResult["allSum_FORMATED"] ?> <span class="rub">₽</span></p>
                    </div>
                    <div class="discount__input">
									<span class="input input--haruki">
                                        <? $last_coupon = end($arResult["COUPON_LIST"])?>
										<input class="input__field input__field--haruki" type="text" name="COUPON" value="<?= $last_coupon['COUPON'] ?>" id="coupon"/>
										<label class="input__label input__label--haruki" for="coupon">
											<span class="input__label-content input__label-content--haruki">Купон на скидку</span>
										</label>
									</span>
                        <input type="submit" class="btn btn_black discount__btn" value="Применить"/>
                    </div>
                    <div class="discount__total hidden-xs">
                        <p class="discount-total__text"><?= GetMessage("FULL_BASKET_TOTAL2") ?></p>
                        <p class="discount__price" id="total_basket_price"><?= $arResult["allSum_FORMATED"] ?> <span class="rub">₽</span></p>
                    </div>
                </div>
            </div>
                <input type="hidden" value="<? echo GetMessage("SALE_REFRESH") ?>" name="BasketRefresh">
            </form>
        </div>
    </div>
</div>

<? } ?>


