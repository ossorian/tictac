var textStatus = false;
BX.ready(function () {

	// Изменение количества в корзине
	if ( $('.product_count').length ) {
		$('.product_count').each( function () {
			var theCountBlock = $(this);
			var theCountInput = theCountBlock.find('input');
			theCountBlock.find('.product_count_up').on('click', function () {
				theCountInput.val( theCountInput.val()*1+1 );
				$(theCountInput).trigger('change');
			});
			theCountBlock.find('.product_count_down').on('click', function () {
				if ( theCountInput.val() != 1 ) {
					theCountInput.val( theCountInput.val()*1-1 );
					$(theCountInput).trigger('change');
				}
			});
		});
	}

	// Вспомагательная функция
	function ajaxpostshow(urlres, datares, wherecontent ){
		$('.product_count_up').css('display', 'none');
		$('.product_count_down').css('display', 'none');
		$('.product_delete').css('display', 'none');
		$.ajax({
			type: "POST",
			url: urlres,
			data: datares,
			dataType: "html",
			success: function(fillter){
				$(wherecontent).html(fillter);
				cartSmallUpdate(0);
			}
		});
	}

	// Удаление товара
	$('.product_delete').live("click",function(){
		var deletebasketid = $(this).attr('id');
		ajaxpostshow("/ajax/cart_full.php", deletebasketid, ".checkout" );
		return false;
	});

	// Изменение количества товара
	$('.product_change').live("change",function(){
		var countbasketid = $(this).attr('id');
		var countbasketcount = $(this).val();
		var ajaxcount = countbasketid + '&ajaxbasketcount=' + countbasketcount;
		ajaxpostshow("/ajax/cart_full.php", ajaxcount, ".checkout" );
		return false;
	});
});






// Малая корзина
function cartSmallUpdate(id) {
	$.ajax({
		type: "POST",
		url: "/ajax/cart.php",
		data: "id=" + id,
		success: function(msg){
			$('#smallcart').html(msg);

			// Корзина
			// появление/скрытие
			var cartSwitcher = $('.cart_switcher');
			var cartInfoBlock = $('.cart_info');
			cartSwitcher.on('click', function () {
				if ( cartSwitcher.hasClass('opened') ) {
					cartInfoBlock.fadeOut(300);
					cartSwitcher.removeClass('opened');
				}
				else {
					cartInfoBlock.fadeIn(300);
					cartSwitcher.addClass('opened');
				}
			});
			// удаление элемента
			$('.cart_info_delete a').live('click', function () {
				$(this).parent().parent().remove();
			});
		}
	});
}