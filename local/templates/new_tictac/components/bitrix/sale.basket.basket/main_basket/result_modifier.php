<?

// Общая cумма скидки
$arResult["DISCOUNT"] = 0;

//WARANTY DATA
/* foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $arItem) {
	$arProductID2ID[$arItem["PRODUCT_ID"]] = $arItem["ID"];
}

$arElements = array_column($arResult["ITEMS"]["AnDelCanBuy"], "PRODUCT_ID");
$dbResult = CIBlockElement::GetList([], ["ID" => $arElements], false, false, ["ID", "IBLOCK_SECTION_ID"]);
while ($element = $dbResult->Fetch()) {
	$arSectionIDs[] = $element["IBLOCK_SECTION_ID"];
	$arProductSections[$element["ID"]] = $element["IBLOCK_SECTION_ID"];
}

$dbSections = CIBlockSection::GetList(["ID" => "ASC"], ["IBLOCK_ID" => 5, "ID" => $arElements2Sections], false, ["ID", "UF_WARANTY_ELEMENT"]);
while ($section = $dbSections->Fetch()) {
	if ($section["UF_WARANTY_ELEMENT"]) {
		$warantyData[$section["ID"]] = $section["UF_WARANTY_ELEMENT"];
	}
}

if ($warantyData) {
	$arWarantyPrices = Waranty::getPrices($warantyData);
	foreach ($arResult["ITEMS"]["AnDelCanBuy"] as &$arBasketItem) {
		$productSection = $arProductSections[$arBasketItem["PRODUCT_ID"]];
		$warantyElement = $warantyData[$productSection];
		
		$arBasketItem["WARANTY_PRICE"] = $arWarantyPrices[$productSection];
		$arBasketItem["WARANTY_ELEMENT"] = $warantyData[$productSection];
		$arBasketItem["WARANTY_BASKET"] = $arProductID2ID[$warantyElement];
	}
} */

foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arItem) {
	$arProductID2keys[$arItem["PRODUCT_ID"]] = $key;
	$arProductID2ID[$arItem["PRODUCT_ID"]] = $arItem["ID"];
}

$arElements = array_column($arResult["ITEMS"]["AnDelCanBuy"], "PRODUCT_ID");
$dbResult = CIBlockElement::GetList([], ["ID" => $arElements], false, false, ["ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PROPERTY_GUARANTY"]);
while ($element = $dbResult->Fetch()) {
	if ($element["IBLOCK_ID"] == 22) {
		$key = $arProductID2keys[$element["ID"]];
		$arResult["ITEMS"]["AnDelCanBuy"][$key]["IS_GUARANTY"] = true;
		$arGuaranyExists[$element["ID"]] = true;
	}
	else {
		if ($element["PROPERTY_GUARANTY_VALUE"]) {
			$key = $arProductID2keys[$element["ID"]];
			$arResult["ITEMS"]["AnDelCanBuy"][$key]["WARANTY_ELEMENT"] = $element["PROPERTY_GUARANTY_VALUE"];
		}
		$arSectionIDs[] = $element["IBLOCK_SECTION_ID"];
		$arProductSections[$element["ID"]] = $element["IBLOCK_SECTION_ID"];
	}
}

$dbSections = CIBlockSection::GetList(["ID" => "ASC"], ["IBLOCK_ID" => 5, "ID" => $arSectionIDs], false, ["ID", "UF_WARANTY_COST"]);
while ($section = $dbSections->Fetch()) {
	if ($section["UF_WARANTY_COST"]) {
		$warantyData[$section["ID"]]["COST"] = $section["UF_WARANTY_COST"];
	}
}

if ($warantyData) {
//	$arWarantyPrices = Waranty::getPrices($warantyData);
	foreach ($arResult["ITEMS"]["AnDelCanBuy"] as &$arBasketItem) {
 		$productSection = $arProductSections[$arBasketItem["PRODUCT_ID"]];
		$arBasketItem["WARANTY_PRICE"] = $warantyData[$productSection]["COST"];
		if ($arBasketItem["WARANTY_PRICE"]) {
	 		if (empty($arBasketItem["WARANTY_ELEMENT"])) $arBasketItem["WARANTY_ELEMENT"] = Waranty::createWarantyProduct($arBasketItem["PRODUCT_ID"], $arBasketItem["WARANTY_PRICE"]);
			if ($arBasketItem["WARANTY_ELEMENT"]) $arBasketItem["WARANTY_BASKET"] = $arProductID2ID[$arBasketItem["WARANTY_ELEMENT"]];
		}
	}
}
// -- WARANTY DATA	

foreach($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arBasketItems) {
 	if ($arBasketItems["IS_GUARANTY"]) {
		unset($arResult["ITEMS"]["AnDelCanBuy"][$key]);
		continue;
	}

 /* 	if ($USER->IsAdmin()) {
		var_dump($arBasketItems);
	} */
	
	// Определяем стоимость с учетом кол-ва
	if ($arBasketItems["WARANTY_ELEMENT"] && $arGuaranyExists[$arBasketItems["WARANTY_ELEMENT"]] && $arBasketItems["WARANTY_PRICE"]) {
		$arResult["ITEMS"]["AnDelCanBuy"][$key]["PRICE_FORMATED_TOTAL"] = SaleFormatCurrency(($arBasketItems["PRICE"] + $arBasketItems["WARANTY_PRICE"])* $arBasketItems["QUANTITY"], $arBasketItems["CURRENCY"]);
	}
	else $arResult["ITEMS"]["AnDelCanBuy"][$key]["PRICE_FORMATED_TOTAL"] = SaleFormatCurrency($arBasketItems["PRICE"] * $arBasketItems["QUANTITY"], $arBasketItems["CURRENCY"]);

	$arResult["DISCOUNT"] += $arBasketItems["DISCOUNT_PRICE"]*$arBasketItems["QUANTITY"];

	// Краткое описание товара
	$res = CIBlockElement::GetByID($arBasketItems["PRODUCT_ID"]);
	if($ar_res = $res->GetNext()) {
		//$arResult["ITEMS"]["AnDelCanBuy"][$key]["PREVIEW_TEXT"] = $ar_res['~PREVIEW_TEXT'];
		$arResult["ITEMS"]["AnDelCanBuy"][$key]["IMAGE"] = $ar_res['PREVIEW_PICTURE'];
	}

	$res = CIBlockElement::GetList(array(),array('ID' => $arBasketItems['PRODUCT_ID']), false, false, array('*'));
	while ($arEl = $res->GetNextElement()) {
		$arFields = $arEl->GetFields();
		$arProps = $arEl->GetProperties();
		$arResult["ITEMS"]["AnDelCanBuy"][$key]['BRAND'] = $arProps['BRAND_MODEL']['VALUE_ENUM'];
		$arResult["ITEMS"]["AnDelCanBuy"][$key]['COLORS'] = implode(',', $arProps['FILTER_COLOR']['VALUE_ENUM']);

		$nav = CIBlockSection::GetNavChain(false, $arFields['IBLOCK_SECTION_ID']);
		if ($arFirst = $nav->GetNext()) {
			if ($arFirst['ID'] == 172) {
				$arResult["ITEMS"]["AnDelCanBuy"][$key]['SEO_SECTION_NAME'] = 'часы';
			} else if ($arFirst['ID'] == 146) {
				$arResult["ITEMS"]["AnDelCanBuy"][$key]['SEO_SECTION_NAME'] = "ремешки";
			} else if ($arFirst['ID'] == 171) {
				$arResult["ITEMS"]["AnDelCanBuy"][$key]['SEO_SECTION_NAME'] = "украшения";
			} else if ($arFirst['ID'] == 165) {
				$arResult["ITEMS"]["AnDelCanBuy"][$key]['SEO_SECTION_NAME'] = "очки";
			}
		}
	}



}


// Общая цена без скидки
$arResult["TOTAL"] = $arResult["allSum"] + $arResult["DISCOUNT"];

// Отформатированные значения
$arResult["TOTAL_FORMATED"] = SaleFormatCurrency($arResult["TOTAL"], "RUB");
$arResult["DISCOUNT_FORMATED"] = SaleFormatCurrency($arResult["DISCOUNT"], "RUB");
?>

