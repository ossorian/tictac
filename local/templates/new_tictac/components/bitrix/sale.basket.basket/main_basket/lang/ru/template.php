<?
$MESS ['SALE_REFRESH'] = "       Обновить       ";
$MESS ['SALE_ORDER'] = "Оформить заказ";
$MESS ['SALE_NAME'] = "Название";
$MESS ['SALE_PROPS'] = "Свойства";
$MESS ['SALE_PRICE'] = "Цена";
$MESS ['SALE_PRICE_TYPE'] = "Тип цены";
$MESS ['SALE_QUANTITY'] = "Количество";
$MESS ['SALE_DELETE'] = "Удалить";
$MESS ['SALE_OTLOG'] = "Отложить";
$MESS ['SALE_WEIGHT'] = "Вес";
$MESS ['SALE_WEIGHT_G'] = "г";
$MESS ['SALE_ITOGO'] = "Итого";
$MESS ['SALE_REFRESH_DESCR'] = "Нажмите эту кнопку, чтобы пересчитать, удалить или отложить товары.";
$MESS ['SALE_ORDER_DESCR'] = "Нажмите эту кнопку, чтобы заказать товары, находящиеся в корзине";
$MESS ['SALE_OTLOG_TITLE'] = "Отложено";
$MESS ['SALE_UNAVAIL_TITLE'] = "Отсутствуют в продаже";
$MESS ['STB_ORDER_PROMT'] = "Для того чтобы начать оформление заказа, нажмите кнопку \"Оформить заказ\".";
$MESS ['STB_COUPON_PROMT'] = "Если у вас есть код купона для получения скидки, введите его здесь:";
$MESS ['SALE_VAT'] = "НДС:";
$MESS ['SALE_VAT_INCLUDED'] = "В том числе НДС:";
$MESS ['SALE_TOTAL'] = "Всего:";
$MESS ['SALE_CONTENT_DISCOUNT'] = "Скидка";
$MESS ['SALE_DISCOUNT'] = "Скидка";

$MESS ['FULL_BASKET_ORDER'] = "Оформление покупки";
$MESS ['FULL_BASKET_UPDATE'] = "Уточнение товара";
$MESS ['FULL_BASKET_CURRENCY'] = "руб.";
$MESS ['FULL_BASKET_DELETE'] = "Удалить";
$MESS ['FULL_BASKET_PCS'] = "шт.";
$MESS ['FULL_BASKET_TOTAL'] = "Стоимость корзины";
$MESS ['FULL_BASKET_TOTAL2'] = "Итого";
$MESS ['FULL_BASKET_TOTAL3'] = "Общая стоимость заказа";
$MESS ['FULL_BASKET_DISCOUNT'] = "Скидка";
$MESS ['FULL_BASKET_PROMO'] = "Применить промо-код";
$MESS ['FULL_BASKET_CONTINUE'] = "Продолжить оформление";

$MESS ['FULL_BASKET_PROMO_LINE_1'] = "У меня";
$MESS ['FULL_BASKET_PROMO_LINE_2'] = "<b>1.</b> Промо-код на последующие заказы предоставляется всем совершившим покупки на сумму более 5000 рублей.<br/ ><b>2.</b> Скидки не суммируются. На один товар предоставляется максимальная скидка из возможных.";
$MESS ['FULL_BASKET_PROMO_LINE_3'] = "есть промо-код";
?>