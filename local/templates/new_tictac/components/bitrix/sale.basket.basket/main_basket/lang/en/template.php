<?
$MESS ['SALE_REFRESH'] = "Refresh";
$MESS ['SALE_ORDER'] = "Check out";
$MESS ['SALE_NAME'] = "Name";
$MESS ['SALE_PRICE'] = "Price";
$MESS ['SALE_PRICE_TYPE'] = "Price type";
$MESS ['SALE_QUANTITY'] = "Quantity";
$MESS ['SALE_DELETE'] = "Delete";
$MESS ['SALE_OTLOG'] = "Hold over";
$MESS ['SALE_WEIGHT'] = "Weight";
$MESS ['SALE_WEIGHT_G'] = "g";
$MESS ['SALE_ITOGO'] = "Total";
$MESS ['SALE_REFRESH_DESCR'] = "Click this button to: recalculate, delete or hold products";
$MESS ['SALE_ORDER_DESCR'] = "Click this button to check out and order products in your cart";
$MESS ['SALE_OTLOG_TITLE'] = "Hold over";
$MESS ['SALE_UNAVAIL_TITLE'] = "Currently unavailable";
$MESS ['STB_ORDER_PROMT'] = "Click \"Check out\" to complete your order";
$MESS ['STB_COUPON_PROMT'] = "If you have special coupon code for discount, please enter it here:";
$MESS ['SALE_VAT'] = "Tax:";
$MESS ['SALE_VAT_INCLUDED'] = "Tax included:";
$MESS ['SALE_TOTAL'] = "Total:";
$MESS ['SALE_CONTENT_DISCOUNT'] = "Discount";
$MESS ['SALE_DISCOUNT'] = "Discount";
$MESS ['SALE_PROPS'] = "Properties";

$MESS ['FULL_BASKET_ORDER'] = "Proceed to checkout";
$MESS ['FULL_BASKET_UPDATE'] = "Order details";
$MESS ['FULL_BASKET_CURRENCY'] = "&euro;";
$MESS ['FULL_BASKET_DELETE'] = "Delete";
$MESS ['FULL_BASKET_PCS'] = "pcs.";
$MESS ['FULL_BASKET_TOTAL'] = "Total cost";
$MESS ['FULL_BASKET_TOTAL2'] = "Total";
$MESS ['FULL_BASKET_TOTAL3'] = "Subtotal";
$MESS ['FULL_BASKET_DISCOUNT'] = "Discount";
$MESS ['FULL_BASKET_PROMO'] = "Apply coupon";
$MESS ['FULL_BASKET_CONTINUE'] = "Proceed to checkout";

$MESS ['FULL_BASKET_PROMO_LINE_1'] = "I have a";
$MESS ['FULL_BASKET_PROMO_LINE_2'] = "<b>1.</b> Every customer gets a coupon code with a discount on any following order.<br/ ><b>2.</b> Discount offers do not stack. In case of multiple offers the highest is applied.";
$MESS ['FULL_BASKET_PROMO_LINE_3'] = "promo code";
?>
