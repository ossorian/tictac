<?php
// Лень в заполнении исправляем автозаменой
if(LANGUAGE_ID == "en") {
	$arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"] = str_replace("мм", "mm", $arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"]);
	$arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"] = str_replace(array("месяца", "месяцев", "месяц"), "month", $arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"]);

	$arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"] = str_replace(
		array("Австралия", "США", "Испания", "Германия", "Франция", "Швеция", "Гонконг", "Япония", "Белоруссия", "Великобритания", "Италия"),
		array("Australia", "USA", "Spain", "Germany", "France", "Sweden", "Hong Kong", "Japan", "Belarus", "United Kingdom", "Italy"),
		$arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"]
	);

	$arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"] = str_replace(
		array("Австралия", "США", "Испания", "Германия", "Франция", "Швеция", "Гонконг", "Япония", "Белоруссия", "Великобритания", "Италия"),
		array("Australia", "USA", "Spain", "Germany", "France", "Sweden", "Hong Kong", "Japan", "Belarus", "United Kingdom", "Italy"),
		$arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"]
	);

	$arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"] = str_replace(
		array("Минеральное", "Сапфировое", "Укрепленное минеральное"),
		array("Mineral", "Sapphire", "Hardened mineral"),
		$arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"]
	);

	$arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"] = str_replace(
		array("Нержавеющая сталь", "полиуретан", "натуральная кожа", "керамика", "Полимер", "силикон", "резиновый ремень", "Ацетат", "Нейлон"),
		array("Stainless steel", "Polyurethane", "Genuine Leather", "Ceramic", "Polymer", "Silicone", "Rubber", "Acetate", "Nylon"),
		$arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"]
	);
}

$nav = CIBlockSection::GetNavChain(false, $arResult['IBLOCK_SECTION_ID']);
if ($arFirst = $nav->GetNext()) {
	if ($arFirst['ID'] == 172) {
		$arResult['SEO_SECTION_NAME'] = 'часы';
		$arResult['MORE_TEXT'] = 'часов';
	} else if ($arFirst['ID'] == 146) {
		$arResult['SEO_SECTION_NAME'] = "ремешки";
		$arResult['MORE_TEXT'] = 'ремешков';
	} else if ($arFirst['ID'] == 171) {
		$arResult['SEO_SECTION_NAME'] = "украшения";
		$arResult['MORE_TEXT'] = 'украшений';
	} else if ($arFirst['ID'] == 165) {
		$arResult['SEO_SECTION_NAME'] = "очки";
		$arResult['MORE_TEXT'] = 'очков';
	}
}


?>

