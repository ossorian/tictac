<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?$this->setFrameMode(true);?>
<style>
    .one-product__slider_navigation .slick-list {
        min-height: 425px;
    }
</style>

<?
$cat = $arResult['SEO_SECTION_NAME'];

?>

<?
$elPrice = 0;
foreach ($arResult["PRICES"] as $code => $arPrice) {
    if ($arPrice["CAN_ACCESS"]) {
        if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
            $elPrice = $arPrice['DISCOUNT_VALUE'];
        } else {
            $elPrice = $arPrice['VALUE'];
        }
    }
}
?>


<script>
    dataLayer.push({
        'event': 'productDetail',
        'ecommerce': {
            'detail': {
                'actionField': {'list': 'Быстрый просмотр'},    // Список в котором находится товар.
                'products': [{
                    'name': '<?= strip_tags($arResult['NAME'])?>',
                    'id': '<?= $arResult['ID']?>',
                    'price': '<?= $elPrice?>',
                    'brand': '<?= $arResult['PROPERTIES']['BRAND_MODEL']['VALUE_ENUM']?>',
                    'category': '<?= $cat?>',
                    'variant': '<?= implode(',', $arResult['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>'
                }]
            }
        }
    });
</script>


<div class="row">
    <div class="col-md-6">
        <div class="one-product">
            <div class="one-product__slider">
                <? if (!empty($arResult['PROPERTIES']['GALLERY']['VALUE'])) { ?>
                    <? foreach ($arResult['PROPERTIES']['GALLERY']['VALUE'] as $detailPicture) { ?>
                        <? //$picture = resizeImage($detailPicture, 460, 598); ?>
                        <? $picture = CFile::GetPath($detailPicture); ?>
                        <div class="item-slide clearfix">
                            <a class="tovar-popup magnifier-thumb-wrapper" href="#">
                                <img src="<?= $picture ?>" class="tovar-img">
                            </a>
                        </div>
                    <? } ?>
                <? } else { ?>
                    <div class="item-slide clearfix">
                        <a class="tovar-popup magnifier-thumb-wrapper" href="#">
                            <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" class="tovar-img">
                        </a>
                    </div>
                <? } ?>
            </div>
            <div class='one-product__slider_navigation-wrapper'>
                <div class="one-product__slider_navigation">
                    <? if (!empty($arResult['PROPERTIES']['GALLERY']['VALUE'])) { ?>

                        <? foreach ($arResult['PROPERTIES']['GALLERY']['VALUE'] as $previewPicture) { ?>

                            <? $picture = resizeImage($previewPicture, 60, 60); ?>
                            <div class="item-nav">
                                <img src="<?= $picture['SRC'] ?>" alt=""/>
                            </div>

                        <? } ?>
                    <? } else { ?>
                        <div class="item-nav">
                            <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" alt=""/>
                        </div>
                    <? } ?>
                </div>
                <? if (!empty($arResult['PROPERTIES']['VIDEO_LINK']['VALUE'])) { ?>
                    <a href="#you_pop" class="product__video"
                       data-src="https://www.youtube.com/embed/<?= substr($arResult['PROPERTIES']['VIDEO_LINK']['VALUE'], -11) ?>">
                        <i class="icon-icon-video"></i>
                        <p class="product-video__text">Видео</p>
                    </a>
                <? } ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <h3 class="product__name"><?= $arResult['NAME'] ?></h3>
        <? $elPrice = '';?>
        <? foreach ($arResult['PRICES'] as $price) {
            $elPrice = $price['VALUE'];
            ?>
            <? if (!empty($price['DISCOUNT_DIFF_PERCENT'])) { ?>
                <p class="product__price"><span class='price__num price__new'><?= $price['PRINT_DISCOUNT_VALUE'] ?></span> <span class="rub">₽</span>
                    <span class="old_price" style="text-decoration: line-through"><?= $price['PRINT_VALUE'] ?> <span
                            class="rub">₽</span></span></p>
            <? } else { ?>
                <p class="product__price"><?= $price['PRINT_VALUE'] ?> <span class="rub">₽</span></p>
            <? } ?>
        <? } ?>

        <div class="product__order clearfix">
            <? if (!empty($arResult["DISPLAY_PROPERTIES"]["WAIT"]["DISPLAY_VALUE"])) { ?>
                <a href="#" class="btn btn_black product__btn">Оформить предзаказ</a>
            <? } else { ?>
                <a href="#" class="btn btn_black product__btn add_to_cart_btn" data-product-id="<?= $arResult["ID"] ?>"
                   data-product-image="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>"
                   onclick="
                       dataLayer.push({
                       'event': 'addToCart',
                       'ecommerce': {
                       'currencyCode': 'RUB',
                       'add': {
                       'products': [{
                       'name': '<?= strip_tags($arResult["NAME"]) ?>',
                       'id': '<?= $arResult["ID"] ?>',
                       'price': '<?= $elPrice ?>',
                       'brand': '<?= $arResult['PROPERTIES']['BRAND_MODEL']['VALUE_ENUM']?>',
                       'category': '<?= $cat?>',
                       'variant': '<?= implode(',', $arResult['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',
                       'quantity': 1
                       }]
                       }
                       }
                       });
                       fbq('track', 'AddToCart', {
                       currency: 'RUB',
                       });
                           ">Добавить в корзину</a>
            <? } ?>
            <span class="favorites__icon" data-id="<?= $arResult['ID'] ?>">
							<i class="icon-icon-wishlist<?= isset($_SESSION["CATALOG_COMPARE_LIST"][$arResult['IBLOCK_ID']]["ITEMS"][$arResult['ID']]) ? '-select' : '' ?>"></i>
						</span>
        </div>
        <div class="product__content">
            <div class="product__body product__body_active">
                <div class="characteristics">
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_SIZE") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= str_replace("?", "&empty;", $arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"]) ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_MM") ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WRIST"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WRIST") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WRIST"]["DISPLAY_VALUE"] ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_MM") ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WEIGHT"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_EIGHT") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WEIGHT"]["DISPLAY_VALUE"] ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_GRAMM") ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_COUNTRY") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_MATERIAL") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_MECHANISM") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_GLASS") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WATER"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WATER") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WATER"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WARRANTY") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["FRAME_MATERIAL"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_FRAME_MATERIAL") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["FRAME_MATERIAL"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["LENSES"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_LENSES") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["LENSES"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["UF_ABSORB"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_UF_ABSORB") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["UF_ABSORB"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["WIDTH_BETWEEN_EARINGS"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WIDTH_BETWEEN_EARINGS") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["WIDTH_BETWEEN_EARINGS"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["WIDTH_OF_STRAP_ATTACHMENT"]["DISPLAY_VALUE"] != false): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WIDTH_OF_STRAP_ATTACHMENT") ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["WIDTH_OF_STRAP_ATTACHMENT"]["DISPLAY_VALUE"] ?>
										</span></p>
                    <? endif; ?>
                    <? if (!empty($arResult["PROPERTIES"]["ACCESSORY_SIZE"]["VALUE"])): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= $arResult["PROPERTIES"]["ACCESSORY_SIZE"]["NAME"] ?>:</span></span>
                            <span
                                class="chr-text__right"><?= $arResult["PROPERTIES"]["ACCESSORY_SIZE"]["VALUE"] ?>
										</span></p>
                    <? endif; ?>

                    <? if (!empty($arResult["PROPERTIES"]["ACCESSORY_COLOR"]["VALUE"])): ?>
                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= $arResult["PROPERTIES"]["ACCESSORY_COLOR"]["NAME"] ?>:</span></span>
                            <span
                                class="chr-text__right"><?= implode(", ", $arResult["PROPERTIES"]["ACCESSORY_COLOR"]["VALUE"]) ?>
										</span></p>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <div class="more-brend">
            <p>
                <span class="more-brend__text">Больше <?= $arResult['MORE_TEXT']?> от</span>
                <a href="<?= $arResult["SECTION"]["SECTION_PAGE_URL"] ?>">
                    <img src="<?= CFile::GetPath($arResult["SECTION"]["PICTURE"]) ?>" class="more-brend__logo" alt="">
                </a>
            </p>
        </div>
        <a href="<?= $arResult['DETAIL_PAGE_URL'] ?>"
           data-seo="Быстрый просмотр"
           class="btn btn_trans_grey on-page-clock"
           onclick="
               dataLayer.push({
               'event': 'productClick',
               'ecommerce': {
               'click': {
               'actionField': {'list': 'Быстрый просмотр'},     // Список с которого был клик.
               'products': [{
               'name': '<?= strip_tags($arResult['NAME'])?>',       	// Наименование товара.
               'id': '<?= $arResult['ID']?>',  				// Идентификатор товара в базе данных.
               'price': '<?= $elPrice?>',   				// Цена одной единицы купленного товара
               'brand': '<?= $arResult['PROPERTIES']['BRAND_MODEL']['VALUE_ENUM']?>',
               'category': '<?= $cat?>',
               'variant': '<?= implode(',', $arResult['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',
               'position': 1  				//Позиция товара в списке
               }]
               }
               }
               });">На страницу товара</a>
    </div>
</div>

