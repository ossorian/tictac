<?php
// Лень в заполнении исправляем автозаменой
if (LANGUAGE_ID == "en") {
    $arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"] = str_replace("мм", "mm", $arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"]);
    $arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"] = str_replace(array("месяца", "месяцев", "месяц"), "month", $arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"]);

    $arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"] = str_replace(
        array("Австралия", "США", "Испания", "Германия", "Франция", "Швеция", "Гонконг", "Япония", "Белоруссия", "Великобритания", "Италия"),
        array("Australia", "USA", "Spain", "Germany", "France", "Sweden", "Hong Kong", "Japan", "Belarus", "United Kingdom", "Italy"),
        $arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"]
    );

    $arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"] = str_replace(
        array("Австралия", "США", "Испания", "Германия", "Франция", "Швеция", "Гонконг", "Япония", "Белоруссия", "Великобритания", "Италия"),
        array("Australia", "USA", "Spain", "Germany", "France", "Sweden", "Hong Kong", "Japan", "Belarus", "United Kingdom", "Italy"),
        $arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"]
    );

    $arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"] = str_replace(
        array("Минеральное", "Сапфировое", "Укрепленное минеральное"),
        array("Mineral", "Sapphire", "Hardened mineral"),
        $arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"]
    );

    $arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"] = str_replace(
        array("Нержавеющая сталь", "полиуретан", "натуральная кожа", "керамика", "Полимер", "силикон", "резиновый ремень", "Ацетат", "Нейлон"),
        array("Stainless steel", "Polyurethane", "Genuine Leather", "Ceramic", "Polymer", "Silicone", "Rubber", "Acetate", "Nylon"),
        $arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"]
    );
}

$res = CIBlockSection::GetByID($arResult['IBLOCK_SECTION_ID']);
if ($ar_res = $res->GetNext()) {

    $arResult['SECTION_DESCRIPTION'] = $ar_res['DESCRIPTION'];
    $arResult['SECTION_IMG']['SRC'] = Cfile::GetPath($ar_res['PICTURE']);
    $arResult['SECTION_IMG']['ALT'] = $ar_res['NAME'];
    $arResult['SECTION_PAGE_URL'] = $ar_res['SECTION_PAGE_URL'];

}

$db_old_groups = CIBlockElement::GetElementGroups($arResult['ID'], true);
$ar_new_groups = Array();
while($ar_group = $db_old_groups->GetNExt()){
    $ar_new_groups[] = $ar_group["ID"];
}

$ar_new_sections = array();

if(!empty($ar_new_groups)){
    $arFilter = array('IBLOCK_ID' => $arResult['IBLOCK_ID'], 'ACTIVE' => "Y", 'ID' => $ar_new_groups);
    $rsSections = CIBlockSection::GetList(array('SORT' => 'ASC'), $arFilter,false,array(),array('nTopCount'=>1));
    while ($arSction = $rsSections->GetNext())
    {
        $ar_new_sections[] = $arSction;
    }
    $arResult['SECTION_DESCRIPTION'] = $ar_new_sections[0]['DESCRIPTION'];
    $arResult['SECTION_IMG']['SRC'] = Cfile::GetPath($ar_new_sections[0]['PICTURE']);
    $arResult['SECTION_IMG']['ALT'] = $ar_new_sections[0]['NAME'];
    $arResult['SECTION_PAGE_URL'] = $ar_new_sections[0]['SECTION_PAGE_URL'];
}


//related products

//get similar watches
/*$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "CATALOG_PRICE_1");
$arFilter = array(
    "IBLOCK_ID" => 5,
    "ACTIVE" => "Y",
    "PROPERTY_FILTER_COLOR" => $arResult['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM_ID'],
    "PROPERTY_SEX" => $arResult['PROPERTIES']['SEX']['VALUE_ENUM_ID'],
    "IBLOCK_SECTION_ID" => 172,
    "SECTION_ACTIVE" => "Y",
    "INCLUDE_SUBSECTIONS" => "Y",
    "><CATALOG_PRICE_1" => array((int)$arResult['CATALOG_PRICE_1'] - 5000, (int)$arResult['CATALOG_PRICE_1'] + 5000),
    "!ID" => $arResult['ID']
);
$res = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 4), array('ID', 'NAME'));
while ($ob = $res->GetNext()) {
    $arResult['RELATED_PRODUCTS'][] = $ob['ID'];//array_merge($arFields, $arProps);
}

$watchWidth = intval($arResult['PROPERTIES']['WIDTH_OF_STRAP_ATTACHMENT']['VALUE']);

//get similar straps
$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "CATALOG_PRICE_1");
$arFilter = array(
    "IBLOCK_ID" => 5,
    "ACTIVE" => "Y",
    //"PROPERTY_SEX" => $arResult['PROPERTIES']['SEX']['VALUE_ENUM_ID'],
    "SECTION_ID" => 146,
    //"SECTION_ACTIVE" => "Y",
);
if (!empty($watchWidth)) {
    $arFilter["PROPERTY_STRAP_WIDTH_VALUE"] = $watchWidth;
}
$res = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 4), array('ID', 'NAME', 'PROPERTY_STRAP_WIDTH'));
while ($ob = $res->GetNext()) {
    $arResult['RELATED_PRODUCTS'][] = $ob['ID'];//array_merge($arFields, $arProps);
}*/




if (!empty($arResult['PROPERTIES']['CHOSE_COLOR'])) {
    $arResult['CHOOSE_COLOR'] = array();

    foreach ($arResult['PROPERTIES']['CHOSE_COLOR']['VALUE'] as $color) {
        $res = CIBlockElement::GetByID($color)->GetNext();
        $color_picture = CFile::GetPath($res['PREVIEW_PICTURE']);
        $arResult['CHOOSE_COLOR'][$res['ID']] = array('COLOR_PICTURE' => $color_picture, 'COLOR_NAME' => $res['NAME']);
    }
}

$nav = CIBlockSection::GetNavChain(false, $arResult['IBLOCK_SECTION_ID']);
if ($arFirst = $nav->GetNext()) {
    if ($arFirst['ID'] == 172) {
//        $arResult['SEO_SECTION_NAME'] = 'часы';
        $arResult['RELATED_PRODUCTS'] = getRelatedProducts(172, $arResult);
        $arResult['MORE_TEXT'] = 'часов';
    } else if ($arFirst['ID'] == 146) {
//        $arResult['SEO_SECTION_NAME'] = "ремешки";
        $arResult['RELATED_PRODUCTS'] = getRelatedProducts(146, $arResult);
        $arResult['MORE_TEXT'] = 'ремешков';
    } else if ($arFirst['ID'] == 171) {
//        $arResult['SEO_SECTION_NAME'] = "украшения";
        $arResult['RELATED_PRODUCTS'] = getRelatedProducts(171, $arResult);
        $arResult['MORE_TEXT'] = 'украшений';
    } else if ($arFirst['ID'] == 165) {
//        $arResult['SEO_SECTION_NAME'] = "очки";
        $arResult['RELATED_PRODUCTS'] = getRelatedProducts(165, $arResult);
        $arResult['MORE_TEXT'] = 'очков';
    }
}
$sectObj = CIBlockSection::GetList(
    Array("SORT"=>"ASC"),
    Array("ID"=>$arResult["~IBLOCK_SECTION_ID"],"IBLOCK_ID"=>$arResult["IBLOCK_ID"]),
    false,
    Array("NAME","ID","IBLOCK_ID"),
    false
);
if($sect= $sectObj->GetNext()){
    $arResult["PARENT_SECTION_NAME"] = $sect["NAME"];
}

function getRelatedProducts($sectionId, $arResult) {
    $arSelect = Array("ID");
    $arFilter = array(
        "IBLOCK_ID" => 5,
        "ACTIVE" => "Y",
//        "PROPERTY_FILTER_COLOR" => $arResult['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM_ID'],
//        "PROPERTY_SEX" => $arResult['PROPERTIES']['SEX']['VALUE_ENUM_ID'],
        "SECTION_ACTIVE" => "Y",
        "INCLUDE_SUBSECTIONS" => "Y",
        "!ID" => $arResult['ID']
    );

    $response = array();

    switch ($sectionId) {
        case 146: // ремешки
            $arFilter['SECTION_ID'] = 146;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 4), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }

            $arFilter['SECTION_ID'] = 172;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 2), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }

            $arFilter['SECTION_ID'] = 165;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }

            $arFilter['SECTION_ID'] = 171;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }
            break;
        case 165: // очки
            $arFilter['SECTION_ID'] = 165;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 5), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }

            $arFilter['SECTION_ID'] = 172;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 2), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }

            $arFilter['SECTION_ID'] = 171;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }
            break;
        case 171: // украшения
            $arFilter['SECTION_ID'] = 171;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 5), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }

            $arFilter['SECTION_ID'] = 172;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 2), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }

            $arFilter['SECTION_ID'] = 165;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }
            break;
        case 172: // часы
            $arFilter["SECTION_ID"] = 172;
//            $arFilter["><CATALOG_PRICE_1"] = array((int)$arResult['CATALOG_PRICE_1'] - 5000, (int)$arResult['CATALOG_PRICE_1'] + 5000);
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 3), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }
//            unset($arFilter["><CATALOG_PRICE_1"]);

            $arFilter['SECTION_ID'] = 146;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 2), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }


            $arFilter['SECTION_ID'] = 165;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }

            $arFilter['SECTION_ID'] = 171;
            $obItems = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, Array("nPageSize" => 2), $arSelect);
            while ($arItem = $obItems->GetNext()) {
                $response[] = $arItem['ID'];
            }
            break;
    }
    return $response;
}
?>