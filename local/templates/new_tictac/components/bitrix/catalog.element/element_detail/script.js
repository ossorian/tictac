function sendFormQuestion() {
	$.ajax({
		type: "POST",
		url: $('#formQuestion').attr("action"),
		data: $('#formQuestion').serialize(),
		success: function(msg){
			if (msg == 'OK') {
				// $('.closelink a').click();
                if(LANGUAGE_ID == "ru") {
				    alert('Спасибо за вопрос. Наш менеджер свяжется с Вами в ближайшее время.');
                }
                else {
                    alert('Thank you for question. Our manager will contact you soon.');
                }
			}
			else {
				alert(msg);
			}
		}
	});
}


function sendForm(form) {
	$.ajax({
		type: "POST",
		url: $('#' + form).attr("action"),
		data: $('#' + form).serialize(),
		success: function(msg){
			/*if (msg == 'OK') {
			 $('#close' + form).click();
			 alert('Спасибо за заявку. Наш менеджер свяжется с Вами как только поступят часы.');
			 }
			 else {*/
			alert(msg);
			/*}*/
		}
	});
}


BX.ready(function () {
	$(document).on('click', '#get_preorder_btn', function () {
		$('#get_preorder_div').fadeIn(400);
	});

	$(document).on('click', '#close_preorder_div', function () {
		$('#get_preorder_div').fadeOut(400);
	});
});