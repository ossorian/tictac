<?
$url = "http://".$_SERVER["HTTP_HOST"].$arResult["DETAIL_PAGE_URL"];
?>
<script type="text/javascript">
(function(window, document) {
  window.___gcfg = { // Настройки языка для Google +1 Button
    lang: 'ru'
  };

  window._gaq = [ // Настройки Google Analytics
    ['_setAccount', 'UA-XXXXXX-1'],
    ['_trackPageview'],
    ['_trackPageLoadTime']
  ];

  var apis = [
    //'https://apis.google.com/js/plusone.js', // Google +1 Button
    '//userapi.com/js/api/openapi.js?34', // Vkontakte API
    '//platform.twitter.com/widgets.js', // Twitter Widgets
    <?if(LANGUAGE_ID == "en"):?>'//connect.facebook.net/en_US/all.js#xfbml=1'<?else:?>'//connect.facebook.net/ru_RU/all.js#xfbml=1'<?endif;?> // Facebook SDK
  ],

  iterator = apis.length,
  script = 'script',

  fragment = document.createDocumentFragment(),
  element = document.createElement(script),

  clone;

  while (iterator--) {
    clone = element.cloneNode(false);
    clone.async = clone.src = apis[iterator];
    fragment.appendChild(clone);
  }

  clone = document.getElementsByTagName(script)[0];
  clone.parentNode.insertBefore(fragment, clone);

<?if(LANGUAGE_ID == "ru"):?>
  window.vkAsyncInit = function() {
    VK.init({apiId: 2978262, onlyWidgets: true});
    VK.Widgets.Like('vk_like', {pageUrl: '<?=$url?>', type: 'mini'});
  }
<?endif;?>

  // Для нормального домена 2978262
  // Для тестового домена 2978293

})(this, document);
</script>



<div class="product_socials">

	<!-- ВКонтакте "Мне нравится" -->
	<div id="icon2" class="product_social_item" style="width: 60px;">
		<div id="vk_like"></div>
	</div>

	<!-- Facebook Like -->

	<div id="icon1" class="product_social_item" style="width: 90px;">
		<div class="fb-like" data-href="<?=$url?>" data-layout="button_count" data-send="false" data-show-faces="false"></div>
	</div>

	<!-- Twitter -->
	<div id="icon3" class="product_social_item" style="width: 80px;">
		<a href="<?=$url?>" class="twitter-share-button" data-lang="<?if(LANGUAGE_ID == "en"):?>en<?else:?>ru<?endif;?>" data-url="<?=$url?>">Tweet</a>
	</div>

	<?if(LANGUAGE_ID == "en"):?>

		<?if(true == false):?>
		<a href="whatsapp://send" data-text="Take a look at this awesome watches" data-href="http://www.itsmywrist.com" class="wa_btn wa_btn_s" style="display:none">Share</a>
		<script type="text/javascript">if(typeof wabtn4fg==="undefined"){wabtn4fg=1;h=document.head||document.getElementsByTagName("head")[0],s=document.createElement("script");s.type="text/javascript";s.src="/js/whatsapp-button.js";h.appendChild(s);}</script>
		<?endif;?>

		<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-color="red"><img src="http://assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png" /></a>
		<!-- Please call pinit.js only once per page -->
		<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>

	<?endif;?>

	<!-- Google +1 -->
	<? /*
	<div id="icon4" class="product_social_item">
		<div class="g-plusone" data-href="<?=$url?>"></div>
	</div>
	*/ ?>
	<? /*
	<div id="icon4" class="product_social_item"  style="width: 60px;">
		<a target="_blank" class="surfinbird__like_button" data-surf-config="{'layout': 'common-gray', 'width': '120', 'height': '21', 'text': 'Серф'}" href="http://surfingbird.ru/share">Серф</a>
		<script type="text/javascript" charset="UTF-8" src="http://surfingbird.ru/share/share.min.js"></script>
	</div>
	*/ ?>
	<div class="roller"></div>
</div>

