<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script>
    var LANGUAGE_ID = '<?= LANGUAGE_ID?>';
</script>
<?
$cat = '';
$cur_page = $APPLICATION->GetCurPage();
if (is_numeric(stripos($cur_page, 'watches'))) {
    $cat= 'часы';
} else if (is_numeric(stripos($cur_page, 'straps'))) {
    $cat= 'ремешки';
} else if (is_numeric(stripos($cur_page, 'sunglasses'))) {
    $cat= 'очки';
} else if (is_numeric(stripos($cur_page, 'accessories'))) {
    $cat = 'украшения';
}
?>

<?
$elPrice = 0;
foreach ($arResult["PRICES"] as $code => $arPrice) {
    if ($arPrice["CAN_ACCESS"]) {
        if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
            $elPrice = $arPrice['DISCOUNT_VALUE'];
        } else {
            $elPrice = $arPrice['VALUE'];
        }
    }
}
?>

<section class="product">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="one-product">
                    <div class="one-product__slider">
                        <? if (!empty($arResult['PROPERTIES']['GALLERY']['VALUE'])) {
                            $pictureNumber = 4?>
                            <? foreach ($arResult['PROPERTIES']['GALLERY']['VALUE'] as $detailPicture) { ?>
                                <?// $picture = resizeImage($detailPicture, 460, 598); ?>
                                <? $picture = CFile::GetPath($detailPicture); ?>
                                <div class="item-slide clearfix">
                                    <a class="tovar-popup magnifier-thumb-wrapper" href="#">
                                        <img src="<?= $picture ?>" class="tovar-img" alt="<?=$cat?> <?=$arResult["PARENT_SECTION_NAME"]?> <?=$arResult["NAME"]?> фото <?=$pictureNumber?>" title="<?=$cat?> <?=$arResult["PARENT_SECTION_NAME"]?> <?=$arResult["NAME"]?> фото <?=$pictureNumber?>">
                                    </a>
                                </div>
                            <? $pictureNumber++;
                            } ?>
                        <? } else { ?>
                            <div class="item-slide clearfix">
                                <a class="tovar-popup magnifier-thumb-wrapper" href="#">
                                    <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" class="tovar-img" alt="<?=$cat?> <?=$arResult["PARENT_SECTION_NAME"]?> <?=$arResult["NAME"]?> фото 3" title="<?=$cat?> <?=$arResult["PARENT_SECTION_NAME"]?> <?=$arResult["NAME"]?> фото 3">
                                </a>
                            </div>
                        <? } ?>
                    </div>
										<div class='one-product__slider_navigation-wrapper'>
											<div class="one-product__slider_navigation">
													<? if (!empty($arResult['PROPERTIES']['GALLERY']['VALUE'])) {
                                                        $pictureNumberNav = 4?>
															<? foreach ($arResult['PROPERTIES']['GALLERY']['VALUE'] as $previewPicture) { ?>
																	<? $picture = resizeImage($previewPicture, 80, 80); ?>
																	<div class="item-nav">
																			<img src="<?= $picture['SRC'] ?>" alt="<?=$cat?> <?=$arResult["PARENT_SECTION_NAME"]?> <?=$arResult["NAME"]?> фото <?=$pictureNumberNav?>" title="<?=$cat?> <?=$arResult["PARENT_SECTION_NAME"]?> <?=$arResult["NAME"]?> фото <?=$pictureNumberNav?>" data-imgid="<?= $previewPicture?>"/>
																	</div>
															<? $pictureNumberNav++;} ?>
													<? } else { ?>
															<div class="item-nav">
																	<img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" alt=""/>
															</div>
													<? } ?>													
											</div>
											<? if (!empty($arResult['PROPERTIES']['VIDEO_LINK']['VALUE'])) { ?>
												<a href="#you_pop" class="product__video" data-src="https://www.youtube.com/embed/<?= substr($arResult['PROPERTIES']['VIDEO_LINK']['VALUE'], -11)?>">
													<i class="icon-icon-video"></i>
													<p class="product-video__text">Видео</p>
												</a>
											<? } ?>
										</div>
                    <div class="favorites hidden-lg hidden-md">
											<span class="favorites__icon">
												<i class="icon-icon-wishlist"></i>
											</span>
                    </div>                    
                </div>
            </div>
            <div itemscope itemtype="http://schema.org/Product" class="col-md-6">
								<div class='product__brend'>
									<a href="<?= $arResult['SECTION_PAGE_URL'] ?>">
											<img itemprop="logo" src="<?= $arResult['SECTION_IMG']['SRC'] ?>" class="more-brend__logo"
													 alt="<?= $arResult['SECTION_IMG']['ALT'] ?>">
									</a>
								</div>
                <h3 itemprop="name" class="product__name"><?= $arResult['NAME'] ?>
                    <? if (!empty($arResult['PROPERTIES']['NEW']['VALUE'])) {?>
                        <span class="label label_new">New</span>
                    <? } ?>
<? $elPrice = ''?>
                    <? foreach ($arResult['PRICES'] as $price) { ?>
                        <? $elPrice = $price['VALUE']?>
                        <? if (!empty($price['DISCOUNT_DIFF_PERCENT'])) { ?>
                            <span class="label label_sale">Sale</span>
                        <? } ?>
                    <? } ?>
										
										<? if ($arResult["PROPERTIES"]["ON_PREORDER"]["VALUE"] === "Y") { ?>
													<span class="label label_sale">Предзаказ</span>
                    <? } ?>

                </h3>
                <? foreach ($arResult['PRICES'] as $price) { ?>
                    <? if (!empty($price['DISCOUNT_DIFF_PERCENT'])) {?>
                        <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="product__price"><span itemprop="price" class="price__num price__new"><?= $price['PRINT_DISCOUNT_VALUE'] ?> </span><span itemprop="priceCurrency" class="rub">₽</span>
                            <span class="old_price" style="text-decoration: line-through"><?= $price['PRINT_VALUE'] ?>
                                 <span class="rub">₽</span></span></p>
                    <? } else { ?>
                        <p itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="product__price"><span itemprop="price" class="price__num"><?= $price['PRINT_VALUE'] ?> </span><span itemprop="priceCurrency" class="rub">₽</span></p>
                    <? } ?>
                <? } ?>

                <? if (!empty($arResult['CHOOSE_COLOR'])) { ?>
                    <? $i = 0;?>
                    <div class="product__color">
                        <p>Выбрать цвет</p>
                        <div class="colors">
                            <? foreach ($arResult['CHOOSE_COLOR'] as $key => $color) { ?>
                            <span <?= empty($i) ? 'class="product_color_active"' : ''?> style="background-image: url(<?=$color['COLOR_PICTURE']?>)" data-id="<?= $key?>"></span>
                                <? $i++;?>
                            <? } ?>
                        </div>
                    </div>
                <? } ?>
                <? if (!empty($arResult['PROPERTIES']['CHOSE_SIZE']['VALUE'])) { ?>
                    <? $i = 0;?>
                    <div class="product__size">
                        <p>Выбрать размер, мм</p>
                        <div class="sizes">
                            <? foreach ($arResult['PROPERTIES']['CHOSE_SIZE']['VALUE'] as $size) { ?>
                                <span <?= empty($i) ? 'class="product_size_active"' : ''?> ><?= $size?></span>
                                <? $i++;?>
                            <? } ?>
                        </div>
                    </div>
                <? } ?>
								<? if ($arResult["PROPERTIES"]["ON_PREORDER"]["VALUE"] === "Y") { ?>
									<p class='price__warning'>
										Данная скидка действует только при 100% предоплате заказа.
									</p>
                <? } ?>
                <div class="product__order clearfix">
                    <? if ($arResult["PROPERTIES"]["ON_PREORDER"]["VALUE"] === "Y") { ?>
													
                            <a
                                data-product-id="<?= $arResult["ID"] ?>"
                                data-product-image="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>"
                                href="<?= $arResult["ADD_URL"] ?>"
                                class="btn btn_black product__btn add_to_cart_btn"
                                onclick="
                                    dataLayer.push({
                                    'event': 'addToCart',
                                    'ecommerce': {
                                    'currencyCode': 'RUB',
                                    'add': {
                                    'products': [{
                                    'name': '<?= strip_tags($arResult["NAME"]) ?>',
                                    'id': '<?= $arResult["ID"] ?>',
                                    'price': '<?= $elPrice ?>',
                                    'brand': '<?= $arResult['PROPERTIES']['BRAND_MODEL']['VALUE_ENUM']?>',
                                    'category': '<?= $cat?>',
                                    'variant': '<?= implode(',', $arResult['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',
                                    'quantity': 1
                                    }]
                                    }
                                    }
                                    });
                                    fbq('track', 'AddToCart', {
                                    currency: 'RUB',
                                    });
                                <? if (RETAIL_ROCKET_ENABLED == true): ?>
                                    try { rrApi.addToBasket(<?= $arResult["ID"] ?><? if(intval($USER->GetID()) > 0):?>, <?=$USER->GetID();?><?endif; ?>) } catch(e) {}
                                <? endif; ?>">Оформить предзаказ</a>

                    <? } else { ?>
                        <a data-product-id="<?= $arResult["ID"] ?>"
                           data-product-image="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>"
                           href="<?= $arResult["ADD_URL"] ?>"
                           class="btn btn_black product__btn add_to_cart_btn"
                           onclick="
                               dataLayer.push({
                               'event': 'addToCart',
                               'ecommerce': {
                               'currencyCode': 'RUB',
                               'add': {
                               'products': [{
                               'name': '<?= strip_tags($arResult["NAME"]) ?>',
                               'id': '<?= $arResult["ID"] ?>',
                               'price': '<?= $elPrice ?>',
                               'brand': '<?= $arResult['PROPERTIES']['BRAND_MODEL']['VALUE_ENUM']?>',
                               'category': '<?= $cat?>',
                               'variant': '<?= implode(',', $arResult['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',
                               'quantity': 1
                               }]
                               }
                               }
                               });
                               fbq('track', 'AddToCart', {
                               currency: 'RUB',
                               });
                           <? if (RETAIL_ROCKET_ENABLED == true): ?>
                               try { rrApi.addToBasket(<?= $arResult["ID"] ?><? if(intval($USER->GetID()) > 0):?>, <?=$USER->GetID();?><?endif; ?>) } catch(e) {}
                           <? endif; ?>">Добавить в корзину</a>
                    <? } ?>
<!--                    --><?// if (strtotime($arResult["DISPLAY_PROPERTIES"]["WAIT"]["DISPLAY_VALUE"]) >= strtotime(date("d.m.Y"))) { ?>
                    <span class="favorites__icon" data-id="<?= $arResult['ID'] ?>">
							<i class="icon-icon-wishlist<?= isset($_SESSION["CATALOG_COMPARE_LIST"][$arResult['IBLOCK_ID']]["ITEMS"][$arResult['ID']]) ? '-select' : '' ?>"></i>
						</span>
<!--                    --><?// } ?>
                    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["WAIT"]["DISPLAY_VALUE"])) { ?>
                        <? if (strtotime($arResult["DISPLAY_PROPERTIES"]["WAIT"]["DISPLAY_VALUE"]) >= strtotime(date("d.m.Y"))) { ?>
                            <p class="delivery__date">
                                Поставка <?= FormatDate("j F", MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["WAIT"]["DISPLAY_VALUE"])) ?></p>
                        <? } ?>
                    <? } ?>
                </div>
                <div style="margin-top: 25px; display: none" id="get_preorder_div">
                    <form action="/ajax/ajax_preorder.php" name="form<?=$arResult["ID"]?>" id="preorder_form<?=$arResult["ID"]?>" method="POST">
                        <input type="hidden" name="id" value="<?=$arResult["ID"]?>" />
                        <input type="hidden" name="link" value="<?=$arResult["DETAIL_PAGE_URL"]?>" />
                        <input type="hidden" name="name" value="<?=$arResult["NAME"]?>" />
                        <?
                        global $USER;
                        $usEmail = $USER->GetEmail();
                        $usName = $USER->GetFullName();
                        ?>
                        <span id="close_preorder_div" style="float: right;">
                                            <i class="icon-icon-small-close"></i>
                                        </span>
                        <span class="input input--haruki">
												<input class="input__field input__field--haruki" value="<?= $usName?>" type="text" id="input-1.1" name="fio"/>
												<label class="input__label input__label--haruki" for="input-1.1">
													<span class="input__label-content input__label-content--haruki">Имя</span>
												</label>
											</span>
                        <span class="input input--haruki">
												<input class="input__field input__field--haruki" value="<?= $usEmail?>" type="text" id="input-1.3" name="email"/>
												<label class="input__label input__label--haruki" for="input-1.3">
													<span class="input__label-content input__label-content--haruki">Эл. почта</span>
												</label>
											</span>
                        <input type="submit" class="btn btn_black" value="Жду" style="width: 130px;" onClick="sendForm('preorder_form<?=$arResult["ID"]?>'); return false;" />
                    </form>
                </div>
                <div class="product__content hidden-xs">
                    <div class="product__tabs">
                        <span class="product__tab product__tab_active">Характеристики</span>
                        <span class="product__tab">О брендe</span>
                        <span class="product__tab">Задать вопрос</span>
                    </div>
                    <div class="product__body product__body_active">
                        <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"] != false OR $arResult["DISPLAY_PROPERTIES"]["PROP_WRIST"]["DISPLAY_VALUE"] != false OR $arResult["DISPLAY_PROPERTIES"]["PROP_WEIGHT"]["DISPLAY_VALUE"] != false OR $arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"]): ?>
                            <div class="characteristics">
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_SIZE") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= str_replace("?", "&empty;", $arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"]) ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_MM") ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WRIST"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WRIST") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WRIST"]["DISPLAY_VALUE"] ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_MM") ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WEIGHT"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_EIGHT") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WEIGHT"]["DISPLAY_VALUE"] ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_GRAMM") ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_COUNTRY") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_MATERIAL") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_MECHANISM") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_GLASS") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WATER"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WATER") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WATER"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WARRANTY") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["FRAME_MATERIAL"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_FRAME_MATERIAL") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["FRAME_MATERIAL"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["LENSES"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_LENSES") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["LENSES"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["UF_ABSORB"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_UF_ABSORB") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["UF_ABSORB"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["WIDTH_BETWEEN_EARINGS"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WIDTH_BETWEEN_EARINGS") ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["WIDTH_BETWEEN_EARINGS"]["DISPLAY_VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if ($arResult["DISPLAY_PROPERTIES"]["WIDTH_OF_STRAP_ATTACHMENT"]["DISPLAY_VALUE"] != false): ?>
                                    <p class="chr__text clearfix">
																			<span class="chr-text__left">
																				<span><?= GetMessage("CATALOG_ELEMENT_PROP_WIDTH_OF_STRAP_ATTACHMENT") ?>:</span>
																			</span>
																			<span class="chr-text__right">
																				<?= $arResult["DISPLAY_PROPERTIES"]["WIDTH_OF_STRAP_ATTACHMENT"]["DISPLAY_VALUE"] ?>
																			</span>
																		</p>
                                <? endif; ?>
                                <? if (!empty($arResult["PROPERTIES"]["ACCESSORY_SIZE"]["VALUE"])): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= $arResult["PROPERTIES"]["ACCESSORY_SIZE"]["NAME"] ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= $arResult["PROPERTIES"]["ACCESSORY_SIZE"]["VALUE"] ?>
										</span></p>
                                <? endif; ?>
                                <? if (!empty($arResult["PROPERTIES"]["ACCESSORY_COLOR"]["VALUE"])): ?>
                                    <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= $arResult["PROPERTIES"]["ACCESSORY_COLOR"]["NAME"] ?>:</span></span>
                                        <span
                                            class="chr-text__right"><?= implode(", ", $arResult["PROPERTIES"]["ACCESSORY_COLOR"]["VALUE"]) ?>
										</span></p>
                                <? endif; ?>
                            </div>
                        <? endif; ?>

                        <p class="ch__about"><?= $arResult['PREVIEW_TEXT'] ?></p>
												<?php if ( $arResult['DETAIL_TEXT'] ): ?>
                        <p class="ch__about_hidden"><?= $arResult['DETAIL_TEXT'] ?></p>
                        <a class="ch__more link_border">Показать подробнее</a>
												<? endif; ?>
                    </div>
                    <div class="product__body">
                        <?= $arResult['SECTION_DESCRIPTION'] ?>
                    </div>
                    <div class="product__body">

                        <form class="product__form" action="/ajax/question.php" id="formQuestion" name="formQuestion">
                            <input type="hidden" name="id" value="<?=$arResult["ID"]?>" />
                            <input type="hidden" name="link" value="<?=$arResult["DETAIL_PAGE_URL"]?>" />
                            <input type="hidden" name="name" value="<?=$arResult["NAME"]?>" />
								<span class="input input--haruki">
									<input class="input__field input__field--haruki" type="text" id="qname" name="fio"/>
									<label class="input__label input__label--haruki" for="qname">
										<span
                                            class="input__label-content input__label-content--haruki"><?=GetMessage("CATALOG_ELEMENT_ASK_NAME")?></span>
									</label>
								</span>
                            <span class="input input--haruki">
									<input class="input__field input__field--haruki" type="text" id="question" name="text"/>
									<label class="input__label input__label--haruki" for="question">
										<span class="input__label-content input__label-content--haruki">Вопрос</span>
									</label>
								</span>
                            <span class="input input--haruki">
									<input class="input__field input__field--haruki" type="text" id="qemail" name="email"/>
									<label class="input__label input__label--haruki" for="qemail" >
										<span class="input__label-content input__label-content--haruki"><?=GetMessage("CATALOG_ELEMENT_ASK_EMAIL")?></span>
									</label>
								</span>
                            <input type="submit" value="<?=GetMessage("CATALOG_ELEMENT_ASK_SUBMIT")?>" class="btn btn_black product-form__btn" onClick="sendFormQuestion(); return false;"/>
                        </form>
                    </div>
                </div>
                <div class="product__content hidden-lg hidden-md hidden-sm">
                    <div class="accordeon">
                        <dl>
                            <dt>Характеристики <span></span></dt>
                            <dd>
                                <div class="characteristics">
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_SIZE") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= str_replace("?", "&empty;", $arResult["DISPLAY_PROPERTIES"]["PROP_SIZE"]["DISPLAY_VALUE"]) ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_MM") ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WRIST"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WRIST") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WRIST"]["DISPLAY_VALUE"] ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_MM") ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WEIGHT"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_EIGHT") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WEIGHT"]["DISPLAY_VALUE"] ?> <?= GetMessage("CATALOG_ELEMENT_PROP_SIGNATURE_GRAMM") ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_COUNTRY") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_COUNTRY"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_MATERIAL") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_MATERIAL"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_MECHANISM") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_MECHANISM"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_GLASS") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_GLASS"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WATER"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WATER") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WATER"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WARRANTY") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["PROP_WARRANTY"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["FRAME_MATERIAL"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_FRAME_MATERIAL") ?>
                                                :</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["FRAME_MATERIAL"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["LENSES"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_LENSES") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["LENSES"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["UF_ABSORB"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_UF_ABSORB") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["UF_ABSORB"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["WIDTH_BETWEEN_EARINGS"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WIDTH_BETWEEN_EARINGS") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["WIDTH_BETWEEN_EARINGS"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                    <? if ($arResult["DISPLAY_PROPERTIES"]["WIDTH_OF_STRAP_ATTACHMENT"]["DISPLAY_VALUE"] != false): ?>
                                        <p class="chr__text clearfix"><span class="chr-text__left">
											<span><?= GetMessage("CATALOG_ELEMENT_PROP_WIDTH_OF_STRAP_ATTACHMENT") ?>:</span></span>
                                            <span
                                                class="chr-text__right"><?= $arResult["DISPLAY_PROPERTIES"]["WIDTH_OF_STRAP_ATTACHMENT"]["DISPLAY_VALUE"] ?>
										</span></p>
                                    <? endif; ?>
                                </div>
                                <p class="ch__about">
                                    <?= $arResult['PREVIEW_TEXT']?>
                                    <br>
                                    <?= $arResult['DETAIL_TEXT']?>
                                </p>
                            </dd>
                        </dl>
                        <dl>
                            <dt>О бренде <span></span></dt>
                            <dd>
                                <?= $arResult['SECTION_DESCRIPTION']?>
                            </dd>
                        </dl>
                        <dl>
                            <dt>Задать вопрос <span></span></dt>
                            <dd>
                                <form class="product__form">
										<span class="input input--haruki">
											<input class="input__field input__field--haruki" type="text" id="input-3"/>
											<label class="input__label input__label--haruki" for="input-3">
												<span class="input__label-content input__label-content--haruki">Имя и фамилия</span>
											</label>
										</span>
                                    <span class="input input--haruki">
											<input class="input__field input__field--haruki" type="text" id="input-3"/>
											<label class="input__label input__label--haruki" for="input-3">
												<span
                                                    class="input__label-content input__label-content--haruki">Вопрос</span>
											</label>
										</span>
                                    <span class="input input--haruki">
											<input class="input__field input__field--haruki" type="text" id="input-3"/>
											<label class="input__label input__label--haruki" for="input-3">
												<span class="input__label-content input__label-content--haruki">Эл. почта</span>
											</label>
										</span>
                                    <button class="btn btn_black product-form__btn">Отправить</button>
                                </form>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div itemscope itemtype="http://schema.org/Brand" itemprop="brand" class="more-brend">
                    <p>
                        <span class="more-brend__text">Больше <?= $arResult['MORE_TEXT']?> от</span>
                        <a href="<?= $arResult['SECTION_PAGE_URL'] ?>">
                            <img itemprop="logo" src="<?= $arResult['SECTION_IMG']['SRC'] ?>" class="more-brend__logo"
                                 alt="<?= $arResult['SECTION_IMG']['ALT'] ?>">
                            <span itemprop="name" style="display:none;"><?= $arResult["SECTION"]["NAME"] ?></span>
                        </a>
                    </p>
                </div>
                <div class="share-friends">
                    <div>
                        <span class="share-friends__text">Поделиться с друзьями</span>
                        <div class="ff__soc">
                            <?
                            $protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
                            $domain = $_SERVER['SERVER_NAME'];

                            $url = $protocol . '://' . $domain . $APPLICATION->GetCurPage();
                            $pin_pic = $protocol . '://' . $domain . $arResult['PREVIEW_PICTURE']['SRC'];
                            ?>
                            <a onclick="sharePopup('http://pinterest.com/pin/create/bookmarklet/?media=<?=$pin_pic?>&url=<?=$url?>&is_video=false&description=<?= $APPLICATION->GetTitle()?>'); return false;"><i class="icon-icon-pin"></i></a>
                            <a onclick="sharePopup('http://www.facebook.com/sharer/sharer.php?u=<?=$url?>&title=<?= $APPLICATION->GetTitle()?>'); return false;"><i class="icon-icon-fb"></i></a>
                            <a onclick="sharePopup('https://vk.com/share.php?url=<?=$url?>'); return false;"><i class="icon-icon-vk"></i></a>
                            <a onclick="sharePopup('http://twitter.com/intent/tweet?status=<?= $APPLICATION->GetTitle()?>+<?=$url?>'); return false;"><i class="icon-icon-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/remarketing.php","PAGE_TYPE"=>"product","PRODUCT"=>$arResult["ID"],"PRICE"=>$arResult["MIN_PRICE"]["VALUE"]), false);?>
<? if (!empty($arResult['RELATED_PRODUCTS'])) { ?>

    <section class="like sections">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4>Вам может понравиться</h4>
                </div>
            </div>

            <?$GLOBALS['arrFilterRelated'] = array("ID"=>$arResult['RELATED_PRODUCTS'])?>
            <?$sectId = $APPLICATION->IncludeComponent("bitrix:catalog.section", "related_products", array(
                "IBLOCK_TYPE" => "TicTacToy",
                "IBLOCK_ID" => "5",
                "SECTION_ID" => "",
                "SECTION_CODE" => '',
                "SECTION_USER_FIELDS" => array(
                    0 => "UF_RETARGETING",
                    1 => "",
                ),
                "ELEMENT_SORT_FIELD" => 'IBLOCK_SECTION_ID ',
                "ELEMENT_SORT_FIELD2" => "id",
                "ELEMENT_SORT_ORDER" => 'asc',
                "ELEMENT_SORT_ORDER2" => "desc",
                "FILTER_NAME" => "arrFilterRelated",
                "INCLUDE_SUBSECTIONS" => "Y",
                "SHOW_ALL_WO_SECTION" => "Y",
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => "999",
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array(
                    0 => "NEW",
                    1 => "SEX",
                    2 => "WAIT",
                    3 => "",
                ),
                "OFFERS_LIMIT" => "5",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "BASKET_URL" => "/cart/",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_GROUPS" => "Y",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "Y",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "Y",
                "SET_STATUS_404" => "Y",
                "CACHE_FILTER" => "Y",
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "PRODUCT_PROPERTIES" => array(
                ),
                "USE_PRODUCT_QUANTITY" => "N",
                "CONVERT_CURRENCY" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
                false
            );?>
        </div>
    </section>
<? } ?>


<?
// Отображаем ретаргетинг контакта для текущей категории
if (!empty($arResult["DISPLAY_PROPERTIES"]["RETARGETING"]["DISPLAY_VALUE"])) {
    echo $arResult["DISPLAY_PROPERTIES"]["RETARGETING"]["~VALUE"];
} elseif ($arResult["SECTION"]["ID"] > 0) {
    $arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_5_SECTION", $arResult["SECTION"]["ID"]);
    foreach ($arUserFields as $FIELD_NAME => $arUserField) {
        if ($arUserField["FIELD_NAME"] == "UF_RETARGETING")
            echo $arUserField["VALUE"];
    }
}
?>
<?
//echo "<pre>"; print_r($arResult['PROPERTIES']['SEO_TEXT']); echo "</pre>";
if ($arResult['PROPERTIES']['SEO_TEXT']['~VALUE']['TEXT']):

    ?>

    <div class="text-open">
        <a href="javascript:void(0)"><span><?= ($arResult['PROPERTIES']['SEO_TEXT']['DESCRIPTION']) ? ($arResult['PROPERTIES']['SEO_TEXT']['DESCRIPTION']) : ('Подробнее') ?></span>
            ↓</a>
    </div>
    <div class='text-open-info'>
        <?= $arResult['PROPERTIES']['SEO_TEXT']['~VALUE']['TEXT'] ?>
    </div>
<? endif; ?>
