<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="auth__soc">
	<p>Или с помощью</p>
<?foreach($arParams["~AUTH_SERVICES"] as $service):?>
	<? if ($service['ID'] == "VKontakte") { ?>
		<a id="vk_auth_btn" title="<?=htmlspecialcharsbx($service["NAME"])?>" href="javascript:void(0)" onclick="$('#bx_auth_serv_form<?=$service["ID"]?> a').eq(0).click();" class="soc__icon vk__icon"><i class="icon-icon-vk"></i></a>
	<? } else if ($service['ID'] == "Facebook") { ?>
		<a id="fb_auth_btn" title="<?=htmlspecialcharsbx($service["NAME"])?>" href="javascript:void(0)" onclick="$('#bx_auth_serv_form<?=$service["ID"]?> a').eq(0).click();" class="soc__icon fb__icon"><i class="icon-icon-fb"></i></a>
	<? } else { ?>
	<a title="<?=htmlspecialcharsbx($service["NAME"])?>" href="javascript:void(0)" onclick="BxShowAuthFloat('<?=$service["ID"]?>', '<?=$arParams["SUFFIX"]?>')"><i class="bx-ss-icon <?=htmlspecialcharsbx($service["ICON"])?>"></i></a>
	<? } ?>
	<?endforeach?>
</div>
