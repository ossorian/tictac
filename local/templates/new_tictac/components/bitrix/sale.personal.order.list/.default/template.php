<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CModule::IncludeModule("iblock")?>
<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

<?else:?>

	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>

		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>

	<?endif?>


	<?if(!empty($arResult['ORDERS'])):?>
		<?$count = count($arResult['ORDERS'])?>
		<?$i = 0;?>

		<div class="order__table">
			<div class="order-table__head clearfix">
				<div class="order-table__tr otr-1">
					<p>Номер заказа</p>
				</div>
				<div class="order-table__tr otr-2">
					<p>Номер отслеживания</p>
				</div>
				<div class="order-table__tr otr-3">
					<p>Дата</p>
				</div>
				<div class="order-table__tr otr-4">
					<p>Стоимость</p>
				</div>
			</div>

		<?foreach($arResult["ORDER_BY_STATUS"] as $key => $group):?>

			<?foreach($group as $k => $order):?>
				<?$i++;?>
				<div class="order-table__body clearfix<?= $i == $count ? ' order-table__body_last' : ''?>">
					<div class="order-table__tr otr-1">
						<p class="order__number link_border"><?=$order["ORDER"]["ACCOUNT_NUMBER"]?></p>
					</div>
					<div class="order-table__tr otr-2">
						<p>—</p>
					</div>
					<div class="order-table__tr otr-3">
						<p class="order-table__date"><?=$order["ORDER"]["DATE_INSERT_FORMAT"];?></p>
					</div>
					<div class="order-table__tr otr-4">
						<p class="order-rable__price"><?=$order["ORDER"]["FORMATED_PRICE"]?><span>₽</span></p>
					</div>
					<div class="order_toggle clearfix">
						<div class="order_toggle__tovars">

							<div class="track-number clearfix hidden-lg hidden-md">
								<p>Трекномер</p>
								<a href="#" class="order link_border">RA 175 278 254 US</a>
							</div>

							<?foreach ($order["BASKET_ITEMS"] as $item):?>
								<?$arElem = CIBlockElement::GetByID($item['PRODUCT_ID'])->GetNext()?>
								<?$picture = CFile::GetPath($arElem['PREVIEW_PICTURE']);?>

								<div class="order_toggle__item">
									<img src="<?= $picture?>" class="order_toggle__img" alt="<?=$item['NAME']?>">
									<a href="<?= $item['DETAIL_PAGE_URL']?>" class="order_toggle__name link"><?=$item['NAME']?></a>
									<p class="order_toggle__count">Кол.: <?=$item['QUANTITY']?></p>
									<p class="order_toggle__price"><?= SaleFormatCurrency($item['PRICE'], $order['ORDER']['CURRENCY'])?> <span class="rub">₽</span></p>
								</div>
							<?endforeach?>
						</div>
						<div class="order_toggle__prices">
							<div class="order_toggle__info clearfix">
								<div class="order_toggle__payment-method">
									<span><?=GetMessage('SPOL_PAYSYSTEM')?></span>
									<p><?=$arResult["INFO"]["PAY_SYSTEM"][$order["ORDER"]["PAY_SYSTEM_ID"]]["NAME"]?></p>
								</div>
								<div class="order_toggle__delivery">
									<span><?=GetMessage('SPOL_DELIVERY')?></span>
									<p><?=$arResult["INFO"]["DELIVERY"][$order["ORDER"]["DELIVERY_ID"]]["NAME"]?></p>
								</div>
								<div class="order_toggle__text clearfix">
									<div class="order_toggle-text__left">
										<p>Товары</p>
									</div>
									<div class="order_toggle-text__right">
										<p class="ottr__price"><?= SaleFormatCurrency($order['ORDER']['PRICE'] - $order['ORDER']['PRICE_DELIVERY'], $order['ORDER']['CURRENCY'])?> <span class="rub">₽</span></p>
									</div>
									<div class="order_toggle-text__left">
										<p>Доставка</p>
									</div>
									<div class="order_toggle-text__right">
										<p class="ottr__price"><?= SaleFormatCurrency($order['ORDER']['PRICE_DELIVERY'], $order['ORDER']['CURRENCY'])?> <span class="rub">₽</span></p>
									</div>
									<?$discount = SaleFormatCurrency($order['ORDER']['DISCOUNT_VALUE'], $order['ORDER']['CURRENCY'])?>
									<? if (!empty($discount)) { ?>
										<div class="order_toggle-text__left">
											<p>Скидка</p>
										</div>
										<div class="order_toggle-text__right">
											<p class="ottr__price color_red"><?= $discount?> <span class="rub">₽</span></p>
										</div>
									<? } ?>
								</div>
								<div class="ottr__total">
									<div class="ottr-total__left">
										<p>Итого</p>
									</div>
									<div class="ottr-total__right">
										<p class="ottr__price"><?=$order["ORDER"]["FORMATED_PRICE"]?><span class="rub">₽</span></p>
									</div>
								</div>
								<? if ($order["ORDER"]["PAYED"] !== "Y") {  ?>
									<a href="/cart/?ORDER_ID=<?=$order["ORDER"]["ACCOUNT_NUMBER"]?>" class="btn btn_black ottr__btn">Перейти к оплате</a>
								<? } ?>
							</div>
						</div>
					</div>


				</div>

			<?endforeach?>

		<?endforeach?>

		<?if(strlen($arResult['NAV_STRING'])):?>
			<?=$arResult['NAV_STRING']?>
		<?endif?>
		</div>
	<?else:?>
		<div class="order__empty">
									<span class="order-empty__icon">
										<i class="icon-icon-empty-orders"></i>
									</span>
			<p class="order-empty__text"><?=GetMessage('SPOL_NO_ORDERS')?></p>
		</div>
	<?endif?>

<?endif?>