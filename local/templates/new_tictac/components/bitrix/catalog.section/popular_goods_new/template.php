<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?
$cat = '';
$cur_page = $APPLICATION->GetCurPage();
if (is_numeric(stripos($cur_page, 'watches'))) {
    $cat = 'часы';
} else if (is_numeric(stripos($cur_page, 'straps'))) {
    $cat = 'ремешки';
} else if (is_numeric(stripos($cur_page, 'sunglasses'))) {
    $cat = 'очки';
}
?>
<? if (!empty($arResult['ITEMS'])) { ?>


    <div class="row by-color">
        <div class="col-xs-12">
            <div class="slider__for">
                <div class="tab__item active-item"
                     data-thumb="">
                    <div class="tab__slider">
                        <? $position = 0; ?>
                        <? foreach ($arResult["ITEMS"] as $key => $item) {
                            $position++;
                            $wishlist = '';
                            if (isset($_SESSION["CATALOG_COMPARE_LIST"][$item['IBLOCK_ID']]["ITEMS"][$item['ID']])) {
                                $wishlist = '-select';
                            } else {
                                $wishlist = '';
                            }
                            ?>
                            <?
                            $elPrice = '';
                            foreach ($item["PRICES"] as $code => $arPrice) {
                                if ($arPrice["CAN_ACCESS"]) {
                                    if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
                                        $elPrice = $arPrice['DISCOUNT_VALUE'];
                                    } else {
                                        $elPrice = $arPrice['VALUE'];

                                    }
                                }
                            }
                            ?>
                            <div class="slide">
                                <div class="item">
                                    <? if (!empty($item['PROPERTIES']['NEW']['VALUE'])) { ?>
                                        <span class="label label_new">New</span>
                                    <? } ?>
                                    <div class="favorites">
								<span class="favorites__icon" data-id="<?= $item['ID'] ?>">
									<i class="icon-icon-wishlist<?= $wishlist ?>" id="wish_<?= $item['ID'] ?>"></i>
								</span>
                                        <p class="favorites__text">Избранное</p>
                                    </div>
                                    <a href="#qv__popup" class="quick-view" data-id="<?= $item['ID'] ?>">
								<span class="quick-view__icon">
									<i class="icon-icon-quick-view"></i>
								</span>
                                        <span class="quick-view__text">Быстрый просмотр</span>
                                    </a>
                                    <? $picture = resizeImage($item["DISPLAY_PROPERTIES"]["IMAGE"]["FILE_VALUE"]['ID'], 263, 291, 1) ?>
                                    <a href="<?= $item['DETAIL_PAGE_URL'] ?>"
                                       data-seo="Популярные товары"
                                       onclick="
                                           dataLayer.push({
                                           'event': 'productClick',
                                           'ecommerce': {
                                           'click': {
                                           'actionField': {'list': 'Популярные товары'},     // Список с которого был клик.
                                           'products': [{
                                           'name': '<?= strip_tags($item['NAME']) ?>',        // Наименование товара.
                                           'id': '<?= $item['ID'] ?>',                // Идентификатор товара в базе данных.
                                           'price': '<?= $elPrice ?>',                // Цена одной единицы купленного товара
                                           'brand': '<?= $item['BRAND_NAME'] ?>',             //Бренд товара
                                           'category': '<?= $item['SEO_SECTION_NAME'] ?>',             //Категория товара
                                           'variant': '<?= implode(',', $item['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',                //Вариант товара, например цвет
                                           'position': <?= $position ?>                //Позиция товара в списке
                                           }]
                                           }
                                           }
                                           });">
                                        <img src="<?= $picture["SRC"] ?>"
                                             alt="<?=$item["PARENT_SECTION_NAME"]?> <?= $item["NAME"] ?> фото 2"
                                             title="<?=$item["PARENT_SECTION_NAME"]?> <?= $item["NAME"] ?> фото 2"
                                             data-imgid="<?= $item["DISPLAY_PROPERTIES"]["IMAGE"]["FILE_VALUE"]['ID'] ?>">

                                        <p class="item__name"><?= $item['NAME'] ?></p>
                                    </a>
                                    <?
                                    $product[]=$item["ID"];
                                    $prices[]=$elPrice;
                                    $elPrice = '';
                                    foreach ($item["PRICES"] as $code => $arPrice) { ?>
                                        <? if ($arPrice["CAN_ACCESS"]) { ?>
                                            <? $elPrice = $arPrice['VALUE']; ?>
                                            <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) { ?>
                                                <s class='item-price__before no-margin'><?= $arPrice["PRINT_VALUE"] ?>
                                                    <span
                                                        class="rub">₽</span></s><br/>
                                                <p class="item__price price__new"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?>
                                                    <span
                                                        class="rub">₽</span></p>
                                            <? } else { ?>
                                                <p class="item__price"><?= $arPrice["PRINT_VALUE"] ?> <span
                                                        class="rub">₽</span></p>
                                            <? } ?>
                                        <? } ?>
                                    <? } ?>

                                </div>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<? }
$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/remarketing.php","PAGE_TYPE"=>"home","PRODUCT"=>$product,"PRICE"=>$prices), false);
?>