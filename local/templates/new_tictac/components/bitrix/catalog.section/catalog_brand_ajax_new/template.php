<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? global $USER; ?>

<?
$cat = '';
$SeoList = $_GET['seo_list'];

$i = ($_GET['PAGEN_1'] - 1) * 20;
?>

<? foreach ($arResult['ITEMS'] as $arElement) {

    $i++;
    $cat = $arElement['SEO_SECTION_NAME'];
    /*echo "<pre>";
    print_r($arElement["PARENT_SECTION_NAME"]);
    echo "</pre>";*/

    $picture = resizeImage($arElement['PREVIEW_PICTURE']['ID'], 270, 322, 1);//print_r($picture);
		$secondPicture = !empty($arElement['PROPERTIES']['PREVIEW_PICTURE2']['VALUE']) ? CFile::GetPath($arElement['PROPERTIES']['PREVIEW_PICTURE2']['VALUE']) : $picture['SRC'];
		$pictureNumber = !empty($arElement['PROPERTIES']['PREVIEW_PICTURE2']['VALUE']) ? "фото 2" : "фото 1";
//                $picture = $arElement['PREVIEW_PICTURE'];
    // определяем цены
    $price = '';
    $sale = '';
    $elPrice = '';
    foreach ($arElement["PRICES"] as $code => $arPrice) {
        if ($arPrice["CAN_ACCESS"]) {

            if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
                $elPrice = $arPrice['DISCOUNT_VALUE'];
                $price = '<p class="item__price">
                                                    <span class="color_red">' . $arPrice["PRINT_DISCOUNT_VALUE"] . '<span class="rub">₽</span></span> 
                                                    <span class="item-price__before">' . $arPrice["PRINT_VALUE"] . ' <span class="rub">₽</span></span>
                                                </p>';
                $sale = '<span class="label label_sale">Sale</span>';
            } else {
                $elPrice = $arPrice['VALUE'];
                $price = '<p class="item__price">' . $arPrice["PRINT_VALUE"] . ' <span class="rub">₽</span></p>';
            }
        }
    }
    ?>


    <? if ($arElement["PROPERTIES"]["ON_PREORDER"]["VALUE"] === "Y") { ?>
				
        <div class="col-md-<?= (!empty($_REQUEST['count_in_row'])) ? 3 : 4 ?> col-sm-6 brend__col">
            <div class="item no-item">
                <?//= $sale ?>
                <span class="label label_sale">Предзаказ</span>
                <div class="favorites hidden-lg hidden-md">
                                    <span class="favorites__icon">
                                        <i class="icon-icon-wishlist"></i>
                                    </span>
                    <p class="favorites__text">Избранное</p>
                </div>
                <div class="no-product">
                    <p class="no-product__title">Нет в наличии</p>
                    <a class="no-product__reminder link_border">Сообщить о поступлении</a><br>
                    <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>" class="no-product__order link_border"
                       data-seo="<?= $SeoList ?>"
                       onclick="
                           dataLayer.push({
                           'event': 'productClick',
                           'ecommerce': {
                           'click': {
                           'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.
                           'products': [{
                           'name': '<?= strip_tags($arElement['NAME']) ?>',        // Наименование товара.
                           'id': '<?= $arElement['ID'] ?>',                // Идентификатор товара в базе данных.
                           'price': '<?= $elPrice ?>',                // Цена одной единицы купленного товара
                           'brand': '<?= $arElement['BRAND_NAME'] ?>',             //Бренд товара
                           'category': '<?= $cat ?>',             //Категория товара
                           'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',                //Вариант товара, например цвет
                           'position': <?= $i ?>                //Позиция товара в списке
                           }]
                           }
                           }
                           });">Оформить
                        предзаказ</a>
                </div>
                <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>"
                   data-seo="<?= $SeoList ?>"
                   onclick="
                       dataLayer.push({
                       'event': 'productClick',
                       'ecommerce': {
                       'click': {
                       'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.
                       'products': [{
                       'name': '<?= strip_tags($arElement['NAME']) ?>',        // Наименование товара.
                       'id': '<?= $arElement['ID'] ?>',                // Идентификатор товара в базе данных.
                       'price': '<?= $elPrice ?>',                // Цена одной единицы купленного товара
                       'brand': '<?= $arElement['BRAND_NAME'] ?>',             //Бренд товара
                       'category': '<?= $cat ?>',             //Категория товара
                       'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',                //Вариант товара, например цвет
                       'position': <?= $i ?>                //Позиция товара в списке
                       }]
                       }
                       }
                       });">

										<img src="<?= $picture["SRC"] ?>"
												alt="<?= $cat ?> <?= $arElement["PARENT_SECTION_NAME"] ?> <?= $arElement["PARENT_SECTION_NAME"] ?> <?= $arElement["NAME"] ?> <?=$pictureNumber?>" title="<?= $cat ?> <?= $arElement["PARENT_SECTION_NAME"] ?> <?= $arElement["NAME"] ?> <?=$pictureNumber?>"
												class="item-preview-img"
												data-second='<?= $secondPicture; ?>'
												/>

                    <p class="item__name"><?= $arElement['NAME'] ?></p>
                </a>
                <?= $price ?>

                <div class="reminder">
                                        <span class="reminder__close">
                                            <i class="icon-icon-small-close"></i>
                                        </span>
                    <form action="/ajax/ajax_preorder.php" name="form<?= $arElement["ID"] ?>"
                          id="preorder_form<?= $arElement["ID"] ?>" method="POST">
                        <input type="hidden" name="id" value="<?= $arElement["ID"] ?>"/>
                        <input type="hidden" name="link"
                               value="<?= $arElement["DETAIL_PAGE_URL"] ?>"/>
                        <input type="hidden" name="name" value="<?= $arElement["NAME"] ?>"/>
                        <?
                        global $USER;
                        $usEmail = $USER->GetEmail();
                        $usName = $USER->GetFullName();
                        ?>

                        <span class="input input--haruki">
												<input class="input__field input__field--haruki" type="text"
                                                       id="input-1.1" name="fio" <?= $usName ?>/>
												<label class="input__label input__label--haruki" for="input-1.1">
													<span
                                                        class="input__label-content input__label-content--haruki">Имя</span>
												</label>
											</span>
                        <span class="input input--haruki">
												<input class="input__field input__field--haruki" value="<?= $usEmail ?>"
                                                       type="text" id="input-1.3" name="email"/>
												<label class="input__label input__label--haruki" for="input-1.3">
													<span class="input__label-content input__label-content--haruki">Эл. почта</span>
												</label>
											</span>
                        <input type="submit" class="btn btn_black o-reminder__btn" value="Жду"
                               onClick="sendForm('preorder_form<?= $arElement["ID"] ?>'); return false;"/>
                    </form>
                </div>
            </div>
        </div>
    <? } else { ?>

        <div class="col-md-<?= (!empty($_REQUEST['count_in_row'])) ? 3 : 4 ?> col-sm-6 brend__col">
            <div class="item">
                <?= $sale ?>
                <? if ($arElement["DISPLAY_PROPERTIES"]["NEW"]["DISPLAY_VALUE"] == "да") { ?>
                    <span class="label label_new">New</span>
                <? } ?>

                <div class="favorites">
													<span class="favorites__icon" data-id="<?= $arElement['ID'] ?>">
														<i class="icon-icon-wishlist"></i>
													</span>
                    <p class="favorites__text">Избранное</p>
                </div>
                <a href="#qv__popup" class="quick-view" data-id="<?= $arElement['ID'] ?>">
													<span class="quick-view__icon">
														<i class="icon-icon-quick-view"></i>
													</span>
                    <span class="quick-view__text">Быстрый просмотр</span>
                </a>
                <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>"
                   data-seo="<?= $SeoList ?>"
                   onclick="
                       dataLayer.push({
                       'event': 'productClick',
                       'ecommerce': {
                       'click': {
                       'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.
                       'products': [{
                       'name': '<?= strip_tags($arElement['NAME']) ?>',        // Наименование товара.
                       'id': '<?= $arElement['ID'] ?>',                // Идентификатор товара в базе данных.
                       'price': '<?= $elPrice ?>',                // Цена одной единицы купленного товара
                       'brand': '<?= $arElement['BRAND_NAME'] ?>',             //Бренд товара
                       'category': '<?= $cat ?>',             //Категория товара
                       'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',                //Вариант товара, например цвет
                       'position': <?= $i ?>                //Позиция товара в списке
                       }]
                       }
                       }
                       });">
                    <img src="<?= $picture["SRC"] ?>"
												alt="<?= $cat ?> <?= $arElement["PARENT_SECTION_NAME"] ?> <?= $arElement["NAME"] ?> <?=$pictureNumber?>" title="<?= $cat ?> <?= $arElement["PARENT_SECTION_NAME"] ?> <?= $arElement["NAME"] ?> <?=$pictureNumber?>"
												class="item-preview-img"
												data-second='<?= $secondPicture ?>'
												/>
                    <p class="item__name"><?= $arElement['NAME'] ?> </p>
                </a>
                <?= $price ?>
                <? if ($arElement["PROPERTIES"]["ON_PREORDER"]["VALUE"] === "Y") { ?>
                    <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>" data-seo="<?= $SeoList ?>" class="btn btn_black item__btn ">Предзаказ</a>
                <? } else { ?>
                    <a href="#" class="btn btn_black item__btn add_to_cart_btn"
                       data-product-id="<?= $arElement["ID"] ?>"
                       data-product-image="<?= $picture["SRC"] ?>"
                       onclick="
                           dataLayer.push({
                           'event': 'addToCart',
                           'ecommerce': {
                           'currencyCode': 'RUB',
                           'add': {
                           'products': [{
                           'name': '<?= strip_tags($arElement["NAME"]) ?>',
                           'id': '<?= $arElement["ID"] ?>',
                           'price': '<?= $elPrice ?>',
                           'brand': '<? $arElement['BRAND_NAME'] ?>',
                           'category': '<?= $cat ?>',
                           'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',
                           'quantity': 1
                           }]
                           }
                           }
                           });
                           fbq('track', 'AddToCart', {
                           currency: 'RUB',
                           });">В корзину</a>
                <? } ?>
                <!--								<a href="#" class="preorder link_grey">Предзаказ</a>-->
            </div>
        </div>

    <? } ?>
<? } ?>

