var womanDisplay = false;
var manDisplay = false;


function filterWoman() {
	if (manDisplay == true && womanDisplay == false) {
		$("div.for_men a").click();
		filterMan();
	}

	if (womanDisplay == false) {
		$("div.woman").css("display", "none");
		$("div.man").css("display", "block");
		womanDisplay = true;
	}
	else {
		$("div.man").css("display", "block");
		$("div.woman").css("display", "block");
		womanDisplay = false;
	}
}


function filterMan() {
	if (womanDisplay == true && manDisplay == false) {
		$("div.for_women a").click();
		filterWoman();
	}

	if (manDisplay == false) {
		$("div.man").css("display", "none");
		$("div.woman").css("display", "block");
		manDisplay = true;
	}
	else {
		$("div.man").css("display", "block");
		$("div.woman").css("display", "block");
		manDisplay = false;
	}
}



function sendForm(form) {
	$.ajax({
		type: "POST",
		url: $('#' + form).attr("action"),
		data: $('#' + form).serialize(),
		success: function(msg){
			/*if (msg == 'OK') {
				$('#close' + form).click();
				alert('Спасибо за заявку. Наш менеджер свяжется с Вами как только поступят часы.');
			}
			else {*/
				alert(msg);
			/*}*/
		}
	});
}
