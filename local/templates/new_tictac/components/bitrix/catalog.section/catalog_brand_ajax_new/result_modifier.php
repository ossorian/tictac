<?
$SECTION_ID = $arResult['ID'];

$min_price = 0;
$max_price = 0;

if (!empty($arResult['ITEMS'])){
    foreach ($arResult['ITEMS'] as $key => $arItem){
        if ($arItem['ID'] == $arProductOfTheDay['ID']){
            unset ($arResult['ITEMS'][$key]);
            continue;
        }
        $name_length = strlen($arItem['NAME']);
        if ($name_length > 20 ){
            $arName = explode(' ', $arItem['NAME']);
            $resName = '';
            $isBr = 0;
            foreach ($arName as $word){
                $resName .= $word . ' ';
                if ((float)($name_length / strlen($resName)) < 2  && empty($isBr)){
                    $isBr++;
                    $resName .= '<br>';
                }
            }
            $arResult['ITEMS'][$key]['NAME'] = $resName;
        }

        if (!empty($arItem['PROPERTIES']['BRAND_MODEL']['VALUE'])) {
            $arResult['ITEMS'][$key]['BRAND_NAME'] = $arItem['PROPERTIES']['BRAND_MODEL']['VALUE_ENUM'];
        }

        $nav = CIBlockSection::GetNavChain(false, $arItem['IBLOCK_SECTION_ID']);
        if ($arFirst = $nav->GetNext()) {
            if ($arFirst['ID'] == 172) {
                $arResult['ITEMS'][$key]['SEO_SECTION_NAME'] = 'часы';
            } else if ($arFirst['ID'] == 146) {
                $arResult['ITEMS'][$key]['SEO_SECTION_NAME'] = "ремешки";
            } else if ($arFirst['ID'] == 171) {
                $arResult['ITEMS'][$key]['SEO_SECTION_NAME'] = "украшения";
            } else if ($arFirst['ID'] == 165) {
                $arResult['ITEMS'][$key]['SEO_SECTION_NAME'] = "очки";
            }
        }
    }
    foreach ($arResult['ITEMS'] as $key => &$arItem)
    {
        $sectObj = CIBlockSection::GetList(
            Array("SORT"=>"ASC"),
            Array("ID"=>$arItem["~IBLOCK_SECTION_ID"],"IBLOCK_ID"=>$arItem["IBLOCK_ID"]),
            false,
            Array("NAME","ID","IBLOCK_ID"),
            false
        );
        if($sect= $sectObj->GetNext()){
            $arItem["PARENT_SECTION_NAME"] = $sect["NAME"];
        }
    }
    unset($arItem);
}

?>