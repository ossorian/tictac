<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<? global $USER; ?>
<?
$SeoList = '';
$cat = '';
$cur_page = $APPLICATION->GetCurPage();
if (is_numeric(stripos($cur_page, '/watches/'))) {
    $SeoList = 'Часы';
    $cat = 'часы';
} else if (is_numeric(stripos($cur_page, '/straps/'))) {
    $SeoList = 'Ремешки';
    $cat = 'ремешки';
} else if (is_numeric(stripos($cur_page, '/sunglasses/'))) {
    $SeoList = 'Очки';
    $cat = 'очки';
}else if (is_numeric(stripos($cur_page, '/accessories/'))) {
    $SeoList = 'Украшения';
    $cat = 'украшения';
}else if (is_numeric(stripos($cur_page, '/sale/'))) {
    $SeoList = 'Распродажа';
    $cat = 'часы';
}

if (!in_array($arResult["ID"], array(172, 146, 171, 165, 65))) {
    $SeoList .= ' - ' . $arResult["NAME"];
}

if (!empty($_GET['q'])) {
    $SeoList = "Search Results";
}

if(!empty($arParams['PROMO_INPUT'])){?>
    <input type="hidden" id="PROMO_INPUT_HIDDEN" value="PROMO" data-id="<?= $arResult["ID"] ?>">
<? } ?>

    <input type="hidden" id="section_id_init" value="<?= $arResult["NAME"] ?>" data-id="<?= $arResult["ID"] ?>"
		
           data-count="<?= count($arResult['ITEMS']) ?>">
    <div class="container">
        <div class="brend__items">
            <div class="row catalog_to_ajax">
                <? $count = 0; ?>
                <input type="hidden" id="is_product_of_the_day" value="Y">
                <input type="hidden" id="SECTION_ID" value="<?= $arResult['ID'] ?>">
                <input type="hidden" id="TOTAL_ELEMENT_COUNT" value="<?= count($arResult['ITEMS']) ?>">

                <? $position = 0; ?>
                <? foreach ($arResult['ITEMS'] as $arElement) {
                    if (!empty($_GET['q'])) {
                        $cat = $arElement['SEO_SECTION_NAME'];
                    }
                    $position++;
                    if ($_REQUEST["SECTION_CODE"] == 'daily_sale') {
                        LocalRedirect('/catalog/daily_sale/' . $arElement["CODE"], false, "301 Moved permanently");
                    }
                    ?>
                    <?
                    // Для поиска показываем постранично
                    $need = 20;
                    if (!empty($_GET['PAGEN_1'])) {
                        $need = 20 * $_GET['PAGEN_1'];
                    }

                    $count++;
                    if (/*isset($_GET["arrFilter_cf"]["1"]["LEFT"]) and*/
                        $count > $need
                    ) break;

                    $picture = resizeImage($arElement['PREVIEW_PICTURE']['ID'], 270, 322, 1);//print_r($picture);
                    $secondPicture = !empty($arElement['PROPERTIES']['PREVIEW_PICTURE2']['VALUE']) ? CFile::GetPath($arElement['PROPERTIES']['PREVIEW_PICTURE2']['VALUE']) : $picture['SRC'];
                    $pictureNumber = !empty($arElement['PROPERTIES']['PREVIEW_PICTURE2']['VALUE']) ? "фото 2" : "фото 1";
//                $picture = $arElement['PREVIEW_PICTURE'];
                    // определяем цены
                    $price = '';
                    $sale = '';
                    $elPrice = '';


                    foreach ($arElement["PRICES"] as $code => $arPrice) {
                        if ($arPrice["CAN_ACCESS"]) {

                            if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
                                $elPrice = $arPrice['DISCOUNT_VALUE'];
                                $price = '<p class="item__price">
                                                    <span class="color_red">' . $arPrice["PRINT_DISCOUNT_VALUE"] . '<span class="rub">₽</span></span> 
                                                    <span class="item-price__before">' . $arPrice["PRINT_VALUE"] . ' <span class="rub">₽</span></span>
                                                </p>';
                                $sale = '<span class="label label_sale">Sale</span>';
                            } else {
                                $elPrice = $arPrice['VALUE'];
                                $price = '<p class="item__price">' . $arPrice["PRINT_VALUE"] . ' <span class="rub">₽</span></p>';
                            }
                        }
                    }
                    ?>

										
                    <? if ($arElement["PROPERTIES"]["ON_PREORDER"]["VALUE"] === "Y") { ?>

                        <div class="col-md-<?= ($arResult["DEPTH_LEVEL"] == 1 ) ? 3 : 4 ?> col-sm-6 brend__col">
                            <div class="item no-item">
                                <?//= $sale ?>
                                <span class="label label_sale">Предзаказ</span>
                                <div class="favorites hidden-lg hidden-md">
                                    <span class="favorites__icon">
                                        <i class="icon-icon-wishlist"></i>
                                    </span>
                                    <p class="favorites__text">Избранное</p>
                                </div>
                                <div class="no-product">
                                    <p class="no-product__title brand"><?= !empty($arElement['PROPERTIES']['WAIT']['VALUE']) ? 'Поставка ' . date('d.m.Y', strtotime($arElement['PROPERTIES']['WAIT']['VALUE'])) : 'Ожидается поступление' ?></p>
                                    <a class="no-product__reminder link_border">Сообщить о поступлении</a><br>
                                    <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>"
                                       class="no-product__order link_border"
                                       data-seo="<?= $SeoList ?>"
                                       onclick="
                                           dataLayer.push({
                                           'event': 'productClick',
                                           'ecommerce': {
                                           'click': {
                                           'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.
                                           'products': [{
                                           'name': '<?= strip_tags($arElement['NAME']) ?>',        // Наименование товара.
                                           'id': '<?= $arElement['ID'] ?>',                // Идентификатор товара в базе данных.
                                           'price': '<?= $elPrice ?>',                // Цена одной единицы купленного товара
                                           'brand': '<?= $arElement['BRAND_NAME'] ?>',             //Бренд товара
                                           'category': '<?= $cat ?>',             //Категория товара
                                           'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',                //Вариант товара, например цвет
                                           'position': <?= $position ?>                //Позиция товара в списке
                                           }]
                                           }
                                           }
                                           });"
                                    >Оформить
                                        предзаказ</a>
                                </div>
                                <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>"
                                   data-seo="<?= $SeoList ?>"
                                   onclick="
                                       dataLayer.push({
                                       'event': 'productClick',
                                       'ecommerce': {
                                       'click': {
                                       'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.
                                       'products': [{
                                       'name': '<?= strip_tags($arElement['NAME']) ?>',        // Наименование товара.
                                       'id': '<?= $arElement['ID'] ?>',                // Идентификатор товара в базе данных.
                                       'price': '<?= $elPrice ?>',                // Цена одной единицы купленного товара
                                       'brand': '<?= $arElement['BRAND_NAME'] ?>',             //Бренд товара
                                       'category': '<?= $cat ?>',             //Категория товара
                                       'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',                //Вариант товара, например цвет
                                       'position': <?= $position ?>                //Позиция товара в списке
                                       }]
                                       }
                                       }
                                       });"
                                >
                                    <img src="<?= $picture["SRC"] ?>"
                                         alt="<?=$cat?> <?=$arElement['PARENT_SECTION_NAME']?> <?= $arElement["NAME"] ?> <?=$pictureNumber?>" title="<?=$cat?> <?=$arElement['PARENT_SECTION_NAME']?> <?= $arElement["NAME"] ?> <?=$pictureNumber?>"
                                         class="item-preview-img"
																				 data-second='<?= $secondPicture; ?>'
																				 />

                                    <p class="item__name"><?= $arElement['NAME'] ?></p>
                                </a>
                                <?= $price ?>

                                <div class="reminder">
                                        <span class="reminder__close">
                                            <i class="icon-icon-small-close"></i>
                                        </span>
                                    <form action="/ajax/ajax_preorder.php" name="form<?= $arElement["ID"] ?>"
                                          id="preorder_form<?= $arElement["ID"] ?>" method="POST">
                                        <input type="hidden" name="id" value="<?= $arElement["ID"] ?>"/>
                                        <input type="hidden" name="link"
                                               value="<?= $arElement["DETAIL_PAGE_URL"] ?>"/>
                                        <input type="hidden" name="name" value="<?= $arElement["NAME"] ?>"/>
                                        <?
                                        global $USER;
                                        $usEmail = $USER->GetEmail();
                                        $usName = $USER->GetFullName();
                                        ?>

                                        <span class="input input--haruki">
												<input class="input__field input__field--haruki" type="text"
                                                       id="input-1.1" name="fio" <?= $usName ?>/>
												<label class="input__label input__label--haruki" for="input-1.1">
													<span
                                                        class="input__label-content input__label-content--haruki">Имя</span>
												</label>
											</span>
                                        <span class="input input--haruki">
												<input class="input__field input__field--haruki" value="<?= $usEmail ?>"
                                                       type="text" id="input-1.3" name="email"/>
												<label class="input__label input__label--haruki" for="input-1.3">
													<span class="input__label-content input__label-content--haruki">Эл. почта</span>
												</label>
											</span>
                                        <input type="submit" class="btn btn_black o-reminder__btn" value="Жду"
                                               onClick="sendForm('preorder_form<?= $arElement["ID"] ?>'); return false;"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <? } else { ?>

                        <div class="col-md-<?= ($arResult["DEPTH_LEVEL"] == 1 ) ? 3 : 4 ?> col-sm-6 brend__col">
                            <div class="item">
                                <?= $sale ?>
                                <? if ($arElement["DISPLAY_PROPERTIES"]["NEW"]["DISPLAY_VALUE"] == "да") { ?>
                                    <span class="label label_new">New</span>
                                <? } ?>

                                <div class="favorites">
													<span class="favorites__icon" data-id="<?= $arElement['ID'] ?>">
														<i class="icon-icon-wishlist"></i>
													</span>
                                    <p class="favorites__text">Избранное</p>
                                </div>
                                <a href="#qv__popup" class="quick-view" data-id="<?= $arElement['ID'] ?>">
													<span class="quick-view__icon">
														<i class="icon-icon-quick-view"></i>
													</span>
                                    <span class="quick-view__text">Быстрый просмотр</span>
                                </a>
                                <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>"
                                   data-seo="<?= $SeoList ?>"
                                   onclick="
                                       dataLayer.push({
                                       'event': 'productClick',
                                       'ecommerce': {
                                       'click': {
                                       'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.666
                                       'products': [{
                                       'name': '<?= strip_tags($arElement['NAME']) ?>',        // Наименование товара.
                                       'id': '<?= $arElement['ID'] ?>',                // Идентификатор товара в базе данных.
                                       'price': '<?= $elPrice ?>',                // Цена одной единицы купленного товара
                                       'brand': '<?= $arElement['BRAND_NAME'] ?>',             //Бренд товара
                                       'category': '<?= $cat ?>',             //Категория товара
                                       'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',                //Вариант товара, например цвет
                                       'position': <?= $position ?>                //Позиция товара в списке
                                       }]
                                       }
                                       }
                                       });"
                                >
                                    <img src="<?= $picture["SRC"] ?>"
                                         alt="<?=$cat?> <?=$arElement['PARENT_SECTION_NAME']?> <?= $arElement["NAME"] ?> <?=$pictureNumber?>" title="<?=$cat?> <?=$arElement['PARENT_SECTION_NAME']?> <?= $arElement["NAME"] ?> <?=$pictureNumber?>"
                                         class="item-preview-img"
																				 data-second='<?= $secondPicture; ?>'
																				 />
                                    <p class="item__name"><?= $arElement['NAME'] ?> </p>
                                </a>
                                <?= $price ?>
                                <? if ($arElement["PROPERTIES"]["ON_PREORDER"]["VALUE"] === "Y") { ?>
                                    <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>" class="btn btn_black item__btn "
                                       data-seo="<?= $SeoList ?>"
                                       onclick="
                                           dataLayer.push({
                                           'event': 'productClick',
                                           'ecommerce': {
                                           'click': {
                                           'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.666
                                           'products': [{
                                           'name': '<?= strip_tags($arElement['NAME']) ?>',        // Наименование товара.
                                           'id': '<?= $arElement['ID'] ?>',                // Идентификатор товара в базе данных.
                                           'price': '<?= $elPrice ?>',                // Цена одной единицы купленного товара
                                           'brand': '<?= $arElement['BRAND_NAME'] ?>',             //Бренд товара
                                           'category': '<?= $cat ?>',             //Категория товара
                                           'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',                //Вариант товара, например цвет
                                           'position': <?= $position ?>                //Позиция товара в списке
                                           }]
                                           }
                                           }
                                           });"
                                    >Предзаказ</a>
                                <? } else { ?>
                                    <a href="#" class="btn btn_black item__btn add_to_cart_btn"
                                       data-product-id="<?= $arElement["ID"] ?>"
                                       data-product-image="<?= $picture["SRC"] ?>"
                                       onclick="
                                           dataLayer.push({
                                           'event': 'addToCart',
                                           'ecommerce': {
                                           'currencyCode': 'RUB',
                                           'add': {
                                           'products': [{
                                           'name': '<?= strip_tags($arElement["NAME"]) ?>',
                                           'id': '<?= $arElement["ID"] ?>',
                                           'price': '<?= $elPrice ?>',
                                           'brand': '<? $arElement['BRAND_NAME'] ?>',
                                           'category': '<?= $cat ?>',
                                           'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM']) ?>',
                                           'quantity': 1
                                           }]
                                           }
                                           }
                                           });
                                           fbq('track', 'AddToCart', {
                                           currency: 'RUB',
                                           });">В корзину</a>
                                <? } ?>
                                <!--								<a href="#" class="preorder link_grey">Предзаказ</a>-->
                            </div>
                        </div>

                    <?} ?>
                <?
                    $product[]=$arElement["ID"];
                    $prices[]=$elPrice;
                } ?>
            </div>
        </div>
    </div>
<?
if(isset($_REQUEST["q"]) && $_REQUEST["q"]!="") {
    $page = "searchresults";
}else{
    $page="category";
}
$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/remarketing.php","PAGE_TYPE"=>$page,"PRODUCT"=>$product,"PRICE"=>$prices), false);?>


<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <? $arResult["NAV_STRING"] ?>
<? endif; ?>


<? if (/*isset($_GET["arrFilter_cf"]["1"]["LEFT"]) and */
    count($arResult["ITEMS"]) > 20
): ?>
    <?
    global $arrFilter;
    $filter = array();
    $filter["filter"] = $arrFilter;
    ?>

    <script type="text/javascript">
        BX.ready(function () {

            $(function () {
                var path = "/ajax/pagen_brands.php?sort=<?=$arParams["ELEMENT_SORT_FIELD"]?>&order=<?=$arParams["ELEMENT_SORT_ORDER"]?>&SECTION_CODE=<?=$arParams["SECTION_CODE"]?>&<?=http_build_query($filter)?><?= $APPLICATION->GetCurPage() === "/catalog/preorder/" ? "&preorder=Y" : ""?>&seo_list=<?=$SeoList?>";
                //счетчик страниц
                var currentPage = <?= !empty($_GET['PAGEN_1']) ? $_GET['PAGEN_1'] : 1?>;

                // Подзагрузка по скролу
                var loading = false;
                $(window).scroll(function () {
                    if ((($(window).scrollTop() + $(window).height()) + 2000) >= $(document).height()) {

                        if (loading == false) {
                            loading = true;
                            // показываем загрузку
                            $("div.loader").attr("style", "background: none repeat scroll 0 0 #000000;opacity: 0.5; overflow: hidden");
                            $("div.loader_inner").attr("style", "opacity: 0.4; background-color: #000000;");

                            $("div.loader").show();
                            $("div.loader_inner").show();
                            //$("div.loader").addClass("arcticmodal-container"); // глюк со скролом

                            currentPage++;

                            //указываем страницу
                            var hashes = window.location.href.toString();

                            if (hashes.indexOf("PAGEN_1") >= 0){
                                var rep = currentPage-1;
                                hashes = hashes.replace('PAGEN_1=' + rep, 'PAGEN_1=' + currentPage);
                            } else {
                                if (hashes.indexOf("?") >= 0) {
                                    hashes += '&PAGEN_1=' + currentPage;
                                } else {
                                    hashes += '?PAGEN_1=' + currentPage;
                                }
                            }
                            history.pushState(null, null, hashes);

                            var count_in_row = 0;
                            if ($('div.view_four_items').hasClass('view_three_items_active')) {
                                count_in_row = 1;
                            }

                            //делаем ajax запрос и сразу инкремент номера страницы
                            $.get(path + '&PAGEN_1=' + currentPage + '&count_in_row=' + count_in_row, function (data) {
                                $(".catalog_to_ajax").append(data);
                                // скрываем "далее"
                                if (currentPage >= <?=ceil(count($arResult["ITEMS"]) / 20)?>) {
                                    $(".show-more").hide();
                                }
                                else {
                                    loading = false;
                                }

                                // убираем показ загрузки
                                $("div.loader").hide();
                                $("div.loader_inner").hide();
                                //$("#showLoad").css("display", "none");
                                //$("#showLoad").removeClass("arcticmodal-container");
                            });
                        }
                    }
                });


            });
        });
    </script>


    <?
    /*
    Кнопка подгрузить далее
        <div class="show-more"><a href="#">подгрузить далее</a></div>
    */
    ?>


<? endif; ?>


<? if ($_REQUEST["SECTION_CODE"] == "promo"): ?>
    <script type="text/javascript">
        $(function () {
            $('.category_switcher a').trigger('click');
        });
    </script>
<? endif; ?>
<? /*$arResult["~UF_RETARGETING"]*/ ?>