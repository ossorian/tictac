<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?$this->setFrameMode(true);?>
<? if (!empty($arResult['ITEMS'])) { ?>
    <div class="favorites__items">
        <div class="row mob_row">
            <? $position = 0;?>
            <? $SeoList = "Избранное"?>
            <? foreach ($arResult["ITEMS"] as $key => $item) {
                $picture = resizeImage($item["PREVIEW_PICTURE"]["ID"], 263, 291, 3);
//                $picture = $item["PREVIEW_PICTURE"];
                //var_dump($picture);
                $position++;
                $price = '';
                $sale = '';
                $elPrice = '';

                foreach ($item["PRICES"] as $code => $arPrice) {
                    if ($arPrice["CAN_ACCESS"]) {

                        if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
                            $elPrice = $arPrice['DISCOUNT_VALUE'];
                            $price = '<p class="item__price">
                                                    <span class="color_red">' . $arPrice["PRINT_DISCOUNT_VALUE"] . '<span class="rub">₽</span></span> 
                                                    <span class="item-price__before">' . $arPrice["PRINT_VALUE"] . ' <span class="rub">₽</span></span>
                                                </p>';
                            $sale = '<span class="label label_sale">Sale</span>';
                        } else {
                            $elPrice = $arPrice['VALUE'];
                            $price = '<p class="item__price">' . $arPrice["PRINT_VALUE"] . ' <span class="rub">₽</span></p>';
                        }
                    }
                }
                ?>
                <div class="col-md-4 col-xs-6 mob_pad">
                    <div class="item">
                        <?= $sale?>
                        <span class="item__close" data-id="<?= $item['ID']?>">
                            <i class="icon-icon-small-close"></i>
                        </span>
                        <a href="<?= $item['DETAIL_PAGE_URL'] ?>"
                           data-seo="<?= $SeoList ?>"
                           onclick="
                               dataLayer.push({
                               'event': 'productClick',
                               'ecommerce': {
                               'click': {
                               'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.
                               'products': [{
                               'name': '<?= strip_tags($item['NAME'])?>',       	// Наименование товара.
                               'id': '<?= $item['ID']?>',  				// Идентификатор товара в базе данных.
                               'price': '<?= $elPrice?>',   				// Цена одной единицы купленного товара
                               'brand': '<?= $item['BRAND_NAME']?>',  			 //Бренд товара
                               'category': '<?= $item['SEO_SECTION_NAME']?>', 			 //Категория товара
                               'variant': '<?= implode(',', $item['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',  				//Вариант товара, например цвет
                               'position': <?= $position?>  				//Позиция товара в списке
                               }]
                               }
                               }
                               });">
                            <img src="<?= $picture["SRC"] ?>" alt="<?= $item['NAME'] ?>">
                            <p class="item__name"><?= $item['NAME'] ?></p>
                        </a>
                        <?= $price?>
                        <? if ($item["PROPERTIES"]["ON_PREORDER"]["VALUE"] === "Y") { ?>
                            <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="btn btn_black item__btn "
                               data-seo="<?= $SeoList ?>"
                               onclick="
                                   dataLayer.push({
                                   'event': 'productClick',
                                   'ecommerce': {
                                   'click': {
                                   'actionField': {'list': '<?= $SeoList ?>'},     // Список с которого был клик.
                                   'products': [{
                                   'name': '<?= strip_tags($item['NAME'])?>',       	// Наименование товара.
                                   'id': '<?= $item['ID']?>',  				// Идентификатор товара в базе данных.
                                   'price': '<?= $elPrice?>',   				// Цена одной единицы купленного товара
                                   'brand': '<?= $item['BRAND_NAME']?>',  			 //Бренд товара
                                   'category': '<?= $item['SEO_SECTION_NAME']?>', 			 //Категория товара
                                   'variant': '<?= implode(',', $item['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',  				//Вариант товара, например цвет
                                   'position': <?= $position?>  				//Позиция товара в списке
                                   }]
                                   }
                                   }
                                   });">Предзаказ</a>
                        <? } else { ?>
                        <a href="#" class="btn btn_black item__btn add_to_cart_btn" data-product-id="<?= $item["ID"] ?>"
                           data-product-image="<?= $picture["SRC"] ?>"
                           onclick="
                               dataLayer.push({
                               'event': 'addToCart',
                               'ecommerce': {
                               'currencyCode': 'RUB',
                               'add': {
                               'products': [{
                               'name': '<?= strip_tags($item["NAME"]) ?>',
                               'id': '<?= $item["ID"] ?>',
                               'price': '<?= $elPrice ?>',
                               'brand': '<? $item['BRAND_NAME']?>',
                               'category': '<?= $item['SEO_SECTION_NAME']?>',
                               'variant': '<?= implode(',', $item['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',
                               'quantity': 1
                               }]
                               }
                               }
                               });">В корзину</a>
                        <? } ?>
                    </div>
                </div>
            <? } ?>
        </div>
    </div>

    <div class="order__empty hidden">
        <span class="order-empty__icon">
            <i class="icon-icon-wishlist"></i>
        </span>
        <p class="order-empty__text">У вас нет избранных товаров</p>
    </div>
<? } else { ?>

    <div class="order__empty">
        <span class="order-empty__icon">
            <i class="icon-icon-wishlist"></i>
        </span>
        <p class="order-empty__text">У вас нет избранных товаров</p>
    </div>

<? } ?>
