<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
?>

<? global $USER; ?>
<?
$cat = '';
$cur_page = $APPLICATION->GetCurPage();
if (is_numeric(stripos($cur_page, 'watches'))) {
    $cat= 'часы';
} else if (is_numeric(stripos($cur_page, 'straps'))) {
    $cat= 'ремешки';
} else if (is_numeric(stripos($cur_page, '/accessories/'))) {
    $cat = 'украшения';
} else if (is_numeric(stripos($cur_page, 'sunglasses'))) {
    $cat= 'очки';
}
?>
<div class="popular__items">
    <div class="row mob_row">
        <? $count = 0; ?>
        <? foreach ($arResult['ITEMS'] as $arElement) { ?>
            <?

            $picture = resizeImage($arElement['PREVIEW_PICTURE']['ID'], 263, 292, 1);//print_r($picture);
            //$picture = $arElement['PREVIEW_PICTURE'];
            // определяем цены
            $price = '';
            $sale = '';
            $elPrice = '';
            foreach ($arElement["PRICES"] as $code => $arPrice) {
                if ($arPrice["CAN_ACCESS"]) {
                    $elPrice = $arPrice['VALUE'];
                    if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]) {
                        $price = '<p class="item__price">
                                                    <span class="color_red">' . $arPrice["PRINT_DISCOUNT_VALUE"] . '<span class="rub">₽</span></span> 
                                                    <span class="item-price__before">' . $arPrice["PRINT_VALUE"] . ' <span class="rub">₽</span></span>
                                                </p>';
                        $sale = '<span class="label label_sale">Sale</span>';
                    } else {
                        $price = '<p class="item__price">' . $arPrice["PRINT_VALUE"] . ' <span class="rub">₽</span></p>';
                    }
                }
            }
            ?>

            <? if (!empty($arElement["DISPLAY_PROPERTIES"]["WAIT"]["DISPLAY_VALUE"])) { ?>

            <? } else { ?>

                <div class="col-md-3 col-xs-6 mob_pad">
                    <div
                        class="item">
                        <?= $sale ?>
                        <? if ($arElement["DISPLAY_PROPERTIES"]["NEW"]["DISPLAY_VALUE"] == "да") { ?>
                            <span class="label label_new">New</span>
                        <? } ?>

                        <div class="favorites">
									<span class="favorites__icon" data-id="<?= $arElement['ID'] ?>">
										<i class="icon-icon-wishlist"></i>
									</span>
                            <p class="favorites__text">Избранное</p>
                        </div>
                        <a href="#qv__popup" class="quick-view" data-id="<?= $arElement['ID'] ?>">
								<span class="quick-view__icon">
									<i class="icon-icon-quick-view"></i>
								</span>
                            <span class="quick-view__text">Быстрый просмотр</span>
                        </a>
                        <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>"
                           data-seo="Вам может понравится"
                           onclick="
                            dataLayer.push({
                            'event': 'productClick',
                            'ecommerce': {
                            'click': {
                            'actionField': {'list': 'Вам может понравится'},     // Список с которого был клик.
                            'products': [{
                            'name': '<?= strip_tags($arElement['NAME'])?>',       	// Наименование товара.
                            'id': '<?= $arElement['ID']?>',  				// Идентификатор товара в базе данных.
                            'price': '<?= $elPrice?>',   				// Цена одной единицы купленного товара
                            'brand': '<?= $arElement['BRAND_NAME']?>',  			 //Бренд товара
                            'category': '<?= $arElement['SEO_SECTION_NAME']?>', 			 //Категория товара
                            'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',  				//Вариант товара, например цвет
                            'position': <?= $count?>  				//Позиция товара в списке
                            }]
                            }
                            }
                            });">
                        <img src="<?= $picture["SRC"] ?>"
                             alt="<?=$arElement["PARENT_SECTION_NAME"]?> <?= $arElement["NAME"] ?> фото 1" title="<?=$arElement["PARENT_SECTION_NAME"]?> <?= $arElement["NAME"] ?> фото 1"
                             class="item-preview-img"/>

                            <p class="item__name"><?= $arElement['NAME'] ?></p>
                        </a>
                        <?= $price ?>
                        <a href="#" class="btn btn_black item__btn add_to_cart_btn" data-product-id="<?= $arElement["ID"] ?>"
                           data-product-image="<?= $picture["SRC"] ?>"
                           onclick="
                               dataLayer.push({
                                    'event': 'addToCart',
                                    'ecommerce': {
                                        'currencyCode': 'RUB',
                                        'add': {
                                           'products': [{
                                               'name': '<?= strip_tags($arElement["NAME"]) ?>',
                                               'id': '<?= $arElement["ID"] ?>',
                                               'price': '<?= $elPrice ?>',
                                               'brand': '<? $arElement['BRAND_NAME']?>',
                                               'category': '<?= $arElement['SEO_SECTION_NAME']?>',
                                               'variant': '<?= implode(',', $arElement['PROPERTIES']['FILTER_COLOR']['VALUE_ENUM'])?>',
                                               'quantity': 1
                                           }]
                                        }
                                    }
                               });">В корзину</a>
<!--                        <a href="#" class="preorder link_grey">Предзаказ</a>-->
                    </div>
                </div>

            <? } ?>
        <? } ?>
    </div>
</div>
