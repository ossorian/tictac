<?
if (!empty($arResult['ITEMS'])){
    foreach ($arResult['ITEMS'] as $key => $arItem){
        $name_length = strlen($arItem['NAME']);
        if ($name_length > 20 ){
            $arName = explode(' ', $arItem['NAME']);
            $resName = '';
            $isBr = 0;
            foreach ($arName as $word){
                $resName .= $word . ' ';
                if ((float)($name_length / strlen($resName)) < 2  && empty($isBr)){
                    $isBr++;
                    $resName .= '<br>';
                }
            }
            $arResult['ITEMS'][$key]['NAME'] = $resName;
        }

        if (!empty($arItem['PROPERTIES']['BRAND_MODEL']['VALUE'])) {
            $arResult['ITEMS'][$key]['BRAND_NAME'] = $arItem['PROPERTIES']['BRAND_MODEL']['VALUE_ENUM'];
        }

        $nav = CIBlockSection::GetNavChain(false, $arItem['IBLOCK_SECTION_ID']);
        if ($arFirst = $nav->GetNext()) {
            if ($arFirst['ID'] == 172) {
                $arResult['ITEMS'][$key]['SEO_SECTION_NAME'] = 'часы';
            } else if ($arFirst['ID'] == 146) {
                $arResult['ITEMS'][$key]['SEO_SECTION_NAME'] = "ремешки";
            } else if ($arFirst['ID'] == 171) {
                $arResult['ITEMS'][$key]['SEO_SECTION_NAME'] = "украшения";
            } else if ($arFirst['ID'] == 165) {
                $arResult['ITEMS'][$key]['SEO_SECTION_NAME'] = "очки";
            }
        }
    }
}


?>