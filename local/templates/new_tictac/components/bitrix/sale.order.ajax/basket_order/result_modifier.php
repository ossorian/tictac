<?

if ($arResult['ORDER']) {
	$dbOrderProps = CSaleOrderPropsValue::GetList(
		array("SORT" => "ASC"),
		array(
			"ORDER_ID" => $arResult['ORDER']['ID'],
			"CODE" => array("PHONE")
		)
	);
	if ($arOrderProps = $dbOrderProps->Fetch())
	{
		$arResult['ORDER']['USER_PHONE'] = $arOrderProps['VALUE'];
	}
}

// Отключаем EMS Москвы и Питера и где его нет
if(in_array($arResult["USER_VALS"]["DELIVERY_LOCATION"], array(4379, 4334)))
	unset($arResult["DELIVERY"]["ems"]);
else {
	/*$result = CSaleDeliveryHandler::CalculateFull(
		"ems",
		"delivery",
		array(
			"PRICE"         => $arResult["ORDER_PRICE"],
			"WEIGHT"        => $arResult["ORDER_WEIGHT"],
			"LOCATION_FROM" => COption::GetOptionInt('sale', 'location'),
			"LOCATION_TO"   => $arResult["USER_VALS"]["DELIVERY_LOCATION"],
			"LOCATION_ZIP"  => $arResult["USER_VALS"]["DELIVERY_LOCATION_ZIP"],
		),
		$arResult["BASE_LANG_CURRENCY"]
	);*/
	if($result["RESULT"] != "OK")
		unset($arResult["DELIVERY"]["ems"]);
}
if($arResult["USER_VALS"]["DELIVERY_ID"] == "ems:delivery" and empty($arResult["DELIVERY"]["ems"])) {
	$key = key($arResult["DELIVERY"]);
	$arResult["USER_VALS"]["DELIVERY_ID"] = $key;
	$arResult["DELIVERY"][$key]["CHECKED"] = "Y";

	$arResult["DELIVERY_PRICE"] = $arResult["DELIVERY"][$key]["PRICE"];
	$arResult["DELIVERY_PRICE_FORMATED"] = $arResult["DELIVERY"][$key]["PRICE_FORMATED"];

	$arResult["ORDER_TOTAL_PRICE_FORMATED"] = SaleFormatCurrency($arResult["ORDER_PRICE"] + $arResult["DELIVERY"][$key]["PRICE"], $arResult["DELIVERY"][$key]["ORDER_CURRENCY"]);
}

// Выделяем текст для москвы красным
if($arResult["USER_VALS"]["DELIVERY_LOCATION"] == 4379){
    foreach($arResult["DELIVERY"] as $k => $v){
        $arResult["DELIVERY"][$k]["DESCRIPTION"] = '<font color="#DD3333">'.$arResult["DELIVERY"][$k]["DESCRIPTION"].'</font>';
	}
}

foreach ($arResult["DELIVERY"] as $key_delivery=> $delivery){
	if($delivery['ID'] == 34 || $delivery['ID'] == 37){
        $arResult["DELIVERY"][$key_delivery]['DESCRIPTION'] = '<font color="#DD3333">'.$delivery['DESCRIPTION'].'</font>';
	}

	if($arResult['USER_VALS']['PAY_SYSTEM_ID'] != 1) {
        if (($delivery['ID'] == 1 || $delivery['ID'] == 6) && $arResult['ORDER_PRICE'] > 10000) {
            $arResult["DELIVERY"][$key_delivery]['PRICE'] = 0;
            $arResult["DELIVERY"][$key_delivery]['PRICE_FORMATED'] = 0;
        }

        if (($delivery['ID'] == 34 || $delivery['ID'] == 35) && $arResult['ORDER_PRICE'] > 7000) {
            $arResult["DELIVERY"][$key_delivery]['PRICE'] = 0;
            $arResult["DELIVERY"][$key_delivery]['PRICE_FORMATED'] = 0;
        }
	}
}

$deliv_id = $arResult['USER_VALS']['DELIVERY_ID'];
$arResult['DELIVERY_PRICE'] = $arResult['DELIVERY'][$deliv_id]['PRICE'];
$arResult['DELIVERY_PRICE_FORMATED'] = $arResult['DELIVERY'][$deliv_id]['PRICE_FORMATED'];
//$arResult["ORDER_TOTAL_PRICE_FORMATED"] = SaleFormatCurrency($arResult["ORDER_PRICE"] + $delivery['PRICE'], $arResult["DELIVERY"][$key_delivery]["ORDER_CURRENCY"]);


foreach($arResult["PAY_SYSTEM"] as $key => $system) {
	$arPayID2Key[$system["ID"]] = $key;
}

// Для русского сайта все платежки кроме пайпал, для английскго только пайпал
$pay_counter = 0;
foreach($arResult["PAY_SYSTEM"] as $k => $system) {

	//Условие от 25-12-2019
/* 	if ($system["ID"] == 17 and empty($arResult["ORDER_ID"]) and $arResult['ORDER_PRICE'] <= 15000) {//Stripe
		unset($arResult["PAY_SYSTEM"][$k]);
		continue;
	}
	if ($system["ID"] == 4 and empty($arResult["ORDER_ID"]) and $arResult['ORDER_PRICE'] > 15000) {//Yandex
		unset($arResult["PAY_SYSTEM"][$k]);
		continue;
	}
	
//	if(LANGUAGE_ID == "ru" and $system["ID"] == 9 or $system["ID"]==17 and empty($arResult["ORDER_ID"])) {
	if(LANGUAGE_ID == "ru" and $system["ID"] == 9) {
		unset($arResult["PAY_SYSTEM"][$k]);
	}
	elseif(LANGUAGE_ID == "en" and $system["ID"] != 9 and empty($arResult["ORDER_ID"])) {
		unset($arResult["PAY_SYSTEM"][$k]);
	}
	elseif(LANGUAGE_ID == "en" and $system["ID"] == 9 and empty($arResult["ORDER_ID"])) {
		$arResult["PAY_SYSTEM"][$k]["CHECKED"] = "Y";
	} */

	if ((LANGUAGE_ID == "ru" and $system["ID"] == 9)
//		or ($system["ID"] == 19 and empty($arResult["ORDER_ID"]))
		or (($arResult['ORDER_PRICE'] < 15000) && ($system["ID"] == 17))
		or (($arResult['ORDER_PRICE'] >= 15000) && ($system["ID"] == 8))
	) {
		if ($arResult["PAY_SYSTEM"][$k]["CHECKED"]) {
			$otherPaySystemKey = $arPayID2Key[$system["ID"] == 17 ? 8 : 17];
			$arResult["PAY_SYSTEM"][$otherPaySystemKey]["CHECKED"] = "Y";
		}
		unset($arResult["PAY_SYSTEM"][$k]);
	}
	elseif(LANGUAGE_ID == "en" and $system["ID"] != 9 and empty($arResult["ORDER_ID"])) {
		unset($arResult["PAY_SYSTEM"][$k]);
	}
	elseif(LANGUAGE_ID == "en" and $system["ID"] == 9 and empty($arResult["ORDER_ID"])) {
		$arResult["PAY_SYSTEM"][$k]["CHECKED"] = "Y";
	}

	if($system['ID'] != 1 && $pay_counter == 0 ){
		if((($deliv_id == 1 || $deliv_id == 6) && $arResult['ORDER_PRICE'] > 10000) ||
			(($deliv_id == 34 || $deliv_id == 35) && $arResult['ORDER_PRICE'] > 7000)
		){
            $arResult["PAY_SYSTEM"][$k]['NAME'] = $system['NAME'].' '.'<font color="#DD3333" style="font-size: 13px;">БЕСПЛАТНАЯ ДОСТАВКА ПРИ ОПЛАТЕ НА САЙТЕ!</font>';
            $pay_counter++;
		}

	}
}
?>
