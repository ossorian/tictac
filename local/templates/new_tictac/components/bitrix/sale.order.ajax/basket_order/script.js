BX.ready(function () {
    $('#paymentDescription').text($('input:radio[name=PAY_SYSTEM_ID]:checked').attr('data-desr'));
    BX.addCustomEvent('onAjaxSuccess', function () {
// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        /*if (!String.prototype.trim) {
            (function () {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(rtrim, '');
                };
            })();
        }

        [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
            // in case the input is already filled..
            if (inputEl.value.trim() !== '') {
                classie.add(inputEl.parentNode, 'input--filled');
            }

            // events:
            inputEl.addEventListener('focus', onInputFocus);
            inputEl.addEventListener('blur', onInputBlur);
        });

        function onInputFocus(ev) {
            classie.add(ev.target.parentNode, 'input--filled');
        }

        function onInputBlur(ev) {
            if (ev.target.value.trim() === '') {
                classie.remove(ev.target.parentNode, 'input--filled');
            }
        }*/


        //selects
        $('select').each(function () {
            if (this.id !== 'ORDER_PROP_20') {
                var $this = $(this), numberOfOptions = $(this).children('option').length;

                $this.addClass('sel-hidden');
                $this.wrap('<div class="sel"></div>');
                $this.after('<div class="sel-styled"></div>');

                var $styledsel = $this.next('div.sel-styled');

                var $list = $('<ul />', {
                    'class': 'sel-options'
                }).insertAfter($styledsel);

                for (var i = 0; i < numberOfOptions; i++) {
                    if (parseInt($this.children('option').eq(i).val()) == $this.val() ||
                        $this.children('option').eq(i).attr('selected') === 'selected') {

                        $('<li />', {
                            text: $this.children('option').eq(i).text(),
                            rel: $this.children('option').eq(i).val(),
                            class: 'li_active',
                            onclick: $this.attr('onchange'),
                            value: $this.children('option').eq(i).val()
                        }).appendTo($list);
                        $styledsel.text($this.children('option').eq(i).text())
                    }
                    else {
                        $('<li />', {
                            text: $this.children('option').eq(i).text(),
                            rel: $this.children('option').eq(i).val(),
                            onclick: $this.attr('onchange'),
                            value: $this.children('option').eq(i).val()
                        }).appendTo($list);
                    }
                }
            }
        });
    });
});