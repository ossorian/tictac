<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="checkout__foot">
    <a name="order_form"></a>

    <script type="text/javascript">

        /*$(function(){
            var
                MP = $('#order_form_div');
            MP.on({
                'focus': function () {
                    var
                        T = $(this),
                        Placeholder = T.attr('data-placeholder') || '',

                        Value = T.val();

                    if (Value == Placeholder) {
                        T.val('');
                        T.removeClass('placeholdered');
                    }
                },
                'blur': function () {
                    var
                        T = $(this),
                        Placeholder = T.attr('data-placeholder') || '',

                        Value = T.val();

                    if (!Value) {
                        T.val(Placeholder);
                        T.addClass('placeholdered');
                    }
                }
            }, ':text');

        });*/


    </script>
    <div id="order_form_div">
        <NOSCRIPT>
            <div class="errortext"><?= GetMessage("SOA_NO_JS") ?></div>
        </NOSCRIPT>
        <?
        if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y") {
        if (strlen($arResult["REDIRECT_URL"]) > 0) {
            ?>
            <script>
                //top.location.replace = '<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
                window.top.location.href = '<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
                //setInterval("window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';",2000);
            </script>
        <?
        die();
        }
        else
            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
        }
        else
        {
        $FORM_NAME = 'ORDERFORM_' . RandString(5);
        if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
        {
        foreach ($arResult["ERROR"] as $v) {
            if (LANGUAGE_ID == "en")
                $v = str_replace("Местоположение", "Region", $v);
            echo ShowError($v);
        }
        ?>
            <script>
                top.location.hash = '#order_form';
            </script>
        <?
        }
        ?>

            <script>
                function isValidEmailAddress(emailAddress) {
                    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                    return pattern.test(emailAddress);
                }
                function submitForm(val) {

                    var offset = 0;

                    emailErr = true;
                    if (val == 'Y') {
                        emailErr = isValidEmailAddress($("#ORDER_PROP_2").val());
                    }

//                    $('select[data-need="1"] option').each(function () {
//                        $(this).removeAttr('selected');
//                        if ($('#ORDER_PROP_6').attr('data-value') == $(this).val()){
//                            $(this).attr('selected', 'selected');
//                        }
//                    });

                    if ($('#agreement-01:checked').length > 0) {
                        $('#agreement-01').next('label').removeAttr('style');
                    } else {
                        $('#agreement-01').next('label').attr('style', 'color: #f00;');

                        return false;
                    }

                    if (val == 'Y') {

                        if ($('#ORDER_PROP_1').val().length <= 0) {
                            $("#ORDER_PROP_1").next('label.input__label').attr('style', 'color: red;');
                            $("#ORDER_PROP_1").attr('style', 'color: red;');

                            if (offset <= 0) {
                                offset = $("div.checkout-form__head").offset().top;
                            }

                        } else {
                            $("#ORDER_PROP_1").next('label.input__label').removeAttr('style', 'color: red;');
                            $("#ORDER_PROP_1").removeAttr('style', 'color: red;');
                        }


                        if ($('#ORDER_PROP_7').val().length <= 0) {
                            $("#ORDER_PROP_7").next('label.input__label').attr('style', 'color: red;');
                            $("#ORDER_PROP_7").attr('style', 'color: red;');

                            if (offset <= 0) {
                                offset = $("div.checkout-form__head").offset().top;
                            }

                        } else {
                            $("#ORDER_PROP_7").next('label.input__label').removeAttr('style', 'color: red;');
                            $("#ORDER_PROP_7").removeAttr('style', 'color: red;');
                        }

                        if ($('#ORDER_PROP_3').val().length <= 0) {
                            $("#ORDER_PROP_3").next('label.input__label').attr('style', 'color: red;');
                            $("#ORDER_PROP_3").attr('style', 'color: red;');

                            if (offset <= 0) {
                                offset = $("div.checkout-form__head").offset().top;
                            }

                        } else {
                            $("#ORDER_PROP_3").next('label.input__label').removeAttr('style', 'color: red;');
                            $("#ORDER_PROP_3").removeAttr('style', 'color: red;');
                        }
                    }

                    if ($('select[data-need="1"]').val() != '(выберите город)' && $('select[data-need="1"]').val() && emailErr && offset<=0) {

                    } else {

                        $("#LOCATION_ORDER_PROP_5 select").each(function () {
                            if (!parseInt($(this).val())) {
                                $(this).css('border', '1px solid red');
                                //$(this).closest('p.form__label').attr('style', 'color: red;');
                                $(this).parent('div.sel').prev('p.form__label').attr('style', 'color: red;');

                                if (offset <= 0) {
                                    offset = $('div.checkout-form__head').offset().top;
                                }

                            } else {
                                $(this).parent('div.sel').prev('p.form__label').removeAttr('style');
                            }
                        });

                        if (val == 'Y') {
                            if (!isValidEmailAddress($("#ORDER_PROP_2").val())) {
                                //$("#ORDER_PROP_2").css('border', '1px solid red');
                                $("#ORDER_PROP_2").next('label.input__label').attr('style', 'color: red;');
                                $("#ORDER_PROP_2").attr('style', 'color: red;');

                                if (offset <= 0) {
                                    offset = $("div.checkout-form__head").offset().top;
                                }

                            } else {
                                $("#ORDER_PROP_2").next('label.input__label').removeAttr('style', 'color: red;');
                                $("#ORDER_PROP_2").removeAttr('style', 'color: red;');
                            }

                            if (offset > 0) {
                                $('span.order-error-message').show();
                                $('html, body').animate({
                                    scrollTop: offset - 30
                                }, 1000);
                                return false;
                            } else {
                                $('span.order-error-message').hide();
                            }
                        }

                        return false;
                    }


                    if (val != 'Y')
                        document.getElementById('confirmorder').value = 'N';

                    var orderForm = document.getElementById('ORDER_FORM_ID_NEW');

                    if ($("#ORDER_PROP_24").val() == '<?=GetMessage("ORDER_FAMILIARITY")?> :)') {
                        $("#ORDER_PROP_24").val('');
                    }

                    if (offset <= 0) {
                        jsAjaxUtil.InsertFormDataToNode(orderForm, 'order_form_div', true);
                        orderForm.submit();
                    }


                    return true;
                }
                function SetContact(profileId) {
                    document.getElementById("profile_change").value = "Y";
                    submitForm();
                }

            </script>



            <div style="display:none;">
                <div id="order_form_id">
                    &nbsp;
                    <?
                    if (count($arResult["PERSON_TYPE"]) > 1) {
                        ?>

                        <b><?= GetMessage("SOA_TEMPL_PERSON_TYPE") ?></b>
                        <table class="sale_order_full_table">
                            <tr>
                                <td>
                                    <?
                                    foreach ($arResult["PERSON_TYPE"] as $v) {
                                        ?><input type="radio" id="PERSON_TYPE_<?= $v["ID"] ?>"
                                                 name="PERSON_TYPE" value="<?= $v["ID"] ?>"<? if ($v["CHECKED"] == "Y") echo " checked=\"checked\""; ?>
                                                 onClick="submitForm()"> <label
                                            for="PERSON_TYPE_<?= $v["ID"] ?>"><?= $v["NAME"] ?></label><br/><?
                                    }
                                    ?>
                                    <input type="hidden" name="PERSON_TYPE_OLD"
                                           value="<?= $arResult["USER_VALS"]["PERSON_TYPE_ID"] ?>">
                                </td>
                            </tr>
                        </table>
                        <br/><br/>
                        <?
                    } else {
                        if (IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0) {
                            ?>
                            <input type="hidden" name="PERSON_TYPE"
                                   value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>">
                            <input type="hidden" name="PERSON_TYPE_OLD"
                                   value="<?= IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) ?>">
                            <?
                        } else {
                            foreach ($arResult["PERSON_TYPE"] as $v) {
                                ?>
                                <input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?= $v["ID"] ?>">11
                                <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $v["ID"] ?>">
                                <?
                            }
                        }
                    }
                    ?>

                    <? /* стандарный шаблон */ ?>



                    <div class="row">
                        <div class="col-xs-12">
<!--                            <form class="checkout__form">-->
                                <div class="checkout-form__head">

                                    <h4><?= GetMessage("ORDER_ADDRESS") ?></h4>

                                    <?
                                    $build_input = function ($A) {
                                        $id = $A['id'];
                                        $placeholder = $A['placeholder'];
                                        $value = $A['value'];
                                        $style = $A['style'] ? ' style="' . $A['style'] . '"' : '';
                                        $autocomplete = $A['autocomplete'] ? ' autocomplete="' . $A['autocomplete'] . '"' : '';
                                        if ($value == $placeholder) $value = '';
                                        ?><input type="text" data-placeholder="<?= $placeholder ?>"
                                                 class="<?= $A['class'] ?><?= $value ? '' : ' placeholdered' ?>"
                                                 name="<?= $id ?>"
                                                 id="<?= $id ?>" value="<?= $value ?: $placeholder ?>"<?= $style ?><?= $autocomplete ?>/><?
                                    };

                                    ?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="checkout__form_left">
                                                <?
                                                $value = 0;
                                                foreach ($arResult["ORDER_PROP"]["USER_PROPS_Y"][5]["VARIANTS"] as $arVariant) {
                                                    if ($arVariant["SELECTED"] == "Y") {
                                                        $value = $arVariant["ID"];
                                                        break;
                                                    }
                                                }
                                                $CURRENT_LOC_ID = $value;
                                                $GLOBALS["APPLICATION"]->IncludeComponent(
                                                    'bitrix:sale.ajax.locations',
                                                    'new_loc',
                                                    array(
                                                        "AJAX_CALL" => "N",
                                                        "COUNTRY_INPUT_NAME" => "COUNTRY_" . $arResult["ORDER_PROP"]["USER_PROPS_Y"][5]["FIELD_NAME"],
                                                        "CITY_INPUT_NAME" => $arResult["ORDER_PROP"]["USER_PROPS_Y"][5]["FIELD_NAME"],
                                                        "CITY_OUT_LOCATION" => "Y",
                                                        "LOCATION_VALUE" => $value,
                                                        "ONCITYCHANGE" => ($arResult["ORDER_PROP"]["USER_PROPS_Y"][5]["IS_LOCATION"] == "Y" || $arResult["ORDER_PROP"]["USER_PROPS_Y"][5]["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
                                                    ),
                                                    null,
                                                    array('HIDE_ICONS' => 'Y')
                                                );?>

                                                <div class="row">
                                                    <div class="col-xs-12">
													<span class="input input--haruki">
                                                        <?
                                                        $build_input(array(
                                                            'id' => 'ORDER_PROP_7',
                                                            'value' => $arResult["ORDER_PROP"]["USER_PROPS_Y"][7]["VALUE"],
                                                            'autocomplete' => "shipping street-address",
                                                            'class' => 'input__field input__field--haruki',
                                                        ));
                                                        ?>
														<label class="input__label input__label--haruki" for="ORDER_PROP_7">
															<span class="input__label-content input__label-content--haruki"><?= GetMessage("ORDER_ADDRESS")?></span>
														</label>
													</span>
                                                        <div class="row">
                                                            <div class="col-md-6">
															<span class="input input--haruki">
                                                                <?
                                                                $build_input(array(
                                                                    'id' => 'ORDER_PROP_4',
                                                                    'class' => 'input__field input__field--haruki',
                                                                    'value' => $arResult["ORDER_PROP"]["USER_PROPS_Y"][4]["VALUE"]
                                                                ));
                                                                ?>
																<label class="input__label input__label--haruki" for="ORDER_PROP_4">
																	<span class="input__label-content input__label-content--haruki">Индекс</span>
																</label>
															</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout__form_right">
											<span class="input input--haruki name__input">
                                                <?
                                                if ($arResult["ORDER_PROP"]["USER_PROPS_Y"][1]["VALUE"] == "Untitled") $arResult["ORDER_PROP"]["USER_PROPS_Y"][1]["VALUE"] = ""; // какая-то бага с этим свойством
                                                $build_input(array(
                                                    'id' => 'ORDER_PROP_1',
                                                    'value' => $arResult["ORDER_PROP"]["USER_PROPS_Y"][1]["VALUE"],
                                                    "autocomplete" => "name",
                                                    "class" => "input__field input__field--haruki"
                                                ));
                                                ?>
												<label class="input__label input__label--haruki" for="ORDER_PROP_1">
													<span class="input__label-content input__label-content--haruki"><?= GetMessage("ORDER_NAME")?></span>
												</label>
											</span>
                                                <span class="input input--haruki mail__input">
                                                    <?
                                                    $build_input(array(
                                                        'id' => 'ORDER_PROP_2',
                                                        'value' => $arResult["ORDER_PROP"]["USER_PROPS_Y"][2]["VALUE"],
                                                        "class" => "input__field input__field--haruki"
                                                    ));

                                                    ?>
												<label class="input__label input__label--haruki" for="ORDER_PROP_2">
													<span class="input__label-content input__label-content--haruki"><?= GetMessage("ORDER_EMAIL")?></span>
												</label>
											</span>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-9">
													<span class="input input--haruki number__input">
                                                        <?
                                                        $build_input(array(
                                                            'id' => 'ORDER_PROP_3',
                                                            'value' => $arResult["ORDER_PROP"]["USER_PROPS_Y"][3]["VALUE"],
                                                            "class" => "input__field input__field--haruki"
                                                        ));
                                                        ?>
														<label class="input__label input__label--haruki" for="ORDER_PROP_3">
															<span class="input__label-content input__label-content--haruki"><?= GetMessage("ORDER_PHONE")?></span>
														</label>
													</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-9">
                                                        <div class="select__wrap select__us">
                                                            <p class="form__label">Откуда узнали о нас?</p>
                                                            <select name="ORDER_PROP_20" id="ORDER_PROP_20">
                                                                <?
                                                                foreach ($arResult["ORDER_PROP"]["USER_PROPS_Y"][20]["VARIANTS"] as $key => $varian) {
                                                                    if (in_array($varian["NAME"], array("Vkontakte", "Yandex")) and LANGUAGE_ID == "en") continue;
                                                                    if (LANGUAGE_ID == "en") $varian["NAME"] = str_replace(array("От знакомых", "Баннерная реклама", "Другое", "Выберите из списка"), array("Friends", "Banner", "Other", "Please choose"), $varian["NAME"]);
                                                                    ?>
                                                                    <option
                                                                        value="<?= $varian["VALUE"] ?>" <? if ($varian["SELECTED"] == "Y") echo "selected"; ?>><?= $varian["NAME"]; ?></option>
                                                                    <?
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 other_text hidden">
															<span class="input input--haruki input--filled">
                                                                  <?
                                                                  $build_input(array(
                                                                      'id' => 'ORDER_PROP_46',
                                                                      'class' => 'input__field input__field--haruki',
                                                                      'value' => $arResult["ORDER_PROP"]["USER_PROPS_Y"][46]["VALUE"]
                                                                  ));
                                                                  ?>
                                                                <label class="input__label input__label--haruki" for="ORDER_PROP_46">
																	<span class="input__label-content input__label-content--haruki">Впишите свой вариант</span>
																</label>
															</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
																				<div class='col-md-9'>
																					<span class="input input--haruki name__input">
																						<?
																						if ($arResult["ORDER_PROP"]["USER_PROPS_Y"][1]["VALUE"] == "Untitled") $arResult["ORDER_PROP"]["USER_PROPS_Y"][1]["VALUE"] = ""; // какая-то бага с этим свойством
																						$build_input(array(
																								'id' => 'ORDER_DESCRIPTION',
																								'value' => "",
																								"autocomplete" => "name",
																								"class" => "input__field input__field--haruki"
																						));

																						?>
																						<label class="input__label input__label--haruki" for="ORDER_DESCRIPTION">
																							<span class="input__label-content input__label-content--haruki"><?= GetMessage("ORDER_COMMENT")?></span>
																						</label>
																					</span>
																				</div>
                                    </div>
                                </div>
                                <div class="checkout-form__foot">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="checkout__payment">
                                                <h4><?= GetMessage("ORDER_CHECKOUT") ?></h4>
                                                <?
                                                $first = true;
                                                foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
                                                    if ($arPaySystem["CHECKED"] == "Y" or $first == true) {
                                                        $first = false;
                                                        $id = $arPaySystem["ID"];
                                                    }
                                                }

                                                $first = true;
                                                $desc = "";
                                                foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
                                                    // Запоминаем текст описания платежки
                                                    if ($arPaySystem["CHECKED"] == "Y" or $first == true) {
                                                        $first = false;
                                                        $desc = $arPaySystem["DESCRIPTION"];
                                                        $id = $arPaySystem["ID"];
                                                        //if(count($arResult["PAY_SYSTEM"]) > 1) echo "<option>Выберите платежную систему</option>";
                                                    }
                                                    ?>
                                                    <div class="radio__wrap">
                                                        <input type="radio" id="payment-<?= $arPaySystem["ID"] ?>"
                                                               name="PAY_SYSTEM_ID"
                                                               onClick="submitForm();"
                                                               value="<?= $arPaySystem["ID"] ?>" <?= ($arPaySystem["CHECKED"] == "Y") ? 'checked' : '' ?>
                                                               data-desr="<?= $arPaySystem["DESCRIPTION"]?>"/>
                                                        <label for="payment-<?= $arPaySystem["ID"] ?>"><span
                                                                class="radio__btn"></span><span
                                                                class="radio__text"><?= $arPaySystem["NAME"]; ?></span></label>
                                                    </div>

                                                    <? $this->SetViewTarget('payment_description');?>
                                                        <p class="ottr-foot__text" id="paymentDescription"><?= $desc ?></p>
                                                    <? $this->EndViewTarget();?>

                                                <? } ?>
                                            </div>
                                            <div class="checkout__delivery">
                                                <h4>Способ доставки</h4>
                                                <?
                                                if (count($arResult["DELIVERY"]) == 0) {
                                                    echo '<p><center><?=GetMessage("ORDER_DELIVERY_INFO")?></center></p>';
                                                }
                                                foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery) {
                                                    if ($arDelivery['ID'] == 14 && $CURRENT_LOC_ID == 4379) {
                                                        continue;
                                                    }
                                                    if ($arDelivery['ID'] == 14 && $CURRENT_LOC_ID == 4334) {
                                                        continue;
                                                    }

                                                    if ($delivery_id !== 0 && intval($delivery_id) <= 0) {
                                                        foreach ($arDelivery["PROFILES"] as $profile_id => $arProfile) {

                                                            ?>
                                                            <div class="radio__wrap">
                                                                <input type="radio" id="ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>"
                                                                       name="<?= $arProfile["FIELD_NAME"] ?>"
                                                                       value="<?= $delivery_id . ":" . $profile_id; ?>" <?= $arProfile["CHECKED"] == "Y" ? "checked=\"checked\"" : ""; ?>
                                                                       onClick="submitForm();"/>
                                                                <label for="ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>">
                                                                    <span class="radio__btn"></span>
                                                                    <span class="radio__text"><?= nl2br($arDelivery["DESCRIPTION"]) ?><?= nl2br($arProfile["DESCRIPTION"]) ?>
                                                                        <br/>
                                                                        <?
                                                                        $APPLICATION->IncludeComponent('bitrix:sale.ajax.delivery.calculator', 'new_calc', array(
                                                                            "NO_AJAX" => $arParams["DELIVERY_NO_AJAX"],
                                                                            "DELIVERY" => $delivery_id,
                                                                            "PROFILE" => $profile_id,
                                                                            "ORDER_WEIGHT" => $arResult["ORDER_WEIGHT"],
                                                                            "ORDER_PRICE" => $arResult["ORDER_PRICE"],
                                                                            "LOCATION_TO" => $arResult["USER_VALS"]["DELIVERY_LOCATION"],
                                                                            //"LOCATION_ZIP" => $arResult["USER_VALS"]["DELIVERY_LOCATION_ZIP"],
                                                                            "CURRENCY" => $arResult["BASE_LANG_CURRENCY"],
                                                                        ), null, array('HIDE_ICONS' => 'Y'));
                                                                        ?>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                            <?
                                                        } // endforeach
                                                        ?>
                                                        <?
                                                    } else {
                                                        ?>
                                                        <div class="radio__wrap">
                                                            <input type="radio" id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
                                                                   name="<?= $arDelivery["FIELD_NAME"] ?>"
                                                                   value="<?= $arDelivery["ID"] ?>"<? if ($arDelivery["CHECKED"] == "Y") echo " checked"; ?>
                                                                   onclick="submitForm();">
                                                            <label for="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>">
                                                                <span class="radio__btn"></span>
                                                                <span class="radio__text"><?= $arDelivery["DESCRIPTION"] ?><br>
                                                                    <span class="radio__price"><?= $arDelivery["PRICE_FORMATED"] ?><span class="rub">₽</span></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <?
                                                    }
                                                } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="order_toggle__info checkout__total clearfix">
                                                <h4>Всего</h4>
                                                <div class="order_toggle__text clearfix">
                                                    <div class="order_toggle-text__left">
                                                        <p><?= GetMessage("ORDER_TOTAL_PRICE") ?></p>
                                                    </div>
                                                    <div class="order_toggle-text__right">
                                                        <p class="ottr__price"><?= $arResult["ORDER_PRICE_FORMATED"] ?><span class="rub">₽</span></p>
                                                    </div>
													<?if ($arResult["DELIVERY_PRICE_FORMATED"]):?>
														<div class="order_toggle-text__left">
															<p><?= GetMessage("ORDER_DELIVERY") ?></p>
														</div>
														<div class="order_toggle-text__right">
															<p class="ottr__price"><?= $arResult["DELIVERY_PRICE_FORMATED"] ?><span class="rub">₽</span></p>
														</div>
													<?endif?>

                                                    <?
                                                    if (doubleval($arResult["DISCOUNT_PRICE"]) > 0) { ?>
                                                        <div class="order_toggle-text__left">
                                                            <p><?= GetMessage("SOA_TEMPL_SUM_DISCOUNT") ?><? if (strLen($arResult["DISCOUNT_PERCENT_FORMATED"]) > 0):?> (<? echo $arResult["DISCOUNT_PERCENT_FORMATED"]; ?>)<?endif; ?></p>
                                                        </div>
                                                        <div class="order_toggle-text__right">
                                                            <p class="ottr__price color_red"><? echo $arResult["DISCOUNT_PRICE_FORMATED"] ?><span class="rub">₽</span></p>
                                                        </div>
                                                    <? } ?>

                                                </div>
                                                <div class="ottr__total">
                                                    <div class="ottr-total__left">
                                                        <p class="hidden-xs"><?= GetMessage("ORDER_TOTAL_TOTAL") ?></p>
                                                        <p class="hidden-lg hidden-md hidden-sm"><?= GetMessage("ORDER_TOTAL_TOTAL") ?></p>
                                                    </div>
                                                    <div class="ottr-total__right">
                                                        <p class="ottr__price"><?= $arResult["ORDER_TOTAL_PRICE_FORMATED"] ?><span class="rub">₽</span></p>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="ORDER_PROP_21" value="<?=$_SERVER['REMOTE_ADDR']?><?if($_SERVER['HTTP_X_FORWARDED_FOR'] != "" and $_SERVER['HTTP_X_FORWARDED_FOR'] != $_SERVER['REMOTE_ADDR']):?> - настоящий <?=$_SERVER['HTTP_X_FORWARDED_FOR']?><?endif;?>">
                                                <input type="button" class="btn btn_black ottr__btn" name="submitbutton"
                                                       onClick="submitForm('Y');"
                                                       value="<?= GetMessage("SOA_TEMPL_BUTTON") ?>">
                                                <span class="order-error-message" style="display:none;">Пожалуйста, заполните все поля</span>
                                                <div class="agreement">
                                                    <div class="check__wrap">
                                                        <input type="checkbox" id="agreement-01" name="checkbox" checked/>
                                                        <label for="agreement-01"><span class="check__btn"></span><span class="check__text">Соглашаюсь с политикой обработки персональных данных</span></label>
                                                    </div>
                                                </div>
                                                <p class="ottr-foot__text" id="paymentDescription"></p>
                                                <p class="ottr-foot__text">Мы свяжемся с вами для подтверждения заказа по указанному телефону или электронной почте в ближайшее время (с 9.00 до 18.00 в рабочие дни).</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
                                <input type="hidden" name="profile_change" id="profile_change" value="N">
<!--                            </form>-->
                                <input type="hidden" name="need_load_scripts" value="1" />

                            <? if (!empty($_REQUEST['need_load_scripts'])) { ?>
                                <script src="<?= SITE_TEMPLATE_PATH ?>/libs/TextInputEffects/js/classie.js"></script>
                                <script src="<?= SITE_TEMPLATE_PATH ?>/libs/jquery/jquery-1.11.2.min.js"></script>
                                <script src="<?= SITE_TEMPLATE_PATH ?>/libs/slick/slick.js"></script>
                                <script>

                                    // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim



                                    //selects
                                    $('select').each(function () {
                                        var $this = $(this), numberOfOptions = $(this).children('option').length;

                                        $this.addClass('sel-hidden');
                                        $this.wrap('<div class="sel"></div>');
                                        $this.after('<div class="sel-styled"></div>');

                                        var $styledsel = $this.next('div.sel-styled');

                                        var $list = $('<ul />', {
                                            'class': 'sel-options'
                                        }).insertAfter($styledsel);

                                        for (var i = 0; i < numberOfOptions; i++) {
                                            if (parseInt($this.children('option').eq(i).val()) == parseInt(<?= $_POST['COUNTRY_ORDER_PROP_5']?>)
                                                || $this.children('option').eq(i).attr('selected') === 'selected' ){

                                                $('<li />', {
                                                    text: $this.children('option').eq(i).text(),
                                                    rel: $this.children('option').eq(i).val(),
                                                    class: 'li_active',
                                                    onclick: $this.attr('onchange'),
                                                    value: $this.children('option').eq(i).val()
                                                }).appendTo($list);
                                                $styledsel.text($this.children('option').eq(i).text())
                                            } else {
                                                $('<li />', {
                                                    text: $this.children('option').eq(i).text(),
                                                    rel: $this.children('option').eq(i).val(),
                                                    onclick: $this.attr('onchange'),
                                                    value: $this.children('option').eq(i).val()
                                                }).appendTo($list);
                                            }
                                        }
                                    });

                                </script>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="form_new"></div>
<!--            <script src="--><?//= SITE_TEMPLATE_PATH?><!--/libs/TextInputEffects/js/classie.js"></script>-->
            <script>
                <!--
                var newform = document.createElement("FORM");
                newform.method = "POST";
                newform.action = "";
                newform.name = "<?=$FORM_NAME?>";
                newform.id = "ORDER_FORM_ID_NEW";
                newform.className = "checkout__form";
                var im = document.getElementById('order_form_id');
                document.getElementById("form_new").appendChild(newform);
                newform.appendChild(im);
                //-->

                // Дополнительное поле "откуда пришли"
                if (document.getElementById("ORDER_PROP_20").value == "other") {
                    document.getElementById("ORDER_PROP_46").style = "display: block";
                }

            </script>

            <?
        }
        ?>
    </div>
</div>

<? if (LANGUAGE_ID == 'en'): ?>
    <script>
        BX.addCustomEvent('onAjaxSuccess', function () {
            $("#ORDER_PROP_5 :nth-child(2)").attr("selected", "selected");
            $('#ORDER_PROP_5').trigger('change');
        });
    </script>
<? endif; ?>

<script src="<?= SITE_TEMPLATE_PATH ?>/libs/TextInputEffects/js/classie.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function(){
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function () {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(rtrim, '');
                };
            })();
        }

        function onInputFocus(ev) {
            console.log(this);
            classie.add(ev.target.parentNode, 'input--filled');
        }

        function onInputBlur(ev) {
            if (ev.target.value.trim() === '') {
                classie.remove(ev.target.parentNode, 'input--filled');
            }
        }

        [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
            // in case the input is already filled..
            if (inputEl.value.trim() !== '') {
                classie.add(inputEl.parentNode, 'input--filled');
            }

        });






    });
</script>