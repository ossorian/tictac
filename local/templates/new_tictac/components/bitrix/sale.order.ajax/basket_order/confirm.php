<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$ORDER_ARTICLE = array();
$dbBasketItems = CSaleBasket::GetList(
    array(
        "NAME" => "ASC",
        "ID" => "ASC"
    ),
    array(
        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID,
        "ORDER_ID" => $arResult["ORDER_ID"]
    ),
    false,
    false,
    array("ID",
        "CALLBACK_FUNC",
        "MODULE",
        "PRODUCT_ID",
        "QUANTITY",
        "DELAY",
        "CAN_BUY",
        "PRICE",
        "WEIGHT")
);

while ($arItems = $dbBasketItems->Fetch()) {
    $ORDER_ARTICLE[] = $arItems["PRODUCT_ID"];
}
?>
<?
if (!empty($arResult["ORDER"])) {

    try {
        $res = CSaleBasket::GetList(array(), array("ORDER_ID" => $arResult["ORDER_ID"]));
        $gaItems = "";

        while ($item = $res->Fetch()) {
            // product properties
            $props = CCatalogProduct::GetByIDEx($item['PRODUCT_ID']);

            // build category tree
            $category = '';
            if (isset($props['IBLOCK_ID']) && isset($props['IBLOCK_SECTION_ID'])) {
                $nav = CIBlockSection::GetNavChain(IntVal($props['IBLOCK_ID']), IntVal($props['IBLOCK_SECTION_ID']));
                if ($nav) {
                    while ($arNav = $nav->GetNext()) {
                        $category .= ' / ' . $arNav['NAME'];
                    }
                }
            }
            $category = ltrim($category, " /");

            // get SKU code
            $sku = $props['ID'];

            // escape
            $itemName = addslashes($item['NAME']);
            $category = addslashes($category);

            // prepare the product info
//            $gaItems .= "\n
//                    ga('ecommerce:addItem', {
//                      'id': '{$arResult['ORDER_ID']}',
//                      'name': '{$itemName}',
//                      'sku': '{$sku}',
//                      'category': '{$category}',
//                      'price': '{$item['PRICE']}',
//                      'quantity': '{$item['QUANTITY']}'
//                    });";
        }

        $location = array();
        $orderProps = CSaleOrderPropsValue::GetOrderProps($arResult["ORDER_ID"]);
        while ($arProps = $orderProps->Fetch()) {
            if ($arProps["TYPE"] == "LOCATION") {
                $location = CSaleLocation::GetByID($arProps["VALUE"], LANGUAGE_ID);
            }
        }

        // prepare the order info
        $order = $arResult["ORDER"];
        $orderTotal = $order['PRICE'];
//        $gaTrans = "\n
//                ga('ecommerce:addTransaction', {
//                  'id': '{$arResult['ORDER_ID']}',
//                  'affiliation': 'tictactoy.ru',
//                  'revenue': '{$orderTotal}',
//                  'shipping': '{$order['PRICE_DELIVERY']}',
//                  'tax': ''
//                });";
        ?>
        <script type="text/javascript">
//            $(document).ready(function () {
//                if (window.location.hash != "#completed") {
//                    window.location.hash = "completed";
//                    ga_trackOrder();
//                }
//                function ga_trackOrder() {
//                    ga('require', 'ecommerce', 'ecommerce.js');
//                    <?//=$gaTrans?>
<!--                    --><?//=$gaItems?>
//                    ga('ecommerce:send');
//                }
//
//                var data = {
//                    uuidProgram: 'e28d4ea4-9f56-11e6-80f5-76304dec7eb7',
//                    fio: '<?//=$arResult['ORDER']['USER_NAME']?>//',
//                    phone: '<?//=$arResult['ORDER']['USER_PHONE']?>//',
//                    email: '<?//=$arResult['ORDER']['USER_EMAIL']?>//',
//                    price: <?//=$arResult['ORDER']['PRICE']?>//,
//                    orderId: '<?//=$arResult['ORDER']['ID']?>//',
//                    webMaster: getCookie("webMaster"),
//                    ref: getCookie("reg")
//
//                };
//                sendToProactionone(data);
//            });
        </script>
        <? if (SITE_ID == 's2') { ?>
            <!-- Google Code for &#1047;&#1072;&#1082;&#1072;&#1079; Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 965024768;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "4xoICOfe71cQgLiUzAM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/965024768/?label=4xoICOfe71cQgLiUzAM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>
        <? } ?>
        <?

    } catch (Exception $myexception) {
    }


    ?>


    <?
// start retail rocket
    ?>
    <? if (RETAIL_ROCKET_ENABLED == true):?>
        <script type="text/javascript">
            function rrAsyncInit() {
                try {
                    rrApi.order({
                        transaction: <?=$arResult['ORDER_ID']?>,
                        <?if(intval($USER->GetID()) > 0):?>userId: <?=$USER->GetID()?>,<?endif;?>
                        items: [
                            <?
                            $res = CSaleBasket::GetList(array(), array("ORDER_ID" => $arResult["ORDER_ID"]));
                            $i = 0;
                            while ($item = $res->Fetch()) {
                            $i++;
                            if ($i > 1) echo ",";
                            ?>
                            {
                                id: <?=$item['PRODUCT_ID']?>,
                                qnt: <?=intval($item['QUANTITY'])?>,
                                price: <?=$item['PRICE']?>}
                            <?}?>
                        ]
                    });
                } catch (e) {
                }
            }
        </script>
    <?endif; ?>
    <?
// end retail rocket
    ?>


    <!-- Google Code for &#1055;&#1056;&#1054;&#1044;&#1040;&#1046;&#1040; Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1040533405;
        var google_conversion_label = "CaLnCO3_3VkQnY-V8AM";
        var google_conversion_value = 0.00;
        var google_conversion_currency = "RUB";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1040533405/?value=0.00&amp;currency_code=RUB&amp;label=CaLnCO3_3VkQnY-V8AM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
    <script src="<?= SITE_TEMPLATE_PATH ?>/libs/jquery/jquery-1.11.2.min.js"></script>
<?//= print_r($arResult);
$coupon = '';
$rsSales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), array('ID' => $_GET['ORDER_ID']), false, false, array('ID', 'NAME', 'BASKET_DISCOUNT_COUPON'));
while ($arSales = $rsSales->Fetch())
{
    $coupon = $arSales['BASKET_DISCOUNT_COUPON'];
}


    $arCart = array();
    $dbBasket = CSaleBasket::GetList(Array("ID"=>"ASC"), Array("ORDER_ID"=>$_GET["ORDER_ID"]));
    while ($arBasketItem = $dbBasket->GetNext()){

        $brand = '';
        $colors = '';
        $cat = '';


        $res = CIBlockElement::GetList(array(),array('ID' => $arBasketItem['PRODUCT_ID']), false, false, array('*'));
        while ($arEl = $res->GetNextElement()) {
            $arFields = $arEl->GetFields();
            $arProps = $arEl->GetProperties();
            $brand = $arProps['BRAND_MODEL']['VALUE_ENUM'];
            $colors = implode(',', $arProps['FILTER_COLOR']['VALUE_ENUM']);

            $nav = CIBlockSection::GetNavChain(false, $arFields['IBLOCK_SECTION_ID']);
            if ($arFirst = $nav->GetNext()) {
                if ($arFirst['ID'] == 172) {
                    $cat = 'часы';
                } else if ($arFirst['ID'] == 146) {
                    $cat = "ремешки";
                } else if ($arFirst['ID'] == 171) {
                    $cat = "украшения";
                } else if ($arFirst['ID'] == 165) {
                    $cat = "очки";
                }
            }
        }

        $product[]=$arBasketItem['PRODUCT_ID'];
        $prices[]=$arBasketItem['PRICE'];
        $arCart[] = array(
            'name'=> $arBasketItem['NAME'],
            'id'=> $arBasketItem['PRODUCT_ID'],
            'price'=> $arBasketItem['PRICE'],
            'brand'=> $brand,
            'category'=> $cat,
            'variant'=> $colors,
            'quantity'=> $arBasketItem['QUANTITY']
        );
    }

    ?>
    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/remarketing.php","PAGE_TYPE"=>"purchase","PRODUCT"=>$product,"PRICE"=>$prices), false);?>

    <?//new FB pixel ?>
    <!— Facebook Pixel Code —>
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f.fbq)f.fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1704759379844791');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=1704759379844791&ev=PageView&noscript=1"
        /></noscript>
    <!— End Facebook Pixel Code —>
    <?//------------------new FB pixel?>

    <script>
        dataLayer.push({
            'event': 'transaction',
            'ecommerce': {
                'currencyCode': 'RUB',
                'purchase': {
                    'actionField': {
                        'id': '<?= $_GET['ORDER_ID']?>',                         	// ID Транзакции
                        'affiliation': 'tictactoy.ru',         //Магазин или филиал, в котором произошла транзакция.
                        'revenue': '<?= $arResult['ORDER']['PRICE']?>',                     // Стоимость транзакции (включая налог и доставку)
                        'tax':'0',                                   //Сумма всех налогов, связанных с транзакцией.
                        'shipping': '<?= $arResult['ORDER']['PRICE_DELIVERY']?>',                       //Стоимость доставки, связанная с транзакцией.
                        'coupon': '<?= $coupon?>'        // Если пользователь ввел купон при оформлении товара
                    },
                    'products': <?= json_encode($arCart)?>
                }
            }
        });
        fbq('track', 'Purchase', {value: '<?= $arResult['ORDER']['PRICE']?>', currency: 'RUB'});
    </script>


    <div class="thank-you">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="thank-you__logo">
                        <div class="logo">
                            <a href="/" class="logo__link">
                                <i class="icon-logo"></i>
                            </a>
                        </div>
                    </div>

                    <div class="thank-you__order">                    
                        <? /* for cash only */ if ($arResult["ORDER"]["PAY_SYSTEM_ID"] == 1 && SITE_ID == 's1'): ?>
                            <h2><?= GetMessage("ORDER_CONFIRMPAGE_CASH_THANKS") ?></h2>
                            <p class="we-will-contact-you" style="margin-top:10px;"><?= GetMessage("ORDER_CONFIRMPAGE_CONTACT"); ?></p>
                            <p style="margin-top:30px;"><?= GetMessage("ORDER_CONFIRMPAGE_YOU_CAN_PAY_NOW"); ?>.</p>
                            <p style="color:red;margin-bottom:20px;"><?= GetMessage("ORDER_CONFIRMPAGE_CASH_CARD"); ?></p>
                            <iframe frameborder="0" allowtransparency="true" scrolling="no"
                                    src="https://money.yandex.ru/embed/shop.xml?account=410015254480664&quickpay=shop&payment-type-choice=on&writer=seller&targets=%D0%9E%D0%BF%D0%BB%D0%B0%D1%82%D0%B0+%D0%B7%D0%B0%D0%BA%D0%B0%D0%B7%D0%B0%20<?= intval($arResult["ORDER"]['ID']) ?>+%D0%B2+%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B5+TicTacToy.ru&targets-hint=&default-sum=<?= intval($arResult["ORDER"]['PRICE']) ?>&button-text=01&successURL=www.tictactoy.ru"
                                    width="450" height="222"></iframe>
                            <div style="color:red;margin-top:20px;">
                                Максимальная сумма оплаты через форму Яндекс.Касса составляет 15000 рублей.<br>
                                В случае, если стоимость вашего заказа более 15000 рублей, оплату необходимо совершить несколькими частями.
                            </div>
                        <? else: ?>
                            <h2><?= GetMessage("ORDER_CONFIRMPAGE_THANKS") ?></h2>
                            <?if($arResult["ORDER"]["PAY_SYSTEM_ID"] !=1 && $arResult['ORDER']['PRICE'] > 15000){?>
                                <p class="thank-you__desc"><?= GetMessage("ORDER_CONFIRMPAGE_YOU_CAN_PAY_NOW") ?></p>
                            <? }?>
                            <div class="payment__wrap">
                                <? if ($arResult["ORDER"]["PAY_SYSTEM_ID"] == 5): ?>
                                    <p><br><br>СУММА ПЕРЕВОДА:
                                        <b><?= $arResult['ORDER']['PRICE'] ?> <?= $arResult['ORDER']['CURRENCY'] ?></b></p>

                                    <p>Получатель: ВЛАДИМИР ВЛАДИМИРОВИЧ КОРКИН<br>
                                        Номер карты: 5469 5500 3421 0820</p>

                                    <p>Номер счета: 40817810855862636631<br>
                                        Банк получателя: СЕВЕРО-ЗАПАДНЫЙ БАНК ПАО СБЕРБАНК Г.САНКТ-ПЕТЕРБУРГ<br>
                                        БИК: 044030653<br>
                                        Корреспондентский счет: 30101810500000000653<br>
                                        КПП: 783502001<br>
                                        ИНН: 7707083893<br>
                                        ОКПО: 09171401<br>
                                        ОГРН: 1027700132195</p>

                                    <?
                                elseif ($arResult["ORDER"]["PAY_SYSTEM_ID"] != 1):?>
                                    <p><?= GetMessage("ORDER_CONFIRMPAGE_GO") ?></p>
                                <? endif; ?>
                                <?
                                if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0 && $arResult["ORDER"]["PAY_SYSTEM_ID"] != 5) // not sberbank
                                {
                                    if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y") {

                                        ?>
                                        <script language="JavaScript">
                                            window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?= $arResult["ORDER_ID"] ?>');
                                        </script>
                                        <form action="<?= $arParams["PATH_TO_PAYMENT"] ?>">
                                            <input type="hidden" name="ORDER_ID" value="<?= $arResult["ORDER_ID"] ?>"/>
                                            <input type="submit" class="submit"
                                                   value="<?= GetMessage("ORDER_CONFIRMPAGE_PAYMENT") ?>"/>
                                        </form>
                                        <?
                                    } else {
                                        if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]) > 0) {
                                            include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                                            if ($arResult['ORDER']['PRICE'] > 15000 && ($arResult['ORDER']["PAY_SYSTEM_ID"] == 8 || $arResult['ORDER']["PAY_SYSTEM_ID"] == 4)) { ?>
                                                <script>
                                                    if($('font.tablebodytext').length > 0){
                                                    $('font.tablebodytext').append('<div style="color:red">Максимальная сумма оплаты через форму Яндекс.Касса составляет 15000 рублей.<br>' +
                                                        'В случае, если стоимость вашего заказа более 15000 рублей, оплату необходимо совершить несколькими частями.<br><br><br></div>');
                                                    }
                                                </script>
                                            <? } ?>
                                            <script>
                                                if($('.payment__wrap').length > 0)
                                                    $('.payment__wrap').append('' +
                                                        '<div style="text-align:center;margin-top:10px;color:red;">Для оплаты банковской картой, пожалуйста,<br>выберите значок VISA/MASTERCARD.</div>');
                                            </script>
                                        <? }
                                    }
                                }
                                if($arResult["ORDER"]["PAY_SYSTEM_ID"] != 1 && $arResult['ORDER']['PRICE'] <= 15000){?>
                                    <script>
                                        if ($('.order_choose_card').length > 0)
                                            $('.order_choose_card').remove();
                                    </script>
                                <?}
                                ?>
                            </div>
                            <p class="we-will-contact-you"><?= GetMessage("ORDER_CONFIRMPAGE_CONTACT") ?></p>
                        </div>
                    <? endif;?>
                </div>
            </div>
        </div>
    </div>

    <? return true; ?>


    <b><?= GetMessage("SOA_TEMPL_ORDER_COMPLETE") ?></b><br/><br/>
    <table class="sale_order_full_table">
        <tr>
            <td>
                <?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER_ID"])) ?>
                <? /* = GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) */ ?>
            </td>
        </tr>
    </table>
    <?
    if (!empty($arResult["PAY_SYSTEM"])) {
        ?>
        <br/><br/>

        <table class="sale_order_full_table">
            <tr>
                <td>
                    <?= GetMessage("SOA_TEMPL_PAY") ?>: <?= $arResult["PAY_SYSTEM"]["NAME"] ?>
                </td>
            </tr>
            <?
            if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0) {
                ?>
                <tr>
                    <td>
                        <?
                        if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y") {
                            ?>
                            <script language="JavaScript">
                                window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?= $arResult["ORDER_ID"] ?>');
                            </script>
                            <?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . $arResult["ORDER_ID"])) ?>
                            <?
                        } else {
                            if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]) > 0) {
                                include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                            }
                        }
                        ?>
                    </td>
                </tr>
                <?
            }
            ?>
        </table>
        <?
    }
} else {
    ?>
    <b><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></b><br/><br/>

    <table class="sale_order_full_table">
        <tr>
            <td>
                <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ORDER_ID"])) ?>
                <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
            </td>
        </tr>
    </table>
    <?
}
?>
