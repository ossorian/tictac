<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$APPLICATION->SetAdditionalCSS("/css/filter.css");
$APPLICATION->SetAdditionalCSS("/css/jslider.css");
$APPLICATION->AddHeadScript('/js/filter.js');
$APPLICATION->AddHeadScript('/js/jshashtable-2.1_src.js');
$APPLICATION->AddHeadScript('/js/jquery.numberformatter-1.2.3.js');
$APPLICATION->AddHeadScript('/js/tmpl.js');
$APPLICATION->AddHeadScript('/js/jquery.dependClass-0.1.js');
$APPLICATION->AddHeadScript('/js/draggable-0.1.js');
$APPLICATION->AddHeadScript('/js/jquery.slider.js');
/*
	<link rel="stylesheet" href="/css/filter.css" type="text/css" />
	<link rel="stylesheet" href="/css/jslider.css" type="text/css" />
	<script type="text/javascript" src="/js/filter.js"></script>
	<script type="text/javascript" src="/js/jshashtable-2.1_src.js"></script>
	<script type="text/javascript" src="/js/jquery.numberformatter-1.2.3.js"></script>
	<script type="text/javascript" src="/js/tmpl.js"></script>
	<script type="text/javascript" src="/js/jquery.dependClass-0.1.js"></script>
	<script type="text/javascript" src="/js/draggable-0.1.js"></script>
	<script type="text/javascript" src="/js/jquery.slider.js"></script>
*/
print_r($arParams);
?>

<?
$min = intval($_GET["arrFilter_cf"]["1"]["LEFT"]);
$max = intval($_GET["arrFilter_cf"]["1"]["RIGHT"]);
if($max == 0 or $max < $min) $max = 100000;
?>
<div class="wrapper">
<? if ($arParams['FILTER_CURRENT_TEMPLATE'] == 1) { ?>
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/catalog/" method="get">
		<?/*foreach($arResult["ITEMS"] as $arItem):
			if(array_key_exists("HIDDEN", $arItem) and strpos($arItem["INPUT"], "SECTION_CODE") === false):
				echo $arItem["INPUT"];
			endif;
		endforeach;*/?>
		<input type="hidden" id="filterPriceMin" name="arrFilter_cf[1][LEFT]" value="<?=$min?>" />
		<input type="hidden" id="filterPriceMax" name="arrFilter_cf[1][RIGHT]" value="<?=$max?>" />
		<input type="hidden" name="arrFilter_cf[CATALOG_QUANTITY][LEFT]" value="1" />
		<input type="hidden" name="set_filter" value="Y" />

		<div class="form_filter">
			<div class="filter_box">
				<div class="filter_block floor_block">
					<?foreach($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_SEX")]["VALUE_LIST"] as $k => $v):?>
						<label class="<?=$k == GetMessage("IBLOCK_FILTER_ID_SEX_VALUE") ? "men" : "women"?><?if(in_array($k, $_GET["arrFilter_pf"]["SEX"])):?> checked<?endif;?>"><input type="checkbox" name="arrFilter_pf[SEX][]" value="<?=$k?>"<?if(in_array($k, $_GET["arrFilter_pf"]["SEX"])):?> checked="checked"<?endif;?>/><?=$v?></label>
					<?endforeach?>
				</div>
				<div class="filter_block mechanism_block">
					<span class="block_name"><?=GetMessage("IBLOCK_FILTER_NAME_MECHANISM")?></span>
					<?foreach($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_MECHANISM")]["VALUE_LIST"] as $k => $v):?>
						<label<?if(in_array($k, $_GET["arrFilter_pf"]["FILTER_MECHANISM"])):?> class="checked"<?endif;?>><input type="checkbox" name="arrFilter_pf[FILTER_MECHANISM][]" value="<?=$k?>"<?if(in_array($k, $_GET["arrFilter_pf"]["FILTER_MECHANISM"])):?> checked="checked"<?endif;?>/><?=$v?></label>
					<?endforeach?>
				</div>
				<div class="filter_block color_block">
					<span class="block_name"><?=GetMessage("IBLOCK_FILTER_NAME_COLOR")?></span>
					<?foreach($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_COLOR")]["VALUE_LIST"] as $k => $v):?>
						<label<?if(in_array($k, $_GET["arrFilter_pf"]["FILTER_COLOR"])):?> class="checked"<?endif;?>><input type="checkbox" name="arrFilter_pf[FILTER_COLOR][]" value="<?=$k?>"<?if(in_array($k, $_GET["arrFilter_pf"]["FILTER_COLOR"])):?> checked="checked"<?endif;?>/><?=$v?></label>
					<?endforeach?>
				</div>
				<div class="filter_block country_block">
					<span class="block_name"><?=GetMessage("IBLOCK_FILTER_NAME_COUNTRY")?></span>
					<?foreach($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_COUNTRY")]["VALUE_LIST"] as $k => $v):?>
						<label<?if(in_array($k, $_GET["arrFilter_pf"]["FILTER_COUNTRY"])):?> class="checked"<?endif;?>><input type="checkbox" name="arrFilter_pf[FILTER_COUNTRY][]" value="<?=$k?>"<?if(in_array($k, $_GET["arrFilter_pf"]["FILTER_COUNTRY"])):?> checked="checked"<?endif;?>/><?=$v?></label>
					<?endforeach?>
				</div>
				<div class="filter_toddler">
					<input name="set_filter" type="submit" value="<?=GetMessage("IBLOCK_FILTER_SEARCH")?>" />
					<div class="toddler_block">
						<input name="price" id="filterSlyderPrice" type="slider" value="<?=$min?>;<?=$max?>" />
					</div>
				</div>
			</div>
			<div class="style_block filter_block">
				<span class="block_name"><?=GetMessage("IBLOCK_FILTER_NAME_STYLE")?></span>
					<?foreach($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_STYLE")]["VALUE_LIST"] as $k => $v):?>
						<label<?if(in_array($k, $_GET["arrFilter_pf"]["FILTER_STYLE"])):?> class="checked"<?endif;?>><input type="checkbox" name="arrFilter_pf[FILTER_STYLE][]" value="<?=$k?>"<?if(in_array($k, $_GET["arrFilter_pf"]["FILTER_STYLE"])):?> checked="checked"<?endif;?>/><?=$v?></label>
					<?endforeach?>
			</div>
		</div><!-- .form_filter-->
		<div class="filter_button">
			<a href="#"><?=GetMessage("IBLOCK_FILTER_SEARCH_BUTTON")?></a>
		</div>
	</form>
<? } else if ($arParams['FILTER_CURRENT_TEMPLATE'] == 2) { ?>
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/catalog/" method="get">
		<p>Фильтр для ремешков</p>
	</form>
<? } else if ($arParams['FILTER_CURRENT_TEMPLATE'] == 3) { ?>
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/catalog/" method="get">
		<p>Фильтр для очков</p>
	</form>
<? } else if ($arParams['FILTER_CURRENT_TEMPLATE'] == 4) { ?>
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/catalog/" method="get">
		<p>Фильтр для украшений</p>
	</form>
<? }?>
</div><!-- .wrapper -->

<?
// Открываем фильтр если поиск был по фильтру
if(isset($_GET["arrFilter_cf"]["1"]["LEFT"])):?>
<script type="text/javascript">
	$(function() {
		$("div.filter_button a").trigger("click");
	});
</script>
<?endif;?>

<br />
