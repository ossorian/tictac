<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?//$this->SetViewTarget('catalog_filter');?>
<?
/*$APPLICATION->SetAdditionalCSS("/css/filter.css");
$APPLICATION->SetAdditionalCSS("/css/jslider.css");
$APPLICATION->AddHeadScript('/js/filter.js');
$APPLICATION->AddHeadScript('/js/jshashtable-2.1_src.js');
$APPLICATION->AddHeadScript('/js/jquery.numberformatter-1.2.3.js');
$APPLICATION->AddHeadScript('/js/tmpl.js');
$APPLICATION->AddHeadScript('/js/jquery.dependClass-0.1.js');
$APPLICATION->AddHeadScript('/js/draggable-0.1.js');
$APPLICATION->AddHeadScript('/js/jquery.slider.js');*/
/*
	<link rel="stylesheet" href="/css/filter.css" type="text/css" />
	<link rel="stylesheet" href="/css/jslider.css" type="text/css" />
	<script type="text/javascript" src="/js/filter.js"></script>
	<script type="text/javascript" src="/js/jshashtable-2.1_src.js"></script>
	<script type="text/javascript" src="/js/jquery.numberformatter-1.2.3.js"></script>
	<script type="text/javascript" src="/js/tmpl.js"></script>
	<script type="text/javascript" src="/js/jquery.dependClass-0.1.js"></script>
	<script type="text/javascript" src="/js/draggable-0.1.js"></script>
	<script type="text/javascript" src="/js/jquery.slider.js"></script>
*/

?>

<?



$min = intval($_GET["arrFilter_cf"]["1"]["LEFT"]);
//$max = intval($_GET["arrFilter_cf"]["1"]["RIGHT"]);
$max = 100000;
if ($max == 0 or $max < $min) $max = 100000;
?>

<div class="filter_toggle" id="filter_init">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p class="filter__title" id="filter_result">

                </p>

            </div>
        </div>
        <div class="row">
            <? if ($arParams['FILTER_CURRENT_TEMPLATE'] == 1) { ?>
            <form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="/catalog/" method="get" id="filter_form">
                <input type="hidden" name="arrFilter_cf[CATALOG_QUANTITY][LEFT]" value="1" />
                <input type="hidden" name="set_filter" value="Y" />
                <input type="hidden" name="PARENT_SECTION" value="172">

                <? if (!empty($arResult['FILTER_SECTIONS'])) { ?>
                    <div class="col-md-4 filter_brands_mobile">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Бренд</p>
                            <? foreach ($arResult['FILTER_SECTIONS'] as $k => $brand) { ?>
                                <label
                                    class="filter__item<? if (in_array($brand['ID'], $_GET["arrFilter_pf"]["FILTER_BRAND"])): ?> filter__item_active<? endif; ?>"><input
                                        type="checkbox" name="arrFilter_pf[FILTER_BRAND][]"
                                        value="<?= $brand['ID'] ?>"<? if (in_array($brand['ID'], $_GET["arrFilter_pf"]["FILTER_BRAND"])): ?> checked="checked"<? endif; ?>/><?= !empty($brand['BOLD']) ? '<b>' . $brand['NAME'] . '</b>' : $brand['NAME'] ?>
                                </label>
                            <? } ?>
                        </div>
                    </div>
                <? } ?>

                <div class="col-md-4">
                    <div class="filter__block clearfix">
                        <p class="filter__title">Пол</p>
                        <? foreach ($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_SEX")]["VALUE_LIST"] as $k => $v): ?>
                            <label class="filter__item
                        <? if (in_array($k, $_GET["arrFilter_pf"]["SEX"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[SEX][]"
                                       value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["SEX"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                            </label>
                        <? endforeach ?>
                    </div>
                    <div class="filter__block clearfix">
                        <p class="filter__title"><?= GetMessage("IBLOCK_FILTER_NAME_COUNTRY") ?></p>
                        <? foreach ($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_COUNTRY")]["VALUE_LIST"] as $k => $v): ?>
                            <label
                                class="filter__item<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_COUNTRY"])): ?> filter__item_active<? endif; ?>"><input
                                    type="checkbox" name="arrFilter_pf[FILTER_COUNTRY][]"
                                    value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_COUNTRY"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                            </label>
                        <? endforeach ?>
                    </div>
                    <div class="filter__block clearfix">
                        <p class="filter__title"><?= GetMessage("IBLOCK_FILTER_NAME_STYLE") ?></p>
                        <? foreach ($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_STYLE")]["VALUE_LIST"] as $k => $v): ?>
                            <label
                                class="filter__item<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_STYLE"])): ?> filter__item_active<? endif; ?>"><input
                                    type="checkbox" name="arrFilter_pf[FILTER_STYLE][]"
                                    value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_STYLE"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                            </label>
                        <? endforeach ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="filter__block clearfix">
                        <p class="filter__title"><?= GetMessage("IBLOCK_FILTER_NAME_COLOR") ?></p>
                        <? foreach ($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_COLOR")]["VALUE_LIST"] as $k => $v): ?>
                            <? if ($k !== 114 && $k !== 120 && $k !== 113) { ?>
                                <label
                                    class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_COLOR"])): ?> filter__item_active<? endif; ?>"><input
                                        type="checkbox" name="arrFilter_pf[FILTER_COLOR][]"
                                        value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_COLOR"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? } ?>
                        <? endforeach ?>
                    </div>
                    <div class="filter__block clearfix">
                        <p class="filter__title"><?= GetMessage("IBLOCK_FILTER_NAME_MECHANISM") ?></p>
                        <? foreach ($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_MECHANISM")]["VALUE_LIST"] as $k => $v): ?>
                            <label
                                class="filter__item<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_MECHANISM"])): ?> filter__item_active<? endif; ?>"><input
                                    type="checkbox" name="arrFilter_pf[FILTER_MECHANISM][]"
                                    value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_MECHANISM"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                            </label>
                        <? endforeach ?>
                        <label class="filter__item"><input type="checkbox" name="arrFilter_pf[FILTER_HRON][]" value="109">Хронограф</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="filter__block clearfix">
                        <p class="filter__title">Цена</p>
                        <div class="filter__price">
                            <div id="keypress"></div>
                            <div class="keypress-input__wrap">
									<span class="keypress-input__icon">
										<i class="icon-icon-small-close"></i>
									</span>
                                <input type="text" name="arrFilter_cf[1][LEFT]" class="keypress__input" id="input-with-keypress-0"
                                       value="<?= $min ?>" data-default-value="<?= $min ?>">
                            </div>
                            <span class="price__dash">—</span>
                            <div class="keypress-input__wrap">
									<span class="keypress-input__icon">
										<i class="icon-icon-small-close"></i>
									</span>
                                <input type="text" name="arrFilter_cf[1][RIGHT]" class="keypress__input" id="input-with-keypress-1"
                                       value=" <?= $max ?>" data-default-value="<?= $max ?>">
                            </div>
                        </div>
<!--                        <div class="filter-check__wrap">-->
<!--                            <input type="checkbox" id="discount-01" name="checkbox"/>-->
<!--                            <label for="discount-01"><span class="check__btn"></span><span class="check__text">Только со скидкой</span></label>-->
<!--                        </div>-->
                    </div>
                    <div class="filter__block clearfix">
                        <p class="filter__title">Ремешок</p>
                        <? foreach ($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_HRON")]["VALUE_LIST"] as $k => $v): ?>
                            <? if ($k == 109) { continue;}?>
                            <label
                                class="filter__item<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_HRON"])): ?> filter__item_active<? endif; ?>"><input
                                    type="checkbox" name="arrFilter_pf[FILTER_HRON][]"
                                    value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["FILTER_HRON"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                            </label>
                        <? endforeach ?>
                    </div>
                    <div class="filter__block clearfix">
                        <p class="filter__title">Стекло</p>
                        <? foreach ($arResult["arrProp"][130]["VALUE_LIST"] as $k => $v): ?>
                            <label
                                class="filter__item<? if (in_array($k, $_GET["arrFilter_pf"]["WATCH_GLASS"])): ?> filter__item_active<? endif; ?>"><input
                                    type="checkbox" name="arrFilter_pf[WATCH_GLASS][]"
                                    value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["WATCH_GLASS"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                            </label>
                        <? endforeach ?>

                    </div>
                    <div class="filter-check__wrap">
                        <input type="checkbox" id="calendar-01" name="arrFilter_pf[FILTER_COLOR][]" value="120"/>
                        <label for="calendar-01" class="label_first" id="calendar_checkbox">
                            <span class="check__btn"></span>
                            <span class="check__text">С календарём</span>
                        </label>
                        <div class="filter__br"></div>
                        <input type="checkbox" id="Illumination-01" name="arrFilter_pf[FILTER_COLOR][]" value="114"/>
                        <label for="Illumination-01" id="illumination_checkbox">
                            <span class="check__btn"></span>
                            <span class="check__text">С подсветкой</span>
                        </label>
                    </div>
                    <div class="filter__btns hidden-lg hidden-md">
                        <a href="#" class="btn btn_black filter__btn">Применить</a>
                        <a href="#" class="reset__btn">Сбросить</a>
                    </div>
                </div>
            </form>

            <? } else if ($arParams['FILTER_CURRENT_TEMPLATE'] == 2) { ?>
                <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/catalog/" method="get" id="filter_form">
                    <input type="hidden" name="arrFilter_cf[CATALOG_QUANTITY][LEFT]" value="1" />
                    <input type="hidden" name="set_filter" value="Y" />
                    <input type="hidden" name="PARENT_SECTION" value="146">

                    <? if (!empty($arResult['FILTER_SECTIONS'])) { ?>
                        <div class="col-md-4 filter_brands_mobile">
                            <div class="filter__block clearfix">
                                <p class="filter__title">Бренд</p>
                                <? foreach ($arResult['FILTER_SECTIONS'] as $k => $brand) { ?>
                                    <label
                                        class="filter__item<? if (in_array($brand['ID'], $_GET["arrFilter_pf"]["FILTER_BRAND"])): ?> filter__item_active<? endif; ?>"><input
                                            type="checkbox" name="arrFilter_pf[FILTER_BRAND][]"
                                            value="<?= $brand['ID'] ?>"<? if (in_array($brand['ID'], $_GET["arrFilter_pf"]["FILTER_BRAND"])): ?> checked="checked"<? endif; ?>/><?= $brand['NAME'] ?>
                                    </label>
                                <? } ?>
                            </div>
                        </div>
                    <? } ?>

                    <div class="col-md-4">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Материал</p>
                            <label class="filter__item
                                 <? if (in_array("Кожа", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[PROP_MATERIAL][]"
                                       value="Кожа"<? if (in_array("Кожа", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Кожа
                            </label>
                            <label class="filter__item
                                 <? if (in_array("металл", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[PROP_MATERIAL][]"
                                       value="металл"<? if (in_array("металл", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Металл
                            </label>
                            <label class="filter__item
                                 <? if (in_array("PU", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[PROP_MATERIAL][]"
                                       value="PU"<? if (in_array("PU", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> checked="checked"<? endif; ?>/>PU
                            </label>
                            <label class="filter__item
                                 <? if (in_array("Эко-кожа", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[PROP_MATERIAL][]"
                                       value="Эко-кожа"<? if (in_array("Эко-кожа", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Эко-кожа
                            </label>
                            <label class="filter__item
                                 <? if (in_array("Текстиль", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[PROP_MATERIAL][]"
                                       value="Текстиль"<? if (in_array("Текстиль", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Текстиль
                            </label>
                            <label class="filter__item
                                 <? if (in_array("Мех", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[PROP_MATERIAL][]"
                                       value="Мех"<? if (in_array("Мех", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Мех
                            </label>
                            <label class="filter__item
                                 <? if (in_array("Джинс", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[PROP_MATERIAL][]"
                                       value="Джинс"<? if (in_array("Джинс", $_GET["arrFilter_pf"]["PROP_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Джинс
                            </label>
                        </div>

                        <div class="filter__block clearfix">
                            <p class="filter__title">Тип</p>
                            <? foreach ($arResult["arrProp"][129]["VALUE_LIST"] as $k => $v): ?>
                                <label class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["TYPE_STRAP"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[TYPE_STRAP][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["TYPE_STRAP"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Ширина</p>
                            <? foreach ($arResult["arrProp"][131]["VALUE_LIST"] as $k => $v): ?>
                                <label class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["STRAP_WIDTH"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[STRAP_WIDTH][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["STRAP_WIDTH"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>

                        <div class="filter__block clearfix">
                            <p class="filter__title"><?= GetMessage("IBLOCK_FILTER_NAME_COLOR") ?></p>
                            <? foreach ($arResult["arrProp"][162]["VALUE_LIST"] as $k => $v): ?>
                                    <label
                                        class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["STRAP_COLOR"])): ?> filter__item_active<? endif; ?>"><input
                                            type="checkbox" name="arrFilter_pf[STRAP_COLOR][]"
                                            value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["STRAP_COLOR"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                    </label>
                            <? endforeach ?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Цена</p>
                            <div class="filter__price">
                                <div id="keypress"></div>
                                <div class="keypress-input__wrap">
									<span class="keypress-input__icon">
										<i class="icon-icon-small-close"></i>
									</span>
                                    <input type="text" name="arrFilter_cf[1][LEFT]" class="keypress__input" id="input-with-keypress-0"
                                           value="<?= $min ?>" data-default-value="<?= $min ?>">
                                </div>
                                <span class="price__dash">—</span>
                                <div class="keypress-input__wrap">
									<span class="keypress-input__icon">
										<i class="icon-icon-small-close"></i>
									</span>
                                    <input type="text" name="arrFilter_cf[1][RIGHT]" class="keypress__input" id="input-with-keypress-1"
                                           value=" <?= $max ?>" data-default-value="<?= $max ?>">
                                </div>
                            </div>
                            <!--                        <div class="filter-check__wrap">-->
                            <!--                            <input type="checkbox" id="discount-01" name="checkbox"/>-->
                            <!--                            <label for="discount-01"><span class="check__btn"></span><span class="check__text">Только со скидкой</span></label>-->
                            <!--                        </div>-->
                        </div>
                    </div>
                </form>
            <? } else if ($arParams['FILTER_CURRENT_TEMPLATE'] == 3) { ?>
                <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/catalog/" method="get" id="filter_form">
                    <input type="hidden" name="arrFilter_cf[CATALOG_QUANTITY][LEFT]" value="1" />
                    <input type="hidden" name="set_filter" value="Y" />
                    <input type="hidden" name="PARENT_SECTION" value="165">

                    <? if (!empty($arResult['FILTER_SECTIONS'])) { ?>
                        <div class="col-md-4 filter_brands_mobile">
                            <div class="filter__block clearfix">
                                <p class="filter__title">Бренд</p>
                                <? foreach ($arResult['FILTER_SECTIONS'] as $k => $brand) { ?>
                                    <label
                                        class="filter__item<? if (in_array($brand['ID'], $_GET["arrFilter_pf"]["FILTER_BRAND"])): ?> filter__item_active<? endif; ?>"><input
                                            type="checkbox" name="arrFilter_pf[FILTER_BRAND][]"
                                            value="<?= $brand['ID'] ?>"<? if (in_array($brand['ID'], $_GET["arrFilter_pf"]["FILTER_BRAND"])): ?> checked="checked"<? endif; ?>/><?= $brand['NAME'] ?>
                                    </label>
                                <? } ?>
                            </div>
                        </div>
                    <? } ?>


                    <div class="col-md-4">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Пол</p>
                            <? foreach ($arResult["arrProp"][GetMessage("IBLOCK_FILTER_ID_SEX")]["VALUE_LIST"] as $k => $v): ?>
                                <label class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["SEX"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[SEX][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["SEX"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>
                        <div class="filter__block clearfix">
                            <p class="filter__title">Материал оправы</p>
                            <label class="filter__item
                                 <? if (in_array("пластик", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[FRAME_MATERIAL][]"
                                       value="пластик"<? if (in_array("пластик", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Пластик
                            </label>
                            <label class="filter__item
                                 <? if (in_array("металл", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[FRAME_MATERIAL][]"
                                       value="металл"<? if (in_array("металл", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Металл
                            </label>
                            <label class="filter__item
                                 <? if (in_array("поликарбонат", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[FRAME_MATERIAL][]"
                                       value="поликарбонат"<? if (in_array("поликарбонат", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Поликарбонат
                            </label>
                            <label class="filter__item
                                 <? if (in_array("сталь", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[FRAME_MATERIAL][]"
                                       value="сталь"<? if (in_array("сталь", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Сталь
                            </label>
                            <label class="filter__item
                                 <? if (in_array("ацетат", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[FRAME_MATERIAL][]"
                                       value="ацетат"<? if (in_array("ацетат", $_GET["arrFilter_pf"]["FRAME_MATERIAL"])): ?> checked="checked"<? endif; ?>/>Ацетат
                            </label>
                        </div>
                    </div>


                    <div class="col-md-4">

                        <div class="filter__block clearfix">
                            <p class="filter__title">Цвет оправы</p>
                            <? foreach ($arResult["arrProp"][156]["VALUE_LIST"] as $k => $v): ?>
                                <label class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["PROP_COLOR_OF_FRAME"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[PROP_COLOR_OF_FRAME][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["PROP_COLOR_OF_FRAME"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>

                        <div class="filter__block clearfix">
                            <p class="filter__title">Цвет линз</p>
                            <? foreach ($arResult["arrProp"][126]["VALUE_LIST"] as $k => $v): ?>
                                <label class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["PROP_LINSES_COLOR"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[PROP_LINSES_COLOR][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["PROP_LINSES_COLOR"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>

                    </div>


                    <div class="col-md-4">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Цена</p>
                            <div class="filter__price">
                                <div id="keypress"></div>
                                <div class="keypress-input__wrap">
									<span class="keypress-input__icon">
										<i class="icon-icon-small-close"></i>
									</span>
                                    <input type="text" name="arrFilter_cf[1][LEFT]" class="keypress__input" id="input-with-keypress-0"
                                           value="<?= $min ?>" data-default-value="<?= $min ?>">
                                </div>
                                <span class="price__dash">—</span>
                                <div class="keypress-input__wrap">
									<span class="keypress-input__icon">
										<i class="icon-icon-small-close"></i>
									</span>
                                    <input type="text" name="arrFilter_cf[1][RIGHT]" class="keypress__input" id="input-with-keypress-1"
                                           value=" <?= $max ?>" data-default-value="<?= $max ?>">
                                </div>
                            </div>
                            <!--                        <div class="filter-check__wrap">-->
                            <!--                            <input type="checkbox" id="discount-01" name="checkbox"/>-->
                            <!--                            <label for="discount-01"><span class="check__btn"></span><span class="check__text">Только со скидкой</span></label>-->
                            <!--                        </div>-->
                        </div>
                    </div>
                </form>
            <? } else if ($arParams['FILTER_CURRENT_TEMPLATE'] == 4) { ?>
                <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="/catalog/" method="get" id="filter_form">
                    <input type="hidden" name="arrFilter_cf[CATALOG_QUANTITY][LEFT]" value="1" />
                    <input type="hidden" name="set_filter" value="Y" />
                    <input type="hidden" name="PARENT_SECTION" value="171">

                    <? if (!empty($arResult['FILTER_SECTIONS'])) { ?>
                        <div class="col-md-4 filter_brands_mobile">
                            <div class="filter__block clearfix">
                                <p class="filter__title">Бренд</p>
                                <? foreach ($arResult['FILTER_SECTIONS'] as $k => $brand) { ?>
                                    <label
                                        class="filter__item<? if (in_array($brand['ID'], $_GET["arrFilter_pf"]["FILTER_BRAND"])): ?> filter__item_active<? endif; ?>"><input
                                            type="checkbox" name="arrFilter_pf[FILTER_BRAND][]"
                                            value="<?= $brand['ID'] ?>"<? if (in_array($brand['ID'], $_GET["arrFilter_pf"]["FILTER_BRAND"])): ?> checked="checked"<? endif; ?>/><?= $brand['NAME'] ?>
                                    </label>
                                <? } ?>
                            </div>
                        </div>
                    <? } ?>

                    <div class="col-md-4">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Вид</p>
                            <? foreach ($arResult["arrProp"][157]["VALUE_LIST"] as $k => $v): ?>
                                <label class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["TYPE_OF_ACCESSORY"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[TYPE_OF_ACCESSORY][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["TYPE_OF_ACCESSORY"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>
                        <div class="filter__block clearfix">
                            <p class="filter__title">Размер колец</p>
                            <? foreach ($arResult["arrProp"][158]["VALUE_LIST"] as $k => $v): ?>
                                <label class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["SIZE_OF_RING"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[SIZE_OF_RING][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["SIZE_OF_RING"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Материал</p>
                            <? foreach ($arResult["arrProp"][159]["VALUE_LIST"] as $k => $v): ?>
                                <label class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["MATERIAL_OF_ACCESSORY"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[MATERIAL_OF_ACCESSORY][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["MATERIAL_OF_ACCESSORY"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>
                        <div class="filter__block clearfix">
                            <p class="filter__title">Цвет</p>
                            <? foreach ($arResult["arrProp"][164]["VALUE_LIST"] as $k => $v): ?>
                                <label
                                    class="filter__item <? if (in_array($k, $_GET["arrFilter_pf"]["ACCESSORY_COLOR"])): ?> filter__item_active<? endif; ?>">
                                    <input type="checkbox" name="arrFilter_pf[ACCESSORY_COLOR][]"
                                           value="<?= $k ?>"<? if (in_array($k, $_GET["arrFilter_pf"]["ACCESSORY_COLOR"])): ?> checked="checked"<? endif; ?>/><?= $v ?>
                                </label>
                            <? endforeach ?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="filter__block clearfix">
                            <p class="filter__title">Цена</p>
                            <div class="filter__price">
                                <div id="keypress"></div>
                                <div class="keypress-input__wrap">
									<span class="keypress-input__icon">
										<i class="icon-icon-small-close"></i>
									</span>
                                    <input type="text" name="arrFilter_cf[1][LEFT]" class="keypress__input" id="input-with-keypress-0"
                                           value="<?= $min ?>" data-default-value="<?= $min ?>">
                                </div>
                                <span class="price__dash">—</span>
                                <div class="keypress-input__wrap">
									<span class="keypress-input__icon">
										<i class="icon-icon-small-close"></i>
									</span>
                                    <input type="text" name="arrFilter_cf[1][RIGHT]" class="keypress__input" id="input-with-keypress-1"
                                           value=" <?= $max ?>" data-default-value="<?= $max ?>">
                                </div>
                            </div>
                        </div>
                        <div class="filter__block clearfix">
                            <p class="filter__title">Вставка</p>
                            <label class="filter__item <? if (!empty($_GET["arrFilter_pf"]["ACCESSORY_INSERTION_VALUE"])): ?> filter__item_active<? endif; ?>">
                                <input type="checkbox" name="arrFilter_pf[ACCESSORY_INSERTION_VALUE]"
                                       value="Y"<? if (!empty($_GET["arrFilter_pf"]["ACCESSORY_INSERTION_VALUE"])): ?> checked="checked"<? endif; ?>/>С камнями
                            </label>
                        </div>
                    </div>
                </form>
            <? }?>

        </div>
    </div>
</div>

<?
// Открываем фильтр если поиск был по фильтру
if (isset($_GET["arrFilter_cf"]["1"]["LEFT"])):?>
    <script type="text/javascript">
       /* $(function () {
            //$("div.filter_button a").trigger("click");
        });*/
    </script>
<? endif; ?>

<br/>

<script>
    BX.ready(function () {
        // PRICE SLIDER
        var keypressSlider = document.getElementById('keypress');
        var input0 = document.getElementById('input-with-keypress-0');
        var input1 = document.getElementById('input-with-keypress-1');
        var inputs = [input0, input1];

        noUiSlider.create(keypressSlider, {
            start: [<?= $min?>, <?= $max?>],
            connect: true,
            tooltips: false,
            range: {
                'min': 0,
                'max': 100000
            }
        });

        function setSliderHandle(i, value) {
            var r = [null, null];
            r[i] = value;
            keypressSlider.noUiSlider.set(r);

            var form = $('#filter_form').serialize();
            var sectionId = $('#section_id').attr('data-id');

            if ($('div.filter_brands_mobile').css('display') !== 'none') {
                sectionId = 0;
            }

            $.ajax({
                type: "GET",
                url: "/ajax/ajax_filter.php",
                data: form + '&section_id=' + sectionId,
                success: function (resp) {
                    $('#filter_result').html(resp);
                }
            });
        }

        function resetSlider() {
            var r = [<?= $min?>, <?= $max?>];
            keypressSlider.noUiSlider.set(r);
        }

        //reset filter
        $(document).on('click', '#reset_filter_btn', function () {
            $('#filter_form')[0].reset();
            $('#filter_form label.filter__item').each(function () {
                $(this).removeClass('filter__item_active');
                $(this).find('input[type=checkbox]').removeAttr('checked');
            });
            resetSlider();
            setSliderHandle();
        });

        //reset price slider
        $(document).on('click', '#filter_form i.icon-icon-small-close', function () {
            var new_val = $(this).parent().parent().find('input').attr('data-default-value');
            $(this).parent().parent().find('input').val(new_val);
            
            var r = [$('#input-with-keypress-0').val(), $('#input-with-keypress-1').val()];
            keypressSlider.noUiSlider.set(r);
            
            setSliderHandle();
        });

        // Listen to keydown events on the input field.
        inputs.forEach(function (input, handle) {

            input.addEventListener('change', function () {
                setSliderHandle(handle, this.value);
            });

            input.addEventListener('keydown', function (e) {

                var values = keypressSlider.noUiSlider.get();
                var value = Number(values[handle]);

                // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
                var steps = keypressSlider.noUiSlider.steps();

                // [down, up]
                var step = steps[handle];

                var position;

                // 13 is enter,
                // 38 is key up,
                // 40 is key down.
                switch (e.which) {

                    case 13:
                        setSliderHandle(handle, this.value);
                        break;

                    case 38:

                        // Get step to go increase slider value (up)
                        position = step[1];

                        // false = no step is set
                        if (position === false) {
                            position = 1;
                        }

                        // null = edge of slider
                        if (position !== null) {
                            setSliderHandle(handle, value + position);
                        }

                        break;

                    case 40:

                        position = step[0];

                        if (position === false) {
                            position = 1;
                        }

                        if (position !== null) {
                            setSliderHandle(handle, value - position);
                        }

                        break;
                }
            });
        });

        keypressSlider.noUiSlider.on('update', function (values, handle) {
            inputs[handle].value = Math.round(values[handle]);
        });

        keypressSlider.onmouseup = function () {
            var form = $('#filter_form').serialize();
            var sectionId = $('#section_id').attr('data-id');

            if ($('div.filter_brands_mobile').css('display') !== 'none') {
                sectionId = 0;
            }


            $.ajax({
                type: "GET",
                url: "/ajax/ajax_filter.php",
                data: form + '&section_id=' + sectionId,
                success: function (resp) {
                    $('#filter_result').html(resp);
                }
            });
        };

        $(document).on('click', '#calendar-01, #Illumination-01', function () {
            var form = $('#filter_form').serialize();
            var sectionId = $('#section_id').attr('data-id');

            if ($('div.filter_brands_mobile').css('display') !== 'none') {
                sectionId = 0;
            }

            $.ajax({
                type: "GET",
                url: "/ajax/ajax_filter.php",
                data: form + '&section_id=' + sectionId,
                success: function (resp) {
                    $('#filter_result').html(resp);
                }
            });
        });

    });
</script>

<?//$this->EndViewTarget();?>