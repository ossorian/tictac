<?php

$section_id = 0;
if (!empty($arParams['FILTER_CURRENT_TEMPLATE'])){
    switch (intval($arParams['FILTER_CURRENT_TEMPLATE'])){
        case 1:
            $section_id = 172;
            break;
        case 2:
            $section_id = 146;
            break;
        case 3:
            $section_id = 165;
            break;
        case 4:
            $section_id = 171;
            break;
    }

    $arSectionsCache = array();
    $arSections = array();

    if (!empty($section_id)){

        $cache = new CPHPCache();
        $cache_time = 3600;
        $cache_id = 'catalog_main_filter_1235';
        if($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, false))
        {
            $arSectionsCache = $cache->GetVars();
        }
        elseif($cache->StartDataCache())
        {
            $rsSection = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => 5, 'SECTION_ID' => $section_id,'DEPTH_LEVEL' => 2, 'ACTIVE' => 'Y'), false, array('ID', 'NAME', 'UF_BRAND_BOLD'));
            while ($arSection = $rsSection->GetNext()) {
                $arSectionsCache[] = $arSection;
            }

            $cache->EndDataCache($arSectionsCache);
        }

        foreach ($arSectionsCache as $arSection) {

            if ($arSection['ID'] == 65 ){
                continue;
            }

            $arSections[] = array(
                'ID' => $arSection['ID'],
                'NAME' => $arSection['NAME'],
                'BOLD' => $arSection['UF_BRAND_BOLD'],
            );
        }
    }


    $arResult['FILTER_SECTIONS'] = $arSections;
}


?>