<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>


<?if($USER->IsAuthorized()):?>


<?else:?>
<?
if (count($arResult["ERRORS"]) > 0):
	foreach ($arResult["ERRORS"] as $key => $error)
		if (intval($key) == 0 && $key !== 0)
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
	echo '<div class="auth__input" id="auth_errors_mob" style="margin: 0px 0 25px 0;">';
	ShowError(implode("<br />", $arResult["ERRORS"]));
	echo '</div>';
elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>

<?endif?>
	
<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data" class="auth__form">
<?
if($arResult["BACKURL"] <> ''):
?>
<!--	<input type="hidden" name="backurl" value="--><?//=$arResult["BACKURL"]?><!--" />-->
<?
endif;
?>

<?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
	<?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true):?>
		<tr>
			<td><?echo GetMessage("main_profile_time_zones_auto")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?></td>
			<td>
				<select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
					<option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
					<option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
					<option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td><?echo GetMessage("main_profile_time_zones_zones")?></td>
			<td>
				<select name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
		<?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
					<option value="<?=htmlspecialcharsbx($tz)?>"<?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>><?=htmlspecialcharsbx($tz_name)?></option>
		<?endforeach?>
				</select>
			</td>
		</tr>
	<?else:
	switch ($FIELD)
	{
		case "EMAIL":
		?>
		<div class="auth__input">
			<span class="input input--haruki">
				<input class="input__field input__field--haruki" type="text"
					   id="user_email_mob" value="<?= $_POST["REGISTER"][$FIELD]?>" name="REGISTER[<?=$FIELD?>]"/>
				<label class="input__label input__label--haruki" for="user_email">
					<span class="input__label-content input__label-content--haruki">Электронная почта</span>
				</label>
			</span>
		</div>
		<?
		break;
		default: ?>

			<?
	}?>
				
	<?endif?>
<?endforeach?>
	<?$password = randString(7);?>
	<input type="hidden" name="REGISTER[PASSWORD]" value="<?=$password?>">
	<input type="hidden" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$password?>">
	<input type="hidden" name="REGISTER[LOGIN]" id="USER_LOGIN_mob" value="">
	<div class="auth-btn__wrap clearfix">
		<input type="submit" onclick="beforeReg()" class="btn btn_black" value="Зарегистрироваться" name="register_submit_button">
	</div>

<!--	<div class="auth__soc">-->
<!--		<p>Или с помощью</p>-->
<!--		<a href="#" class="soc__icon vk__icon" id="vk_reg_btn"><i class="icon-icon-vk"></i></a>-->
<!--		<a href="#" class="soc__icon fb__icon" id="fb_reg_btn"><i class="icon-icon-fb"></i></a>-->
<!--	</div>-->

</form>
<?endif?>

<script>
	BX.ready(function () {
		[].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
			// in case the input is already filled..
			if (inputEl.value.trim() !== '') {
				classie.add(inputEl.parentNode, 'input--filled');
			}
		});
	})
</script>