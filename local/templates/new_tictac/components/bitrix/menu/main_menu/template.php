<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$this->setFrameMode(true);
?>
<? if (!empty($arResult)) {
    $iii = 0;
    $count = count($arResult);

    foreach ($arResult as $arItem) { ?>

        <? if ($arItem['DEPTH_LEVEL'] == 1) { ?>
            <? if ($arItem['LINK'] === "/catalog/watches/") { ?>
                <?
                $arBrands = array();
                $arFilter = Array('IBLOCK_ID' => 5, 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', 'SECTION_ID' => 172, '!ID' => 65);
                $db_list = CIBlockSection::GetList(Array("sort" => "asc"), $arFilter, false, array('ID', 'NAME', 'UF_*', 'SECTION_PAGE_URL'));
                while ($ar_result = $db_list->GetNext()) {
                    $arBrands[] = $ar_result;
                }
                ?>
                <li class="nav__item nav__item__dd">
                    <a href="<?= $arItem['LINK'] ?>" class="nav__link dropdown_toggle" title="<?echo $arItem['TEXT'];?>"><?= $arItem['TEXT'] ?></a>
                    <div class="dropdown dropdown-clocks">
                        <div class="dd-brends clearfix">
                            <?
                                $brandsCount = count($arBrands);
                                $brandsInRow = ceil($brandsCount / 7);
                                $brandsI = 0;
                            ?>
                            <? $i = 0 ?>
                            <? foreach ($arBrands as $arBrand) { ?>
                                <? if ($brandsI == 0 || $brandsI % $brandsInRow == 0) { ?>
                                    <div class="col-2">
                                    <div class="text_center">
                                    <ul class="dropdown__nav">
                                <? } ?>

                                <li  <?= empty($i) ? 'class="brend_active"' : '' ?>>
                                    <a href="<?= $arBrand['SECTION_PAGE_URL'] ?>"
                                       class="link<?= !empty($arBrand['UF_BRAND_BOLD']) ? '_bold' : '' ?> watch_dynamic_link"
                                       data-id="<?= $arBrand['ID'] ?>">
                                        <?= $arBrand['NAME'] ?>
                                    </a>

                                    <div class="dd-clocks__wrap">

                                        <div class="row dynamic_content_watch" data-role="<?= $arBrand['ID'] ?>">
                                            <?
                                            $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IMAGE", "PROPERTY_BRAND");
                                            $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $arBrand['ID'], ">=PROPERTY_BRAND" => 1);
                                            $res = CIBlockElement::GetList(Array("PROPERTY_BRAND" => "ASC"), $arFilter, false, Array("nPageSize" => 6), $arSelect);
                                            while ($ob = $res->GetNext()) {
                                                $picture = !empty($ob['PROPERTY_IMAGE_VALUE']) ? resizeImage($ob['PROPERTY_IMAGE_VALUE'], 152, 174, 1) : resizeImage($ob['PREVIEW_PICTURE'], 152, 174, 1);
                                                ?>

                                                <div class="col-md-2">
                                                    <div class="dropdown__item">
                                                        <div class="dd-item-img__wrap">
                                                            <a href="<?= $arBrand['SECTION_PAGE_URL'] ?>">
                                                                <img src="<?= $picture['SRC'] ?>" class="dd-item__img"
                                                                     alt="">
                                                            </a>
                                                        </div>
                                                        <a href="<?= $arBrand['SECTION_PAGE_URL'] ?>">
                                                            <p class="dd-item__name"><?= $ob['NAME'] ?></p>
                                                        </a>
                                                    </div>
                                                </div>
                                            <? } ?>
                                        </div>

                                    </div>

                                </li>

                                <? if ($brandsI == ($brandsCount - 1) || $brandsI % $brandsInRow == ($brandsInRow - 1)) { ?>
                                    </ul>
                                    </div>
                                    </div>
                                <? } ?>
                                <? $brandsI++; ?>
                                <? $i++ ?>
                            <? } ?>
                        </div>
                    </div>
                </li>


            <? } else if ($arItem['LINK'] === "/test") { ?>
                <li class="nav__item">
                    <a href="<?= $arItem['LINK'] ?>"
                       class="nav__link  nav__link_red">
                        <?= $arItem['TEXT'] ?>
                    </a>

                </li>						
            <? } else if ($arItem['LINK'] === "/catalog/straps/") { ?>
                <li class="nav__item nav__item__dd">
                    <a href="<?= $arItem['LINK'] ?>" class="nav__link dropdown_toggle" title="<?echo $arItem['TEXT'];?>"><?= $arItem['TEXT'] ?></a>
                    <div class="dropdown dropdown-thongs">
                        <div class="dd-thongs__wrap">
                            <div class="row">
                                <?
                                $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IMAGE", "PROPERTY_BRAND");
                                $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", ">=PROPERTY_BRAND" => 1, "SECTION_ID" => 146);
                                $res = CIBlockElement::GetList(Array("PROPERTY_BRAND" => "ASC"), $arFilter, false, Array("nPageSize" => 6), $arSelect);
                                while ($ob = $res->GetNext()) {
                                    $picture = !empty($ob['PROPERTY_IMAGE_VALUE']) ? resizeImage($ob['PROPERTY_IMAGE_VALUE'], 152, 170, 1) : resizeImage($ob['PREVIEW_PICTURE'], 152, 170, 1);

                                    ?>

                                    <div class="col-md-2">
                                        <div class="dropdown__item">
                                            <div class="dd-item-img__wrap">
                                                <a href="<?= $arItem['LINK'] ?>">
                                                    <img src="<?= $picture['SRC'] ?>" class="dd-item__img" alt="">
                                                </a>
                                            </div>
                                            <a href="<?= $arItem['LINK'] ?>">
                                                <p class="dd-item__name"><?= $ob['NAME'] ?></p>
                                            </a>
                                        </div>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </li>
            <? } else if ($arItem['LINK'] === "/catalog/sunglasses/") { ?>
                <?
                $arSunglasses = array();
                $arFilter = Array('IBLOCK_ID' => 5, 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', 'SECTION_ID' => 165);
                $db_list = CIBlockSection::GetList(Array("sort" => "asc"), $arFilter, false, array('ID', 'NAME', 'UF_*', 'SECTION_PAGE_URL'));
                while ($ar_result = $db_list->GetNext()) {
                    $arSunglasses[] = $ar_result;
                }
                ?>
                <li class="nav__item nav__item__dd">
                    <a href="<?= $arItem['LINK'] ?>" class="nav__link dropdown_toggle" title="<?echo $arItem['TEXT'];?>"><?= $arItem['TEXT'] ?></a>
                    <div class="dropdown dropdown-goggles">
                        <div class="dd-goggles__wrap">
                            <? $i = 0; ?>
                            <? foreach ($arSunglasses as $arGlass) { ?>
                                <div
                                    class="row dynamic_content_sunglasses" <?= !empty($i) ? 'style="display: none;"' : '' ?>
                                    data-role="<?= $arGlass['ID'] ?>">
                                    <?
                                    $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IMAGE", "PROPERTY_BRAND");
                                    $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $arGlass['ID'], ">=PROPERTY_BRAND_VALUE" => 1);
                                    $res = CIBlockElement::GetList(Array("PROPERTY_BRAND" => "ASC"), $arFilter, false, Array("nPageSize" => 6), $arSelect);
                                    while ($ob = $res->GetNext()) {
                                        $picture = !empty($ob['PROPERTY_IMAGE_VALUE']) ? resizeImage($ob['PROPERTY_IMAGE_VALUE'], 152, 95, 1) : resizeImage($ob['PREVIEW_PICTURE'], 152, 95, 1);
                                        ?>
                                        <div class="col-md-2">
                                            <div class="dropdown__item">
                                                <div class="dd-item-img__wrap">
                                                    <a href="<?= $arGlass['SECTION_PAGE_URL'] ?>">
                                                        <img src="<?= $picture['SRC'] ?>" class="dd-item__img" alt="">
                                                    </a>
                                                </div>
                                                <a href="<?= $arGlass['SECTION_PAGE_URL'] ?>">
                                                    <p class="dd-item__name"><?= $ob['NAME'] ?></p>
                                                </a>
                                            </div>
                                        </div>
                                    <? } ?>
                                </div>
                                <? $i++; ?>
                            <? } ?>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="goggles__nav">
                                    <?
                                    foreach ($arSunglasses as $ar_result) {																				
                                        ?>
                                        <li>
                                            <a href="<?= $ar_result['SECTION_PAGE_URL'] ?>"
                                               class="link<?= !empty($ar_result['UF_BRAND_BOLD']) ? '_bold' : '' ?> sunglasses_dynamic_link"
                                               data-id="<?= $ar_result['ID'] ?>">
                                                <?= $ar_result['NAME'] ?>
                                            </a>
                                        </li>
                                        <?
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            <? } else { ?>
                <li class="nav__item">
                    <a href="<?= $arItem['LINK'] ?>"
                       class="nav__link  <?= ($count - 1 == $iii) || ($count - 2 == $iii) ? 'nav__link_red' : '' ?>">
                        <?= $arItem['TEXT'] ?>
                    </a>

                </li>
            <? } ?>
        <? } ?>
        <? $iii++; ?>
    <? } ?>
<? } ?>
