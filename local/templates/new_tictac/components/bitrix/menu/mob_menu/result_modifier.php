<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arSelectedBrand = array();
$obSelectedSect = CIBlockSection::GetList(Array("timestamp_x "=>"DESC"), Array("IBLOCK_ID" => 5, "!UF_SELECTED_BRAND" => false, 'DEPTH_LEVEL' => 2, 'ACTIVE'=>'Y'), false, array('ID', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL', 'UF_SELECTED_SECTION'), array('nPageSize'=>1));
while ($arSelectedSect = $obSelectedSect->GetNext()){
    $arSelectedBrand = $arSelectedSect;
}

$arResult['SELECTED_BRAND'] = $arSelectedBrand;