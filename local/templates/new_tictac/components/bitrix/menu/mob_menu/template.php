<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="mob_nav">

<?
$count = count($arResult);
$i = 0;

if (!empty($arResult['SELECTED_BRAND'])) {
	echo '<li class="mob_nav__item "><a href="'.$arResult['SELECTED_BRAND']['SECTION_PAGE_URL'].'" class="mob_nav__link mob_nav__link_red">'.$arResult['SELECTED_BRAND']['NAME'].'</a></li>';
}

foreach($arResult as $arItem):
	$i++;
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li class="mob_nav__item"><a href="<?=$arItem["LINK"]?>" class="mob_nav__link" title="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li class="mob_nav__item"><a href="<?=$arItem["LINK"]?>" class="mob_nav__link <?= /*($count - $i) < 2*/$arItem["LINK"] == '/catalog/sale/' ? 'mob_nav__link_red' : ''?>" title="<?=$arItem["TEXT"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>

</ul>
<?endif?>

