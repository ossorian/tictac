<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?
?>
<?= ShowError($arResult["strProfileError"]); ?>
<?

if ($arResult['DATA_SAVED'] == 'Y')
echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>

<div class="settings">
    <div class="row">

        <div class="col-md-6 col-md-push-6">
            <form method="post" id="profileForm" name="form1" action="<?= $arResult["FORM_TARGET"] ?>?"
                  enctype="multipart/form-data" class="delivery__form">
                <?= $arResult["BX_SESSION_CHECK"] ?>
                <?
                $rsUser = CUser::GetByID(IntVal($USER->GetID()));
                $arUser = $rsUser->Fetch();
                ?>
                <input type="hidden" name="lang" value="<?= LANG ?>"/>
                <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
                <input type="hidden" name="LOGIN" value="<?= $arUser['LOGIN'] ?>">
                <input type="hidden" name="EMAIL" value="<?= $arUser['EMAIL'] ?>">


                <h4>Адрес доставки</h4>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:sale.ajax.locations",
                    "profile_loc",
                        Array(
                            "ALLOW_EMPTY_CITY" => "Y",
                            "CITY_INPUT_NAME" => "LOCATION",
                            "CITY_OUT_LOCATION" => "Y",
                            "COMPONENT_TEMPLATE" => ".default",
                            "COUNTRY" => !empty($arUser['PERSONAL_COUNTRY']) ? $arUser['PERSONAL_COUNTRY'] : '45',
                            "REGION" => $arUser['PERSONAL_STATE'],
                            "CITY" => $arUser['PERSONAL_CITY'],
                            "COUNTRY_INPUT_NAME" => "COUNTRY",
                            "NAME" => "q",
                            "ONCITYCHANGE" => "",
                            "REGION_INPUT_NAME" => "REGION",
                            "CITY_OUT_LOCATION" => "Y",
                        )
                ); ?>
                    <span class="input input--haruki">
													<input class="input__field input__field--haruki" type="text"
                                                           id="input-3" name="PERSONAL_STREET"
                                                           value="<?= $arUser['PERSONAL_STREET'] ?>"/>
													<label class="input__label input__label--haruki" for="input-3">
														<span class="input__label-content input__label-content--haruki">Улица, дом, квартира</span>
													</label>
												</span>
                    <span class="input input--haruki">
													<input class="input__field input__field--haruki" type="text"
                                                           id="input-3" name="PERSONAL_ZIP"
                                                           value="<?= $arUser['PERSONAL_ZIP'] ?>"/>
													<label class="input__label input__label--haruki" for="input-3">
														<span class="input__label-content input__label-content--haruki">Индекс</span>
													</label>
												</span>
                    <span class="input input--haruki">
													<input class="input__field input__field--haruki" type="text"
                                                           id="input-3" name="NAME" value="<?= $arUser['NAME'] ?>"/>
													<label class="input__label input__label--haruki" for="input-3">
														<span class="input__label-content input__label-content--haruki">Имя и фамилия</span>
													</label>
												</span>
                    <span class="input input--haruki">
													<input class="input__field input__field--haruki" type="text"
                                                           id="input-3" name="PERSONAL_MOBILE"
                                                           value="<?= $arUser['PERSONAL_MOBILE'] ?>"/>
													<label class="input__label input__label--haruki" for="input-3">
														<span class="input__label-content input__label-content--haruki">Номер телефона</span>
													</label>
												</span>
                    <input type="submit" value="Сохранить" name="save" class="btn btn_black password__btn">

                </form>
        </div>


        <div class="col-md-6 col-md-pull-6">
            <div class="row">
                <div class="col-md-6">
                    <form class="settings__form" method="post" id="profileFormPass" name="form1"
                          action="<?= $arResult["FORM_TARGET"] ?>?"
                          enctype="multipart/form-data">
                        <?= $arResult["BX_SESSION_CHECK"] ?>
                        <input type="hidden" name="lang" value="<?= LANG ?>"/>
                        <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
                        <input type="hidden" name="LOGIN" value="<?= $arUser['LOGIN'] ?>">
                        <input type="hidden" name="EMAIL" value="<?= $arUser['EMAIL'] ?>">


                        <h4>Сменить пароль</h4>
                        <span class="input input--haruki">
                    <input class="input__field input__field--haruki"
                           type="password" name="NEW_PASSWORD" id="input-3"/>
                    <label class="input__label input__label--haruki"
                           for="input-3">
                        <span
                            class="input__label-content input__label-content--haruki">Новый пароль</span>
                    </label>
                </span>
                        <span class="input input--haruki">
															<input class="input__field input__field--haruki"
                                                                   type="password" name="NEW_PASSWORD_CONFIRM"
                                                                   id="input-3"/>
															<label class="input__label input__label--haruki"
                                                                   for="input-3">
																<span
                                                                    class="input__label-content input__label-content--haruki">Подтверждение пароля</span>
															</label>
														</span>
                        <input type="submit" value="Сохранить" name="save" class="btn btn_black password__btn"
                               onclick="changePass()">
                    </form>

                </div>
            </div>
        </div>


    </div>
</div>