<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult['NUM_PRODUCTS'])) { ?>
    <span class="basket__count"><?= $arResult['NUM_PRODUCTS'] ?></span>
<? } ?>