<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?//print_r($arResult)?>
<?
$GLOBALS["APPLICATION"]->AddHeadScript('/bitrix/js/main/ajax.js');
/*
 * Отладка - не работает с обновленным компонентом
 *
global $USER;
if($USER->GetID() == 25) {
//	$arr = explode("\n", print_r($arResult, 1));
//	foreach ($arr as $line) {
//		echo '<script>console.log("'.str_replace("\"", "\\\"", rtrim($line)).'");</script>';
//	}
	$test= $arResult;
	unset($test["COUNTRY_LIST"]);
	echo "<pre>";
	print_r($test);
	echo "</pre>";
}*/

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$disabled = false;
if ($arParams["AJAX_CALL"] != "Y" && count($arParams["LOC_DEFAULT"]) > 0 && $arParams["PUBLIC"] != "N"):
	$isChecked = "";
	foreach ($arParams["LOC_DEFAULT"] as $val):
		$checked = "";

		if ((($val["ID"] == IntVal($_REQUEST["NEW_LOCATION_".$arParams["ORDER_PROPS_ID"]])) || ($val["ID"] == $arParams["CITY"])) && (!isset($_REQUEST["CHANGE_ZIP"]) || $_REQUEST["CHANGE_ZIP"] != "Y"))
		{
			$checked = "checked";
			$isChecked = "Y";
			$disabled = true;
		}?>

		<div><input onChange="<?=$arParams["ONCITYCHANGE"]?>;" <?=$checked?> type="radio" name="NEW_LOCATION_<?=$arParams["ORDER_PROPS_ID"]?>" value="<?=$val["ID"]?>" id="loc_<?=$val["ID"]?>" /><label for="loc_<?=$val["ID"]?>"><?=$val["LOC_DEFAULT_NAME"]?></label></div>

	<?endforeach;?>

	<input <? if($isChecked!="Y") echo 'checked';?> type="radio" onclick="newlocation(<?=$arParams["ORDER_PROPS_ID"]?>);" name="NEW_LOCATION_<?=$arParams["ORDER_PROPS_ID"]?>" value="0" id="loc_0" /><label for="loc_0"><?=GetMessage("LOC_DEFAULT_NAME_NULL")?></label>
<?endif;?>

<?
if (isset($_REQUEST["NEW_LOCATION_".$arParams["ORDER_PROPS_ID"]]) && IntVal($_REQUEST["NEW_LOCATION_".$arParams["ORDER_PROPS_ID"]]) > 0)
{
	$disabled = true;
}
?>

<?if ($arParams["AJAX_CALL"] != "Y"):?><div id="LOCATION_<?=$arParams["CITY_INPUT_NAME"];?>"><?endif?>

    <div class="row">

        <? if (count($arResult["COUNTRY_LIST"]) > 0): ?>
            <div class="col-md-6">
                <div class="select__wrap">
                    <p class="form__label">Страна</p>
                    <select <? if ($disabled) echo "disabled"; ?> id="<?= $arParams["COUNTRY_INPUT_NAME"] ?>"
                                                                  name="PERSONAL_COUNTRY"
                                                                  onChange="getLocation(this.value, '', '', <?= $arResult["JS_PARAMS"] ?>, '<?= CUtil::JSEscape($arParams["SITE_ID"]) ?>')"
                                                                  type="location">
                        <option><? echo GetMessage('SAL_CHOOSE_COUNTRY') ?></option>
                        <? foreach ($arResult["COUNTRY_LIST"] as $arCountry): ?>
                            <option
                                value="<?= $arCountry["ID"] ?>"<? if ($arCountry["ID"] == $arParams["COUNTRY"]): ?> selected="selected"<? endif; ?>><?= $arCountry["NAME_LANG"] ?></option>
                        <? endforeach; ?>
                    </select>

                </div>
            </div>
        <? endif; ?>

        <? if (count($arResult["REGION_LIST"]) > 0){ ?>
            <div class="col-md-6">
                <div class="select__wrap">
                    <p class="form__label">Область</p>
                    <?
                    $id = "";
                    if (count($arResult["COUNTRY_LIST"]) <= 0):
                        $id = "id=\"" . $arParams["COUNTRY_INPUT_NAME"] . "\"";
                    endif; ?>

                    <select <?= $id ?> <? if ($disabled) echo "disabled"; ?>
                        name="PERSONAL_STATE"
                        onChange="getLocation(<?= $arParams["COUNTRY"] ?>, this.value, '', <?= $arResult["JS_PARAMS"] ?>, '<?= CUtil::JSEscape($arParams["SITE_ID"]) ?>')"
                        type="location" <? if (count($arResult["COUNTRY_LIST"]) == 0): ?><? endif; ?>>
                        <option><? echo GetMessage('SAL_CHOOSE_REGION') ?></option>
                        <? foreach ($arResult["REGION_LIST"] as $arRegion): ?>
                            <option
                                value="<?= $arRegion["ID"] ?>"<? if ($arRegion["ID"] == $arParams["REGION"]): ?> selected="selected"<? endif; ?>><?= $arRegion["NAME_LANG"] ?></option>
                        <? endforeach; ?>
                    </select>

                </div>
            </div>
        <? } else { ?>
			<input type="hidden" name="PERSONAL_STATE" value="">
		<? } ?>

    </div>



<?if (count($arResult["CITY_LIST"]) > 0):?>
	<div class="row">
		<div class="col-xs-12">
			<div class="select__wrap">
				<p class="form__label">Город</p>
                <?
                $id = "";
                if (count($arResult["COUNTRY_LIST"]) <= 0 && count($arResult["REGION_LIST"]) <= 0):
                    $id = "id=\"".$arParams["COUNTRY_INPUT_NAME"]."\"";
                else:
                    $id = "id=\"".$arParams["CITY_INPUT_NAME"]."\"";
                endif;?>

                <select data-need='1' <?=$id?> <?if($disabled) echo "disabled";?> name="PERSONAL_CITY" onchange="$('#ORDER_PROP_6').val($(this).text()); $('#ORDER_PROP_6').attr('data-value',$(this).val()); <?if (strlen($arParams["ONCITYCHANGE"]) > 0):?><?=$arParams["ONCITYCHANGE"]?><?endif;?>" type="location">
                    <option data-id='simple' <?= empty($arParams['CITY']) ? 'selected="selected"' : ''?>><?echo GetMessage('SAL_CHOOSE_CITY')?></option>
                    <?foreach ($arResult["CITY_LIST"] as $arCity):?>
                    <option value="<?=$arCity["ID"]?>"<?if ($arCity["ID"] == $arParams["CITY"]):?> selected="selected"<?endif;?>><?=($arCity['CITY_ID'] > 0 ? $arCity["CITY_NAME"] : GetMessage('SAL_CHOOSE_CITY_OTHER'))?></option>
                    <?endforeach;?>
                </select>
                <input type='hidden' name='ORDER_PROP_6' id='ORDER_PROP_6' value='<?=$arParams["CITY"]?>'>
				</select>
			</div>
		</div>
	</div>
<?endif;?>

<?if ($arParams["AJAX_CALL"] != "Y"):?></div><div id="wait_container_<?=$arParams["CITY_INPUT_NAME"]?>" ></div><?endif;?>

<?if ($arParams["AJAX_CALL"] != "Y" && $arParams["PUBLIC"] != "N"):?>
<script>
	function newlocation(orderPropId)
	{
		var select = document.getElementById("LOCATION_ORDER_PROP_" + orderPropId);

		arSelect = select.getElementsByTagName("select");
		if (arSelect.length > 0)
		{
			for (var i in arSelect)
			{
				var elem = arSelect[i];
				elem.disabled = false;
			}
		}
	}
</script>
<?endif;?>
