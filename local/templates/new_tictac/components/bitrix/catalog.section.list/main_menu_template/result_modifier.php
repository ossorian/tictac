<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
    'VIEW_MODE' => 'LIST',
    'SHOW_PARENT_NAME' => 'Y',
    'HIDE_SECTION_NAME' => 'N'
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
    $arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
    $arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
    $arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT']) {
    if ('LIST' != $arParams['VIEW_MODE']) {
        $boolClear = false;
        $arNewSections = array();
        foreach ($arResult['SECTIONS'] as &$arOneSection) {
            if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL']) {
                $boolClear = true;
                continue;
            }
            $arNewSections[] = $arOneSection;
        }
        unset($arOneSection);
        if ($boolClear) {
            $arResult['SECTIONS'] = $arNewSections;
            $arResult['SECTIONS_COUNT'] = count($arNewSections);
        }
        unset($arNewSections);
    }
}

if (0 < $arResult['SECTIONS_COUNT']) {
    $boolPicture = false;
    $boolDescr = false;
    $arSelect = array('ID');
    $arMap = array();
    if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE']) {
        reset($arResult['SECTIONS']);
        $arCurrent = current($arResult['SECTIONS']);
        if (!isset($arCurrent['PICTURE'])) {
            $boolPicture = true;
            $arSelect[] = 'PICTURE';
        }
        if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent)) {
            $boolDescr = true;
            $arSelect[] = 'DESCRIPTION';
            $arSelect[] = 'DESCRIPTION_TYPE';
        }
    }
    if ($boolPicture || $boolDescr) {
        foreach ($arResult['SECTIONS'] as $key => $arSection) {
            $arMap[$arSection['ID']] = $key;
        }
        $rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
        while ($arSection = $rsSections->GetNext()) {
            if (!isset($arMap[$arSection['ID']]))
                continue;
            $key = $arMap[$arSection['ID']];
            if ($boolPicture) {
                $arSection['PICTURE'] = intval($arSection['PICTURE']);
                $arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
                $arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
                $arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
            }
            if ($boolDescr) {
                $arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
                $arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
                $arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
                $arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
            }
        }
    }
}
$rsSale = CIBlockSection::GetById(65)->GetNext();
if (!empty($arResult['SECTIONS'])) {
    $sectId = 0;
    $i = 0;

    foreach ($arResult['SECTIONS'] as $key => $arItem) {
        if ($arItem['DEPTH_LEVEL'] == 1) {
            $sectId = $key;
            $i = 0;
        }

        if ($arItem['DEPTH_LEVEL'] == 2) {
            $i++;
        }

        if (($arResult['SECTIONS'][$key + 1]['DEPTH_LEVEL'] == 1) || empty($arResult['SECTIONS'][$key + 1])) {
            $arResult['SECTIONS'][$sectId]['BRAND_COUNT'] = $rsSale['ACTIVE'] == 'Y' ? $i : $i +1;
        }
    }


    $arSelectedBrand = array();
    $obSelectedSect = CIBlockSection::GetList(Array("timestamp_x "=>"DESC"), Array("IBLOCK_ID" => 5, "!UF_SELECTED_BRAND" => false, 'DEPTH_LEVEL' => 2, 'ACTIVE'=>'Y'), false, array('ID', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL', 'UF_SELECTED_SECTION'), array('nPageSize'=>1));
    while ($arSelectedSect = $obSelectedSect->GetNext()){
        $arSelectedBrand = $arSelectedSect;
    }

    $newMenu = array(
        array(
            'NAME' => 'Для него',
            'SECTION_PAGE_URL' => '/for_him/',
            'DEPTH_LEVEL' => 1
        ),
        array(
            'NAME' => 'Для нее',
            'SECTION_PAGE_URL' => '/for_her/',
            'DEPTH_LEVEL' => 1
        )
    );

    if (!empty($arSelectedBrand)) {
        $newMenu[] = array(
            'NAME' => $arSelectedBrand['NAME'],
            'SECTION_PAGE_URL' => $arSelectedBrand['SECTION_PAGE_URL'],
            'DEPTH_LEVEL' => 1,
            'BLINK' => 'Y'
        );
    }

    foreach ($arResult['SECTIONS'] as $arSection) {
        if (($arSection['ID'] == 152) || ($arSection['ID'] == 65) ) {
            continue;
        }
        $newMenu[] = $arSection;
    }

    $newMenu[] = array(
        'NAME' => 'Новинки',
        'SECTION_PAGE_URL' => '/catalog/new/',
        'DEPTH_LEVEL' => 1
    );

    $newMenu[] = array(
        'NAME' => 'Бренды',
        'SECTION_PAGE_URL' => '/brands/',
        'DEPTH_LEVEL' => 1
    );

    $newMenu[] = array(
        'NAME' => 'Предзаказ',
        'SECTION_PAGE_URL' => '/catalog/preorder/',
        'DEPTH_LEVEL' => 1
    );



    if ($rsSale['ACTIVE'] == 'Y') {
        $newMenu[] = array(
            'NAME' => 'Распродажа',
            'SECTION_PAGE_URL' => '/catalog/sale/',
            'DEPTH_LEVEL' => 1
        );
    } else {
        $newMenu[] = array(
            'NAME' => '',
            'SECTION_PAGE_URL' => '',
            'DEPTH_LEVEL' => 1
        );
    }

//    print_r($newMenu);

    $arResult['SECTIONS'] = $newMenu;

}


?>


