<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if (!empty($arResult['SECTIONS'])) { ?>
    <?
    $iii = 0;
    $count = count($arResult['SECTIONS']);
    $brandsCount = 0;
    $brandsInRow = 0;
    $brandsI = 0;
    $i = 0;
    $parentSection = 0;
    $gogglesTop = '';
    $gogglesTop1 = '';
    $gogglesBottom = '';
    $gogglesBottom1 = '';
    ?>
    <? foreach ($arResult['SECTIONS'] as $key => $arItem) { ?>
        <? if ($arItem['DEPTH_LEVEL'] == 1) { ?>
            <? if ($arItem['ID'] == 172) { ?>
                <li class="nav__item nav__item__dd">
                <a href="<?= $arItem['SECTION_PAGE_URL'] ?>"
                   class="nav__link dropdown_toggle" title="<?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?>"><?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?></a>
                <div class="dropdown dropdown-clocks">
                <div class="dd-brends clearfix">
            <? } else if ($arItem['ID'] == 165) { ?>
                <li class="nav__item nav__item__dd">
                <a href="<?= $arItem['SECTION_PAGE_URL'] ?>"
                   class="nav__link dropdown_toggle" title="<?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?>"><?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?></a>
                <div class="dropdown dropdown-goggles">
                <div class="dd-goggles__wrap">
            <? } else if ($arItem['ID'] == 171) { ?>
                <li class="nav__item nav__item__dd">
                <a href="<?= $arItem['SECTION_PAGE_URL'] ?>"
                   class="nav__link dropdown_toggle" title="<?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?>"><?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?></a>
                <div class="dropdown dropdown-goggles">
                <div class="dd-goggles__wrap">
            <? } else if ($arItem['ID'] == 146) { ?>
                <li class="nav__item nav__item__dd">
                <a href="<?= $arItem['SECTION_PAGE_URL'] ?>"
                   class="nav__link dropdown_toggle" title="<?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?>"><?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?></a>
                <div class="dropdown dropdown-thongs">
                <div class="dd-thongs__wrap">
                <div class="row">
            <? } else { ?>
                <li class="nav__item <?= !empty($arItem['BLINK']) ? 'blink' : ''?>">
                <a href="<?= $arItem['SECTION_PAGE_URL'] ?>"
                   class="nav__link  <?= ($count - 1 == $iii) || ($count - 2 == $iii) ? 'nav__link_red' : '' ?>" title="<?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?>">
                    <?= mb_convert_case($arItem['NAME'], MB_CASE_TITLE, "UTF-8"); ?>
                </a>
            <? } ?>

            <?
                $brandsCount = $arItem['BRAND_COUNT'] - 1;
                $brandsInRow = floor($brandsCount / 7);
                $brandsExtra = $brandsCount % 7;
                $brandsI = 0;
                $i = 0;
                $parentSection = $arItem['ID'];
            ?>
        <? } ?>
        <? if (($arItem['DEPTH_LEVEL'] == 2) || ($parentSection == 146 && $arItem['DEPTH_LEVEL'] == 1)) { ?>
            <? if ($parentSection == 172) { ?>
                <? if ($brandsI == 0) { ?>
                    <div class="col-7">
                    <div class="text_center">
                    <ul class="dropdown__nav">
                <? } ?>
                    <li <?= empty($i) ? 'class="brend_active"' : '' ?> data="<?=$brandsI?>|<?=$brandsInRow?>|<?=$brandsExtra?>">
                    <a href="<?= $arItem['SECTION_PAGE_URL'] ?>"
                       class="link<?= !empty($arItem['UF_BRAND_BOLD']) ? '_bold' : '' ?> watch_dynamic_link <?= !empty($arItem['UF_SELECTED_BRAND']) ? 'promo_link' : '' ?>"
                       data-id="<?= $arItem['ID'] ?>"
                        title="<?= $arItem['NAME']?>"
                    >
                    <?= $arItem['NAME'] ?>
                    </a>
                    <div class="dd-clocks__wrap">
                        <div class="row dynamic_content_watch" data-role="<?= $arItem['ID'] ?>">
                            <?
                            $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IMAGE", "PROPERTY_BRAND");
                            $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $arItem['ID'], ">=PROPERTY_BRAND" => 1);
                            $res = CIBlockElement::GetList(Array("PROPERTY_BRAND" => "ASC"), $arFilter, false, Array("nPageSize" => 6), $arSelect);
                            while ($ob = $res->GetNext()) {
                                $picture = !empty($ob['PROPERTY_IMAGE_VALUE']) ? resizeImage($ob['PROPERTY_IMAGE_VALUE'], 152, 174, 1) : resizeImage($ob['PREVIEW_PICTURE'], 152, 174, 1);
                                $pictureNumber = !empty($ob['PROPERTY_IMAGE_VALUE']) ? 'фото 2' : 'фото 1';
                                ?>
                                <div class="col-md-2">
                                    <div class="dropdown__item">
                                        <div class="dd-item-img__wrap">
                                            <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" title="<?= $ob['NAME'] ?>">
                                                <img src="<?= $picture['SRC'] ?>" class="dd-item__img" alt="<?= $arItem['NAME']?> <?= $ob['NAME'] ?> <?=$pictureNumber?>">
                                            </a>
                                        </div>
                                        <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" title="<?= $ob['NAME'] ?>"><p
                                                class="dd-item__name" ><?= $ob['NAME'] ?></p></a>
                                    </div>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </li>                
                <? if ($brandsI == $brandsInRow + (($brandsExtra > 0) ? 1 : 0) - 1) { ?>

					<?if ($brandsExtra == 0):?>
						<li><a class="menu_nav__link_red nav__link_red" href="/catalog/sale/">Распродажа</a></li>					
					<?endif?>

                    </ul>
                    </div>
                    </div>
                    <? $brandsExtra--; ?>
                    <? $brandsI = 0; ?>
                <? } else { ?>
                    <? $brandsI++; ?>
                <? } ?>
                <? $i++; ?>
            <? } else if ($parentSection == 165) {
                $gogglesTop .= '
                <div class="row dynamic_content_sunglasses" ' . (!empty($i) ? 'style="display: none;"' : '') . '
                     data-role="' . $arItem['ID'] . '">';
                $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IMAGE", "PROPERTY_BRAND");
                $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $arItem['ID'], ">=PROPERTY_BRAND" => 1);
                $hasItems = CIBlockElement::GetList(Array("PROPERTY_BRAND" => "ASC"), $arFilter, array(), false, $arSelect);
                if (empty($hasItems)) {
                    $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $arItem['ID']);
                }
                $res = CIBlockElement::GetList(Array("PROPERTY_BRAND" => "ASC"), $arFilter, false, Array("nPageSize" => 6), $arSelect);
                while ($ob = $res->GetNext()) {
                    $picture = !empty($ob['PROPERTY_IMAGE_VALUE']) ? resizeImage($ob['PROPERTY_IMAGE_VALUE'], 152, 95, 1) : resizeImage($ob['PREVIEW_PICTURE'], 152, 95, 1);
                    $pictureNumber = !empty($ob['PROPERTY_IMAGE_VALUE']) ? 'фото 2' : 'фото 1';
                    $gogglesTop .= '
                        <div class="col-md-2">
                            <div class="dropdown__item">
                                <div class="dd-item-img__wrap">
                                    <a href="' . $arItem['SECTION_PAGE_URL'] . '" title="'.$ob['NAME'].'">
                                        <img src="' . $picture['SRC'] . '" class="dd-item__img" alt="'. $arItem['NAME'].' '.$ob['NAME'].' '.$pictureNumber .'">
                                    </a>
                                </div>
                                <a href="' . $arItem['SECTION_PAGE_URL'] . '" title="'.$ob['NAME'].'">
                                    <p class="dd-item__name">' . $ob['NAME'] . '</p>
                                </a>
                            </div>
                        </div>';
                }
                $gogglesTop .= '</div>';
                $gogglesBottom .= '
                <li>
                            <a href="' . $arItem['SECTION_PAGE_URL'] . '"
                               class="link' . (!empty($arItem['UF_BRAND_BOLD']) ? '_bold' : '') . ' sunglasses_dynamic_link"
                               data-id="' . $arItem['ID'] . '" title="' . $arItem['NAME'] . '">
                                ' . $arItem['NAME'] . '
                            </a>
                        </li>
                ';
                $i++;
            } else if ($parentSection == 171) {
                $gogglesTop1 .= '
                <div class="row dynamic_content_accessories" ' . (!empty($i) ? 'style="display: none;"' : '') . '
                     data-role="' . $arItem['ID'] . '">';
                $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IMAGE", "PROPERTY_BRAND");
                $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $arItem['ID'], ">=PROPERTY_BRAND" => 1);
                $res = CIBlockElement::GetList(Array("PROPERTY_BRAND" => "ASC"), $arFilter, false, Array("nPageSize" => 6), $arSelect);
                while ($ob = $res->GetNext()) {
                    $picture = !empty($ob['PROPERTY_IMAGE_VALUE']) ? resizeImage($ob['PROPERTY_IMAGE_VALUE'], 152, 170, 1) : resizeImage($ob['PREVIEW_PICTURE'], 152, 170, 1);
                    $pictureNumber = !empty($ob['PROPERTY_IMAGE_VALUE']) ?  'фото 2' :  'фото 1';
                    $gogglesTop1 .= '
                        <div class="col-md-2" data-id="'.$ob['ID'].'">
                            <div class="dropdown__item">
                                <div class="dd-item-img__wrap">
                                    <a href="' . $arItem['SECTION_PAGE_URL'] . '" title="'.$ob['NAME'].'">
                                        <img src="' . $picture['SRC'] . '" class="dd-item__img" alt="'.$arItem['NAME'].' '.$ob['NAME'].' '.$pictureNumber.'">
                                    </a>
                                </div>
                                <a href="' . $arItem['SECTION_PAGE_URL'] . '" title="'.$ob['NAME'].'">
                                    <p class="dd-item__name">' . $ob['NAME'] . '</p>
                                </a>
                            </div>
                        </div>';
                }
                $gogglesTop1 .= '</div>';
                $gogglesBottom1 .= '
                <li>
                            <a href="' . $arItem['SECTION_PAGE_URL'] . '"
                               class="link' . (!empty($arItem['UF_BRAND_BOLD']) ? '_bold' : '') . ' accessories_dynamic_link"
                               data-id="' . $arItem['ID'] . '" title="'.$arItem['NAME'].'">
                                ' . $arItem['NAME'] . '
                            </a>
                        </li>
                ';
                $i++;
            } else if ($parentSection == 146) { ?>
                <?
                $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IMAGE", "PROPERTY_BRAND");
                $arFilter = Array("IBLOCK_ID" => 5, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", ">=PROPERTY_BRAND" => 1, "SECTION_ID" => 146);
                $res = CIBlockElement::GetList(Array("PROPERTY_BRAND" => "ASC"), $arFilter, false, Array("nPageSize" => 6), $arSelect);
                while ($ob = $res->GetNext()) {
                    $picture = !empty($ob['PROPERTY_IMAGE_VALUE']) ? resizeImage($ob['PROPERTY_IMAGE_VALUE'], 152, 170, 1) : resizeImage($ob['PREVIEW_PICTURE'], 152, 170, 1);
                    $pictureNumber = !empty($ob['PROPERTY_IMAGE_VALUE']) ? 'фото 2' : 'фото 1';

                    ?>

                    <div class="col-md-2">
                        <div class="dropdown__item">
                            <div class="dd-item-img__wrap">
                                <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" title="<?= $ob['NAME'] ?>">
                                    <img src="<?= $picture['SRC'] ?>" class="dd-item__img" alt="<?= $arItem['NAME']?> <?= $ob['NAME'] ?> <?=$pictureNumber?>">
                                </a>
                            </div>
                            <a href="<?= $arItem['SECTION_PAGE_URL'] ?>" title="<?= $ob['NAME'] ?>">
                                <p class="dd-item__name"><?= $ob['NAME'] ?></p>
                            </a>
                        </div>
                    </div>
                <? } ?>

            <? } else { ?>
                
            <? } ?>
        <? } ?>
        <? if (($arResult['SECTIONS'][$key + 1]['DEPTH_LEVEL'] == 1) || empty($arResult['SECTIONS'][$key + 1])) { ?>
            <? if ($parentSection == 172) { ?>
                </div>
                </div>
                </li>
            <? } else if ($parentSection == 165) { ?>
                <div class="dd-goggles__wrap">
                    <?= $gogglesTop ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="goggles__nav">
                            <?= $gogglesBottom ?>
                        </ul>
                    </div>
                </div>
                </div>
                </div>
                </li>
            <? } else if ($parentSection == 171) { ?>
                <div class="dd-goggles__wrap">
                    <?= $gogglesTop1 ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="goggles__nav">
                            <?= $gogglesBottom1 ?>
                        </ul>
                    </div>
                </div>
                </div>
                </div>
                </li>
            <? } else if ($parentSection == 146) { ?>
                </div>
                </div>
                </div>
                </li>
            <? } else { ?>
                </li>
            <? } ?>
        <? } ?>
        <? $iii++; ?>
    <? } ?>

<? } ?>


