<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['SECTIONS'])) { ?>
<div class='NDD'></div>
    <section class="brends sections">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>Популярные бренды</h2>
                </div>
            </div>
            <div class="brends__items">
                <div class="row hidden-xs">
                    <? foreach ($arResult['SECTIONS'] as $section) {
                        $picture = resizeImage($section['UF_POPULAR_LOGO'], 204, 204, 1);
                        ?>
                        <a href="<?= $section['SECTION_PAGE_URL'] ?>">
                            <div class="col-lg-2 col-md-3 col-sm-4">
                                <div class="item_square item_square_opacity">
                                    <img src="<?= $picture['SRC'] ?>" alt="<?= $section['NAME'] ?>">
                                    <div class="item_square__text">
                                        <p class="item_square__text__title"><?= $section['NAME'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <? } ?>
                </div>
                <div class="row hidden-lg hidden-md hidden-sm">
                    <div class="col-xs-12">
                        <div class="brends__slider">
                            <? foreach ($arResult['SECTIONS'] as $section) {
                                $picture = resizeImage($section['UF_POPULAR_LOGO'], 242, 242, 1);
                                ?>
                                <div class="slide">
																	<a href="<?= $section['SECTION_PAGE_URL'] ?>">
                                    <div class="item_square item_square_opacity">
                                        <img src="<?= $picture['SRC']?>" alt="<?= $section['NAME'] ?>">
                                        <div class="item_square__text">
                                            <p class="item_square__text__title"><?= $section['NAME'] ?></p>
                                        </div>
                                    </div>
																	</a>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? } ?>