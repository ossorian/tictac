<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


if (!empty($arResult['SECTIONS'])) { ?>

    <? foreach ($arResult['SECTIONS'] as $section) {
        $title = '';
        switch ((int)$section['ID']){
            case 146:
                $title = morph($section['ELEMENT_CNT'], 'ремешок', 'ремешка', 'ремешков');
                break;
            case 165:
                $title = morph($section['ELEMENT_CNT'], 'пара очков', 'пары очков', 'пар очков');
                break;
            case 171:
                $title = morph($section['ELEMENT_CNT'], 'украшение', 'украшения', 'украшений');
                break;
            case 172:
                $title = morph($section['ELEMENT_CNT'], 'часы', 'часов', 'часов');
                break;
        }

        $picture = CFile::GetPath($section['UF_MAIN_PICTURE']);//resizeImage($section['UF_MAIN_PICTURE'], 263, 263, 1);
        ?>
        <a href="<?= $section['SECTION_PAGE_URL'] ?>">
            <div class="col-xs-6 mob_pad">
                <div class="item_square item_square_opacity">
                    <img src="<?= $picture?>" alt="<?= $section['NAME'] ?>">
                    <div class="item_square__text popular-nav__text">
                        <p class="item_square__text__title"><?= $section['NAME'] ?></p>
                        <p class="item_square__text__desc"><?= $section['ELEMENT_CNT']?> <?= $title?></p>
                    </div>
                </div>
            </div>
        </a>
    <? } ?>

<? } ?>

