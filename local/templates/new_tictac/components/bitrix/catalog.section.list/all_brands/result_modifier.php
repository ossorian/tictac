<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if (0 < $arResult['SECTIONS_COUNT'])
{
    foreach ($arResult['SECTIONS'] as $key => $arSection)
    {
        if (in_array($arSection['ID'], array(65, 146, 165, 152)) || $arSection['DEPTH_LEVEL'] == 1){
            unset($arResult['SECTIONS'][$key]);
        }
    }
}

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
        }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

$sorted = array_orderby($arResult['SECTIONS'], 'NAME', SORT_ASC);

$arResult['SECTIONS'] = $sorted;
?>