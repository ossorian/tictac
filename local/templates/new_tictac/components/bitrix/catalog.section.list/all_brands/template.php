<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['SECTIONS'])) { ?>

    <? foreach ($arResult['SECTIONS'] as $section) {
        $picture = resizeImage($section['UF_POPULAR_LOGO'], 204, 204, 1);
        ?>
        <a href="<?= $section['SECTION_PAGE_URL'] ?>">
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 mob_pad">
                <div class="item_square item_square_opacity">
                    <img src="<?= empty($picture['SRC']) ? SITE_TEMPLATE_PATH . '/img/brends/no_image.png' : $picture['SRC'] ?>" alt="<?= $section['NAME'] ?>">
                    <div class="item_square__text">
                        <p class="item_square__text__title"><?= $section['NAME'] ?></p>
                    </div>
                </div>
            </div>
        </a>
    <? } ?>

<? } ?>

