<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult['ITEMS'])) { ?>
    <div class="header__body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12" data-edit='edit'>
                    <div class="header__slider">
                        <? foreach ($arResult['ITEMS'] as $item) {
                            $this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                            ?>
                            <div class="slide">
                            <a href="<?= $item["DISPLAY_PROPERTIES"]["LINK"]["VALUE"] ?>"
                               id="<?= $this->GetEditAreaId($item['ID']); ?>">

                                    <div class="header__slide">
										<img class='h-slide__img' src='<?= !empty($item["DETAIL_PICTURE"]["SRC"]) ? $item["DETAIL_PICTURE"]["SRC"] : $item["PREVIEW_PICTURE"]["SRC"] ?>'>
                                    </div>
																		

                            </a>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? } ?>