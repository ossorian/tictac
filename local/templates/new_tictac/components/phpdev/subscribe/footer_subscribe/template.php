<p style="display: none" id="succes_subscribe">Вы успешно подписались на рассылку</p>
<form class="footer__form" id="form-subscribe" action="" method="post" onsubmit="fbq('track', 'Subscribe');">
    <?=bitrix_sessid_post()?>
    <div class="footer__input">
        <span class="input input--haruki">
            <input class="input__field input__field--haruki" type="email" name="EMAIL" id="input-5" onClick="$('.google-captcha-container').show('slow')"/>
            <label class="input__label input__label--haruki" for="input-5">
                <span class="input__label-content input__label-content--haruki">Электронная почта</span>
            </label>
        </span>
    </div>
    <input type="submit" name="submit" value="Подписаться" class="btn btn_trans_grey footer__btn"/>
	<input type="hidden" name="recaptcha_response" id="recaptchaResponse">

    <input type="hidden" name="NAME" value=" ">
    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
</form>