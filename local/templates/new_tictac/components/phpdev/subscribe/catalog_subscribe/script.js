BX.ready(function () {
    jQuery(window).load(function () {
        $('#form-subscribe').submit(function () {
            $this = $(this);

            $.ajax({
                type: "POST",
                dataType: "html",
                data: $this.serialize(),
                success: function (data) {

                    if (typeof data.error === 'undefined') {
                        var resp = JSON.parse(data);
                        if (resp['ERROR']) {
                            $('#succes_subscribe').text(resp['ERROR']);
                            $('#succes_subscribe').show();
                        } else {
                            $('#succes_subscribe').text('Вы успешно подписались на рассылку');
                            $('#succes_subscribe').show();
                        }
                    }


                }
            });

            return false;
        });
    });
});