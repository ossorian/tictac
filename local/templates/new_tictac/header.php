<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
    <!DOCTYPE html>
    <!--[if lt IE 7 ]>
    <html class="ie ie6" lang="en"> <![endif]-->
    <!--[if IE 7 ]>
    <html class="ie ie7" lang="en"> <![endif]-->
    <!--[if IE 8 ]>
    <html class="ie ie8" lang="en"> <![endif]-->
    <!--[if (gte IE 9)|!(IE)]><!-->
    <html lang="ru"> <!--<![endif]-->

    <head>

        <meta charset="utf-8">

        <? $APPLICATION->ShowHead(); ?>
        <title><? $APPLICATION->ShowTitle() ?></title>

        <? include_once( dirname(dirname(__FILE__))."/includes/google_analytics.php"); ?>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?
$protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
$domain = $_SERVER['SERVER_NAME'];
?>
        <link rel="shortcut icon" href="<?= $protocol . '://' . $domain . SITE_TEMPLATE_PATH ?>/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="<?= $protocol . '://' . $domain . SITE_TEMPLATE_PATH ?>/img/favicon/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72"
              href="<?= $protocol . '://' . $domain . SITE_TEMPLATE_PATH ?>/img/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114"
              href="<?= $protocol . '://' . $domain . SITE_TEMPLATE_PATH ?>/img/favicon/apple-touch-icon-114x114.png">

        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/libs/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/libs/Magnific-Popup/magnific-popup.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/fonts/TTT__icons/css/ttt.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/libs/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/libs/slick/slick.css?rev=1">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/libs/sliderLine/nouislider.min.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/libs/TextInputEffects/css/set1.css">

        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/fonts.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/main.css">

        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/game.css">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/phpdev.css?rev=123qwdq33ferger2342342343e23e8496446754">
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/media.css?rev=1234d34d3">



        <script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/f2f442b4a547324202ac2b69c919abf8_1.js" async></script>

				<!-- Global site tag (gtag.js) - Google Analytics -->
				<?/*<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39386232-1"></script>
				<script>
					window.dataLayer = window.dataLayer || [];
					function gtag(){dataLayer.push(arguments);}
					gtag('js', new Date());

					gtag('config', 'UA-39386232-1');
				</script>*/?>
				<script type="text/javascript">
					//(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-186042-4gRQs';
				</script>

        <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-186042-4gRQs';</script>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TJMVXRZ');</script>
		<!-- End Google Tag Manager -->

		<?/*?>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TJMVXRZ');</script>
        <!-- End Google Tag Manager -->
		<?*/?>

    </head>
    <script>
        fbq('track', 'ViewContent', {
            currency: 'RUB',
        });
    </script>
<body>
<script>
    (function(w, d, s, h, id) {
        w.roistatProjectId = id; w.roistatHost = h;
        var p = d.location.protocol == "https:" ? "https://" : "http://";
        var u = /^.roistat_visit=[^;]+(.)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
        var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
    })(window, document, 'script', 'cloud.roistat.com', 'b41b86998cc519ddc4ab9fbfafb0a873');
</script>
<? $currPage = $APPLICATION->GetCurPage();?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJMVXRZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?/*?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJMVXRZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?*/?>

		<!--LiveInternet counter-->
		<script type="text/javascript">
			new Image().src = "//counter.yadro.ru/hit?r"+
			escape(document.referrer)+((typeof(screen)=="undefined")?"":
			";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
			screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
			";h"+escape(document.title.substring(0,150))+
			";"+Math.random();
		</script><!--/LiveInternet-->
    <div><? $APPLICATION->ShowPanel(); ?></div>

<? if (defined('ERROR_404') === false) { ?>
	<!-- HEADER -->
	
    <header class="header header__main">
        <div class="header__head_mob hidden-lg hidden-md">
			<div class="header-head__top header-head__top_mob">
				<? $APPLICATION->IncludeComponent("bitrix:news.list", "last_news", Array(
					"DISPLAY_DATE" => "Y",    // Выводить дату элемента
					"DISPLAY_NAME" => "Y",    // Выводить название элемента
					"DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
					"DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
					"AJAX_MODE" => "N",    // Включить режим AJAX
					"IBLOCK_TYPE" => "TicTacToy",    // Тип информационного блока (используется только для проверки)
					"IBLOCK_ID" => "4",    // Код информационного блока
					"NEWS_COUNT" => "1",    // Количество новостей на странице
					"SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
					"SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
					"SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
					"SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
					"FILTER_NAME" => "",    // Фильтр
					"FIELD_CODE" => "",    // Поля
					"PROPERTY_CODE" => "",    // Свойства
					"CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
					"DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
					"PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
					"ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
					"SET_TITLE" => "N",    // Устанавливать заголовок страницы
					"SET_STATUS_404" => "N",    // Устанавливать статус 404, если не найдены элемент или раздел
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
					"ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
					"PARENT_SECTION" => "",    // ID раздела
					"PARENT_SECTION_CODE" => "",    // Код раздела
					"CACHE_TYPE" => "A",    // Тип кеширования
					"CACHE_TIME" => "3600",    // Время кеширования (сек.)
					"CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
					"CACHE_GROUPS" => "N",    // Учитывать права доступа
					"DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
					"DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
					"PAGER_TITLE" => "Новости",    // Название категорий
					"PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
					"PAGER_TEMPLATE" => "",    // Название шаблона
					"PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
					"PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
					"AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
					"AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
					"AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
				),
					false
				); ?>
			</div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <div class="menu__btn">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="logo">
                            <? if ($currPage !== "/") { ?>
                                <a href="/" class="logo__link">
							<? } else { ?>
								<a href="" class="logo__link" style="pointer-events: none;">
							<? } ?>
								<i class="icon-logo"></i>
							</a>
                        </div>
                    </div>
                    <div class="col-xs-3">
                         <div class="h-b-right__content">
                                <div class="header__wishlist">
																	<span class="wishlist__icon">
																		<i class=" icon-icon-wishlist"></i>
																			<? if (!empty($_SESSION["CATALOG_COMPARE_LIST"][5]["ITEMS"])) { ?>
																					<span class="wish"></span>
																			<? } ?>
																	</span>
                                </div>


                                <div class="header__basket">

																		<span class="basket__icon">
																			<i class="icon-icon-cart"></i>
																		</span>
                                    <? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "header_basket_counter", Array(
                                        "HIDE_ON_BASKET_PAGES" => "N",    // Не показывать на страницах корзины и оформления заказа
                                        "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",    // Страница корзины
                                        "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",    // Страница оформления заказа
                                        "PATH_TO_PERSONAL" => SITE_DIR . "personal/",    // Страница персонального раздела
                                        "PATH_TO_PROFILE" => SITE_DIR . "personal/",    // Страница профиля
                                        "PATH_TO_REGISTER" => SITE_DIR . "login/",    // Страница регистрации
                                        "POSITION_FIXED" => "N",    // Отображать корзину поверх шаблона
                                        "SHOW_AUTHOR" => "N",    // Добавить возможность авторизации
                                        "SHOW_EMPTY_VALUES" => "Y",    // Выводить нулевые значения в пустой корзине
                                        "SHOW_NUM_PRODUCTS" => "Y",    // Показывать количество товаров
                                        "SHOW_PERSONAL_LINK" => "Y",    // Отображать персональный раздел
                                        "SHOW_PRODUCTS" => "N",    // Показывать список товаров
                                        "SHOW_TOTAL_PRICE" => "Y",    // Показывать общую сумму по товарам
                                        "COMPONENT_TEMPLATE" => ".default"
                                    ),
                                        false
                                    ); ?>

                                </div>
                            </div>

                    </div>
                </div>
                <div id="smallcart_mob">
                    <? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "header_basket_mob", array(
                        "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                        "COLUMNS_LIST" => array(
                            0 => "NAME",
                            1 => "PRICE",
                            2 => "TYPE",
                            3 => "QUANTITY",
                            4 => "DELETE",
                            5 => "DISCOUNT",
                        ),
                        "PATH_TO_ORDER" => "/cart/",
                        "HIDE_COUPON" => "N",
                        "QUANTITY_FLOAT" => "N",
                        "PRICE_VAT_SHOW_VALUE" => "N",
                        "SET_TITLE" => "N"
                    ),
                        false
                    ); ?>
                </div>
            </div>

            <div class="toggle_menu">
				<div class="toggle_menu__container">
					<div class="mob_search">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<form action="/catalog/" method="GET">
										<input type="text" class="mob_search__input"
											placeholder="<?= empty($_GET["q"]) ? "Поиск" : $_GET["q"] ?>" name="q"">
										<button type="submit" class="mob_search__icon">
											<i class="icon-icon-search"></i>
										</button>
									</form>

								</div>
							</div>
						</div>
					</div>

					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<?$APPLICATION->IncludeComponent(
									"bitrix:menu",
									"mob_menu",
									array(
										"ALLOW_MULTI_SELECT" => "N",
										"CHILD_MENU_TYPE" => "left",
										"COMPONENT_TEMPLATE" => "mob_menu",
										"DELAY" => "N",
										"MAX_LEVEL" => "1",
										"MENU_CACHE_GET_VARS" => array(
										),
										"MENU_CACHE_TIME" => "36000",
										"MENU_CACHE_TYPE" => "N",
										"MENU_CACHE_USE_GROUPS" => "N",
										"ROOT_MENU_TYPE" => "top",
										"USE_EXT" => "N"
									),
									false
								);?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:menu",
									"mob_menu2",
									array(
										"ALLOW_MULTI_SELECT" => "N",
										"CHILD_MENU_TYPE" => "left",
										"COMPONENT_TEMPLATE" => "mob_menu2",
										"DELAY" => "N",
										"MAX_LEVEL" => "1",
										"MENU_CACHE_GET_VARS" => array(
										),
										"MENU_CACHE_TIME" => "36000",
										"MENU_CACHE_TYPE" => "A",
										"MENU_CACHE_USE_GROUPS" => "Y",
										"ROOT_MENU_TYPE" => "bottom",
										"USE_EXT" => "N"
									),
									false
								);?>
								<div class="phone">
									<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/include_header_phone_mob.php"), false); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="mob_acc__wrap">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<div class="mob_acc clearfix">
										<? if (!$USER->IsAuthorized()) { ?>
											<a href="/auth/">
												<span class="acc__icon">
													<i class="icon-icon-profile"></i>
												</span>
												<span class="acc__text link_border">Войти на сайт</span>
											</a>
										<? } else { ?>
											<a href="/profile/">
												<span class="acc__icon">
													<i class="icon-icon-profile"></i>
												</span>
												<span class="acc__text link_border">Профиль</span>
											</a>
											<br>
											<a href="<? echo $APPLICATION->GetCurPageParam("logout=yes", array(
												"login",
												"logout",
												"register",
												"forgot_password",
												"change_password")); ?>" style="color: #000;">
											<span class="logout__icon">
												<i class="icon-icon-sign-out"></i>
											</span>
												<span class="acc__text link_border">Выход</span>
											</a>
										<? } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>


        <div class="header__head hidden-sm hidden-xs">
            <div class="header-head__top">
                <? $APPLICATION->IncludeComponent("bitrix:news.list", "last_news", Array(
                    "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                    "DISPLAY_NAME" => "Y",    // Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                    "AJAX_MODE" => "N",    // Включить режим AJAX
                    "IBLOCK_TYPE" => "TicTacToy",    // Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => "4",    // Код информационного блока
                    "NEWS_COUNT" => "1",    // Количество новостей на странице
                    "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                    "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                    "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                    "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                    "FILTER_NAME" => "",    // Фильтр
                    "FIELD_CODE" => "",    // Поля
                    "PROPERTY_CODE" => "",    // Свойства
                    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                    "SET_STATUS_404" => "N",    // Устанавливать статус 404, если не найдены элемент или раздел
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                    "PARENT_SECTION" => "",    // ID раздела
                    "PARENT_SECTION_CODE" => "",    // Код раздела
                    "CACHE_TYPE" => "A",    // Тип кеширования
                    "CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",    // Учитывать права доступа
                    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
                    "PAGER_TITLE" => "Новости",    // Название категорий
                    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                    "PAGER_TEMPLATE" => "",    // Название шаблона
                    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                ),
                    false
                ); ?>
            </div>
            <div class="header-head__bottom">
                <div class="container">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="search_visible hidden">
                                <div class="search_visible__field">
									<span class="search_visible__icon">
										<i class="icon-icon-search"></i>
									</span>
                                    <form action="/catalog/" method="GET">
                                        <input type="text" class="search_visible__input"
                                               placeholder="<?= empty($_GET["q"]) ? "Что вы ищете?" : $_GET["q"] ?>"
                                               name="q"">
                                        <span class="search_visible__close">
										<i class="icon-icon-big-close"></i>
									</span>
                                        <button type="submit" class="btn btn_black search__btn">Найти</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="h-b-left__content">
                                <div class="phone">
                                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/include_header_phone.php"), false); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="h-b-center__content text_center">
                                <div class="certificate">
                                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/include_header_certificate.php"), false); ?>
                                </div>
                                <div class="logo">
                                    <? if ($currPage !== "/") { ?>
                                        <a href="/" class="logo__link">
                                            <i class="icon-logo"></i>
                                        </a>
                                    <? } else { ?>
                                        <a href="" class="logo__link" style="pointer-events: none;">
                                            <i class="icon-logo"></i>
                                        </a>
                                    <? } ?>
                                </div>
                                <div class="search">
									<span class="search__icon">
										<i class="icon-icon-search"></i>
									</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="h-b-right__content <?= $USER->IsAuthorized() ? 'header_left__profile' : '' ?>">
                                <? if (!$USER->IsAuthorized()) { ?>
                                    <div class="header__acc">
                                        <span class="acc__icon">
                                            <i class="icon-icon-profile"></i>
                                        </span>
                                    </div>
                                <? } else { ?>
                                    <div class="header__acc">
                                        <a href="/profile/" style="color: #000;">
                                            <span class="acc__icon">Профиль</span>
                                        </a>
                                    </div>
                                <? } ?>
                                <div class="header__wishlist">
									<span class="wishlist__icon">
										<i class=" icon-icon-wishlist"></i>
                                        <? if (!empty($_SESSION["CATALOG_COMPARE_LIST"][5]["ITEMS"])) { ?>
                                            <span class="wish"></span>
                                        <? } ?>
									</span>
                                </div>

                                <? if ($USER->IsAuthorized()) { ?>
                                    <div class="header__logout">
                                        <a href="<? echo $APPLICATION->GetCurPageParam("logout=yes", array(
                                            "login",
                                            "logout",
                                            "register",
                                            "forgot_password",
                                            "change_password")); ?>" style="color: #000;">
                                        <span class="logout__icon">
                                            <i class="icon-icon-sign-out"></i>
                                        </span>
                                        </a>
                                    </div>
                                <? } ?>

                                <div class="header__basket">
																	<span class="basket__icon">
																		<i class="icon-icon-cart"></i>
																	</span>
                                    <? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "header_basket_counter", Array(
                                        "HIDE_ON_BASKET_PAGES" => "N",    // Не показывать на страницах корзины и оформления заказа
                                        "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",    // Страница корзины
                                        "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",    // Страница оформления заказа
                                        "PATH_TO_PERSONAL" => SITE_DIR . "personal/",    // Страница персонального раздела
                                        "PATH_TO_PROFILE" => SITE_DIR . "personal/",    // Страница профиля
                                        "PATH_TO_REGISTER" => SITE_DIR . "login/",    // Страница регистрации
                                        "POSITION_FIXED" => "N",    // Отображать корзину поверх шаблона
                                        "SHOW_AUTHOR" => "N",    // Добавить возможность авторизации
                                        "SHOW_EMPTY_VALUES" => "Y",    // Выводить нулевые значения в пустой корзине
                                        "SHOW_NUM_PRODUCTS" => "Y",    // Показывать количество товаров
                                        "SHOW_PERSONAL_LINK" => "Y",    // Отображать персональный раздел
                                        "SHOW_PRODUCTS" => "N",    // Показывать список товаров
                                        "SHOW_TOTAL_PRICE" => "Y",    // Показывать общую сумму по товарам
                                        "COMPONENT_TEMPLATE" => ".default"
                                    ),
                                        false
                                    ); ?>
                                </div>
                            </div>

											 </div>
                    </div>
                    <? if (!$USER->IsAuthorized()) { ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="auth">
                                    <div class="auth__tabs clearfix">
                                        <span class="auth__tab">Войти</span>
                                        <span class="auth__tab">Регистрация</span>
                                    </div>
                                <div class="auth__content">
									<span class="auth__close">
										<i class="icon-icon-small-close"></i>
									</span>
                                    <div class="auth__tab-item auth__tab-item_active">
                                        <?
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:system.auth.form",
                                            "main_auth",
                                            Array(
                                                "COMPONENT_TEMPLATE" => "main_auth",
                                                "FORGOT_PASSWORD_URL" => "",
                                                "PROFILE_URL" => "",
                                                "REGISTER_URL" => "",
                                                "SHOW_ERRORS" => "Y"
                                            )
                                        );
                                        ?>
                                    </div>
                                    <div class="auth__tab-item">
                                        <? $APPLICATION->IncludeComponent(
                                            "bitrix:main.register",
                                            "main_register",
                                            array(
                                                "AUTH" => "Y",
                                                "REQUIRED_FIELDS" => array(
                                                    0 => "EMAIL",
                                                ),
                                                "SET_TITLE" => "N",
                                                "SHOW_FIELDS" => array(
                                                    0 => "EMAIL",
                                                ),
                                                "SUCCESS_PAGE" => "",
                                                "USER_PROPERTY" => array(
                                                ),
                                                "USER_PROPERTY_NAME" => "",
                                                "USE_BACKURL" => "Y",
                                                "AJAX_MODE" => "Y",
                                                "AJAX_OPTION_SHADOW" => "N",
                                                "AJAX_OPTION_JUMP" => "N",
                                                "AJAX_OPTION_STYLE" => "Y",
                                                "AJAX_OPTION_HISTORY" => "N",
                                                "COMPONENT_TEMPLATE" => "main_register"
                                            ),
                                            false
                                        ); ?>
                                    </div>
                                    <div class="forgot-pass__content hidden">
                                        <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "change_pass", Array(
                                            "FORGOT_PASSWORD_URL" => "",    // Страница забытого пароля
                                            "PROFILE_URL" => "",    // Страница профиля
                                            "REGISTER_URL" => "",    // Страница регистрации
                                            "SHOW_ERRORS" => "Y",    // Показывать ошибки
                                        ),
                                            false
                                        ); ?>
                                        <?
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:system.auth.forgotpasswd",
                                            ".default",
                                            Array(
                                                "AJAX_MODE" => "Y",
                                                "AJAX_OPTION_SHADOW" => "N",
                                                "AJAX_OPTION_JUMP" => "N",
                                                "AJAX_OPTION_STYLE" => "Y",
                                                "AJAX_OPTION_HISTORY" => "N"
                                            )
                                        ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? } else { ?>

                    <? } ?>
                    <div id="smallcart">
                        <? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "header_basket", array(
                            "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                            "COLUMNS_LIST" => array(
                                0 => "NAME",
                                1 => "PRICE",
                                2 => "TYPE",
                                3 => "QUANTITY",
                                4 => "DELETE",
                                5 => "DISCOUNT",
                            ),
                            "PATH_TO_ORDER" => "/cart/",
                            "HIDE_COUPON" => "N",
                            "QUANTITY_FLOAT" => "N",
                            "PRICE_VAT_SHOW_VALUE" => "N",
                            "SET_TITLE" => "N"
                        ),
                            false
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav__wrap hidden-sm hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?
                        $arSelectedBrand = array();
                        $obSelectedSect = CIBlockSection::GetList(Array("timestamp_x "=>"DESC"), Array("IBLOCK_ID" => 5, "!UF_SELECTED_BRAND" => false, 'DEPTH_LEVEL' => 2, 'ACTIVE'=>'Y'), false, array('ID', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL', 'UF_SELECTED_SECTION'), array('nPageSize'=>1));
                        while ($arSelectedSect = $obSelectedSect->GetNext()){
                            $arSelectedBrand = $arSelectedSect;
                        }
                        ?>
                        <ul class="nav clearfix<?= !empty($arSelectedBrand) ? " nav-resp" : ""?>">

                            <?$APPLICATION->IncludeComponent(
                                "bitrix:catalog.section.list",
                                "main_menu_template",
                                array(
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "CACHE_GROUPS" => "N",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "A",
                                    "COMPONENT_TEMPLATE" => "main_menu_template",
                                    "COUNT_ELEMENTS" => "Y",
                                    "IBLOCK_ID" => "5",
                                    "IBLOCK_TYPE" => "TicTacToy",
                                    "SECTION_CODE" => "",
                                    "SECTION_FIELDS" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "SECTION_ID" => $_REQUEST["SECTION_ID"],
                                    "SECTION_URL" => "",
                                    "SECTION_USER_FIELDS" => array(
                                        0 => "UF_LOGO",
                                        1 => "UF_RETARGETING",
                                        2 => "UF_MAIN_PICTURE",
                                        3 => "UF_BRAND_BOLD",
                                        4 => "UF_POPULAR_BRAND",
                                        5 => "UF_POPULAR_LOGO",
                                        6 => "UF_SELECTED_BRAND",
                                        7 => "",
                                    ),
                                    "SHOW_PARENT_NAME" => "Y",
                                    "TOP_DEPTH" => "3",
                                    "VIEW_MODE" => "LIST"
                                ),
                                false
                            );?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <? if ($currPage === "/") { ?>
            <? $APPLICATION->IncludeComponent("bitrix:news.list", "slider", array(
                "IBLOCK_TYPE" => "TicTacToy",
                "IBLOCK_ID" => "6",
                "NEWS_COUNT" => "9",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "NAME_1",
                    1 => "NAME_2",
                    2 => "LINK",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
                false
            ); ?>
        <? } ?>
    </header>
    <!-- HEADER -->
<? } ?>
